//
// Created by Jordi Montornes on 10/04/2021
//

#ifndef GOTRIS_CONSTANTDATA_H_H
#define GOTRIS_CONSTANTDATA_H_H

extern const u8* levels[];
extern const u8 levelfallSpeeds[];

extern const u8 black_tile[];
extern const u8 black_tile_sprite[];
extern const u8 ending_face_row1[];
extern const u8 ending_face_row2[];
extern const u8 ending_face_row3[];
extern const u8 ending_face_row4[];
extern const u8 default_pieces_palette[];

extern const u8 black_palette[];

extern const u8 white_palette[];

extern const u8 title_first_palette[];

extern const u8 background_palette_array[];

extern const u8 psgInit[];

extern const u8 credits[];

extern const char** long_texts[];

extern const char long_texts_size[];

extern const u8 credits_secondary_palette[];
extern const u8 waveLut[];
extern const u8 randLUT[256];
extern const u16 ending2tilemap[448];
extern const u16 ending2tilemapcolumns[196];
#endif //GOTRIS_CONSTANTDATA_H_H