#include "game_state.h"
#include <stdbool.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/board.h"
#include "../engine/gamelogic.h"
#include "../engine/states.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include <SMSlib.h>
#include <PSGlib.h>


u8 game_state_status = 0;

 void game_state_start(void) {
    load_game_assets();
    draw_game_background();
    init_gamestatus(0);
    init_board();
    frame_cnt = 0;
    game_state_status = 0;
}

 void game_state_update(void) {
     if(game_state_status == 0) {
        start_fadein_music();
        game_fade_in(0);
        play_song(START_OST);
        game_state_status = 1;
    } else if(game_state_status == 1) {
        frame_cnt = 0;
        while(frame_cnt < XS_PERIOD) {
            waitForFrame();
        }
        game_state_status = 2;
    } else if(game_state_status == 2) {
        print_string(12,10,"GET^");
        print_string(16,10,"READY[^");
        game_state_status = 3;
    } else if(game_state_status == 3) {
        frame_cnt = 0;
        while(frame_cnt < L_PERIOD) {
            waitForFrame();
        }
        game_state_status = 4;
    } else if(game_state_status == 4) {
        stop_music();
        stop_fx();
        waitForFrame();
        play_sample(GETREADY_SAMPLE);
        waitForFrame();
        play_song(INGAME_OST);
        draw_game_screen();
        print_level_number(2,23,level+1);
        print_string(2,19,"LVL]GOAL^");
        print_score_value(2,21,(level+1)<<LEVEL_DELIMITER);
        print_score_value(24,19,score_p1);
        print_score_value(24,23,score_p2);
        initialize_game_lists();
        pause = false;
        SMS_resetPauseRequest();
        game_state_status = 5;
    }  else if (game_state_status == 5) {
        if (game_status!=GAME_STATUS_RESET && game_status != GAME_STATUS_ENDGAME2) { 
            if(!pause) {
                execute_game_logic();
                get_game_actions();
            }
            if(SMS_queryPauseRequested()) {
                if(pause) {
                    pause = false;
                    game_full_palette();
                    if(game_status != GAME_STATUS_GAMEOVER) {
                        restore_board();
                    }
                    play_fx(SELECT_FX);
                    //start_fadein_music();
                } else {
                    pause = true;
                    game_half_palette();
                    if(game_status != GAME_STATUS_GAMEOVER) {
                        print_string(13,10,"PAUSE^");
                    }
                    
                    play_fx(SELECT_FX);
                    //start_fadeout_music();
                }
                SMS_resetPauseRequest();
            }
            waitForFrame();
        } else {   
            general_fade_out();
            blackBackgroundPalette();
            blackSpritePalette();
            frame_cnt = 0;
            while(frame_cnt < M_PERIOD) {
                waitForFrame();
            }
            clear_game_lists();
            clear_input();
            game_state_status = 6;
        }
    } else if(game_state_status == 6) {
        if(game_status == GAME_STATUS_RESET) {
            if(highScoreReached) {
                transition_to_state(ENTER_SCORE_STATE);
            } else {
                transition_to_state(LOGO_STATE);
            }
        } else if(game_status == GAME_STATUS_ENDGAME2) {
            transition_to_state(END_GAME_STATE);
        }
    }
}

 void game_state_stop(void) {
    clear_input();
}