//
// Created by JordiM on 21/12/2020.
//

#ifndef GOTRIS_DEMO_STATE_H
#define GOTRIS_DEMO_STATE_H

void demo_state_start(void);

void demo_state_update(void);

void demo_state_stop(void);

#endif //GOTRIS_DEMO_STATE_H
