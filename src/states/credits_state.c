#include "credits_state.h"
#include <stdbool.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/states.h"
#include "../engine/gamelogic.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include <PSGlib.h>
#include <SMSlib.h>

u8 credits_state_status;

 void credits_state_start(void) {
    frame_cnt = 0;
    credits_state_status = 0;
    load_tileset(SIMPLE_FONT_ASSET,DIGITS_BASE_ADDRESS);
    load_tileset(CREDITS_ASSETS,BACKGROUND_BASE_ADRESS);
    load_tilemap(CREDITS_ASSETS);
}

 void credits_state_update(void) {
    if(credits_state_status == 0) {
        start_fadein_music();
        play_song(CREDITS_OST);
        background_fade_in(CREDITS_PALETTE,1);
        sprite_fade_in(PIECES_PALETTE,1);
        credits_state_status = 1;
    } else if(credits_state_status == 1) {
        //start drawing credits
        init_credits(); 
        frame_cnt = 0;
        credits_state_status = 2;
    } else if(credits_state_status == 2) {
        if(frame_cnt > XXXL_PERIOD) {
            credits_state_status = 3;
        }
        write_credits(frame_cnt);
        waitForFrame();
        rasterize_credits();
    } else if(credits_state_status == 3) {
        disable_credits_effects();
        start_fadeout_music();
        general_fade_out();
        credits_state_status = 4;    
    } else if(credits_state_status == 4) {
        if(highScoreReached) {
            transition_to_state(ENTER_SCORE_STATE);
        } else {
            transition_to_state(TITLE_STATE);
        }
    }
}

 void credits_state_stop(void) {
    clear_input();
    stop_music();
}