//
// Created by JordiM on 02/01/2020.
//

#ifndef GOTRIS_GAME_STATE_H
#define GOTRIS_GAME_STATE_H

 void game_state_start(void);

 void game_state_update(void);

 void game_state_stop(void);

#endif //GOTRIS_GAME_STATE_H
