#include "enter_score_state.h"
#include <stdbool.h>
#include "../engine/globaldefinitions.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"
#include "../resources/music.h"
#include "../engine/states.h"
#include "../engine/gamelogic.h"
#include "../engine/input.h"
#include "../libs/montylib.h"
#include "../libs/saves.h"
#include <PSGlib.h>


u8 scorePosition = 0;
u8 charValue = 0;
u8 charPosition = 0;
u8 enter_score_status = 0;
u16 currentScore = 0;
u16 KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_ONE, KEY_TWO;
bool space = false;


void enter_score_state_start(void) {
    scorePosition = 0;
    frame_cnt = 0;
    enter_score_status = 0;
    load_highscore_assets();    
}

void enter_score_state_update(void) {
    if(enter_score_status == 0) {
        start_fadein_music();
        highscore_fade_in();
        current_player = PLAYER_1;
        currentScore = score_p1;
        enter_score_status = 1;
        KEY_DOWN = KEY_DOWN_P1;
        KEY_LEFT = KEY_LEFT_P1;
        KEY_RIGHT = KEY_RIGHT_P1;
        KEY_ONE = KEY_ONE_P1;
        KEY_TWO = KEY_TWO_P1;
        play_song(SCORE_OST);
    } else if(enter_score_status == 1){
        draw_highscore_table();
        scorePosition = get_highscore_position(currentScore);
        if(scorePosition < 10) {
            insert_highscore_position(scorePosition, currentScore);
            draw_highscore_table();
            charValue = get_highscore_position_char(scorePosition, charPosition);
            enter_score_status = 3;
            if(number_human_players == 2) {
                if(current_player == PLAYER_1) {
                    print_string(4,10,"PLAYER]A^");
                } else {
                    print_string(4,10,"PLAYER]B^");
                }
            }
        }  else {
            if(current_player == PLAYER_1 && number_human_players == 2) {
                current_player = PLAYER_2;
                currentScore = score_p2;
                scorePosition = 0;
                KEY_DOWN = KEY_DOWN_P2;
                KEY_LEFT = KEY_LEFT_P2;
                KEY_RIGHT = KEY_RIGHT_P2;
                KEY_ONE = KEY_ONE_P2;
                KEY_TWO = KEY_TWO_P2;
                //it'll come back to status 1
            } else {
                while(frame_cnt < M_PERIOD) {
                    waitForFrame();
                }
                start_fadeout_music();
                general_fade_out();
                enter_score_status = 4;
            }
        }
    }  else if(enter_score_status == 3){
        if(charPosition < 3) {
            if(frame_cnt > XXS_PERIOD) {
                if(space) {
                    update_highscore_position(scorePosition, charPosition, charValue);
                    space = false;  
                } else {
                    update_highscore_position(scorePosition, charPosition, 27);
                    space = true;
                }
                frame_cnt = 0;
            }
            draw_highscore_row(scorePosition);
            clear_input();
            update_input();
            if((getButtonsPressed() & KEY_LEFT) && charValue > 0) {
                charValue--;
                //play_fx(MOVE_FX);  
            } else if((getButtonsPressed() & KEY_RIGHT) && charValue < 25) {
                charValue++; 
                //play_fx(MOVE_FX);
            }
            if(getButtonsPressed() & KEY_ONE) {
                update_highscore_position(scorePosition, charPosition, charValue); 
                charPosition++;
                if(charPosition < 3) {
                    play_fx(FALL_FX);
                    charValue = get_highscore_position_char(scorePosition, charPosition);
                }
            } else if(getButtonsPressed() & KEY_TWO) {
                update_highscore_position(scorePosition, charPosition, charValue); 
                if(charPosition > 0) {
                    charPosition--;
                    play_fx(FALL_FX);
                    charValue = get_highscore_position_char(scorePosition, charPosition);
                }
            }
            waitForFrame();
        } else {
            draw_highscore_table();
            frame_cnt = 0;
            play_fx(START_FX);
            if(current_player == PLAYER_1 && number_human_players == 2) {
                while(frame_cnt < M_PERIOD) {
                    waitForFrame();
                }
                current_player = PLAYER_2;
                currentScore = score_p2;
                enter_score_status = 1;
                scorePosition = 0;
                charPosition = 0;
                KEY_DOWN = KEY_DOWN_P2;
                KEY_LEFT = KEY_LEFT_P2;
                KEY_RIGHT = KEY_RIGHT_P2;
                KEY_ONE = KEY_ONE_P2;
                KEY_TWO = KEY_TWO_P2;
            } else {
                while(frame_cnt < XXL_PERIOD) {
                    waitForFrame();
                }
                clear_input();
                start_fadeout_music();
                general_fade_out();
                saveChanges();
                enter_score_status = 4;
            }
            
        }
    } else if(enter_score_status == 4) {
        highScoreReached = false;
        transition_to_state(TITLE_STATE);
    }
}

void enter_score_state_stop(void) {
    stop_music();
    stop_fx();
}