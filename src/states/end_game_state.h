//
// Created by JordiM on 13/03/2021.
//

#ifndef GOTRIS_END_GAME_STATE_H
#define GOTRIS_END_GAME_STATE_H

void end_game_state_start(void);

void end_game_state_update(void);

void end_game_state_stop(void);

#endif //GOTRIS_END_GAME_STATE_H
