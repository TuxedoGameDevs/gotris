//
// Created by Jordi Montornes on 09/09/2020.
//

#ifndef GOTRIS_CPU_H
#define GOTRIS_CPU_H


void getTargetBottomPart(u8 color, u8 column);

void getTargetUpperPart(u8 color, u8 column);

void decideNextMove(void);
u16 decideDirection(u8 current_column, u8 current_row);
u16 decideRotation(u8 current_row);

#endif //GOTRIS_CPU_H