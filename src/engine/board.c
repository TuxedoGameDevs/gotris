//
// Created by Jordi Montornes on 04/04/2017.
//
#include <stdbool.h>
#include <SMSlib.h>
#include "globaldefinitions.h"
#include "board.h"
#include "../ram/globalvariables.h"
#include "../resources/resources.h"

void init_board(void) {
    u8 i,j;
    i=0;
    while(i<BOARD_ROWS) {
        j = 0;
        while(j<BOARD_COLUMNS) {
            board[(i << 3) + (i << 1) + j] = EMPTY_CELL;
            j++;
        }
        i++;
    }
    j = 0;
    while(j<BOARD_COLUMNS) {
        suitable_columns[j] = NO_SUITABLE;
        j++;
    }
}

void clear_suitable_columns(void) {
    u8 i;
    i = 0;
    while(i<BOARD_COLUMNS) {
        suitable_columns[i] = NO_SUITABLE;
        i++;
    }
}

void delete_square_pieces_board(void) {
    u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
    board[position] = EMPTY_CELL;
    board[position+1] = EMPTY_CELL;
    board[position+10] = EMPTY_CELL;
    board[position+11] = EMPTY_CELL;
    draw_board_position(current_square.x, current_square.y, EMPTY_CELL);
    draw_board_position(current_square.x+1, current_square.y, EMPTY_CELL);
    draw_board_position(current_square.x, current_square.y+1, EMPTY_CELL);
    draw_board_position(current_square.x+1, current_square.y+1, EMPTY_CELL);
}

void delete_bonus_board(void) {
    u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
    board[position] = EMPTY_CELL;
    draw_board_position(current_square.x, current_square.y, EMPTY_CELL);
}


void create_individual_pieces_from_square(void) {
    //always create downside pieces before upside's
    active_pieces[piece_index].x = current_square.x;
    active_pieces[piece_index].y = current_square.y+1;
    active_pieces[piece_index].color = current_square.color10;
    piece_index++;
    active_pieces[piece_index].x = current_square.x+1;
    active_pieces[piece_index].y = current_square.y+1;
    active_pieces[piece_index].color = current_square.color11;
    piece_index++;
    active_pieces[piece_index].x = current_square.x;
    active_pieces[piece_index].y = current_square.y;
    active_pieces[piece_index].color = current_square.color00;
    piece_index++;
    active_pieces[piece_index].x = current_square.x+1;
    active_pieces[piece_index].y = current_square.y;
    active_pieces[piece_index].color = current_square.color01;
    piece_index++;
}

void create_individual_pieces_from_board_position(u8 x, u8 y, u8 position2check) {
    active_pieces[piece_index].x = x;
    active_pieces[piece_index].y = y;
    if(board[position2check] == CONNECTED_WHITE_CELL || board[position2check] == NOT_PROCESSED_WHITE_CELL) {
        active_pieces[piece_index].color = WHITE_PIECE;
    } else if(board[position2check] == CONNECTED_BLACK_CELL || board[position2check] == NOT_PROCESSED_BLACK_CELL) {
        active_pieces[piece_index].color = BLACK_PIECE;
    }
    piece_index++;
}

void rotate_square_piece(void) {
    u8 backupColor00, backupColor01, backupColor10, backupColor11;
    backupColor00 =  current_square.color00;
    backupColor01 =  current_square.color01;
    backupColor10 =  current_square.color10;
    backupColor11 =  current_square.color11;
    current_square.color00 = backupColor01;
    current_square.color01 = backupColor11;
    current_square.color10 = backupColor00;
    current_square.color11 = backupColor10;
}

void rotate_inv_square_piece(void) {
    u8 backupColor00, backupColor01, backupColor10, backupColor11;
    backupColor00 =  current_square.color00;
    backupColor01 =  current_square.color01;
    backupColor10 =  current_square.color10;
    backupColor11 =  current_square.color11;
    current_square.color00 = backupColor10;
    current_square.color01 = backupColor00;
    current_square.color10 = backupColor11;
    current_square.color11 = backupColor01;
}

void delete_individual_pieces_board(u8 index) {     
    u8 position = (active_pieces[index].y << 3) + (active_pieces[index].y << 1) + active_pieces[index].x;
    board[position] = EMPTY_CELL;
    draw_board_position(active_pieces[index].x, active_pieces[index].y, EMPTY_CELL);
}

void put_individual_pieces_board(u8 index) {
    u8 position = (active_pieces[index].y << 3) + (active_pieces[index].y << 1) + active_pieces[index].x;
    board[position] = active_pieces[index].color;
    draw_board_position(active_pieces[index].x, active_pieces[index].y, active_pieces[index].color);
}

u8 reverse_piece_board(u8 x, u8 y) {
    u8 position = (y << 3) + (y << 1) + x;
    u8 original_color = NOT_PROCESSED_WHITE_CELL;
    if(board[position] == NOT_PROCESSED_WHITE_CELL) {
        board[position] = NOT_PROCESSED_BLACK_CELL;
    } else if(board[position] == NOT_CONNECTED_WHITE_CELL) {
        board[position] = NOT_PROCESSED_BLACK_CELL;
    } else if(board[position] == CONNECTED_WHITE_CELL) {
        board[position] = NOT_PROCESSED_BLACK_CELL;
    } else if(board[position] == NOT_PROCESSED_BLACK_CELL) {
        original_color = NOT_PROCESSED_BLACK_CELL;
        board[position] = NOT_PROCESSED_WHITE_CELL;
    } else if(board[position] == NOT_CONNECTED_BLACK_CELL) {
        original_color = NOT_PROCESSED_BLACK_CELL;
        board[position] = NOT_PROCESSED_WHITE_CELL;
    } else if(board[position] == CONNECTED_BLACK_CELL) {
        original_color = NOT_PROCESSED_BLACK_CELL;
        board[position] = NOT_PROCESSED_WHITE_CELL;
    }
    draw_board_position(x,y,board[position]);
    return original_color;
}

void destroy_piece_board(u8 x, u8 y) {
    u8 position = (y << 3) + (y << 1) + x;
    board[position] = EXPLODING_CELL;
    draw_board_position(x,y, EXPLODING_CELL);
}

void mark_position_for_change_in_board(u8 x, u8 y) {
    //posible optimización usando restas 
    u8 position = (y << 3) + (y << 1) + x;
    if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
        board[position-1] = NOT_PROCESSED_WHITE_CELL;
        draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
        mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_WHITE_CELL);
    }
    if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
        board[position+1] = NOT_PROCESSED_WHITE_CELL;
        draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
        mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_WHITE_CELL);
    }
    if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
        board[position+10] = NOT_PROCESSED_WHITE_CELL;
        draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
        mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_WHITE_CELL);
    }
    if(x > 0 && board[position-1] == CONNECTED_BLACK_CELL) {
        board[position-1] = NOT_PROCESSED_BLACK_CELL;
        draw_board_position(x-1, y, NOT_PROCESSED_BLACK_CELL);
        mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_BLACK_CELL);
    }
    if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_BLACK_CELL) {
        board[position+1] = NOT_PROCESSED_BLACK_CELL;
        draw_board_position(x+1, y, NOT_PROCESSED_BLACK_CELL);
        mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_BLACK_CELL);
    }
    if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_BLACK_CELL) {
        board[position+10] = NOT_PROCESSED_BLACK_CELL;
        draw_board_position(x, y+1, NOT_PROCESSED_BLACK_CELL);
        mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_BLACK_CELL);
    }
}

void mark_position_for_change_in_board_rec(u8 x, u8 y, u8 color) {
    u8 position = (y << 3) + (y << 1) + x;
    if(color == NOT_PROCESSED_WHITE_CELL) {
        if(x > 0 && board[position-1] == CONNECTED_WHITE_CELL) {
            board[position-1] = NOT_PROCESSED_WHITE_CELL;
            draw_board_position(x-1, y, NOT_PROCESSED_WHITE_CELL);
            mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_WHITE_CELL);
        }
        if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_WHITE_CELL) {
            board[position+1] = NOT_PROCESSED_WHITE_CELL;
            draw_board_position(x+1, y, NOT_PROCESSED_WHITE_CELL);
            mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_WHITE_CELL);
        }
        if(y > 0 && board[position-10] == CONNECTED_WHITE_CELL) {
            board[position-10] = NOT_PROCESSED_WHITE_CELL;
            draw_board_position(x, y-1, NOT_PROCESSED_WHITE_CELL);
            mark_position_for_change_in_board_rec(x, y-1, NOT_PROCESSED_WHITE_CELL);
        }
        if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_WHITE_CELL) {
            board[position+10] = NOT_PROCESSED_WHITE_CELL;
            draw_board_position(x, y+1, NOT_PROCESSED_WHITE_CELL);
            mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_WHITE_CELL);
        }
    } else if(color == NOT_PROCESSED_BLACK_CELL) {
        if(x > 0 && board[position-1] == CONNECTED_BLACK_CELL) {
            board[position-1] = NOT_PROCESSED_BLACK_CELL;
            draw_board_position(x-1, y, NOT_PROCESSED_BLACK_CELL);
            mark_position_for_change_in_board_rec(x-1, y, NOT_PROCESSED_BLACK_CELL);
        }
        if(x<BOARD_COLUMNS-1 && board[position+1] == CONNECTED_BLACK_CELL) {
            board[position+1] = NOT_PROCESSED_BLACK_CELL;
            draw_board_position(x+1, y, NOT_PROCESSED_BLACK_CELL);
            mark_position_for_change_in_board_rec(x+1, y, NOT_PROCESSED_BLACK_CELL);
        }
        if(y > 0 && board[position-10] == CONNECTED_BLACK_CELL) {
            board[position-10] = NOT_PROCESSED_BLACK_CELL;
            draw_board_position(x, y-1, NOT_PROCESSED_BLACK_CELL);
            mark_position_for_change_in_board_rec(x, y-1, NOT_PROCESSED_BLACK_CELL);
        }
        if(y<BOARD_ROWS-1 && board[position+10] == CONNECTED_BLACK_CELL) {
            board[position+10] = NOT_PROCESSED_BLACK_CELL;
            draw_board_position(x, y+1, NOT_PROCESSED_BLACK_CELL);
            mark_position_for_change_in_board_rec(x, y+1, NOT_PROCESSED_BLACK_CELL);
        }
    }
}

void put_square_pieces_board(void) {
    u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
    board[position] = current_square.color00;
    board[position+1] = current_square.color01;
    board[position+10] = current_square.color10;
    board[position+11] = current_square.color11;
    draw_board_position(current_square.x, current_square.y, current_square.color00);
    draw_board_position(current_square.x+1, current_square.y, current_square.color01);
    draw_board_position(current_square.x, current_square.y+1, current_square.color10);
    draw_board_position(current_square.x+1, current_square.y+1, current_square.color11);
}

void put_bonus_board(void) {
    u8 position = (current_square.y << 3) + (current_square.y << 1) + current_square.x;
    board[position] = current_square.color00;
    draw_board_position(current_square.x, current_square.y, current_square.color00);
}

bool compute_path(u8 x, u8 y) {
    u8 position = (y << 3) + (y << 1) + x;
    u8 empty_directions;
    if(board[position] == NOT_PROCESSED_WHITE_CELL) {
        if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
            board[position] = CONNECTED_WHITE_CELL;
            draw_board_position(x, y, CONNECTED_WHITE_CELL);
            suitable_columns[x] = NO_SUITABLE;
            return true;
        } else {
            if(neighbour_connected(position)) {
                board[position] = CONNECTED_WHITE_CELL;
                draw_board_position(x, y, CONNECTED_WHITE_CELL);
                suitable_columns[x] = 0;
                return true;
            } else {
                empty_directions = neighbour_emptyspace(position);
                if(empty_directions) {
                    //Emplenar array de posibilitats
                    //Using counter separatedely for White and Black: 00 (white) 00 (black)
                    if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
                        suitable_columns[x] = ((suitable_columns[x] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x]+ 1) &0xF) ;
                    } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {
                        if(is_free_board_position(position+9)) {
                            suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 1) & 0xF);
                        } else {
                            suitable_columns[x-1] = ((suitable_columns[x-1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x-1]+1)& 0xF);
                        }
                    } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
                        if(is_free_board_position(position+11)) {
                            suitable_columns[x+1] =((suitable_columns[x+1] | BLACK_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
                        } else {
                            suitable_columns[x+1] = ((suitable_columns[x+1] | BLACK_SUITABLE) & 0xF0) + ((suitable_columns[x+1] +1 )& 0xF);
                        }
                    }
                    board[position] = CONNECTED_WHITE_CELL;
                    draw_board_position(x, y, CONNECTED_WHITE_CELL);
                    return true;
                } else if(!neighbour_notprocessed(position)) {
                    board[position] = NOT_CONNECTED_WHITE_CELL;
                    draw_board_position(x, y, NOT_CONNECTED_WHITE_CELL);
                    return true;
                }
            } 
        }
    } else if(board[position] == NOT_PROCESSED_BLACK_CELL) {
        if(y == 0 || y == (BOARD_ROWS - 1) || x == 0 || x == (BOARD_COLUMNS - 1)) {
            board[position] = CONNECTED_BLACK_CELL;
            draw_board_position(x, y, CONNECTED_BLACK_CELL);
            suitable_columns[x] = 0;
            return true;
        } else {
            if(neighbour_connected(position)) {
                board[position] = CONNECTED_BLACK_CELL;
                draw_board_position(x, y, CONNECTED_BLACK_CELL);
                //suitable_columns[x] = 0;
                return true;
            } else {
                empty_directions = neighbour_emptyspace(position);
                if(empty_directions) {
                    //Emplenar array de posibilitats
                    //Using counter separatedely for White and Black: 00 (white) 00 (black) (so adding 4 here)
                    if((empty_directions  & EMPTY_UP) && ((empty_directions ^ EMPTY_UP ) == 0)) {
                        suitable_columns[x] = ((suitable_columns[x] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x] + 4) & 0xF);                        
                    } else if((empty_directions & EMPTY_LEFT) && ((empty_directions ^ EMPTY_LEFT ) == 0)) {                        
                        if(is_free_board_position(position+9)) {
                            suitable_columns[x-1] =((suitable_columns[x-1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x-1] + 4) & 0xF);
                        } else {
                            suitable_columns[x-1] = ((suitable_columns[x-1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x-1] + 4) & 0xF);
                        }                        
                    } else if((empty_directions & EMPTY_RIGHT) && ((empty_directions ^ EMPTY_RIGHT ) == 0)) {
                        if(is_free_board_position(position+11)) {
                            suitable_columns[x+1] =((suitable_columns[x+1] | WHITE_SUITABLE_UPPER_PART) & 0xF0) + ((suitable_columns[x+1] + 4) & 0xF);
                        } else {
                            suitable_columns[x+1] = ((suitable_columns[x+1] | WHITE_SUITABLE) & 0xF0) + ((suitable_columns[x+1] + 4) & 0xF);
                        }
                    }
                    board[position] = CONNECTED_BLACK_CELL;
                    draw_board_position(x, y, CONNECTED_BLACK_CELL);
                    return true;
                } else if(!neighbour_notprocessed(position)) {
                    board[position] = NOT_CONNECTED_BLACK_CELL;
                    draw_board_position(x, y, NOT_CONNECTED_BLACK_CELL);
                    return true;
                }
            } 
        }
    } else if(board[position] == EMPTY_CELL) {
        suitable_columns[x] = 0;
    }
    return false;
}

u8 neighbour_emptyspace(u8 position2check) {
    u8 result = 0;
    if(board[position2check - 10] == EMPTY_CELL) 
        result = result | EMPTY_UP;
    if(board[position2check - 1] == EMPTY_CELL) 
        result = result | EMPTY_LEFT;
    if(board[position2check + 1] == EMPTY_CELL) 
        result = result | EMPTY_RIGHT;
    if(board[position2check + 10] == EMPTY_CELL) 
        result = result | EMPTY_DOWN;
    return result;

    
    /*if((board[position2check - 10] == EMPTY_CELL || board[position2check - 1] == EMPTY_CELL 
            || board[position2check + 1] == EMPTY_CELL || board[position2check + 10] == EMPTY_CELL)) {
        return true;
    }
    return false;*/
}

bool neighbour_connected(u8 position2check) {
    if(board[position2check] == NOT_PROCESSED_WHITE_CELL && 
        (board[position2check - 10] == CONNECTED_WHITE_CELL || board[position2check - 1] == CONNECTED_WHITE_CELL 
            || board[position2check + 1] == CONNECTED_WHITE_CELL || board[position2check + 10] == CONNECTED_WHITE_CELL)) {
        return true;
    }
    if(board[position2check] == NOT_PROCESSED_BLACK_CELL && 
        (board[position2check - 10] == CONNECTED_BLACK_CELL || board[position2check - 1] == CONNECTED_BLACK_CELL 
            || board[position2check + 1] == CONNECTED_BLACK_CELL || board[position2check + 10] == CONNECTED_BLACK_CELL)) {
        return true;
    }
    return false;
}

bool neighbour_notprocessed(u8 position2check) {
    if(board[position2check] == NOT_PROCESSED_WHITE_CELL && 
        (board[position2check - 10] == NOT_PROCESSED_WHITE_CELL || board[position2check - 1] == NOT_PROCESSED_WHITE_CELL 
            || board[position2check + 1] == NOT_PROCESSED_WHITE_CELL || board[position2check + 10] == NOT_PROCESSED_WHITE_CELL)) {
        return true;
    }
    if(board[position2check] == NOT_PROCESSED_BLACK_CELL && 
        (board[position2check - 10] == NOT_PROCESSED_BLACK_CELL || board[position2check - 1] == NOT_PROCESSED_BLACK_CELL 
            || board[position2check + 1] == NOT_PROCESSED_BLACK_CELL || board[position2check + 10] == NOT_PROCESSED_BLACK_CELL)) {
        return true;
    }
    return false;
}

bool mark_as_disconnected(u8 x, u8 y, u8 position2check) {
    if(board[position2check] == NOT_PROCESSED_WHITE_CELL || board[position2check] == NOT_PROCESSED_BLACK_CELL) {
        board[position2check] = board[position2check] + 1;
        draw_board_position(x, y, board[position2check]);
        return true;
    }
    if(board[position2check] == NOT_CONNECTED_WHITE_CELL || board[position2check] == NOT_CONNECTED_BLACK_CELL) {
        return true;
    }
    return false;
}

u8 destroy_disconnected(u8 x, u8 y, u8 position2check) {
    u8 numPiecesDestroyed = 0;
    if(board[position2check] == NOT_CONNECTED_WHITE_CELL) {
        board[position2check] = EXPLODING_CELL;
        draw_board_position(x, y, EXPLODING_CELL);
        numPiecesDestroyed++;
        if((x > 0) && board[position2check-1] != NOT_CONNECTED_WHITE_CELL) {
            board[position2check-1] = EXPLODING_CELL;
            draw_board_position(x-1, y, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
        if((x < BOARD_COLUMNS - 1) && board[position2check+1] != NOT_CONNECTED_WHITE_CELL) {
            board[position2check+1] = EXPLODING_CELL;
            draw_board_position(x+1, y, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
        if((y>0) && board[position2check-10] != NOT_CONNECTED_WHITE_CELL) {
            board[position2check-10] = EXPLODING_CELL;
            draw_board_position(x, y-1, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
        if((y < BOARD_ROWS-1) && board[position2check+10] != NOT_CONNECTED_WHITE_CELL) {
            board[position2check+10] = EXPLODING_CELL;
            draw_board_position(x, y+1, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
    } else if(board[position2check] == NOT_CONNECTED_BLACK_CELL) {
        board[position2check] = EXPLODING_CELL;
        draw_board_position(x, y, EXPLODING_CELL);
        numPiecesDestroyed++;
        if((x > 0) && board[position2check-1] != NOT_CONNECTED_BLACK_CELL) {
            board[position2check-1] = EXPLODING_CELL;
            draw_board_position(x-1, y, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
        if((x < BOARD_COLUMNS - 1) && board[position2check+1] != NOT_CONNECTED_BLACK_CELL) {
            board[position2check+1] = EXPLODING_CELL;
            draw_board_position(x+1, y, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
        if((y>0) && board[position2check-10] != NOT_CONNECTED_BLACK_CELL) {
            board[position2check-10] = EXPLODING_CELL;
            draw_board_position(x, y-1, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
        if((y < BOARD_ROWS-1) && board[position2check+10] != NOT_CONNECTED_BLACK_CELL) {
            board[position2check+10] = EXPLODING_CELL;
            draw_board_position(x, y+1, EXPLODING_CELL);
            numPiecesDestroyed++;
        }
    }
    return numPiecesDestroyed;
}

void clean_explosion(u8 x, u8 y, u8 position2check) {
    board[position2check] = EMPTY_CELL;
    draw_board_position(x, y, EMPTY_CELL);
}

void clear_board_position(u8 x, u8 y) {
    u8 position = (y << 3) + (y << 1) + x;
    board[position] = EMPTY_CELL;
    draw_board_position(x,y,EMPTY_CELL);
}

void block_board_position(u8 x, u8 y) {
    u8 position = (y << 3) + (y << 1) + x;
    board[position] = BLOCKED_CELL;
    draw_board_position(x,y,BLOCKED_CELL);
}

void add_piece_board_position(u8 x, u8 y, u8 color) {
    u8 position = (y << 3) + (y << 1) + x;
    board[position] = color;
    draw_board_position(x,y,color);
}

u8 getCollisionRowFromColumn(u8 position2check) {
    u8 current_row = 0;
    while(position2check < 209) { //220 - 1 - 10 ya que nunca estamos en la última fila
        if(board[position2check] != EMPTY_CELL || board[++position2check] != EMPTY_CELL) {
            return current_row;
        }
        position2check = position2check + 9;
        current_row++;
    }
    return current_row;
}
