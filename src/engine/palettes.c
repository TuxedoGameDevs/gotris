#include <stdbool.h>
#include "globaldefinitions.h"
#include "palettes.h"
#include "../ram/globalvariables.h"
#include <SMSlib.h>
#include "../libs/montylib.h"

void setBGPalette(u8 *palette, u8 numcolors) {
    u8 i;
    for(i = 0;i<numcolors;i++) {
        background_palette[i] = palette[i];
    }
    SMS_loadBGPalette(palette);
}

void setBGPaletteColor(u8 position, u8 value) {
    background_palette[position] = value;
}

u8* getBGPalette(void) {
    return background_palette;
}

void setSpritePalette(u8 *palette, u8 numcolors) {
    u8 i;
    for(i = 0;i<numcolors;i++) {
        sprite_palette[i] = palette[i];
    }
    SMS_loadSpritePalette(palette);
}

void setSpritePaletteColor(u8 position, u8 value) {
    sprite_palette[position] = value;
}

u8* getSpritePalette(void) {
    return sprite_palette;
}

/*void createBWPaletteVersion(u8 *original_palette, u8 *bw_palette, u8 numcolors) {
    u8 j,redComponent, greenComponent, blueComponent, addedColor;
    for(j = 0;j<numcolors;j++) {
        redComponent = getRedFromRGB(original_palette[j]);
        greenComponent = getGreenFromRGB(original_palette[j]);
        blueComponent = getBlueFromRGB(original_palette[j]);
        addedColor = redComponent + greenComponent + blueComponent;
        switch (addedColor)
        {
            case 0:
                redComponent = 0;
                greenComponent = 0;
                blueComponent = 0;
                break;
            case 1:
            case 2:
            case 3:
                redComponent = 1;
                greenComponent = 1;
                blueComponent = 1;
                break;
            case 4:
            case 5:
            case 6:
                redComponent = 2;
                greenComponent = 2;
                blueComponent = 2;
                break;  
            case 7:  
            case 8:
            case 9:
                redComponent = 3;
                greenComponent = 3;
                blueComponent = 3;
                break;
            default:
                redComponent = 3;
                greenComponent = 3;
                blueComponent = 3;
            break;
        }
        bw_palette[j] = RGB(redComponent,greenComponent,blueComponent);
    }
}*/

void createSepiaPaletteVersion(const u8 *original_palette, u8 *bw_palette, u8 numcolors) {
    u8 j,redComponent, greenComponent, blueComponent, addedColor;
    for(j = 0;j<numcolors;j++) {
        redComponent = getRedFromRGB(original_palette[j]);
        greenComponent = getGreenFromRGB(original_palette[j]);
        blueComponent = getBlueFromRGB(original_palette[j]);
        addedColor = redComponent + greenComponent + blueComponent;
        switch (addedColor)
        {
            case 0:
                redComponent = 0;
                greenComponent = 0;
                blueComponent = 0;
                break;
            case 1:
            case 2:
            case 3:
                redComponent = 1;
                greenComponent = 1;
                blueComponent = 0;
                break;
            case 4:
            case 5:
            case 6:
                redComponent = 2;
                greenComponent = 2;
                blueComponent = 1;
                break;  
            case 7:  
            case 8:
            case 9:
                redComponent = 3;
                greenComponent = 3;
                blueComponent = 2;
                break;
            default:
                redComponent = 3;
                greenComponent = 3;
                blueComponent = 3;
            break;
        }
        bw_palette[j] = RGB(redComponent,greenComponent,blueComponent);
    }
}

void createBluePaletteVersion(const u8 *original_palette, u8 *bw_palette, u8 numcolors) {
    u8 j,redComponent, greenComponent, blueComponent, addedColor;
    for(j = 0;j<numcolors;j++) {
        redComponent = getRedFromRGB(original_palette[j]);
        greenComponent = getGreenFromRGB(original_palette[j]);
        blueComponent = getBlueFromRGB(original_palette[j]);
        addedColor = redComponent + greenComponent + blueComponent;
        switch (addedColor)
        {
            case 0:
                redComponent = 0;
                greenComponent = 0;
                blueComponent = 0;
                break;
            case 1:
            case 2:
                redComponent = 0;
                greenComponent = 0;
                blueComponent = 1;
                break;
            case 3:
            case 4:
                redComponent = 0;
                greenComponent = 0;
                blueComponent = 2;
                break;
            
            case 5:
            case 6:
                redComponent = 0;
                greenComponent = 0;
                blueComponent = 3;
                break;  
            case 7:  
            case 8:
                redComponent = 0;
                greenComponent = 1;
                blueComponent = 3;
            case 9:
                redComponent = 0;
                greenComponent = 2;
                blueComponent = 3;  
                break;
            default:
                redComponent = 0;
                greenComponent = 3;
                blueComponent = 3;
            break;
        }
        bw_palette[j] = RGB(redComponent,greenComponent,blueComponent);
    }
}

/*void fadeToTargetBGPalette(u8 *target_palette, u8 numcolors, u8 framesperstep) {
    u8 i,j,redComponent, greenComponent, blueComponent, targetComponent;
    u8 temporal_palette[16];
    j= 0;
    for(j = 0;j<numcolors;j++) {
        temporal_palette[j] = background_palette[j];
    }
    SMS_loadBGPalette(background_palette);
    //green 
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getGreenFromRGB(target_palette[j]);
            if(greenComponent>targetComponent) {
                greenComponent--;
            } else if(greenComponent<targetComponent) {
                greenComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        SMS_loadBGPalette(temporal_palette);
        i++;
    }
    //red
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getRedFromRGB(target_palette[j]);
            if(redComponent>targetComponent) {
                redComponent--;
            } else if(redComponent<targetComponent) {
                redComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        SMS_loadBGPalette(temporal_palette);
        i++;
    }
    //blue
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getBlueFromRGB(target_palette[j]);
            if(blueComponent>targetComponent) {
                blueComponent--;
            } else if(blueComponent<targetComponent) {
                blueComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        SMS_loadBGPalette(temporal_palette);
        i++;
    }
    setBGPalette(target_palette, numcolors);
}

void fadeToTargetSpritePalette(u8 *target_palette, u8 numcolors, u8 framesperstep) {
    u8 i,j,redComponent, greenComponent, blueComponent, targetComponent;
    u8 temporal_palette[16];
    j= 0;
    for(j = 0;j<numcolors;j++) {
        temporal_palette[j] = sprite_palette[j];
    }
    SMS_loadSpritePalette(sprite_palette);
    //green 
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getGreenFromRGB(target_palette[j]);
            if(greenComponent>targetComponent) {
                greenComponent--;
            } else if(greenComponent<targetComponent) {
                greenComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        
        SMS_loadSpritePalette(temporal_palette);
        i++;
    }
    //red
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getRedFromRGB(target_palette[j]);
            if(redComponent>targetComponent) {
                redComponent--;
            } else if(redComponent<targetComponent) {
                redComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        SMS_loadSpritePalette(temporal_palette);
        i++;
    }
    //blue
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getBlueFromRGB(target_palette[j]);
            if(blueComponent>targetComponent) {
                blueComponent--;
            } else if(blueComponent<targetComponent){
                blueComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        SMS_loadSpritePalette(temporal_palette);
        i++;
    }
    setSpritePalette(target_palette, numcolors);
}*/
void fadeToTargetBGPalette(const u8 *target_palette, u8 numcolors, u8 framesperstep) {
    u8 i,j,redComponent, greenComponent, blueComponent, targetComponent;
    u8 temporal_palette[16];
    //framesperstep = framesperstep << 1;
    j= 0;
    for(j = 0;j<numcolors;j++) {
        temporal_palette[j] = background_palette[j];
    }
    SMS_loadBGPalette(background_palette);
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getGreenFromRGB(target_palette[j]);
            if(greenComponent>targetComponent) {
                greenComponent--;
            } else if(greenComponent<targetComponent) {
                greenComponent++;
            }
            if(redComponent>targetComponent) {
                redComponent--;
            } else if(redComponent<targetComponent) {
                redComponent++;
            }
            if(blueComponent>targetComponent) {
                blueComponent--;
            } else if(blueComponent<targetComponent){
                blueComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        SMS_loadBGPalette(temporal_palette);
        i++;
    }    
    setBGPalette(target_palette, numcolors);
}

void fadeToTargetSpritePalette(const u8 *target_palette, u8 numcolors, u8 framesperstep) {
    u8 i,j,redComponent, greenComponent, blueComponent, targetComponent;
    u8 temporal_palette[16];
    //framesperstep = framesperstep << 1;
    j= 0;
    for(j = 0;j<numcolors;j++) {
        temporal_palette[j] = sprite_palette[j];
    }
    SMS_loadSpritePalette(sprite_palette);
    i = 0;
    while(i<4) {
        j = 0;
        while(j<numcolors) {
            redComponent = getRedFromRGB(temporal_palette[j]);
            greenComponent = getGreenFromRGB(temporal_palette[j]);
            blueComponent = getBlueFromRGB(temporal_palette[j]);
            targetComponent = getGreenFromRGB(target_palette[j]);
            if(greenComponent>targetComponent) {
                greenComponent--;
            } else if(greenComponent<targetComponent) {
                greenComponent++;
            }
            if(redComponent>targetComponent) {
                redComponent--;
            } else if(redComponent<targetComponent) {
                redComponent++;
            }
            if(blueComponent>targetComponent) {
                blueComponent--;
            } else if(blueComponent<targetComponent){
                blueComponent++;
            }
            temporal_palette[j] = RGB(redComponent,greenComponent,blueComponent);
            j++;
        }
        j= 0;
        while(j<framesperstep) {
            waitForFrame();
            j++;
        }
        
        SMS_loadSpritePalette(temporal_palette);
        i++;
    }
    setSpritePalette(target_palette, numcolors);
}