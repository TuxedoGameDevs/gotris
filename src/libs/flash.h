/************************************************************************//**
 * \file flash.h
 * \brief Low level NOR flash erase/write interface.
 * \author Jesus Alonso (doragasu)
 * \date 05/2023
 *
 * This module allows reading, erasing and writing data from/to the cartridge
 * flash chip. It has been tested with SST39SF040 chips on FrugalMapper carts,
 * but any chip using the standard AMD format should work.
 *
 * Functions in this module disable interrupts and re-enable them on finish.
 * They can also be quite intensive in stack usage (50+ bytes), so make sure
 * you test them thoroughly before releasing your game!
 *****************************************************************************/

#ifndef __FLASH_H__
#define __FLASH_H__

#include <stdint.h>

/************************************************************************//**
 * \brief Read flash chip manufacturer and device IDs.
 *
 * For SST39SF040 chips mounted on FrugalMap cartridges, manufacturer ID
 * should be 0xBF, and device ID should be 0xB7.
 *
 * \param[out] ids Manufacturer ID is left on byte 0, device ID is left on
 *                 byte 1.
 ****************************************************************************/
void flash_ids_get(uint8_t *ids);

/************************************************************************//**
 * \brief Erase one sector.
 *
 * On SST39SF040 chips mounted on FrugalMap cartridges, all sectors are 4 KiB
 * long. Erased sectors can be read as 0xFF.
 *
 * \param[in] addr Address contained in the sector to erase.
 ****************************************************************************/
void flash_sector_erase(uint32_t addr);

/************************************************************************//**
 * \brief Program an array of bytes to the flash chip.
 *
 * Prior to the program operation, you have to make sure the address range
 * to program is erased (has value 0xFF). Otherwise this function might lock
 * the machine. Also you must make sure data to program does not cross a
 * 16 KiB bank. Otherwise this function will fail and might lock the machine.
 *
 * \param[in] addr Address of the area to program.
 * \param[in] data Array to program to addr.
 * \param[in] len  Length of the data array.
 *
 * \warning If you try programming to a non erased address, this function
 * might fail and lock the machine.
 * \warning If the buffer to program (addr + len) crosses a 16 KiB bank, this
 * function will fail and might lock the machine.
 ****************************************************************************/
void flash_program(uint32_t addr, const uint8_t *data, int16_t len);

/************************************************************************//**
 * \brief Program a single byte to the flash chip.
 *
 * Prior to the program operation, you have to make sure the byte to program
 * is erased (has value 0xFF). Otherwise this function might lock the machine.
 *
 * \param[in] addr  Address of the byte to program.
 * \param[in] value Value to program on addr.
 *
 * \warning If you try programming to a non erased address, this function
 * might fail and lock the machine.
 ****************************************************************************/
void flash_program_byte(uint32_t addr, uint8_t value);

#endif /*__FLASH_H__*/
