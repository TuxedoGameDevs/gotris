//
// Created by Jordi Montornes on 04/04/2017.
//

#ifndef GOTRIS_MONTYLIB_H_H
#define GOTRIS_MONTYLIB_H_H

#define SCREEN_ROWS 24
#define SCREEN_COLUMNS 32

extern bool pause;
extern u8 rand_index;
extern u16 frame_cnt;

extern u8 current_resource_bank;
extern u8 current_music_bank;
extern u8 current_fx_bank;

extern u8 target_atenuation;
extern u8 current_atenuation;

void init_console(void);
void clear_tilemap(void);
void waitForFrame(void);
void waitForFrameNoMusic(void);

u8 get_highscore_position(u16 score);
void insert_highscore_position(u8 scorePosition, u16 score);
u8 get_highscore_position_char(u8 scorePosition, u8 charPosition);
void update_highscore_position(u8 scorePosition, u8 charPosition, u8 charValue);

u8 get_rand(void);

//#define SMS_loadTileMap(x,y,src,size)            SMS_VRAMmemcpy (XYtoADDR((x),(y)),(src),(size));
void setTileMapToHighPriority(u8 x, u8 y);

//list mandanga
linked_list_t* createList(int typeSize);
void addElementBeginingList(linked_list_t* list, void* element);

void addElementEndList(linked_list_t* list, void* element);


void deleteElementBeginingList(linked_list_t* list);

void deleteElementEndList(linked_list_t* list);

void deleteElementPosList(linked_list_t* list, u8 pos);

void deleteElement_Unsafe(linked_list_t* list, list_node_t* element_to_delete);

void destroyList(linked_list_t* list);

#endif //GOTRIS_MONTYLIB_H_H
