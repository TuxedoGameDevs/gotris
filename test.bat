SET CURRENTDIR="%cd%"
SET DOCKER_FLAGS=--rm -v %CURRENTDIR%:/home/sms-tk/host -w /home/sms-tk/host -t retcon85/toolchain-sms -c
docker run  %DOCKER_FLAGS% "make test SYSTEM=foo"
if %errorlevel% neq 0 exit
java -jar %EMULICIOUS_PATH%\Emulicious.jar test.sms