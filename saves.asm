;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module saves
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _flash_program
	.globl _flash_sector_erase
	.globl _flash_ids_get
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _init_savesystem
	.globl _saveChanges
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$init_savesystem$0$0	= .
	.globl	G$init_savesystem$0$0
	C$saves.c$12$0_0$109	= .
	.globl	C$saves.c$12$0_0$109
;src/libs/saves.c:12: bool init_savesystem(void) {
;	---------------------------------
; Function init_savesystem
; ---------------------------------
_init_savesystem::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	C$saves.c$13$2_0$109	= .
	.globl	C$saves.c$13$2_0$109
;src/libs/saves.c:13: bool saveToInitialize = false;
	ld	-2 (ix), #0x00
	C$saves.c$14$2_0$109	= .
	.globl	C$saves.c$14$2_0$109
;src/libs/saves.c:14: bool failureToSave = false;
	ld	-1 (ix), #0x00
	C$saves.c$15$1_0$109	= .
	.globl	C$saves.c$15$1_0$109
;src/libs/saves.c:15: currentSaveSystem = NO_SAVE;
	ld	hl, #_currentSaveSystem
	ld	(hl), #0x00
	C$saves.c$18$1_1$110	= .
	.globl	C$saves.c$18$1_1$110
;src/libs/saves.c:18: flash_ids_get(ids);
	ld	hl, #0
	add	hl, sp
	call	_flash_ids_get
	C$saves.c$19$1_1$110	= .
	.globl	C$saves.c$19$1_1$110
;src/libs/saves.c:19: if(ids[0]==0xBF && ids[1]==0xB7) {
	ld	a, -4 (ix)
	sub	a, #0xbf
	jr	NZ, 00102$
	ld	a, -3 (ix)
	sub	a, #0xb7
	jr	NZ, 00102$
	C$saves.c$21$2_1$111	= .
	.globl	C$saves.c$21$2_1$111
;src/libs/saves.c:21: currentSaveSystem = FLASH_SAVE;       
	ld	hl, #_currentSaveSystem
	ld	(hl), #0x02
	C$saves.c$22$2_1$111	= .
	.globl	C$saves.c$22$2_1$111
;src/libs/saves.c:22: SMS_mapROMBank(FLASH_SAVING_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x1f
	C$saves.c$23$2_1$111	= .
	.globl	C$saves.c$23$2_1$111
;src/libs/saves.c:23: highscore_table_backup = (highscore_struct*)(0x8000 | (FLASH_SAVING_ADDRESS & 0x3FFF));
	ld	hl, #0x8000
	ld	(_highscore_table_backup), hl
	jr	00103$
00102$:
	C$saves.c$25$2_1$112	= .
	.globl	C$saves.c$25$2_1$112
;src/libs/saves.c:25: currentSaveSystem = SRAM_SAVE; //tentative
	ld	hl, #_currentSaveSystem
	ld	(hl), #0x01
	C$saves.c$26$2_1$112	= .
	.globl	C$saves.c$26$2_1$112
;src/libs/saves.c:26: highscore_table_backup = (highscore_struct*)(&SMS_SRAM);
	ld	hl, #_SMS_SRAM+0
	ld	(_highscore_table_backup), hl
	C$saves.c$27$2_1$112	= .
	.globl	C$saves.c$27$2_1$112
;src/libs/saves.c:27: SMS_enableSRAM();
	ld	hl, #_SRAM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x08
00103$:
	C$saves.c$32$1_1$110	= .
	.globl	C$saves.c$32$1_1$110
;src/libs/saves.c:32: memcpy(&highscore_table, highscore_table_backup, sizeof(highscore_table)); 
	ld	de, #_highscore_table
	ld	hl, (_highscore_table_backup)
	ld	bc, #0x0034
	ldir
	C$saves.c$33$1_1$110	= .
	.globl	C$saves.c$33$1_1$110
;src/libs/saves.c:33: if( highscore_table.marker1== 69 && highscore_table.marker2 == 148) {
	ld	a, (#_highscore_table + 0)
	ld	bc, #_highscore_table + 1
	sub	a, #0x45
	jr	NZ, 00113$
	ld	a, (bc)
	sub	a, #0x94
	jr	NZ, 00113$
	C$saves.c$35$2_1$113	= .
	.globl	C$saves.c$35$2_1$113
;src/libs/saves.c:35: saveToInitialize = false;
	ld	-2 (ix), #0x00
	jp	00114$
00113$:
	C$saves.c$38$2_1$114	= .
	.globl	C$saves.c$38$2_1$114
;src/libs/saves.c:38: highscore_table.marker1 = 69;
	ld	hl, #_highscore_table
	ld	(hl), #0x45
	C$saves.c$39$2_1$114	= .
	.globl	C$saves.c$39$2_1$114
;src/libs/saves.c:39: highscore_table.marker2 = 148;
	ld	a, #0x94
	ld	(bc), a
	C$saves.c$40$2_1$114	= .
	.globl	C$saves.c$40$2_1$114
;src/libs/saves.c:40: if(currentSaveSystem == SRAM_SAVE) {
	ld	a, (_currentSaveSystem+0)
	dec	a
	jr	NZ, 00106$
	C$saves.c$42$3_1$115	= .
	.globl	C$saves.c$42$3_1$115
;src/libs/saves.c:42: memcpy(highscore_table_backup, &highscore_table, sizeof(highscore_table));
	ld	de, (_highscore_table_backup)
	push	bc
	ld	bc, #0x0034
	ldir
	pop	bc
	jr	00107$
00106$:
	C$saves.c$45$3_1$116	= .
	.globl	C$saves.c$45$3_1$116
;src/libs/saves.c:45: flash_sector_erase(FLASH_SAVING_ADDRESS);
	push	bc
	ld	de, #0xc000
	ld	hl, #0x0007
	call	_flash_sector_erase
	ld	hl, #0x0034
	push	hl
	ld	hl, #_highscore_table
	push	hl
	ld	de, #0xc000
	ld	hl, #0x0007
	call	_flash_program
	pop	bc
00107$:
	C$saves.c$49$2_1$114	= .
	.globl	C$saves.c$49$2_1$114
;src/libs/saves.c:49: memcpy(&highscore_table, highscore_table_backup, sizeof(highscore_table));
	ld	hl, (_highscore_table_backup)
	push	bc
	ld	de, #_highscore_table
	ld	bc, #0x0034
	ldir
	pop	bc
	C$saves.c$51$2_1$114	= .
	.globl	C$saves.c$51$2_1$114
;src/libs/saves.c:51: if(highscore_table.marker1 == 69 && highscore_table.marker2 == 148) {
	ld	a, (#_highscore_table + 0)
	sub	a, #0x45
	jp	NZ,00109$
	ld	a, (bc)
	sub	a, #0x94
	jp	NZ,00109$
	C$saves.c$52$3_1$117	= .
	.globl	C$saves.c$52$3_1$117
;src/libs/saves.c:52: saveToInitialize = true;
	ld	-2 (ix), #0x01
	C$saves.c$54$3_1$117	= .
	.globl	C$saves.c$54$3_1$117
;src/libs/saves.c:54: highscore_table.table[0].name[0] = 9;
	ld	hl, #(_highscore_table + 2)
	ld	(hl), #0x09
	C$saves.c$55$3_1$117	= .
	.globl	C$saves.c$55$3_1$117
;src/libs/saves.c:55: highscore_table.table[0].name[1] = 12;
	ld	hl, #(_highscore_table + 3)
	ld	(hl), #0x0c
	C$saves.c$56$3_1$117	= .
	.globl	C$saves.c$56$3_1$117
;src/libs/saves.c:56: highscore_table.table[0].name[2] = 18;
	ld	hl, #(_highscore_table + 4)
	ld	(hl), #0x12
	C$saves.c$57$3_1$117	= .
	.globl	C$saves.c$57$3_1$117
;src/libs/saves.c:57: highscore_table.table[0].score = 1000;
	ld	hl, #0x03e8
	ld	((_highscore_table + 5)), hl
	C$saves.c$59$3_1$117	= .
	.globl	C$saves.c$59$3_1$117
;src/libs/saves.c:59: highscore_table.table[1].name[0] = 0;
	ld	hl, #(_highscore_table + 7)
	ld	(hl), #0x00
	C$saves.c$60$3_1$117	= .
	.globl	C$saves.c$60$3_1$117
;src/libs/saves.c:60: highscore_table.table[1].name[1] = 12;
	ld	hl, #(_highscore_table + 8)
	ld	(hl), #0x0c
	C$saves.c$61$3_1$117	= .
	.globl	C$saves.c$61$3_1$117
;src/libs/saves.c:61: highscore_table.table[1].name[2] = 2;
	ld	hl, #(_highscore_table + 9)
	ld	(hl), #0x02
	C$saves.c$62$3_1$117	= .
	.globl	C$saves.c$62$3_1$117
;src/libs/saves.c:62: highscore_table.table[1].score = 900;
	ld	hl, #0x0384
	ld	((_highscore_table + 10)), hl
	C$saves.c$64$3_1$117	= .
	.globl	C$saves.c$64$3_1$117
;src/libs/saves.c:64: highscore_table.table[2].name[0] = 14;
	ld	hl, #(_highscore_table + 12)
	ld	(hl), #0x0e
	C$saves.c$65$3_1$117	= .
	.globl	C$saves.c$65$3_1$117
;src/libs/saves.c:65: highscore_table.table[2].name[1] = 17;
	ld	hl, #(_highscore_table + 13)
	ld	(hl), #0x11
	C$saves.c$66$3_1$117	= .
	.globl	C$saves.c$66$3_1$117
;src/libs/saves.c:66: highscore_table.table[2].name[2] = 12;
	ld	hl, #(_highscore_table + 14)
	ld	(hl), #0x0c
	C$saves.c$67$3_1$117	= .
	.globl	C$saves.c$67$3_1$117
;src/libs/saves.c:67: highscore_table.table[2].score = 800;
	ld	hl, #0x0320
	ld	((_highscore_table + 15)), hl
	C$saves.c$69$3_1$117	= .
	.globl	C$saves.c$69$3_1$117
;src/libs/saves.c:69: highscore_table.table[3].name[0] = 9;
	ld	hl, #(_highscore_table + 17)
	ld	(hl), #0x09
	C$saves.c$70$3_1$117	= .
	.globl	C$saves.c$70$3_1$117
;src/libs/saves.c:70: highscore_table.table[3].name[1] = 1;
	ld	hl, #(_highscore_table + 18)
	ld	(hl), #0x01
	C$saves.c$71$3_1$117	= .
	.globl	C$saves.c$71$3_1$117
;src/libs/saves.c:71: highscore_table.table[3].name[2] = 2;
	ld	hl, #(_highscore_table + 19)
	ld	(hl), #0x02
	C$saves.c$72$3_1$117	= .
	.globl	C$saves.c$72$3_1$117
;src/libs/saves.c:72: highscore_table.table[3].score = 700;
	ld	hl, #0x02bc
	ld	((_highscore_table + 20)), hl
	C$saves.c$74$3_1$117	= .
	.globl	C$saves.c$74$3_1$117
;src/libs/saves.c:74: highscore_table.table[4].name[0] = 17;
	ld	hl, #(_highscore_table + 22)
	ld	(hl), #0x11
	C$saves.c$75$3_1$117	= .
	.globl	C$saves.c$75$3_1$117
;src/libs/saves.c:75: highscore_table.table[4].name[1] = 2;
	ld	hl, #(_highscore_table + 23)
	ld	(hl), #0x02
	C$saves.c$76$3_1$117	= .
	.globl	C$saves.c$76$3_1$117
;src/libs/saves.c:76: highscore_table.table[4].name[2] = 1;
	ld	hl, #(_highscore_table + 24)
	ld	(hl), #0x01
	C$saves.c$77$3_1$117	= .
	.globl	C$saves.c$77$3_1$117
;src/libs/saves.c:77: highscore_table.table[4].score = 600;
	ld	hl, #0x0258
	ld	((_highscore_table + 25)), hl
	C$saves.c$79$3_1$117	= .
	.globl	C$saves.c$79$3_1$117
;src/libs/saves.c:79: highscore_table.table[5].name[0] = 0;
	ld	hl, #(_highscore_table + 27)
	ld	(hl), #0x00
	C$saves.c$80$3_1$117	= .
	.globl	C$saves.c$80$3_1$117
;src/libs/saves.c:80: highscore_table.table[5].name[1] = 12;
	ld	hl, #(_highscore_table + 28)
	ld	(hl), #0x0c
	C$saves.c$81$3_1$117	= .
	.globl	C$saves.c$81$3_1$117
;src/libs/saves.c:81: highscore_table.table[5].name[2] = 18;
	ld	hl, #(_highscore_table + 29)
	ld	(hl), #0x12
	C$saves.c$82$3_1$117	= .
	.globl	C$saves.c$82$3_1$117
;src/libs/saves.c:82: highscore_table.table[5].score = 500;
	ld	hl, #0x01f4
	ld	((_highscore_table + 30)), hl
	C$saves.c$84$3_1$117	= .
	.globl	C$saves.c$84$3_1$117
;src/libs/saves.c:84: highscore_table.table[6].name[0] = 12;
	ld	hl, #(_highscore_table + 32)
	ld	(hl), #0x0c
	C$saves.c$85$3_1$117	= .
	.globl	C$saves.c$85$3_1$117
;src/libs/saves.c:85: highscore_table.table[6].name[1] = 18;
	ld	hl, #(_highscore_table + 33)
	ld	(hl), #0x12
	C$saves.c$86$3_1$117	= .
	.globl	C$saves.c$86$3_1$117
;src/libs/saves.c:86: highscore_table.table[6].name[2] = 12;
	ld	hl, #(_highscore_table + 34)
	ld	(hl), #0x0c
	C$saves.c$87$3_1$117	= .
	.globl	C$saves.c$87$3_1$117
;src/libs/saves.c:87: highscore_table.table[6].score = 400;
	ld	hl, #0x0190
	ld	((_highscore_table + 35)), hl
	C$saves.c$89$3_1$117	= .
	.globl	C$saves.c$89$3_1$117
;src/libs/saves.c:89: highscore_table.table[7].name[0] = 3;
	ld	hl, #(_highscore_table + 37)
	ld	(hl), #0x03
	C$saves.c$90$3_1$117	= .
	.globl	C$saves.c$90$3_1$117
;src/libs/saves.c:90: highscore_table.table[7].name[1] = 18;
	ld	hl, #(_highscore_table + 38)
	ld	(hl), #0x12
	C$saves.c$91$3_1$117	= .
	.globl	C$saves.c$91$3_1$117
;src/libs/saves.c:91: highscore_table.table[7].name[2] = 12;
	ld	hl, #(_highscore_table + 39)
	ld	(hl), #0x0c
	C$saves.c$92$3_1$117	= .
	.globl	C$saves.c$92$3_1$117
;src/libs/saves.c:92: highscore_table.table[7].score = 300;
	ld	hl, #0x012c
	ld	((_highscore_table + 40)), hl
	C$saves.c$94$3_1$117	= .
	.globl	C$saves.c$94$3_1$117
;src/libs/saves.c:94: highscore_table.table[8].name[0] = 4;
	ld	hl, #(_highscore_table + 42)
	ld	(hl), #0x04
	C$saves.c$95$3_1$117	= .
	.globl	C$saves.c$95$3_1$117
;src/libs/saves.c:95: highscore_table.table[8].name[1] = 8;
	ld	hl, #(_highscore_table + 43)
	ld	(hl), #0x08
	C$saves.c$96$3_1$117	= .
	.globl	C$saves.c$96$3_1$117
;src/libs/saves.c:96: highscore_table.table[8].name[2] = 15;
	ld	hl, #(_highscore_table + 44)
	ld	(hl), #0x0f
	C$saves.c$97$3_1$117	= .
	.globl	C$saves.c$97$3_1$117
;src/libs/saves.c:97: highscore_table.table[8].score = 200;
	ld	hl, #0x00c8
	ld	((_highscore_table + 45)), hl
	C$saves.c$99$3_1$117	= .
	.globl	C$saves.c$99$3_1$117
;src/libs/saves.c:99: highscore_table.table[9].name[0] = 9;
	ld	hl, #(_highscore_table + 47)
	ld	(hl), #0x09
	C$saves.c$100$3_1$117	= .
	.globl	C$saves.c$100$3_1$117
;src/libs/saves.c:100: highscore_table.table[9].name[1] = 21;
	ld	hl, #(_highscore_table + 48)
	ld	(hl), #0x15
	C$saves.c$101$3_1$117	= .
	.globl	C$saves.c$101$3_1$117
;src/libs/saves.c:101: highscore_table.table[9].name[2] = 18;
	ld	hl, #(_highscore_table + 49)
	ld	(hl), #0x12
	C$saves.c$102$3_1$117	= .
	.globl	C$saves.c$102$3_1$117
;src/libs/saves.c:102: highscore_table.table[9].score = 100;
	ld	hl, #0x0064
	ld	((_highscore_table + 50)), hl
	jr	00114$
00109$:
	C$saves.c$105$3_1$118	= .
	.globl	C$saves.c$105$3_1$118
;src/libs/saves.c:105: failureToSave = true;
	ld	-1 (ix), #0x01
00114$:
	C$saves.c$108$1_1$110	= .
	.globl	C$saves.c$108$1_1$110
;src/libs/saves.c:108: if(!failureToSave) {
	bit	0, -1 (ix)
	jr	NZ, 00120$
	C$saves.c$109$2_1$119	= .
	.globl	C$saves.c$109$2_1$119
;src/libs/saves.c:109: if(currentSaveSystem == SRAM_SAVE) {
	ld	a, (_currentSaveSystem+0)
	dec	a
	jr	NZ, 00117$
	C$saves.c$111$3_1$120	= .
	.globl	C$saves.c$111$3_1$120
;src/libs/saves.c:111: memcpy(highscore_table_backup, &highscore_table, sizeof(highscore_table));
	ld	de, (_highscore_table_backup)
	ld	hl, #_highscore_table
	ld	bc, #0x0034
	ldir
	jr	00118$
00117$:
	C$saves.c$114$3_1$121	= .
	.globl	C$saves.c$114$3_1$121
;src/libs/saves.c:114: flash_sector_erase(FLASH_SAVING_ADDRESS);
	ld	de, #0xc000
	ld	hl, #0x0007
	call	_flash_sector_erase
	C$saves.c$115$3_1$121	= .
	.globl	C$saves.c$115$3_1$121
;src/libs/saves.c:115: flash_program(FLASH_SAVING_ADDRESS, (char*)&highscore_table, sizeof(highscore_table));
	ld	hl, #0x0034
	push	hl
	ld	hl, #_highscore_table
	push	hl
	ld	de, #0xc000
	ld	hl, #0x0007
	call	_flash_program
00118$:
	C$saves.c$118$2_1$119	= .
	.globl	C$saves.c$118$2_1$119
;src/libs/saves.c:118: memcpy(&highscore_table, highscore_table_backup, sizeof(highscore_table));
	ld	hl, (_highscore_table_backup)
	ld	de, #_highscore_table
	ld	bc, #0x0034
	ldir
00120$:
	C$saves.c$120$1_1$110	= .
	.globl	C$saves.c$120$1_1$110
;src/libs/saves.c:120: if(currentSaveSystem == SRAM_SAVE) {     
	ld	a, (_currentSaveSystem+0)
	dec	a
	jr	NZ, 00122$
	C$saves.c$121$2_1$122	= .
	.globl	C$saves.c$121$2_1$122
;src/libs/saves.c:121: SMS_disableSRAM();
	ld	hl, #_SRAM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x00
	jr	00123$
00122$:
	C$saves.c$123$2_1$123	= .
	.globl	C$saves.c$123$2_1$123
;src/libs/saves.c:123: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
00123$:
	C$saves.c$125$1_1$110	= .
	.globl	C$saves.c$125$1_1$110
;src/libs/saves.c:125: if(failureToSave) {
	bit	0, -1 (ix)
	jr	Z, 00125$
	C$saves.c$126$2_1$124	= .
	.globl	C$saves.c$126$2_1$124
;src/libs/saves.c:126: currentSaveSystem = NO_SAVE;
	ld	hl, #_currentSaveSystem
	ld	(hl), #0x00
00125$:
	C$saves.c$128$1_1$110	= .
	.globl	C$saves.c$128$1_1$110
;src/libs/saves.c:128: return saveToInitialize;
	ld	a, -2 (ix)
	C$saves.c$129$1_1$109	= .
	.globl	C$saves.c$129$1_1$109
;src/libs/saves.c:129: }
	ld	sp, ix
	pop	ix
	C$saves.c$129$1_1$109	= .
	.globl	C$saves.c$129$1_1$109
	XG$init_savesystem$0$0	= .
	.globl	XG$init_savesystem$0$0
	ret
	G$saveChanges$0$0	= .
	.globl	G$saveChanges$0$0
	C$saves.c$132$1_1$126	= .
	.globl	C$saves.c$132$1_1$126
;src/libs/saves.c:132: void saveChanges(void) {
;	---------------------------------
; Function saveChanges
; ---------------------------------
_saveChanges::
	C$saves.c$133$1_0$126	= .
	.globl	C$saves.c$133$1_0$126
;src/libs/saves.c:133: switch (currentSaveSystem)
	ld	a, (_currentSaveSystem+0)
	or	a, a
	ret	Z
	ld	a, (_currentSaveSystem+0)
	dec	a
	jr	Z, 00102$
	ld	a, (_currentSaveSystem+0)
	sub	a, #0x02
	jr	Z, 00103$
	ret
	C$saves.c$137$2_0$127	= .
	.globl	C$saves.c$137$2_0$127
;src/libs/saves.c:137: case SRAM_SAVE:
00102$:
	C$saves.c$138$2_0$127	= .
	.globl	C$saves.c$138$2_0$127
;src/libs/saves.c:138: SMS_enableSRAM();
	ld	iy, #_SRAM_bank_to_be_mapped_on_slot2
	ld	0 (iy), #0x08
	C$saves.c$139$2_0$127	= .
	.globl	C$saves.c$139$2_0$127
;src/libs/saves.c:139: memcpy(highscore_table_backup, &highscore_table, sizeof(highscore_table));
	ld	de, (_highscore_table_backup)
	ld	hl, #_highscore_table
	ld	bc, #0x0034
	ldir
	C$saves.c$140$2_0$127	= .
	.globl	C$saves.c$140$2_0$127
;src/libs/saves.c:140: SMS_disableSRAM();
	ld	0 (iy), #0x00
	C$saves.c$141$2_0$127	= .
	.globl	C$saves.c$141$2_0$127
;src/libs/saves.c:141: break;
	ret
	C$saves.c$142$2_0$127	= .
	.globl	C$saves.c$142$2_0$127
;src/libs/saves.c:142: case FLASH_SAVE:
00103$:
	C$saves.c$143$2_0$127	= .
	.globl	C$saves.c$143$2_0$127
;src/libs/saves.c:143: SMS_mapROMBank(FLASH_SAVING_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x1f
	C$saves.c$144$2_0$127	= .
	.globl	C$saves.c$144$2_0$127
;src/libs/saves.c:144: flash_sector_erase(FLASH_SAVING_ADDRESS);
	ld	de, #0xc000
	ld	hl, #0x0007
	call	_flash_sector_erase
	C$saves.c$145$2_0$127	= .
	.globl	C$saves.c$145$2_0$127
;src/libs/saves.c:145: flash_program(FLASH_SAVING_ADDRESS, (char*)&highscore_table, sizeof(highscore_table));
	ld	hl, #0x0034
	push	hl
	ld	hl, #_highscore_table
	push	hl
	ld	de, #0xc000
	ld	hl, #0x0007
	call	_flash_program
	C$saves.c$146$2_0$127	= .
	.globl	C$saves.c$146$2_0$127
;src/libs/saves.c:146: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$saves.c$150$1_0$126	= .
	.globl	C$saves.c$150$1_0$126
;src/libs/saves.c:150: }
	C$saves.c$151$1_0$126	= .
	.globl	C$saves.c$151$1_0$126
;src/libs/saves.c:151: }
	C$saves.c$151$1_0$126	= .
	.globl	C$saves.c$151$1_0$126
	XG$saveChanges$0$0	= .
	.globl	XG$saveChanges$0$0
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
