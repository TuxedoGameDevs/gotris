CC=sdcc
TOOLCHAIN_DIR=/usr/local/share/sdcc
SMSLIB_INCDIR=$(TOOLCHAIN_DIR)/include/sms
PEEP_RULES=$(TOOLCHAIN_DIR)/lib/sms/peep-rules.txt
CRT0=$(TOOLCHAIN_DIR)/lib/sms/crt0_sms.rel
SMSLIB_LIB=$(TOOLCHAIN_DIR)/lib/sms/SMSlib.lib
PSGLIB_LIB=$(TOOLCHAIN_DIR)/lib/sms/PSGlib.rel

CFLAGS = --debug -c -mz80 -I$(SMSLIB_INCDIR) --peep-file $(PEEP_RULES) -D$(SYSTEM)
BANKFLAG = --constseg BANK2
LDFLAGS = --debug -mz80 --no-std-crt0  --data-loc 0xC000 -Wl-b_BANK2=0x8000 -Wl-b_BANK3=0x8000 -Wl-b_BANK4=0x8000 -Wl-b_BANK5=0x8000 -Wl-b_BANK6=0x8000 -Wl-b_BANK7=0x8000 -Wl-b_BANK8=0x8000 -Wl-b_BANK9=0x8000 -Wl-b_BANK10=0x8000 -Wl-b_BANK11=0x8000 -Wl-b_BANK12=0x8000 -Wl-b_BANK13=0x8000 -Wl-b_BANK14=0x8000 -Wl-b_BANK15=0x8000 -Wl-b_BANK16=0x8000 

OBJS= game.rel montylib.rel fxsample.rel flash.rel saves.rel gamelogic.rel resources.rel board.rel input.rel palettes.rel states.rel music.rel cpu.rel globalvariables.rel constantdata.rel default_state.rel enter_score_state.rel game_state.rel logo_state.rel  score_state.rel  credits_state.rel title_state.rel demo_state.rel intro_state.rel end_game_state.rel bank3.rel bank4.rel bank5.rel bank6.rel bank7.rel bank8.rel bank9.rel bank10.rel bank11.rel bank12.rel bank13.rel bank14.rel bank15.rel bank16.rel
LIBS =  $(SMSLIB_LIB) $(PSGLIB_LIB) 
HEADERS = src/engine/globaldefinitions.h
ASSETS = src/assets2banks.h
IHX2SMS=ihx2sms
IHXFILE=gotris.ihx
ROM=gotris.sms

TESTLDFLAGS = --debug -mz80 --no-std-crt0  --data-loc 0xC000 -Wl-b_BANK2=0x8000
TESTOBJS= boardtest.rel board.rel globalvariables.rel cpu.rel montylib.rel constantdata.rel
TESTIHXFILE=test.ihx
TESTROM=test.sms

test: $(TESTROM)

$(TESTROM): $(TESTIHXFILE)
	$(IHX2SMS) $(TESTIHXFILE) $(TESTROM)

$(TESTIHXFILE): $(TESTOBJS) 
	$(CC) -o $(TESTIHXFILE) $(TESTLDFLAGS)  $(CRT0) $(TESTOBJS) $(LIBS)

boardtest.rel: src/boardtest.c $(HEADERS)
	$(CC) -c $(CFLAGS) $<

all: build

build: $(ROM)

$(ROM): $(IHXFILE)
	$(IHX2SMS) $(IHXFILE) $(ROM)

$(HEADERS):

$(LIBS):

$(IHXFILE): $(OBJS) 
	$(CC) -o $(IHXFILE) $(LDFLAGS)  $(CRT0) $(OBJS) $(LIBS)

game.rel: src/game.c $(HEADERS)
	$(CC) -c $(CFLAGS) $<
resources.rel: src/resources/resources.c $(ASSETS)
	$(CC) -c $(CFLAGS) $<
music.rel: src/resources/music.c $(ASSETS)
	$(CC) -c $(CFLAGS) $<
constantdata.rel: src/data/constantdata.c 
	$(CC) -c $(CFLAGS) $(BANKFLAG) $<
%.rel: src/ram/%.c 
	$(CC) -c $(CFLAGS) $<
%.rel: src/engine/%.c $(HEADERS)
	$(CC) -c $(CFLAGS) $<
%.rel: src/libs/%.c
	$(CC) -c $(CFLAGS) $<
%.rel: src/states/%.c
	$(CC) -c $(CFLAGS) $<



.PHONY: clean
clean:
	$(RM) -v $(ROM) *.sym *.lnk *.ihx *.map *.lst $(OBJS) main.rel *.asm *.bin *.lk *.noi 

help:
	echo Usage:
	echo make          = builds all except main.c
	echo make clean    = clean intermediate files
