;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module flash
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _flash_program
	.globl _flash_program_byte
	.globl _flash_sector_erase
	.globl _flash_ids_get
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$flash_ids_get$0$0	= .
	.globl	G$flash_ids_get$0$0
	C$flash.c$105$0_0$29	= .
	.globl	C$flash.c$105$0_0$29
;src/libs/flash.c:105: void flash_ids_get(uint8_t *ids)
;	---------------------------------
; Function flash_ids_get
; ---------------------------------
_flash_ids_get::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-30
	add	iy, sp
	ld	sp, iy
	push	hl
	ld	hl, #2
	add	hl, sp
	ex	de, hl
	ld	hl, #_IDS_READ
	ld	bc, #0x001e
	ldir
	pop	de
	C$flash.c$110$1_0$29	= .
	.globl	C$flash.c$110$1_0$29
;src/libs/flash.c:110: ((ids_read_fn)code)(ids);
	ex	de, hl
	ld	iy, #0
	add	iy, sp
	call	___sdcc_call_iy
	C$flash.c$111$1_0$29	= .
	.globl	C$flash.c$111$1_0$29
;src/libs/flash.c:111: }
	ld	sp, ix
	pop	ix
	C$flash.c$111$1_0$29	= .
	.globl	C$flash.c$111$1_0$29
	XG$flash_ids_get$0$0	= .
	.globl	XG$flash_ids_get$0$0
	ret
Fflash$IDS_READ$0_0$0 == .
_IDS_READ:
	.db #0xf3	; 243
	.db #0xeb	; 235
	.db #0x21	; 33
	.db #0x55	; 85	'U'
	.db #0x55	; 85	'U'
	.db #0x36	; 54	'6'
	.db #0xaa	; 170
	.db #0x21	; 33
	.db #0xaa	; 170
	.db #0x2a	; 42
	.db #0x36	; 54	'6'
	.db #0x55	; 85	'U'
	.db #0x21	; 33
	.db #0x55	; 85	'U'
	.db #0x55	; 85	'U'
	.db #0x36	; 54	'6'
	.db #0x90	; 144
	.db #0x3a	; 58
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x12	; 18
	.db #0x13	; 19
	.db #0x3a	; 58
	.db #0x01	; 1
	.db #0x00	; 0
	.db #0x12	; 18
	.db #0x36	; 54	'6'
	.db #0xf0	; 240
	.db #0xfb	; 251
	.db #0xc9	; 201
Fflash$SECTOR_ERASE$0_0$0 == .
_SECTOR_ERASE:
	.db #0xf3	; 243
	.db #0x21	; 33
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0x4e	; 78	'N'
	.db #0x77	; 119	'w'
	.db #0x21	; 33
	.db #0x55	; 85	'U'
	.db #0x55	; 85	'U'
	.db #0x36	; 54	'6'
	.db #0xaa	; 170
	.db #0x21	; 33
	.db #0xaa	; 170
	.db #0x2a	; 42
	.db #0x36	; 54	'6'
	.db #0x55	; 85	'U'
	.db #0x21	; 33
	.db #0x55	; 85	'U'
	.db #0x55	; 85	'U'
	.db #0x36	; 54	'6'
	.db #0x80	; 128
	.db #0x36	; 54	'6'
	.db #0xaa	; 170
	.db #0x21	; 33
	.db #0xaa	; 170
	.db #0x2a	; 42
	.db #0x36	; 54	'6'
	.db #0x55	; 85	'U'
	.db #0x6b	; 107	'k'
	.db #0x62	; 98	'b'
	.db #0x36	; 54	'6'
	.db #0x30	; 48	'0'
	.db #0x7e	; 126
	.db #0xe6	; 230
	.db #0x80	; 128
	.db #0x28	; 40
	.db #0xfb	; 251
	.db #0x21	; 33
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0x71	; 113	'q'
	.db #0xfb	; 251
	.db #0xc9	; 201
Fflash$PROGRAM$0_0$0 == .
_PROGRAM:
	.db #0xf3	; 243
	.db #0x21	; 33
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0x4e	; 78	'N'
	.db #0x77	; 119	'w'
	.db #0xfd	; 253
	.db #0x21	; 33
	.db #0x02	; 2
	.db #0x00	; 0
	.db #0xfd	; 253
	.db #0x39	; 57	'9'
	.db #0xfd	; 253
	.db #0x7e	; 126
	.db #0x00	; 0
	.db #0x21	; 33
	.db #0x55	; 85	'U'
	.db #0x55	; 85	'U'
	.db #0x36	; 54	'6'
	.db #0xaa	; 170
	.db #0x21	; 33
	.db #0xaa	; 170
	.db #0x2a	; 42
	.db #0x36	; 54	'6'
	.db #0x55	; 85	'U'
	.db #0x21	; 33
	.db #0x55	; 85	'U'
	.db #0x55	; 85	'U'
	.db #0x36	; 54	'6'
	.db #0xa0	; 160
	.db #0x6b	; 107	'k'
	.db #0x62	; 98	'b'
	.db #0x77	; 119	'w'
	.db #0x47	; 71	'G'
	.db #0x7e	; 126
	.db #0xb8	; 184
	.db #0x20	; 32
	.db #0xfc	; 252
	.db #0x21	; 33
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0x71	; 113	'q'
	.db #0xfb	; 251
	.db #0xe1	; 225
	.db #0x33	; 51	'3'
	.db #0xe9	; 233
	G$flash_sector_erase$0$0	= .
	.globl	G$flash_sector_erase$0$0
	C$flash.c$113$1_0$31	= .
	.globl	C$flash.c$113$1_0$31
;src/libs/flash.c:113: void flash_sector_erase(uint32_t addr)
;	---------------------------------
; Function flash_sector_erase
; ---------------------------------
_flash_sector_erase::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-47
	add	iy, sp
	ld	sp, iy
	C$flash.c$116$1_0$31	= .
	.globl	C$flash.c$116$1_0$31
;src/libs/flash.c:116: const uint8_t bank = addr >> 14;
	ld	-4 (ix), d
	ld	-3 (ix), l
	ld	-2 (ix), h
	ld	-1 (ix), #0x00
	ld	b, #0x06
00103$:
	srl	-2 (ix)
	rr	-3 (ix)
	rr	-4 (ix)
	djnz	00103$
	ld	c, -4 (ix)
	C$flash.c$118$1_0$31	= .
	.globl	C$flash.c$118$1_0$31
;src/libs/flash.c:118: const uint16_t offset = 0x8000 | (addr & 0x3FFF);
	ld	a, d
	and	a, #0x3f
	ld	d, a
	set	7, d
	C$flash.c$120$1_0$31	= .
	.globl	C$flash.c$120$1_0$31
;src/libs/flash.c:120: memcpy(code, SECTOR_ERASE, SECTOR_ERASE_LEN);
	push	de
	push	bc
	ex	de, hl
	ld	hl, #4
	add	hl, sp
	ex	de, hl
	ld	hl, #_SECTOR_ERASE
	ld	bc, #0x002b
	ldir
	pop	bc
	pop	de
	C$flash.c$121$1_0$31	= .
	.globl	C$flash.c$121$1_0$31
;src/libs/flash.c:121: ((sect_erase_fn)code)(bank, offset);
	ld	a, c
	ld	hl, #0
	add	hl, sp
	call	___sdcc_call_hl
	C$flash.c$122$1_0$31	= .
	.globl	C$flash.c$122$1_0$31
;src/libs/flash.c:122: }
	ld	sp, ix
	pop	ix
	C$flash.c$122$1_0$31	= .
	.globl	C$flash.c$122$1_0$31
	XG$flash_sector_erase$0$0	= .
	.globl	XG$flash_sector_erase$0$0
	ret
	G$flash_program_byte$0$0	= .
	.globl	G$flash_program_byte$0$0
	C$flash.c$124$1_0$33	= .
	.globl	C$flash.c$124$1_0$33
;src/libs/flash.c:124: void flash_program_byte(uint32_t addr, uint8_t value)
;	---------------------------------
; Function flash_program_byte
; ---------------------------------
_flash_program_byte::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-50
	add	iy, sp
	ld	sp, iy
	C$flash.c$127$1_0$33	= .
	.globl	C$flash.c$127$1_0$33
;src/libs/flash.c:127: const uint8_t bank = addr >> 14;
	ld	-4 (ix), d
	ld	-3 (ix), l
	ld	-2 (ix), h
	ld	-1 (ix), #0x00
	ld	b, #0x06
00103$:
	srl	-2 (ix)
	rr	-3 (ix)
	rr	-4 (ix)
	djnz	00103$
	ld	c, -4 (ix)
	C$flash.c$129$1_0$33	= .
	.globl	C$flash.c$129$1_0$33
;src/libs/flash.c:129: const uint16_t offset = 0x8000 | (addr & 0x3FFF);
	ld	a, d
	and	a, #0x3f
	ld	d, a
	set	7, d
	C$flash.c$131$1_0$33	= .
	.globl	C$flash.c$131$1_0$33
;src/libs/flash.c:131: memcpy(code, PROGRAM, PROGRAM_LEN);
	push	de
	push	bc
	ex	de, hl
	ld	hl, #4
	add	hl, sp
	ex	de, hl
	ld	hl, #_PROGRAM
	ld	bc, #0x002e
	ldir
	pop	bc
	pop	de
	C$flash.c$132$1_0$33	= .
	.globl	C$flash.c$132$1_0$33
;src/libs/flash.c:132: ((program_fn)code)(bank, offset, value);
	ld	a, 4 (ix)
	push	af
	inc	sp
	ld	a, c
	ld	hl, #1
	add	hl, sp
	call	___sdcc_call_hl
	C$flash.c$133$1_0$33	= .
	.globl	C$flash.c$133$1_0$33
;src/libs/flash.c:133: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
	G$flash_program$0$0	= .
	.globl	G$flash_program$0$0
	C$flash.c$135$1_0$35	= .
	.globl	C$flash.c$135$1_0$35
;src/libs/flash.c:135: void flash_program(uint32_t addr, const uint8_t *data, int16_t len)
;	---------------------------------
; Function flash_program
; ---------------------------------
_flash_program::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-55
	add	iy, sp
	ld	sp, iy
	C$flash.c$139$1_0$35	= .
	.globl	C$flash.c$139$1_0$35
;src/libs/flash.c:139: const uint8_t bank = addr >> 14;
	ld	-4 (ix), d
	ld	-3 (ix), l
	ld	-2 (ix), h
	ld	-1 (ix), #0x00
	ld	b, #0x06
00117$:
	srl	-2 (ix)
	rr	-3 (ix)
	rr	-4 (ix)
	djnz	00117$
	ld	a, -4 (ix)
	ld	-9 (ix), a
	C$flash.c$140$1_0$35	= .
	.globl	C$flash.c$140$1_0$35
;src/libs/flash.c:140: uint16_t offset = 0x8000 | (addr & 0x3FFF);
	ld	c, e
	ld	a, d
	and	a, #0x3f
	ld	b, a
	set	7, b
	C$flash.c$141$1_0$35	= .
	.globl	C$flash.c$141$1_0$35
;src/libs/flash.c:141: memcpy(code, PROGRAM, PROGRAM_LEN);
	ld	hl, #0
	add	hl, sp
	push	bc
	ex	de, hl
	ld	hl, #_PROGRAM
	ld	bc, #0x002e
	ldir
	pop	bc
	C$flash.c$143$2_0$36	= .
	.globl	C$flash.c$143$2_0$36
;src/libs/flash.c:143: while (len--) {
	ld	-6 (ix), c
	ld	-5 (ix), b
	ld	a, 4 (ix)
	ld	-4 (ix), a
	ld	a, 5 (ix)
	ld	-3 (ix), a
	ld	a, 6 (ix)
	ld	-2 (ix), a
	ld	a, 7 (ix)
	ld	-1 (ix), a
00101$:
	ld	a, -2 (ix)
	ld	-8 (ix), a
	ld	a, -1 (ix)
	ld	-7 (ix), a
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	dec	hl
	ld	-2 (ix), l
	ld	-1 (ix), h
	ld	a, -7 (ix)
	or	a, -8 (ix)
	jr	Z, 00104$
	C$flash.c$144$2_0$36	= .
	.globl	C$flash.c$144$2_0$36
;src/libs/flash.c:144: ((program_fn)code)(bank, offset, *data);
	ld	l, -4 (ix)
	ld	h, -3 (ix)
	ld	a, (hl)
	push	af
	inc	sp
	ld	e, -6 (ix)
	ld	d, -5 (ix)
	ld	a, -9 (ix)
	ld	hl, #1
	add	hl, sp
	call	___sdcc_call_hl
	C$flash.c$145$2_0$36	= .
	.globl	C$flash.c$145$2_0$36
;src/libs/flash.c:145: offset++;
	inc	-6 (ix)
	jr	NZ, 00119$
	inc	-5 (ix)
00119$:
	C$flash.c$146$2_0$36	= .
	.globl	C$flash.c$146$2_0$36
;src/libs/flash.c:146: data++;
	inc	-4 (ix)
	jr	NZ, 00101$
	inc	-3 (ix)
	jr	00101$
00104$:
	C$flash.c$148$1_0$35	= .
	.globl	C$flash.c$148$1_0$35
;src/libs/flash.c:148: }
	ld	sp, ix
	pop	ix
	pop	hl
	pop	af
	pop	af
	jp	(hl)
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
