;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module states
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _end_game_state_stop
	.globl _end_game_state_update
	.globl _end_game_state_start
	.globl _enter_score_state_stop
	.globl _enter_score_state_update
	.globl _enter_score_state_start
	.globl _demo_state_stop
	.globl _demo_state_update
	.globl _demo_state_start
	.globl _game_state_stop
	.globl _game_state_update
	.globl _game_state_start
	.globl _credits_state_stop
	.globl _credits_state_update
	.globl _credits_state_start
	.globl _score_state_stop
	.globl _score_state_update
	.globl _score_state_start
	.globl _title_state_stop
	.globl _title_state_update
	.globl _title_state_start
	.globl _intro_state_stop
	.globl _intro_state_update
	.globl _intro_state_start
	.globl _logo_state_stop
	.globl _logo_state_update
	.globl _logo_state_start
	.globl _default_state_stop
	.globl _default_state_update
	.globl _default_state_start
	.globl _free
	.globl _malloc
	.globl _push_state
	.globl _pop_state
	.globl _top_state
	.globl _init_states
	.globl _clear_states
	.globl _update_state
	.globl _transition_to_state
	.globl _move_to_sub_state
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$push_state$0$0	= .
	.globl	G$push_state$0$0
	C$states.c$19$0_0$72	= .
	.globl	C$states.c$19$0_0$72
;src/engine/states.c:19: void push_state(State* state) {
;	---------------------------------
; Function push_state
; ---------------------------------
_push_state::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, l
	ld	b, h
	C$states.c$20$1_0$72	= .
	.globl	C$states.c$20$1_0$72
;src/engine/states.c:20: if(statemanager.current_state_index + 1 < statemanager.state_stack_capacity) {
	ld	a, (#(_statemanager + 2) + 0)
	ld	-1 (ix), a
	ld	e, a
	rlca
	sbc	a, a
	ld	d, a
	inc	de
	ld	a, (#_statemanager + 3)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, e
	sub	a, l
	ld	a, d
	sbc	a, h
	jp	PO, 00117$
	xor	a, #0x80
00117$:
	jp	P, 00105$
	C$states.c$21$2_0$73	= .
	.globl	C$states.c$21$2_0$73
;src/engine/states.c:21: statemanager.current_state_index++;
	ld	e, -1 (ix)
	inc	e
	ld	hl, #(_statemanager + 2)
	ld	(hl), e
	C$states.c$22$2_0$73	= .
	.globl	C$states.c$22$2_0$73
;src/engine/states.c:22: statemanager.state_stack[statemanager.current_state_index] = state;
	ld	hl, (#_statemanager + 0)
	ld	a, e
	rlca
	sbc	a, a
	ld	d, a
	ex	de, hl
	add	hl, hl
	ex	de, hl
	add	hl, de
	ld	(hl), c
	inc	hl
	ld	(hl), b
	C$states.c$23$2_0$73	= .
	.globl	C$states.c$23$2_0$73
;src/engine/states.c:23: statemanager.current_target_state = -1;
	ld	hl, #(_statemanager + 4)
	ld	(hl), #0xff
	C$states.c$24$2_0$73	= .
	.globl	C$states.c$24$2_0$73
;src/engine/states.c:24: statemanager.preserve_context = false;
	ld	hl, #(_statemanager + 5)
	ld	(hl), #0x00
	C$states.c$25$2_0$73	= .
	.globl	C$states.c$25$2_0$73
;src/engine/states.c:25: if(state->start != NULL) {
	ld	l, c
	ld	h, b
	ld	c, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	a, h
	or	a, c
	jr	Z, 00105$
	C$states.c$26$3_0$74	= .
	.globl	C$states.c$26$3_0$74
;src/engine/states.c:26: state->start();
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	call	___sdcc_call_hl
00105$:
	C$states.c$29$1_0$72	= .
	.globl	C$states.c$29$1_0$72
;src/engine/states.c:29: }
	inc	sp
	pop	ix
	C$states.c$29$1_0$72	= .
	.globl	C$states.c$29$1_0$72
	XG$push_state$0$0	= .
	.globl	XG$push_state$0$0
	ret
	G$pop_state$0$0	= .
	.globl	G$pop_state$0$0
	C$states.c$31$1_0$76	= .
	.globl	C$states.c$31$1_0$76
;src/engine/states.c:31: void pop_state(void) {
;	---------------------------------
; Function pop_state
; ---------------------------------
_pop_state::
	C$states.c$33$1_0$76	= .
	.globl	C$states.c$33$1_0$76
;src/engine/states.c:33: if(statemanager.current_state_index == -1)
	ld	a, (#(_statemanager + 2) + 0)
	inc	a
	ret	Z
	C$states.c$34$1_0$76	= .
	.globl	C$states.c$34$1_0$76
;src/engine/states.c:34: return;
	jr	00102$
00102$:
	C$states.c$35$1_0$76	= .
	.globl	C$states.c$35$1_0$76
;src/engine/states.c:35: topState = top_state();
	call	_top_state
	C$states.c$36$1_0$76	= .
	.globl	C$states.c$36$1_0$76
;src/engine/states.c:36: if(topState->stop != NULL) {
	ld	hl, #4
	add	hl, de
	ld	c, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	a, h
	or	a, c
	ret	Z
	C$states.c$37$2_0$77	= .
	.globl	C$states.c$37$2_0$77
;src/engine/states.c:37: topState->stop();
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	call	___sdcc_call_hl
	C$states.c$38$2_0$77	= .
	.globl	C$states.c$38$2_0$77
;src/engine/states.c:38: statemanager.state_stack[statemanager.current_state_index] = NULL;
	ld	bc, (#_statemanager + 0)
	ld	a, (#(_statemanager + 2) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	rlca
	sbc	a, a
	ld	h, a
	add	hl, hl
	add	hl, bc
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	C$states.c$39$2_0$77	= .
	.globl	C$states.c$39$2_0$77
;src/engine/states.c:39: statemanager.current_state_index--;
	ld	a, (#(_statemanager + 2) + 0)
	dec	a
	ld	(#(_statemanager + 2)),a
	C$states.c$41$1_0$76	= .
	.globl	C$states.c$41$1_0$76
;src/engine/states.c:41: }
	C$states.c$41$1_0$76	= .
	.globl	C$states.c$41$1_0$76
	XG$pop_state$0$0	= .
	.globl	XG$pop_state$0$0
	ret
	G$top_state$0$0	= .
	.globl	G$top_state$0$0
	C$states.c$43$1_0$79	= .
	.globl	C$states.c$43$1_0$79
;src/engine/states.c:43: State *top_state(void) {
;	---------------------------------
; Function top_state
; ---------------------------------
_top_state::
	C$states.c$44$1_0$79	= .
	.globl	C$states.c$44$1_0$79
;src/engine/states.c:44: return statemanager.state_stack[statemanager.current_state_index];
	ld	bc, (#_statemanager + 0)
	ld	a, (#(_statemanager + 2) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	rlca
	sbc	a, a
	ld	h, a
	add	hl, hl
	add	hl, bc
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	C$states.c$45$1_0$79	= .
	.globl	C$states.c$45$1_0$79
;src/engine/states.c:45: }
	C$states.c$45$1_0$79	= .
	.globl	C$states.c$45$1_0$79
	XG$top_state$0$0	= .
	.globl	XG$top_state$0$0
	ret
	G$init_states$0$0	= .
	.globl	G$init_states$0$0
	C$states.c$47$1_0$81	= .
	.globl	C$states.c$47$1_0$81
;src/engine/states.c:47: void init_states(void) {
;	---------------------------------
; Function init_states
; ---------------------------------
_init_states::
	C$states.c$48$1_0$81	= .
	.globl	C$states.c$48$1_0$81
;src/engine/states.c:48: statemanager.current_state_index = -1;
	ld	hl, #(_statemanager + 2)
	ld	(hl), #0xff
	C$states.c$49$1_0$81	= .
	.globl	C$states.c$49$1_0$81
;src/engine/states.c:49: statemanager.state_stack_capacity = 10;
	ld	hl, #(_statemanager + 3)
	ld	(hl), #0x0a
	C$states.c$50$1_0$81	= .
	.globl	C$states.c$50$1_0$81
;src/engine/states.c:50: statemanager.state_stack = malloc(statemanager.state_stack_capacity * sizeof(State*));
	ld	hl, #0x0014
	call	_malloc
	ld	(_statemanager), de
	C$states.c$51$1_0$81	= .
	.globl	C$states.c$51$1_0$81
;src/engine/states.c:51: statemanager.current_target_state = -1;
	ld	hl, #(_statemanager + 4)
	ld	(hl), #0xff
	C$states.c$52$1_0$81	= .
	.globl	C$states.c$52$1_0$81
;src/engine/states.c:52: statemanager.state_array[DEFAULT_STATE].start = default_state_start;
	ld	hl, #_default_state_start
	ld	((_statemanager + 6)), hl
	C$states.c$53$1_0$81	= .
	.globl	C$states.c$53$1_0$81
;src/engine/states.c:53: statemanager.state_array[DEFAULT_STATE].update = default_state_update;
	ld	hl, #_default_state_update
	ld	((_statemanager + 8)), hl
	C$states.c$54$1_0$81	= .
	.globl	C$states.c$54$1_0$81
;src/engine/states.c:54: statemanager.state_array[DEFAULT_STATE].stop = default_state_stop;
	ld	hl, #_default_state_stop
	ld	((_statemanager + 10)), hl
	C$states.c$55$1_0$81	= .
	.globl	C$states.c$55$1_0$81
;src/engine/states.c:55: statemanager.state_array[LOGO_STATE].start = logo_state_start;
	ld	hl, #_logo_state_start
	ld	((_statemanager + 12)), hl
	C$states.c$56$1_0$81	= .
	.globl	C$states.c$56$1_0$81
;src/engine/states.c:56: statemanager.state_array[LOGO_STATE].update = logo_state_update;
	ld	hl, #_logo_state_update
	ld	((_statemanager + 14)), hl
	C$states.c$57$1_0$81	= .
	.globl	C$states.c$57$1_0$81
;src/engine/states.c:57: statemanager.state_array[LOGO_STATE].stop = logo_state_stop;
	ld	hl, #_logo_state_stop
	ld	((_statemanager + 16)), hl
	C$states.c$58$1_0$81	= .
	.globl	C$states.c$58$1_0$81
;src/engine/states.c:58: statemanager.state_array[INTRO_STATE].start = intro_state_start;
	ld	hl, #_intro_state_start
	ld	((_statemanager + 18)), hl
	C$states.c$59$1_0$81	= .
	.globl	C$states.c$59$1_0$81
;src/engine/states.c:59: statemanager.state_array[INTRO_STATE].update = intro_state_update;
	ld	hl, #_intro_state_update
	ld	((_statemanager + 20)), hl
	C$states.c$60$1_0$81	= .
	.globl	C$states.c$60$1_0$81
;src/engine/states.c:60: statemanager.state_array[INTRO_STATE].stop = intro_state_stop;
	ld	hl, #_intro_state_stop
	ld	((_statemanager + 22)), hl
	C$states.c$61$1_0$81	= .
	.globl	C$states.c$61$1_0$81
;src/engine/states.c:61: statemanager.state_array[TITLE_STATE].start = title_state_start;
	ld	hl, #_title_state_start
	ld	((_statemanager + 24)), hl
	C$states.c$62$1_0$81	= .
	.globl	C$states.c$62$1_0$81
;src/engine/states.c:62: statemanager.state_array[TITLE_STATE].update = title_state_update;
	ld	hl, #_title_state_update
	ld	((_statemanager + 26)), hl
	C$states.c$63$1_0$81	= .
	.globl	C$states.c$63$1_0$81
;src/engine/states.c:63: statemanager.state_array[TITLE_STATE].stop = title_state_stop;
	ld	hl, #_title_state_stop
	ld	((_statemanager + 28)), hl
	C$states.c$64$1_0$81	= .
	.globl	C$states.c$64$1_0$81
;src/engine/states.c:64: statemanager.state_array[SCORE_STATE].start = score_state_start;
	ld	hl, #_score_state_start
	ld	((_statemanager + 30)), hl
	C$states.c$65$1_0$81	= .
	.globl	C$states.c$65$1_0$81
;src/engine/states.c:65: statemanager.state_array[SCORE_STATE].update = score_state_update;
	ld	hl, #_score_state_update
	ld	((_statemanager + 32)), hl
	C$states.c$66$1_0$81	= .
	.globl	C$states.c$66$1_0$81
;src/engine/states.c:66: statemanager.state_array[SCORE_STATE].stop = score_state_stop;
	ld	hl, #_score_state_stop
	ld	((_statemanager + 34)), hl
	C$states.c$67$1_0$81	= .
	.globl	C$states.c$67$1_0$81
;src/engine/states.c:67: statemanager.state_array[CREDITS_STATE].start = credits_state_start;
	ld	hl, #_credits_state_start
	ld	((_statemanager + 36)), hl
	C$states.c$68$1_0$81	= .
	.globl	C$states.c$68$1_0$81
;src/engine/states.c:68: statemanager.state_array[CREDITS_STATE].update = credits_state_update;
	ld	hl, #_credits_state_update
	ld	((_statemanager + 38)), hl
	C$states.c$69$1_0$81	= .
	.globl	C$states.c$69$1_0$81
;src/engine/states.c:69: statemanager.state_array[CREDITS_STATE].stop = credits_state_stop;
	ld	hl, #_credits_state_stop
	ld	((_statemanager + 40)), hl
	C$states.c$70$1_0$81	= .
	.globl	C$states.c$70$1_0$81
;src/engine/states.c:70: statemanager.state_array[GAME_STATE].start = game_state_start;
	ld	hl, #_game_state_start
	ld	((_statemanager + 42)), hl
	C$states.c$71$1_0$81	= .
	.globl	C$states.c$71$1_0$81
;src/engine/states.c:71: statemanager.state_array[GAME_STATE].update = game_state_update;
	ld	hl, #_game_state_update
	ld	((_statemanager + 44)), hl
	C$states.c$72$1_0$81	= .
	.globl	C$states.c$72$1_0$81
;src/engine/states.c:72: statemanager.state_array[GAME_STATE].stop = game_state_stop;
	ld	hl, #_game_state_stop
	ld	((_statemanager + 46)), hl
	C$states.c$73$1_0$81	= .
	.globl	C$states.c$73$1_0$81
;src/engine/states.c:73: statemanager.state_array[DEMO_STATE].start = demo_state_start;
	ld	hl, #_demo_state_start
	ld	((_statemanager + 48)), hl
	C$states.c$74$1_0$81	= .
	.globl	C$states.c$74$1_0$81
;src/engine/states.c:74: statemanager.state_array[DEMO_STATE].update = demo_state_update;
	ld	hl, #_demo_state_update
	ld	((_statemanager + 50)), hl
	C$states.c$75$1_0$81	= .
	.globl	C$states.c$75$1_0$81
;src/engine/states.c:75: statemanager.state_array[DEMO_STATE].stop = demo_state_stop;
	ld	hl, #_demo_state_stop
	ld	((_statemanager + 52)), hl
	C$states.c$76$1_0$81	= .
	.globl	C$states.c$76$1_0$81
;src/engine/states.c:76: statemanager.state_array[ENTER_SCORE_STATE].start = enter_score_state_start;
	ld	hl, #_enter_score_state_start
	ld	((_statemanager + 54)), hl
	C$states.c$77$1_0$81	= .
	.globl	C$states.c$77$1_0$81
;src/engine/states.c:77: statemanager.state_array[ENTER_SCORE_STATE].update = enter_score_state_update;
	ld	hl, #_enter_score_state_update
	ld	((_statemanager + 56)), hl
	C$states.c$78$1_0$81	= .
	.globl	C$states.c$78$1_0$81
;src/engine/states.c:78: statemanager.state_array[ENTER_SCORE_STATE].stop = enter_score_state_stop;
	ld	hl, #_enter_score_state_stop
	ld	((_statemanager + 58)), hl
	C$states.c$79$1_0$81	= .
	.globl	C$states.c$79$1_0$81
;src/engine/states.c:79: statemanager.state_array[END_GAME_STATE].start = end_game_state_start;
	ld	hl, #_end_game_state_start
	ld	((_statemanager + 60)), hl
	C$states.c$80$1_0$81	= .
	.globl	C$states.c$80$1_0$81
;src/engine/states.c:80: statemanager.state_array[END_GAME_STATE].update = end_game_state_update;
	ld	hl, #_end_game_state_update
	ld	((_statemanager + 62)), hl
	C$states.c$81$1_0$81	= .
	.globl	C$states.c$81$1_0$81
;src/engine/states.c:81: statemanager.state_array[END_GAME_STATE].stop = end_game_state_stop;
	ld	hl, #_end_game_state_stop
	ld	((_statemanager + 64)), hl
	C$states.c$82$1_0$81	= .
	.globl	C$states.c$82$1_0$81
;src/engine/states.c:82: push_state(statemanager.state_array);
	ld	hl, #(_statemanager + 6)
	C$states.c$83$1_0$81	= .
	.globl	C$states.c$83$1_0$81
;src/engine/states.c:83: }
	C$states.c$83$1_0$81	= .
	.globl	C$states.c$83$1_0$81
	XG$init_states$0$0	= .
	.globl	XG$init_states$0$0
	jp	_push_state
	G$clear_states$0$0	= .
	.globl	G$clear_states$0$0
	C$states.c$85$1_0$83	= .
	.globl	C$states.c$85$1_0$83
;src/engine/states.c:85: void clear_states(void) {
;	---------------------------------
; Function clear_states
; ---------------------------------
_clear_states::
	C$states.c$86$1_0$83	= .
	.globl	C$states.c$86$1_0$83
;src/engine/states.c:86: do {
00101$:
	C$states.c$87$2_0$84	= .
	.globl	C$states.c$87$2_0$84
;src/engine/states.c:87: pop_state();
	call	_pop_state
	C$states.c$88$1_0$83	= .
	.globl	C$states.c$88$1_0$83
;src/engine/states.c:88: } while(statemanager.current_state_index > -1);
	ld	hl, #_statemanager + 2
	ld	c, (hl)
	ld	a, #0xff
	sub	a, c
	jp	PO, 00116$
	xor	a, #0x80
00116$:
	jp	M, 00101$
	C$states.c$89$1_0$83	= .
	.globl	C$states.c$89$1_0$83
;src/engine/states.c:89: free(statemanager.state_stack);
	ld	hl, (#_statemanager + 0)
	C$states.c$90$1_0$83	= .
	.globl	C$states.c$90$1_0$83
;src/engine/states.c:90: }
	C$states.c$90$1_0$83	= .
	.globl	C$states.c$90$1_0$83
	XG$clear_states$0$0	= .
	.globl	XG$clear_states$0$0
	jp	_free
	G$update_state$0$0	= .
	.globl	G$update_state$0$0
	C$states.c$92$1_0$86	= .
	.globl	C$states.c$92$1_0$86
;src/engine/states.c:92: void update_state(void) {
;	---------------------------------
; Function update_state
; ---------------------------------
_update_state::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$states.c$94$1_0$86	= .
	.globl	C$states.c$94$1_0$86
;src/engine/states.c:94: if(statemanager.current_state_index != -1) {
	ld	hl, #_statemanager + 2
	ld	b, (hl)
	C$states.c$95$1_0$86	= .
	.globl	C$states.c$95$1_0$86
;src/engine/states.c:95: if(statemanager.current_target_state != -1) {
	ld	hl, #(_statemanager + 4)
	ld	c, (hl)
	C$states.c$99$1_0$86	= .
	.globl	C$states.c$99$1_0$86
;src/engine/states.c:99: push_state(statemanager.state_array + statemanager.current_target_state);
	C$states.c$95$1_0$86	= .
	.globl	C$states.c$95$1_0$86
;src/engine/states.c:95: if(statemanager.current_target_state != -1) {
	ld	a, c
	inc	a
	ld	a, #0x01
	jr	Z, 00141$
	xor	a, a
00141$:
	ld	-1 (ix), a
	C$states.c$94$1_0$86	= .
	.globl	C$states.c$94$1_0$86
;src/engine/states.c:94: if(statemanager.current_state_index != -1) {
	inc	b
	jr	Z, 00111$
	C$states.c$95$2_0$87	= .
	.globl	C$states.c$95$2_0$87
;src/engine/states.c:95: if(statemanager.current_target_state != -1) {
	bit	0, -1 (ix)
	jr	NZ, 00106$
	C$states.c$96$3_0$88	= .
	.globl	C$states.c$96$3_0$88
;src/engine/states.c:96: if(!statemanager.preserve_context) {
	ld	hl, #_statemanager + 5
	bit	0, (hl)
	jr	NZ, 00102$
	C$states.c$97$4_0$89	= .
	.globl	C$states.c$97$4_0$89
;src/engine/states.c:97: pop_state();
	call	_pop_state
00102$:
	C$states.c$99$3_0$88	= .
	.globl	C$states.c$99$3_0$88
;src/engine/states.c:99: push_state(statemanager.state_array + statemanager.current_target_state);
	ld	a, (#(_statemanager + 4) + 0)
	ld	c, a
	rlca
	sbc	a, a
	ld	b, a
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, bc
	add	hl, hl
	ld	de, #(_statemanager + 6)
	add	hl, de
	call	_push_state
	jr	00113$
00106$:
	C$states.c$101$3_0$90	= .
	.globl	C$states.c$101$3_0$90
;src/engine/states.c:101: topState = top_state();
	call	_top_state
	ex	de, hl
	C$states.c$102$3_0$90	= .
	.globl	C$states.c$102$3_0$90
;src/engine/states.c:102: if(topState->update != NULL) {
	inc	hl
	inc	hl
	ld	c, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	a, h
	or	a, c
	jr	Z, 00113$
	C$states.c$103$4_0$91	= .
	.globl	C$states.c$103$4_0$91
;src/engine/states.c:103: topState->update();
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	call	___sdcc_call_hl
	jr	00113$
00111$:
	C$states.c$107$2_0$92	= .
	.globl	C$states.c$107$2_0$92
;src/engine/states.c:107: if(statemanager.current_target_state != -1) {
	bit	0, -1 (ix)
	jr	NZ, 00113$
	C$states.c$108$3_0$93	= .
	.globl	C$states.c$108$3_0$93
;src/engine/states.c:108: push_state(statemanager.state_array + statemanager.current_target_state);
	ld	a, c
	rlca
	sbc	a, a
	ld	b, a
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, bc
	add	hl, hl
	ld	de, #(_statemanager + 6)
	add	hl, de
	call	_push_state
00113$:
	C$states.c$111$1_0$86	= .
	.globl	C$states.c$111$1_0$86
;src/engine/states.c:111: }
	inc	sp
	pop	ix
	C$states.c$111$1_0$86	= .
	.globl	C$states.c$111$1_0$86
	XG$update_state$0$0	= .
	.globl	XG$update_state$0$0
	ret
	G$transition_to_state$0$0	= .
	.globl	G$transition_to_state$0$0
	C$states.c$113$1_0$95	= .
	.globl	C$states.c$113$1_0$95
;src/engine/states.c:113: void transition_to_state(s8 target_state) {
;	---------------------------------
; Function transition_to_state
; ---------------------------------
_transition_to_state::
	ld	c, a
	C$states.c$114$1_0$95	= .
	.globl	C$states.c$114$1_0$95
;src/engine/states.c:114: statemanager.current_target_state = target_state;
	ld	hl, #(_statemanager + 4)
	ld	(hl), c
	C$states.c$115$1_0$95	= .
	.globl	C$states.c$115$1_0$95
;src/engine/states.c:115: statemanager.preserve_context = false;
	ld	hl, #(_statemanager + 5)
	ld	(hl), #0x00
	C$states.c$116$1_0$95	= .
	.globl	C$states.c$116$1_0$95
;src/engine/states.c:116: }
	C$states.c$116$1_0$95	= .
	.globl	C$states.c$116$1_0$95
	XG$transition_to_state$0$0	= .
	.globl	XG$transition_to_state$0$0
	ret
	G$move_to_sub_state$0$0	= .
	.globl	G$move_to_sub_state$0$0
	C$states.c$118$1_0$97	= .
	.globl	C$states.c$118$1_0$97
;src/engine/states.c:118: void move_to_sub_state(s8 target_state) {
;	---------------------------------
; Function move_to_sub_state
; ---------------------------------
_move_to_sub_state::
	ld	c, a
	C$states.c$119$1_0$97	= .
	.globl	C$states.c$119$1_0$97
;src/engine/states.c:119: statemanager.current_target_state = target_state;
	ld	hl, #(_statemanager + 4)
	ld	(hl), c
	C$states.c$120$1_0$97	= .
	.globl	C$states.c$120$1_0$97
;src/engine/states.c:120: statemanager.preserve_context = true;
	ld	hl, #(_statemanager + 5)
	ld	(hl), #0x01
	C$states.c$121$1_0$97	= .
	.globl	C$states.c$121$1_0$97
;src/engine/states.c:121: }
	C$states.c$121$1_0$97	= .
	.globl	C$states.c$121$1_0$97
	XG$move_to_sub_state$0$0	= .
	.globl	XG$move_to_sub_state$0$0
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
