@echo off
set PATH=%SDCC_SMS_PATH%\sdcc\bin;%PATH%
echo Copiar musicas NTSC
copy .\music-source\ntsc\credits.psg .\assets
copy .\music-source\ntsc\gameover.psg .\assets
copy .\music-source\ntsc\highscore.psg .\assets
copy .\music-source\ntsc\hurryup.psg .\assets
copy .\music-source\ntsc\ingame.psg .\assets
copy .\music-source\ntsc\intro.psg .\assets
copy .\music-source\ntsc\startjingle.psg .\assets
copy .\music-source\ntsc\titlescreen.psg .\assets
copy .\music-source\ntsc\fanfa1.psg .\assets
copy .\music-source\ntsc\fanfa2.psg .\assets
copy .\music-source\ntsc\move.psg .\assets
copy .\music-source\ntsc\select.psg .\assets
copy .\music-source\ntsc\start.psg .\assets
assets2banks assets --firstbank=3 --compile --singleheader
copy .\assets2banks.h .\src
set SYSTEM=NTSC_MACHINE
SET CURRENTDIR="%cd%"
SET DOCKER_FLAGS=--rm -v %CURRENTDIR%:/home/sms-tk/host -w /home/sms-tk/host -t retcon85/toolchain-sms -c
docker run  %DOCKER_FLAGS% "make build SYSTEM=NTSC_MACHINE"
copy gotris.sms gotris-ntsc.sms
java -jar %EMULICIOUS_PATH%\Emulicious.jar gotris-ntsc.sms