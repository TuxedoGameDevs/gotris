;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module game
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl ___SMS__SDSC_signature
	.globl ___SMS__SDSC_descr
	.globl ___SMS__SDSC_name
	.globl ___SMS__SDSC_author
	.globl ___SMS__SEGA_signature
	.globl _main
	.globl _update_state
	.globl _init_states
	.globl _init_game
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$main$0$0	= .
	.globl	G$main$0$0
	C$game.c$10$0_0$99	= .
	.globl	C$game.c$10$0_0$99
;src/game.c:10: void main(void) {
;	---------------------------------
; Function main
; ---------------------------------
_main::
	C$game.c$11$1_0$99	= .
	.globl	C$game.c$11$1_0$99
;src/game.c:11: while(true) {
00105$:
	C$game.c$12$2_0$100	= .
	.globl	C$game.c$12$2_0$100
;src/game.c:12: resetPressed = false;
	ld	hl, #_resetPressed
	ld	(hl), #0x00
	C$game.c$13$2_0$100	= .
	.globl	C$game.c$13$2_0$100
;src/game.c:13: init_game();
	call	_init_game
	C$game.c$14$2_0$100	= .
	.globl	C$game.c$14$2_0$100
;src/game.c:14: init_states();
	call	_init_states
	C$game.c$15$2_0$100	= .
	.globl	C$game.c$15$2_0$100
;src/game.c:15: while(!resetPressed) {
00101$:
	ld	hl, #_resetPressed
	bit	0, (hl)
	jr	NZ, 00105$
	C$game.c$16$3_0$101	= .
	.globl	C$game.c$16$3_0$101
;src/game.c:16: update_state();
	call	_update_state
	C$game.c$20$1_0$99	= .
	.globl	C$game.c$20$1_0$99
;src/game.c:20: }
	C$game.c$20$1_0$99	= .
	.globl	C$game.c$20$1_0$99
	XG$main$0$0	= .
	.globl	XG$main$0$0
	jr	00101$
	.area _CODE
Fgame$__str_0$0_0$0 == .
__str_0:
	.ascii "TuxedoGames"
	.db 0x00
Fgame$__str_1$0_0$0 == .
__str_1:
	.ascii "GOTRIS"
	.db 0x00
Fgame$__str_2$0_0$0 == .
__str_2:
	.ascii "Tetris meets GO - NTSC VERSION"
	.db 0x00
	.area _INITIALIZER
	.area _CABS (ABS)
	.org 0x7FF0
G$__SMS__SEGA_signature$0_0$0 == .
___SMS__SEGA_signature:
	.db #0x54	; 84	'T'
	.db #0x4d	; 77	'M'
	.db #0x52	; 82	'R'
	.db #0x20	; 32
	.db #0x53	; 83	'S'
	.db #0x45	; 69	'E'
	.db #0x47	; 71	'G'
	.db #0x41	; 65	'A'
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0x99	; 153
	.db #0x99	; 153
	.db #0x00	; 0
	.db #0x4c	; 76	'L'
	.org 0x7FD4
G$__SMS__SDSC_author$0_0$0 == .
___SMS__SDSC_author:
	.ascii "TuxedoGames"
	.db 0x00
	.org 0x7FCD
G$__SMS__SDSC_name$0_0$0 == .
___SMS__SDSC_name:
	.ascii "GOTRIS"
	.db 0x00
	.org 0x7FAE
G$__SMS__SDSC_descr$0_0$0 == .
___SMS__SDSC_descr:
	.ascii "Tetris meets GO - NTSC VERSION"
	.db 0x00
	.org 0x7FE0
G$__SMS__SDSC_signature$0_0$0 == .
___SMS__SDSC_signature:
	.db #0x53	; 83	'S'
	.db #0x44	; 68	'D'
	.db #0x53	; 83	'S'
	.db #0x43	; 67	'C'
	.db #0x01	; 1
	.db #0x03	; 3
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0xd4	; 212
	.db #0x7f	; 127
	.db #0xcd	; 205
	.db #0x7f	; 127
	.db #0xae	; 174
	.db #0x7f	; 127
