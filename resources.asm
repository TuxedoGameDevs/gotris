;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.3.0 #14184 (Linux)
;--------------------------------------------------------
	.module resources
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _destroyList
	.globl _deleteElementEndList
	.globl _createList
	.globl _setTileMapToHighPriority
	.globl _waitForFrame
	.globl _fadeToTargetSpritePalette
	.globl _fadeToTargetBGPalette
	.globl _createSepiaPaletteVersion
	.globl _setSpritePaletteColor
	.globl _getSpritePalette
	.globl _getBGPalette
	.globl _setSpritePalette
	.globl _setBGPalette
	.globl _SMS_VRAMmemcpy
	.globl _SMS_setLineCounter
	.globl _SMS_setLineInterruptHandler
	.globl _SMS_loadSpritePaletteHalfBrightness
	.globl _SMS_loadBGPaletteHalfBrightness
	.globl _SMS_setColor
	.globl _SMS_loadSpritePalette
	.globl _SMS_loadBGPalette
	.globl _SMS_addSprite_f
	.globl _SMS_initSprites
	.globl _SMS_loadSTMcompressedTileMapatAddr
	.globl _SMS_loadTileMapAreaatAddr
	.globl _SMS_loadPSGaidencompressedTilesatAddr
	.globl _SMS_crt0_RST18
	.globl _SMS_crt0_RST08
	.globl _SMS_setSpriteMode
	.globl _SMS_setBGScrollY
	.globl _SMS_setBGScrollX
	.globl _SMS_VDPturnOffFeature
	.globl _SMS_VDPturnOnFeature
	.globl _palettes
	.globl _tilemaps
	.globl _tiles
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _palettes_bank
	.globl _palettes_size
	.globl _assets_bank
	.globl _assets_size
	.globl _turn_black
	.globl _background_fade_out
	.globl _sprite_fade_out
	.globl _general_fade_out
	.globl _background_fade_in_to_sepia
	.globl _background_fade_in
	.globl _sprite_fade_in
	.globl _load_tileset
	.globl _load_tileset_batches
	.globl _load_tilemap
	.globl _draw_sram_present
	.globl _change_title_priority
	.globl _game_fade_in
	.globl _init_scroll_title_screen
	.globl _enable_scroll_title_screen
	.globl _scroll_title_screen
	.globl _titleScrollHandler
	.globl _write_push_start
	.globl _clear_push_start
	.globl _clear_scroll_title_screen
	.globl _load_highscore_assets
	.globl _highscore_fade_in
	.globl _draw_highscore_row
	.globl _draw_highscore_table
	.globl _init_credits
	.globl _write_credits
	.globl _rasterize_credits
	.globl _creditsScrollHandler1
	.globl _creditsScrollHandler2
	.globl _disable_credits_effects
	.globl _load_ending1_assets
	.globl _load_ending2_assets
	.globl _ending_sprites
	.globl _scroll_ending
	.globl _endingScrollFinish
	.globl _ending_update_face
	.globl _load_game_assets
	.globl _game_half_palette
	.globl _game_full_palette
	.globl _draw_game_background
	.globl _initialize_game_lists
	.globl _clear_game_lists
	.globl _draw_game_screen
	.globl _draw_congratulations
	.globl _draw_point_sprites
	.globl _clear_point_sprites
	.globl _draw_cursor_sprites
	.globl _clear_cursor_sprites
	.globl _connected_pieces_to_bw_palette
	.globl _connected_pieces_to_normal_palette
	.globl _not_connected_pieces_to_red_palette
	.globl _not_connected_pieces_to_normal_palette
	.globl _swap_fire_colors
	.globl _restore_fire_colors
	.globl _background_to_level_palette
	.globl _create_dialog_box
	.globl _update_dialog_box
	.globl _load_tiles_in_batches
	.globl _print_score_value
	.globl _print_level_number
	.globl _print_string
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$tiles$0_0$0==.
_tiles::
	.ds 18
G$tilemaps$0_0$0==.
_tilemaps::
	.ds 18
G$palettes$0_0$0==.
_palettes::
	.ds 26
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$turn_black$0$0	= .
	.globl	G$turn_black$0$0
	C$resources.c$113$0_0$194	= .
	.globl	C$resources.c$113$0_0$194
;src/resources/resources.c:113: void turn_black(void) {
;	---------------------------------
; Function turn_black
; ---------------------------------
_turn_black::
	C$resources.c$114$1_0$194	= .
	.globl	C$resources.c$114$1_0$194
;src/resources/resources.c:114: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$115$1_0$194	= .
	.globl	C$resources.c$115$1_0$194
;src/resources/resources.c:115: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$116$1_0$194	= .
	.globl	C$resources.c$116$1_0$194
;src/resources/resources.c:116: setBGPalette(black_palette, 16);
	ld	a, #0x10
	push	af
	inc	sp
	ld	hl, #_black_palette
	call	_setBGPalette
	C$resources.c$117$1_0$194	= .
	.globl	C$resources.c$117$1_0$194
;src/resources/resources.c:117: setSpritePalette(black_palette, 16);
	ld	a, #0x10
	push	af
	inc	sp
	ld	hl, #_black_palette
	call	_setSpritePalette
	C$resources.c$118$1_0$194	= .
	.globl	C$resources.c$118$1_0$194
;src/resources/resources.c:118: }
	C$resources.c$118$1_0$194	= .
	.globl	C$resources.c$118$1_0$194
	XG$turn_black$0$0	= .
	.globl	XG$turn_black$0$0
	ret
G$assets_size$0_0$0 == .
_assets_size:
	.dw #0x064d
	.dw #0x0755
	.dw #0x023a
	.dw #0x1d91
	.dw #0x2680
	.dw #0x26a0
	.dw #0x1c7c
	.dw #0x1292
	.dw #0x0b94
G$assets_bank$0_0$0 == .
_assets_bank:
	.db #0x0b	; 11
	.db #0x03	; 3
	.db #0x08	; 8
	.db #0x09	; 9
	.db #0x0c	; 12
	.db #0x0d	; 13
	.db #0x0b	; 11
	.db #0x10	; 16
	.db #0x03	; 3
G$palettes_size$0_0$0 == .
_palettes_size:
	.db #0x10	; 16
	.db #0x0d	; 13
	.db #0x06	; 6
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x0e	; 14
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x0f	; 15
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
G$palettes_bank$0_0$0 == .
_palettes_bank:
	.db #0x02	; 2
	.db #0x0b	; 11
	.db #0x03	; 3
	.db #0x09	; 9
	.db #0x0c	; 12
	.db #0x0d	; 13
	.db #0x02	; 2
	.db #0x0b	; 11
	.db #0x02	; 2
	.db #0x10	; 16
	.db #0x02	; 2
	.db #0x03	; 3
	.db #0x03	; 3
	G$background_fade_out$0$0	= .
	.globl	G$background_fade_out$0$0
	C$resources.c$121$1_0$196	= .
	.globl	C$resources.c$121$1_0$196
;src/resources/resources.c:121: void background_fade_out(void) {
;	---------------------------------
; Function background_fade_out
; ---------------------------------
_background_fade_out::
	C$resources.c$122$1_0$196	= .
	.globl	C$resources.c$122$1_0$196
;src/resources/resources.c:122: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$123$1_0$196	= .
	.globl	C$resources.c$123$1_0$196
;src/resources/resources.c:123: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$124$1_0$196	= .
	.globl	C$resources.c$124$1_0$196
;src/resources/resources.c:124: fadeToTargetBGPalette(black_palette, 16,2);
	ld	hl, #0x210
	push	hl
	ld	hl, #_black_palette
	call	_fadeToTargetBGPalette
	C$resources.c$125$1_0$196	= .
	.globl	C$resources.c$125$1_0$196
;src/resources/resources.c:125: }
	C$resources.c$125$1_0$196	= .
	.globl	C$resources.c$125$1_0$196
	XG$background_fade_out$0$0	= .
	.globl	XG$background_fade_out$0$0
	ret
	G$sprite_fade_out$0$0	= .
	.globl	G$sprite_fade_out$0$0
	C$resources.c$127$1_0$198	= .
	.globl	C$resources.c$127$1_0$198
;src/resources/resources.c:127: void sprite_fade_out(void) {
;	---------------------------------
; Function sprite_fade_out
; ---------------------------------
_sprite_fade_out::
	C$resources.c$128$1_0$198	= .
	.globl	C$resources.c$128$1_0$198
;src/resources/resources.c:128: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$129$1_0$198	= .
	.globl	C$resources.c$129$1_0$198
;src/resources/resources.c:129: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$130$1_0$198	= .
	.globl	C$resources.c$130$1_0$198
;src/resources/resources.c:130: fadeToTargetSpritePalette(black_palette, 16,2);
	ld	hl, #0x210
	push	hl
	ld	hl, #_black_palette
	call	_fadeToTargetSpritePalette
	C$resources.c$131$1_0$198	= .
	.globl	C$resources.c$131$1_0$198
;src/resources/resources.c:131: }
	C$resources.c$131$1_0$198	= .
	.globl	C$resources.c$131$1_0$198
	XG$sprite_fade_out$0$0	= .
	.globl	XG$sprite_fade_out$0$0
	ret
	G$general_fade_out$0$0	= .
	.globl	G$general_fade_out$0$0
	C$resources.c$133$1_0$200	= .
	.globl	C$resources.c$133$1_0$200
;src/resources/resources.c:133: void general_fade_out(void) {
;	---------------------------------
; Function general_fade_out
; ---------------------------------
_general_fade_out::
	C$resources.c$134$1_0$200	= .
	.globl	C$resources.c$134$1_0$200
;src/resources/resources.c:134: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$135$1_0$200	= .
	.globl	C$resources.c$135$1_0$200
;src/resources/resources.c:135: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$136$1_0$200	= .
	.globl	C$resources.c$136$1_0$200
;src/resources/resources.c:136: fadeToTargetBGPalette(black_palette, 16,2);
	ld	hl, #0x210
	push	hl
	ld	hl, #_black_palette
	call	_fadeToTargetBGPalette
	C$resources.c$137$1_0$200	= .
	.globl	C$resources.c$137$1_0$200
;src/resources/resources.c:137: fadeToTargetSpritePalette(black_palette, 16,2);
	ld	hl, #0x210
	push	hl
	ld	hl, #_black_palette
	call	_fadeToTargetSpritePalette
	C$resources.c$138$1_0$200	= .
	.globl	C$resources.c$138$1_0$200
;src/resources/resources.c:138: }
	C$resources.c$138$1_0$200	= .
	.globl	C$resources.c$138$1_0$200
	XG$general_fade_out$0$0	= .
	.globl	XG$general_fade_out$0$0
	ret
	G$background_fade_in_to_sepia$0$0	= .
	.globl	G$background_fade_in_to_sepia$0$0
	C$resources.c$140$1_0$202	= .
	.globl	C$resources.c$140$1_0$202
;src/resources/resources.c:140: void background_fade_in_to_sepia(u8 palette_num, u8 framesperstep) {
;	---------------------------------
; Function background_fade_in_to_sepia
; ---------------------------------
_background_fade_in_to_sepia::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-17
	add	iy, sp
	ld	sp, iy
	ld	e, a
	ld	-1 (ix), l
	C$resources.c$142$1_0$202	= .
	.globl	C$resources.c$142$1_0$202
;src/resources/resources.c:142: current_resource_bank = palettes_bank[palette_num];
	ld	hl, #_palettes_bank
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	ld	(_current_resource_bank+0), a
	C$resources.c$143$1_0$202	= .
	.globl	C$resources.c$143$1_0$202
;src/resources/resources.c:143: SMS_mapROMBank(palettes_bank[palette_num]);
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$resources.c$144$1_0$202	= .
	.globl	C$resources.c$144$1_0$202
;src/resources/resources.c:144: createSepiaPaletteVersion(palettes[palette_num], bw_palette, palettes_size[palette_num]);
	ld	a, e
	add	a, #<(_palettes_size)
	ld	c, a
	ld	a, #0x00
	adc	a, #>(_palettes_size)
	ld	b, a
	ld	a, (bc)
	ld	d, a
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	a, #<(_palettes)
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_palettes)
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	push	de
	inc	sp
	ex	de, hl
	ld	hl, #3
	add	hl, sp
	ex	de, hl
	call	_createSepiaPaletteVersion
	pop	bc
	C$resources.c$145$1_0$202	= .
	.globl	C$resources.c$145$1_0$202
;src/resources/resources.c:145: fadeToTargetBGPalette(bw_palette,  palettes_size[palette_num], framesperstep);
	ld	a, (bc)
	ld	h, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	push	hl
	inc	sp
	push	af
	inc	sp
	ld	hl, #2
	add	hl, sp
	call	_fadeToTargetBGPalette
	C$resources.c$146$1_0$202	= .
	.globl	C$resources.c$146$1_0$202
;src/resources/resources.c:146: }
	ld	sp, ix
	pop	ix
	C$resources.c$146$1_0$202	= .
	.globl	C$resources.c$146$1_0$202
	XG$background_fade_in_to_sepia$0$0	= .
	.globl	XG$background_fade_in_to_sepia$0$0
	ret
	G$background_fade_in$0$0	= .
	.globl	G$background_fade_in$0$0
	C$resources.c$148$1_0$204	= .
	.globl	C$resources.c$148$1_0$204
;src/resources/resources.c:148: void background_fade_in(u8 palette_num, u8 framesperstep) {
;	---------------------------------
; Function background_fade_in
; ---------------------------------
_background_fade_in::
	ld	c, a
	ld	d, l
	C$resources.c$149$1_0$204	= .
	.globl	C$resources.c$149$1_0$204
;src/resources/resources.c:149: current_resource_bank = palettes_bank[palette_num];
	ld	hl, #_palettes_bank+0
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	ld	(_current_resource_bank+0), a
	C$resources.c$150$1_0$204	= .
	.globl	C$resources.c$150$1_0$204
;src/resources/resources.c:150: SMS_mapROMBank(palettes_bank[palette_num]);
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$resources.c$151$1_0$204	= .
	.globl	C$resources.c$151$1_0$204
;src/resources/resources.c:151: fadeToTargetBGPalette(palettes[palette_num], palettes_size[palette_num],framesperstep);
	ld	hl, #_palettes_size+0
	ld	b, #0x00
	add	hl, bc
	ld	e, (hl)
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	bc, #_palettes
	add	hl, bc
	ld	c, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	push	de
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	call	_fadeToTargetBGPalette
	C$resources.c$152$1_0$204	= .
	.globl	C$resources.c$152$1_0$204
;src/resources/resources.c:152: }
	C$resources.c$152$1_0$204	= .
	.globl	C$resources.c$152$1_0$204
	XG$background_fade_in$0$0	= .
	.globl	XG$background_fade_in$0$0
	ret
	G$sprite_fade_in$0$0	= .
	.globl	G$sprite_fade_in$0$0
	C$resources.c$154$1_0$206	= .
	.globl	C$resources.c$154$1_0$206
;src/resources/resources.c:154: void sprite_fade_in(u8 palette_num, u8 framesperstep) {
;	---------------------------------
; Function sprite_fade_in
; ---------------------------------
_sprite_fade_in::
	ld	c, a
	ld	d, l
	C$resources.c$155$1_0$206	= .
	.globl	C$resources.c$155$1_0$206
;src/resources/resources.c:155: current_resource_bank = palettes_bank[palette_num];
	ld	hl, #_palettes_bank+0
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	ld	(_current_resource_bank+0), a
	C$resources.c$156$1_0$206	= .
	.globl	C$resources.c$156$1_0$206
;src/resources/resources.c:156: SMS_mapROMBank(palettes_bank[palette_num]);
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$resources.c$157$1_0$206	= .
	.globl	C$resources.c$157$1_0$206
;src/resources/resources.c:157: fadeToTargetSpritePalette(palettes[palette_num], palettes_size[palette_num],framesperstep);
	ld	hl, #_palettes_size+0
	ld	b, #0x00
	add	hl, bc
	ld	e, (hl)
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	bc, #_palettes
	add	hl, bc
	ld	c, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	push	de
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	call	_fadeToTargetSpritePalette
	C$resources.c$158$1_0$206	= .
	.globl	C$resources.c$158$1_0$206
;src/resources/resources.c:158: }
	C$resources.c$158$1_0$206	= .
	.globl	C$resources.c$158$1_0$206
	XG$sprite_fade_in$0$0	= .
	.globl	XG$sprite_fade_in$0$0
	ret
	G$load_tileset$0$0	= .
	.globl	G$load_tileset$0$0
	C$resources.c$160$1_0$208	= .
	.globl	C$resources.c$160$1_0$208
;src/resources/resources.c:160: void load_tileset(u8 asset_num, u8 base_address) {
;	---------------------------------
; Function load_tileset
; ---------------------------------
_load_tileset::
	ld	c, a
	ld	e, l
	C$resources.c$161$1_0$208	= .
	.globl	C$resources.c$161$1_0$208
;src/resources/resources.c:161: SMS_mapROMBank(assets_bank[asset_num]);
	ld	hl, #_assets_bank+0
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$resources.c$162$1_0$208	= .
	.globl	C$resources.c$162$1_0$208
;src/resources/resources.c:162: current_resource_bank = assets_bank[asset_num];
	ld	(_current_resource_bank+0), a
	C$resources.c$163$1_0$208	= .
	.globl	C$resources.c$163$1_0$208
;src/resources/resources.c:163: SMS_loadPSGaidencompressedTiles(tiles[asset_num], base_address);
	ld	d, #0x00
	ex	de, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	set	6, d
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	bc, #_tiles
	add	hl, bc
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$164$1_0$208	= .
	.globl	C$resources.c$164$1_0$208
;src/resources/resources.c:164: }
	C$resources.c$164$1_0$208	= .
	.globl	C$resources.c$164$1_0$208
	XG$load_tileset$0$0	= .
	.globl	XG$load_tileset$0$0
	jp	_SMS_loadPSGaidencompressedTilesatAddr
	G$load_tileset_batches$0$0	= .
	.globl	G$load_tileset_batches$0$0
	C$resources.c$166$1_0$210	= .
	.globl	C$resources.c$166$1_0$210
;src/resources/resources.c:166: void load_tileset_batches(u8 asset_num, u8 base_address) {
;	---------------------------------
; Function load_tileset_batches
; ---------------------------------
_load_tileset_batches::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	ld	c, a
	ld	e, l
	C$resources.c$167$1_0$210	= .
	.globl	C$resources.c$167$1_0$210
;src/resources/resources.c:167: SMS_mapROMBank(assets_bank[asset_num]);
	ld	hl, #_assets_bank+0
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$resources.c$168$1_0$210	= .
	.globl	C$resources.c$168$1_0$210
;src/resources/resources.c:168: current_resource_bank = assets_bank[asset_num];
	ld	(_current_resource_bank+0), a
	C$resources.c$169$1_0$210	= .
	.globl	C$resources.c$169$1_0$210
;src/resources/resources.c:169: load_tiles_in_batches(tiles[asset_num],base_address,assets_size[asset_num]);
	ld	hl, #_assets_size+0
	ld	b, #0x00
	sla	c
	rl	b
	add	hl, bc
	ld	a, (hl)
	ld	-2 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-1 (ix), a
	ld	d, #0x00
	ld	hl, #_tiles+0
	add	hl, bc
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	pop	bc
	push	bc
	push	bc
	call	_load_tiles_in_batches
	C$resources.c$170$1_0$210	= .
	.globl	C$resources.c$170$1_0$210
;src/resources/resources.c:170: }
	ld	sp, ix
	pop	ix
	C$resources.c$170$1_0$210	= .
	.globl	C$resources.c$170$1_0$210
	XG$load_tileset_batches$0$0	= .
	.globl	XG$load_tileset_batches$0$0
	ret
	G$load_tilemap$0$0	= .
	.globl	G$load_tilemap$0$0
	C$resources.c$172$1_0$212	= .
	.globl	C$resources.c$172$1_0$212
;src/resources/resources.c:172: void load_tilemap(u8 asset_num) {
;	---------------------------------
; Function load_tilemap
; ---------------------------------
_load_tilemap::
	ld	e, a
	C$resources.c$173$1_0$212	= .
	.globl	C$resources.c$173$1_0$212
;src/resources/resources.c:173: SMS_mapROMBank(assets_bank[asset_num]);
	ld	hl, #_assets_bank
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	ld	(_ROM_bank_to_be_mapped_on_slot2+0), a
	C$resources.c$174$1_0$212	= .
	.globl	C$resources.c$174$1_0$212
;src/resources/resources.c:174: current_resource_bank = assets_bank[asset_num];
	ld	(_current_resource_bank+0), a
	C$resources.c$175$1_0$212	= .
	.globl	C$resources.c$175$1_0$212
;src/resources/resources.c:175: SMS_loadSTMcompressedTileMap(0, 0, tilemaps[asset_num]);
	ld	bc, #_tilemaps+0
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, bc
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	ld	hl, #0x7800
	C$resources.c$176$1_0$212	= .
	.globl	C$resources.c$176$1_0$212
;src/resources/resources.c:176: }
	C$resources.c$176$1_0$212	= .
	.globl	C$resources.c$176$1_0$212
	XG$load_tilemap$0$0	= .
	.globl	XG$load_tilemap$0$0
	jp	_SMS_loadSTMcompressedTileMapatAddr
	G$draw_sram_present$0$0	= .
	.globl	G$draw_sram_present$0$0
	C$resources.c$178$1_0$214	= .
	.globl	C$resources.c$178$1_0$214
;src/resources/resources.c:178: void draw_sram_present(void) {
;	---------------------------------
; Function draw_sram_present
; ---------------------------------
_draw_sram_present::
	C$resources.c$179$1_0$214	= .
	.globl	C$resources.c$179$1_0$214
;src/resources/resources.c:179: SMS_mapROMBank(simplefont_psgcompr_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x08
	C$resources.c$180$1_0$214	= .
	.globl	C$resources.c$180$1_0$214
;src/resources/resources.c:180: current_resource_bank = simplefont_psgcompr_bank;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x08
	C$resources.c$181$1_0$214	= .
	.globl	C$resources.c$181$1_0$214
;src/resources/resources.c:181: SMS_loadPSGaidencompressedTiles(simplefont_psgcompr,DIGITS_BASE_ADDRESS);
	ld	de, #0x4300
	ld	hl, #_simplefont_psgcompr
	call	_SMS_loadPSGaidencompressedTilesatAddr
	C$resources.c$182$1_0$214	= .
	.globl	C$resources.c$182$1_0$214
;src/resources/resources.c:182: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$183$1_0$214	= .
	.globl	C$resources.c$183$1_0$214
;src/resources/resources.c:183: setSpritePalette(default_pieces_palette,16);
	ld	a, #0x10
	push	af
	inc	sp
	ld	hl, #_default_pieces_palette
	call	_setSpritePalette
	C$resources.c$184$1_0$214	= .
	.globl	C$resources.c$184$1_0$214
;src/resources/resources.c:184: if(currentSaveSystem == SRAM_SAVE) {
	ld	a, (_currentSaveSystem+0)
	dec	a
	jr	NZ, 00104$
	C$resources.c$185$2_0$215	= .
	.globl	C$resources.c$185$2_0$215
;src/resources/resources.c:185: print_string(10,10,"SRAM]PRESENT^");
	ld	hl, #___str_0
	push	hl
;	spillPairReg hl
;	spillPairReg hl
	ld	a,#0x0a
	ld	l,a
	call	_print_string
	ret
00104$:
	C$resources.c$186$1_0$214	= .
	.globl	C$resources.c$186$1_0$214
;src/resources/resources.c:186: } else if (currentSaveSystem == FLASH_SAVE) {
	ld	a, (_currentSaveSystem+0)
	sub	a, #0x02
	ret	NZ
	C$resources.c$187$2_0$216	= .
	.globl	C$resources.c$187$2_0$216
;src/resources/resources.c:187: print_string(10,10,"FLASH]PRESENT^");
	ld	hl, #___str_1
	push	hl
;	spillPairReg hl
;	spillPairReg hl
	ld	a,#0x0a
	ld	l,a
	call	_print_string
	C$resources.c$189$1_0$214	= .
	.globl	C$resources.c$189$1_0$214
;src/resources/resources.c:189: }
	C$resources.c$189$1_0$214	= .
	.globl	C$resources.c$189$1_0$214
	XG$draw_sram_present$0$0	= .
	.globl	XG$draw_sram_present$0$0
	ret
Fresources$__str_0$0_0$0 == .
___str_0:
	.ascii "SRAM]PRESENT^"
	.db 0x00
Fresources$__str_1$0_0$0 == .
___str_1:
	.ascii "FLASH]PRESENT^"
	.db 0x00
	G$change_title_priority$0$0	= .
	.globl	G$change_title_priority$0$0
	C$resources.c$191$1_0$219	= .
	.globl	C$resources.c$191$1_0$219
;src/resources/resources.c:191: void change_title_priority(void) {
;	---------------------------------
; Function change_title_priority
; ---------------------------------
_change_title_priority::
	C$resources.c$193$2_0$219	= .
	.globl	C$resources.c$193$2_0$219
;src/resources/resources.c:193: for(j=0;j<4;j++) {
	ld	c, #0x00
	C$resources.c$194$2_0$219	= .
	.globl	C$resources.c$194$2_0$219
;src/resources/resources.c:194: for(i=0;i<32;i++) {
00109$:
	ld	b, #0x00
00103$:
	C$resources.c$195$5_0$222	= .
	.globl	C$resources.c$195$5_0$222
;src/resources/resources.c:195: setTileMapToHighPriority(i,j);
	push	bc
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_setTileMapToHighPriority
	pop	bc
	C$resources.c$194$4_0$221	= .
	.globl	C$resources.c$194$4_0$221
;src/resources/resources.c:194: for(i=0;i<32;i++) {
	inc	b
	ld	a, b
	sub	a, #0x20
	jr	C, 00103$
	C$resources.c$193$2_0$219	= .
	.globl	C$resources.c$193$2_0$219
;src/resources/resources.c:193: for(j=0;j<4;j++) {
	inc	c
	ld	a, c
	sub	a, #0x04
	jr	C, 00109$
	C$resources.c$198$2_0$219	= .
	.globl	C$resources.c$198$2_0$219
;src/resources/resources.c:198: }
	C$resources.c$198$2_0$219	= .
	.globl	C$resources.c$198$2_0$219
	XG$change_title_priority$0$0	= .
	.globl	XG$change_title_priority$0$0
	ret
	G$game_fade_in$0$0	= .
	.globl	G$game_fade_in$0$0
	C$resources.c$200$2_0$224	= .
	.globl	C$resources.c$200$2_0$224
;src/resources/resources.c:200: void game_fade_in(u8 startLevel) {
;	---------------------------------
; Function game_fade_in
; ---------------------------------
_game_fade_in::
	ld	c, a
	C$resources.c$201$1_0$224	= .
	.globl	C$resources.c$201$1_0$224
;src/resources/resources.c:201: SMS_mapROMBank(backgroundpalette_bin_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x08
	C$resources.c$202$1_0$224	= .
	.globl	C$resources.c$202$1_0$224
;src/resources/resources.c:202: current_resource_bank = backgroundpalette_bin_bank;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x08
	C$resources.c$203$1_0$224	= .
	.globl	C$resources.c$203$1_0$224
;src/resources/resources.c:203: background_to_level_palette(startLevel);
	ld	a, c
	call	_background_to_level_palette
	C$resources.c$204$1_0$224	= .
	.globl	C$resources.c$204$1_0$224
;src/resources/resources.c:204: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$205$1_0$224	= .
	.globl	C$resources.c$205$1_0$224
;src/resources/resources.c:205: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$206$1_0$224	= .
	.globl	C$resources.c$206$1_0$224
;src/resources/resources.c:206: fadeToTargetSpritePalette(default_pieces_palette, 16,2);
	ld	hl, #0x210
	push	hl
	ld	hl, #_default_pieces_palette
	call	_fadeToTargetSpritePalette
	C$resources.c$207$1_0$224	= .
	.globl	C$resources.c$207$1_0$224
;src/resources/resources.c:207: }
	C$resources.c$207$1_0$224	= .
	.globl	C$resources.c$207$1_0$224
	XG$game_fade_in$0$0	= .
	.globl	XG$game_fade_in$0$0
	ret
	G$init_scroll_title_screen$0$0	= .
	.globl	G$init_scroll_title_screen$0$0
	C$resources.c$209$1_0$226	= .
	.globl	C$resources.c$209$1_0$226
;src/resources/resources.c:209: void init_scroll_title_screen(void) {
;	---------------------------------
; Function init_scroll_title_screen
; ---------------------------------
_init_scroll_title_screen::
	C$resources.c$210$1_0$226	= .
	.globl	C$resources.c$210$1_0$226
;src/resources/resources.c:210: scroll_x[0] = 0xFFFF;
	ld	hl, #0xffff
	ld	(_scroll_x), hl
	C$resources.c$211$1_0$226	= .
	.globl	C$resources.c$211$1_0$226
;src/resources/resources.c:211: scroll_x[1] = 0xFFFF;
	ld	((_scroll_x + 2)), hl
	C$resources.c$212$1_0$226	= .
	.globl	C$resources.c$212$1_0$226
;src/resources/resources.c:212: scroll_x[2] = 0xFFFF;
	ld	((_scroll_x + 4)), hl
	C$resources.c$213$1_0$226	= .
	.globl	C$resources.c$213$1_0$226
;src/resources/resources.c:213: scroll_x[3] = 0xFFFF;
	ld	((_scroll_x + 6)), hl
	C$resources.c$214$1_0$226	= .
	.globl	C$resources.c$214$1_0$226
;src/resources/resources.c:214: scroll_x[4] = 0xFFFF;
	ld	((_scroll_x + 8)), hl
	C$resources.c$215$1_0$226	= .
	.globl	C$resources.c$215$1_0$226
;src/resources/resources.c:215: scroll_x[5] = 0xFFFF;
	ld	((_scroll_x + 10)), hl
	C$resources.c$216$1_0$226	= .
	.globl	C$resources.c$216$1_0$226
;src/resources/resources.c:216: lineCnt = 0;
	ld	hl, #_lineCnt
	C$resources.c$217$1_0$226	= .
	.globl	C$resources.c$217$1_0$226
;src/resources/resources.c:217: SMS_setBGScrollX(0);
	ld	(hl), #0x00
	ld	l, (hl)
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setBGScrollX
	C$resources.c$218$1_0$226	= .
	.globl	C$resources.c$218$1_0$226
;src/resources/resources.c:218: SMS_setBGScrollY(0);    
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$219$1_0$226	= .
	.globl	C$resources.c$219$1_0$226
;src/resources/resources.c:219: }
	C$resources.c$219$1_0$226	= .
	.globl	C$resources.c$219$1_0$226
	XG$init_scroll_title_screen$0$0	= .
	.globl	XG$init_scroll_title_screen$0$0
	jp	_SMS_setBGScrollY
	G$enable_scroll_title_screen$0$0	= .
	.globl	G$enable_scroll_title_screen$0$0
	C$resources.c$221$1_0$228	= .
	.globl	C$resources.c$221$1_0$228
;src/resources/resources.c:221: void enable_scroll_title_screen(void) {
;	---------------------------------
; Function enable_scroll_title_screen
; ---------------------------------
_enable_scroll_title_screen::
	C$resources.c$223$1_0$228	= .
	.globl	C$resources.c$223$1_0$228
;src/resources/resources.c:223: SMS_setLineInterruptHandler(&titleScrollHandler);
	ld	hl, #_titleScrollHandler
	call	_SMS_setLineInterruptHandler
	C$resources.c$224$1_0$228	= .
	.globl	C$resources.c$224$1_0$228
;src/resources/resources.c:224: SMS_setLineCounter(31);    /* we're updating every 32 scanlines... */
	ld	l, #0x1f
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setLineCounter
	C$resources.c$225$1_0$228	= .
	.globl	C$resources.c$225$1_0$228
;src/resources/resources.c:225: SMS_enableLineInterrupt();
	ld	hl, #0x0010
	C$resources.c$226$1_0$228	= .
	.globl	C$resources.c$226$1_0$228
;src/resources/resources.c:226: }
	C$resources.c$226$1_0$228	= .
	.globl	C$resources.c$226$1_0$228
	XG$enable_scroll_title_screen$0$0	= .
	.globl	XG$enable_scroll_title_screen$0$0
	jp	_SMS_VDPturnOnFeature
	G$scroll_title_screen$0$0	= .
	.globl	G$scroll_title_screen$0$0
	C$resources.c$228$1_0$230	= .
	.globl	C$resources.c$228$1_0$230
;src/resources/resources.c:228: void scroll_title_screen(void) {
;	---------------------------------
; Function scroll_title_screen
; ---------------------------------
_scroll_title_screen::
	C$resources.c$229$1_0$230	= .
	.globl	C$resources.c$229$1_0$230
;src/resources/resources.c:229: scroll_x[0] = scroll_x[0] - 128; //0.5<<8
	ld	hl, (#_scroll_x + 0)
	ld	a, l
	add	a, #0x80
	ld	c, a
	ld	a, h
	adc	a, #0xff
	ld	b, a
	ld	(_scroll_x), bc
	C$resources.c$230$1_0$230	= .
	.globl	C$resources.c$230$1_0$230
;src/resources/resources.c:230: scroll_x[1] = scroll_x[1] - 160; //0.5<<8 + (0.125<<8)
	ld	hl, (#(_scroll_x + 2) + 0)
	ld	a, l
	add	a, #0x60
	ld	c, a
	ld	a, h
	adc	a, #0xff
	ld	b, a
	ld	((_scroll_x + 2)), bc
	C$resources.c$231$1_0$230	= .
	.globl	C$resources.c$231$1_0$230
;src/resources/resources.c:231: scroll_x[2] = scroll_x[2] - 160;
	ld	hl, (#(_scroll_x + 4) + 0)
	ld	a, l
	add	a, #0x60
	ld	c, a
	ld	a, h
	adc	a, #0xff
	ld	b, a
	ld	((_scroll_x + 4)), bc
	C$resources.c$232$1_0$230	= .
	.globl	C$resources.c$232$1_0$230
;src/resources/resources.c:232: scroll_x[3] = scroll_x[3] - 160;
	ld	hl, (#(_scroll_x + 6) + 0)
	ld	a, l
	add	a, #0x60
	ld	c, a
	ld	a, h
	adc	a, #0xff
	ld	b, a
	ld	((_scroll_x + 6)), bc
	C$resources.c$233$1_0$230	= .
	.globl	C$resources.c$233$1_0$230
;src/resources/resources.c:233: scroll_x[4] = scroll_x[4] - 160;
	ld	hl, (#(_scroll_x + 8) + 0)
	ld	a, l
	add	a, #0x60
	ld	c, a
	ld	a, h
	adc	a, #0xff
	ld	b, a
	ld	((_scroll_x + 8)), bc
	C$resources.c$236$1_0$230	= .
	.globl	C$resources.c$236$1_0$230
;src/resources/resources.c:236: lineCnt = 0;
	ld	hl, #_lineCnt
	ld	(hl), #0x00
	C$resources.c$237$1_0$230	= .
	.globl	C$resources.c$237$1_0$230
;src/resources/resources.c:237: }
	C$resources.c$237$1_0$230	= .
	.globl	C$resources.c$237$1_0$230
	XG$scroll_title_screen$0$0	= .
	.globl	XG$scroll_title_screen$0$0
	ret
	G$titleScrollHandler$0$0	= .
	.globl	G$titleScrollHandler$0$0
	C$resources.c$239$1_0$232	= .
	.globl	C$resources.c$239$1_0$232
;src/resources/resources.c:239: void titleScrollHandler(void) {
;	---------------------------------
; Function titleScrollHandler
; ---------------------------------
_titleScrollHandler::
	C$resources.c$240$1_0$232	= .
	.globl	C$resources.c$240$1_0$232
;src/resources/resources.c:240: SMS_setBGScrollX((scroll_x[lineCnt++])>>8);
	ld	a, (_lineCnt+0)
	ld	c, a
	ld	iy, #_lineCnt
	inc	0 (iy)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, c
	add	hl, hl
	ld	de, #_scroll_x
	add	hl, de
	ld	c, (hl)
	inc	hl
	ld	b, (hl)
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$241$1_0$232	= .
	.globl	C$resources.c$241$1_0$232
;src/resources/resources.c:241: }
	C$resources.c$241$1_0$232	= .
	.globl	C$resources.c$241$1_0$232
	XG$titleScrollHandler$0$0	= .
	.globl	XG$titleScrollHandler$0$0
	jp	_SMS_setBGScrollX
	G$write_push_start$0$0	= .
	.globl	G$write_push_start$0$0
	C$resources.c$243$1_0$234	= .
	.globl	C$resources.c$243$1_0$234
;src/resources/resources.c:243: void write_push_start(u16 counter) {
;	---------------------------------
; Function write_push_start
; ---------------------------------
_write_push_start::
	C$resources.c$244$1_0$234	= .
	.globl	C$resources.c$244$1_0$234
;src/resources/resources.c:244: SMS_initSprites();
	push	hl
	call	_SMS_initSprites
	pop	hl
	C$resources.c$245$1_0$234	= .
	.globl	C$resources.c$245$1_0$234
;src/resources/resources.c:245: if(counter & 0x8) {
	bit	3, l
	ret	Z
	C$resources.c$246$2_0$235	= .
	.globl	C$resources.c$246$2_0$235
;src/resources/resources.c:246: SMS_addSprite(80,150, CHARS_BASE_ADDRESS+'P'-'A');
	ld	de, #0x5031
	ld	hl, #0x0096
	call	_SMS_addSprite_f
	C$resources.c$247$2_0$235	= .
	.globl	C$resources.c$247$2_0$235
;src/resources/resources.c:247: SMS_addSprite(90,150, CHARS_BASE_ADDRESS+'U'-'A');
	ld	de, #0x5a36
	ld	hl, #0x0096
	call	_SMS_addSprite_f
	C$resources.c$248$2_0$235	= .
	.globl	C$resources.c$248$2_0$235
;src/resources/resources.c:248: SMS_addSprite(100,150, CHARS_BASE_ADDRESS+'S'-'A');
	ld	de, #0x6434
	ld	hl, #0x0096
	call	_SMS_addSprite_f
	C$resources.c$249$2_0$235	= .
	.globl	C$resources.c$249$2_0$235
;src/resources/resources.c:249: SMS_addSprite(110,150, CHARS_BASE_ADDRESS+'H'-'A');
	ld	de, #0x6e29
	ld	hl, #0x0096
	call	_SMS_addSprite_f
	C$resources.c$251$2_0$235	= .
	.globl	C$resources.c$251$2_0$235
;src/resources/resources.c:251: SMS_addSprite(130,150, DIGITS_BASE_ADDRESS+1);
	ld	de, #0x8219
	ld	hl, #0x0096
	call	_SMS_addSprite_f
	C$resources.c$252$2_0$235	= .
	.globl	C$resources.c$252$2_0$235
;src/resources/resources.c:252: SMS_addSprite(150,150, CHARS_BASE_ADDRESS+'O'-'A');
	ld	de, #0x9630
	ld	hl, #0x0096
	call	_SMS_addSprite_f
	C$resources.c$253$2_0$235	= .
	.globl	C$resources.c$253$2_0$235
;src/resources/resources.c:253: SMS_addSprite(160,150, CHARS_BASE_ADDRESS+'R'-'A');
	ld	de, #0xa033
	ld	hl, #0x0096
	call	_SMS_addSprite_f
	C$resources.c$254$2_0$235	= .
	.globl	C$resources.c$254$2_0$235
;src/resources/resources.c:254: SMS_addSprite(180,150, DIGITS_BASE_ADDRESS+2);
	ld	de, #0xb41a
	ld	hl, #0x0096
	C$resources.c$257$1_0$234	= .
	.globl	C$resources.c$257$1_0$234
;src/resources/resources.c:257: }
	C$resources.c$257$1_0$234	= .
	.globl	C$resources.c$257$1_0$234
	XG$write_push_start$0$0	= .
	.globl	XG$write_push_start$0$0
	jp	_SMS_addSprite_f
	G$clear_push_start$0$0	= .
	.globl	G$clear_push_start$0$0
	C$resources.c$259$1_0$237	= .
	.globl	C$resources.c$259$1_0$237
;src/resources/resources.c:259: void clear_push_start(void) {
;	---------------------------------
; Function clear_push_start
; ---------------------------------
_clear_push_start::
	C$resources.c$260$1_0$237	= .
	.globl	C$resources.c$260$1_0$237
;src/resources/resources.c:260: SMS_initSprites();
	C$resources.c$261$1_0$237	= .
	.globl	C$resources.c$261$1_0$237
;src/resources/resources.c:261: }
	C$resources.c$261$1_0$237	= .
	.globl	C$resources.c$261$1_0$237
	XG$clear_push_start$0$0	= .
	.globl	XG$clear_push_start$0$0
	jp	_SMS_initSprites
	G$clear_scroll_title_screen$0$0	= .
	.globl	G$clear_scroll_title_screen$0$0
	C$resources.c$263$1_0$239	= .
	.globl	C$resources.c$263$1_0$239
;src/resources/resources.c:263: void clear_scroll_title_screen(void) {
;	---------------------------------
; Function clear_scroll_title_screen
; ---------------------------------
_clear_scroll_title_screen::
	C$resources.c$264$1_0$239	= .
	.globl	C$resources.c$264$1_0$239
;src/resources/resources.c:264: SMS_disableLineInterrupt();
	ld	hl, #0x0010
	call	_SMS_VDPturnOffFeature
	C$resources.c$265$1_0$239	= .
	.globl	C$resources.c$265$1_0$239
;src/resources/resources.c:265: scroll_x[0] = 0xFFFF;
	ld	hl, #0xffff
	ld	(_scroll_x), hl
	C$resources.c$266$1_0$239	= .
	.globl	C$resources.c$266$1_0$239
;src/resources/resources.c:266: scroll_x[1] = 0xFFFF;
	ld	((_scroll_x + 2)), hl
	C$resources.c$267$1_0$239	= .
	.globl	C$resources.c$267$1_0$239
;src/resources/resources.c:267: scroll_x[2] = 0xFFFF;
	ld	((_scroll_x + 4)), hl
	C$resources.c$268$1_0$239	= .
	.globl	C$resources.c$268$1_0$239
;src/resources/resources.c:268: scroll_x[3] = 0xFFFF;
	ld	((_scroll_x + 6)), hl
	C$resources.c$269$1_0$239	= .
	.globl	C$resources.c$269$1_0$239
;src/resources/resources.c:269: scroll_x[4] = 0xFFFF;
	ld	((_scroll_x + 8)), hl
	C$resources.c$270$1_0$239	= .
	.globl	C$resources.c$270$1_0$239
;src/resources/resources.c:270: scroll_x[5] = 0xFFFF;
	ld	((_scroll_x + 10)), hl
	C$resources.c$271$1_0$239	= .
	.globl	C$resources.c$271$1_0$239
;src/resources/resources.c:271: SMS_setBGScrollX(scroll_x[0]>>8);
	ld	hl, (#_scroll_x + 0)
	ld	l, h
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$273$1_0$239	= .
	.globl	C$resources.c$273$1_0$239
;src/resources/resources.c:273: }
	C$resources.c$273$1_0$239	= .
	.globl	C$resources.c$273$1_0$239
	XG$clear_scroll_title_screen$0$0	= .
	.globl	XG$clear_scroll_title_screen$0$0
	jp	_SMS_setBGScrollX
	G$load_highscore_assets$0$0	= .
	.globl	G$load_highscore_assets$0$0
	C$resources.c$275$1_0$241	= .
	.globl	C$resources.c$275$1_0$241
;src/resources/resources.c:275: void load_highscore_assets(void) {
;	---------------------------------
; Function load_highscore_assets
; ---------------------------------
_load_highscore_assets::
	C$resources.c$276$1_0$241	= .
	.globl	C$resources.c$276$1_0$241
;src/resources/resources.c:276: SMS_mapROMBank(simplefont_psgcompr_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x08
	C$resources.c$277$1_0$241	= .
	.globl	C$resources.c$277$1_0$241
;src/resources/resources.c:277: SMS_loadPSGaidencompressedTiles(simplefont_psgcompr,DIGITS_BASE_ADDRESS);
	ld	de, #0x4300
	ld	hl, #_simplefont_psgcompr
	call	_SMS_loadPSGaidencompressedTilesatAddr
	C$resources.c$278$1_0$241	= .
	.globl	C$resources.c$278$1_0$241
;src/resources/resources.c:278: SMS_mapROMBank(scorescreentiles_psgcompr_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x0e
	C$resources.c$279$1_0$241	= .
	.globl	C$resources.c$279$1_0$241
;src/resources/resources.c:279: current_resource_bank = scorescreentiles_psgcompr_bank;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x0e
	C$resources.c$280$1_0$241	= .
	.globl	C$resources.c$280$1_0$241
;src/resources/resources.c:280: SMS_loadPSGaidencompressedTiles(scorescreentiles_psgcompr, BACKGROUND_BASE_ADRESS);
	ld	de, #0x47e0
	ld	hl, #_scorescreentiles_psgcompr
	call	_SMS_loadPSGaidencompressedTilesatAddr
	C$resources.c$281$1_0$241	= .
	.globl	C$resources.c$281$1_0$241
;src/resources/resources.c:281: SMS_loadSTMcompressedTileMap(0, 0, scorescreentilemap_stmcompr);
	ld	de, #_scorescreentilemap_stmcompr
	ld	hl, #0x7800
	call	_SMS_loadSTMcompressedTileMapatAddr
	C$resources.c$282$1_0$241	= .
	.globl	C$resources.c$282$1_0$241
;src/resources/resources.c:282: SMS_setBGScrollX(0);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$283$1_0$241	= .
	.globl	C$resources.c$283$1_0$241
;src/resources/resources.c:283: }
	C$resources.c$283$1_0$241	= .
	.globl	C$resources.c$283$1_0$241
	XG$load_highscore_assets$0$0	= .
	.globl	XG$load_highscore_assets$0$0
	jp	_SMS_setBGScrollX
	G$highscore_fade_in$0$0	= .
	.globl	G$highscore_fade_in$0$0
	C$resources.c$285$1_0$243	= .
	.globl	C$resources.c$285$1_0$243
;src/resources/resources.c:285: void highscore_fade_in(void) {
;	---------------------------------
; Function highscore_fade_in
; ---------------------------------
_highscore_fade_in::
	C$resources.c$286$1_0$243	= .
	.globl	C$resources.c$286$1_0$243
;src/resources/resources.c:286: SMS_mapROMBank(scorescreenpalette_bin_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x0e
	C$resources.c$287$1_0$243	= .
	.globl	C$resources.c$287$1_0$243
;src/resources/resources.c:287: current_resource_bank = scorescreenpalette_bin_bank;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x0e
	C$resources.c$288$1_0$243	= .
	.globl	C$resources.c$288$1_0$243
;src/resources/resources.c:288: fadeToTargetBGPalette(scorescreenpalette_bin, scorescreenpalette_bin_size,2);
	ld	hl, #0x20e
	push	hl
	ld	hl, #_scorescreenpalette_bin
	call	_fadeToTargetBGPalette
	C$resources.c$289$1_0$243	= .
	.globl	C$resources.c$289$1_0$243
;src/resources/resources.c:289: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$290$1_0$243	= .
	.globl	C$resources.c$290$1_0$243
;src/resources/resources.c:290: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$291$1_0$243	= .
	.globl	C$resources.c$291$1_0$243
;src/resources/resources.c:291: fadeToTargetSpritePalette(default_pieces_palette, 16,2);
	ld	hl, #0x210
	push	hl
	ld	hl, #_default_pieces_palette
	call	_fadeToTargetSpritePalette
	C$resources.c$292$1_0$243	= .
	.globl	C$resources.c$292$1_0$243
;src/resources/resources.c:292: }
	C$resources.c$292$1_0$243	= .
	.globl	C$resources.c$292$1_0$243
	XG$highscore_fade_in$0$0	= .
	.globl	XG$highscore_fade_in$0$0
	ret
	G$draw_highscore_row$0$0	= .
	.globl	G$draw_highscore_row$0$0
	C$resources.c$294$1_0$245	= .
	.globl	C$resources.c$294$1_0$245
;src/resources/resources.c:294: void draw_highscore_row(u8 row) {
;	---------------------------------
; Function draw_highscore_row
; ---------------------------------
_draw_highscore_row::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	C$resources.c$295$2_0$246	= .
	.globl	C$resources.c$295$2_0$246
;src/resources/resources.c:295: SMS_setTileatXY(3,12+row,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+highscore_table.table[row].name[0]);
	ld	-1 (ix), a
	ld	c, a
	ld	b, #0x00
	ld	hl, #0x000c
	add	hl, bc
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
;	spillPairReg hl
;	spillPairReg hl
	ld	e,l
	ld	d,h
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	inc	hl
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, -1 (ix)
	ld	c, a
	add	a, a
	add	a, a
	add	a, c
	ld	-2 (ix), a
	ld	a, #<((_highscore_table + 2))
	add	a, -2 (ix)
	ld	c, a
	ld	a, #>((_highscore_table + 2))
	adc	a, #0x00
	ld	b, a
	ld	a, (bc)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	push	de
	ld	de, #0x0022
	add	hl, de
	pop	de
	set	3, h
	rst	#0x18
	C$resources.c$296$2_0$247	= .
	.globl	C$resources.c$296$2_0$247
;src/resources/resources.c:296: SMS_setTileatXY(4,12+row,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+highscore_table.table[row].name[1]);
	ld	hl, #0x0004
	add	hl, de
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	rst	#0x08
	pop	bc
	ld	iy, #(_highscore_table + 2)
	push	bc
	ld	c, -2 (ix)
	ld	b, #0x00
	add	iy, bc
	pop	bc
	push	iy
	pop	hl
	inc	hl
	ld	l, (hl)
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	push	de
	ld	de, #0x0022
	add	hl, de
	pop	de
	set	3, h
	push	iy
	rst	#0x18
	pop	iy
	C$resources.c$297$2_0$248	= .
	.globl	C$resources.c$297$2_0$248
;src/resources/resources.c:297: SMS_setTileatXY(5,12+row,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+highscore_table.table[row].name[2]);
	ld	hl, #0x0005
	add	hl, de
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	push	iy
	rst	#0x08
	pop	iy
	pop	bc
	ld	e, 2 (iy)
	ld	d, #0x00
	ld	hl, #0x0022
	add	hl, de
	set	3, h
	rst	#0x18
	C$resources.c$298$1_0$245	= .
	.globl	C$resources.c$298$1_0$245
;src/resources/resources.c:298: print_score_value(7,12+row,highscore_table.table[row].score);
	ld	hl, #3
	add	hl, bc
	ld	c, (hl)
	inc	hl
	ld	b, (hl)
	ld	a, -1 (ix)
	add	a, #0x0c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	ld	a, #0x07
	call	_print_score_value
	C$resources.c$299$1_0$245	= .
	.globl	C$resources.c$299$1_0$245
;src/resources/resources.c:299: }
	ld	sp, ix
	pop	ix
	C$resources.c$299$1_0$245	= .
	.globl	C$resources.c$299$1_0$245
	XG$draw_highscore_row$0$0	= .
	.globl	XG$draw_highscore_row$0$0
	ret
	G$draw_highscore_table$0$0	= .
	.globl	G$draw_highscore_table$0$0
	C$resources.c$301$1_0$251	= .
	.globl	C$resources.c$301$1_0$251
;src/resources/resources.c:301: void draw_highscore_table(void) {
;	---------------------------------
; Function draw_highscore_table
; ---------------------------------
_draw_highscore_table::
	C$resources.c$303$2_0$251	= .
	.globl	C$resources.c$303$2_0$251
;src/resources/resources.c:303: for(i=0;i<10;i++) {
	ld	c, #0x00
00102$:
	C$resources.c$304$3_0$252	= .
	.globl	C$resources.c$304$3_0$252
;src/resources/resources.c:304: draw_highscore_row(i);
	push	bc
	ld	a, c
	call	_draw_highscore_row
	pop	bc
	C$resources.c$303$2_0$251	= .
	.globl	C$resources.c$303$2_0$251
;src/resources/resources.c:303: for(i=0;i<10;i++) {
	inc	c
	ld	a, c
	sub	a, #0x0a
	jr	C, 00102$
	C$resources.c$306$2_0$251	= .
	.globl	C$resources.c$306$2_0$251
;src/resources/resources.c:306: }
	C$resources.c$306$2_0$251	= .
	.globl	C$resources.c$306$2_0$251
	XG$draw_highscore_table$0$0	= .
	.globl	XG$draw_highscore_table$0$0
	ret
	G$init_credits$0$0	= .
	.globl	G$init_credits$0$0
	C$resources.c$308$2_0$254	= .
	.globl	C$resources.c$308$2_0$254
;src/resources/resources.c:308: void init_credits(void) {
;	---------------------------------
; Function init_credits
; ---------------------------------
_init_credits::
	C$resources.c$309$1_0$254	= .
	.globl	C$resources.c$309$1_0$254
;src/resources/resources.c:309: cursorx = 1;
	ld	hl, #_cursorx
	ld	(hl), #0x01
	C$resources.c$310$1_0$254	= .
	.globl	C$resources.c$310$1_0$254
;src/resources/resources.c:310: cursory = 2;
	ld	hl, #_cursory
	ld	(hl), #0x02
	C$resources.c$311$1_0$254	= .
	.globl	C$resources.c$311$1_0$254
;src/resources/resources.c:311: waveIndex = 0;
	ld	hl, #_waveIndex
	ld	(hl), #0x00
	C$resources.c$312$1_0$254	= .
	.globl	C$resources.c$312$1_0$254
;src/resources/resources.c:312: creditsEnd = false;
	ld	hl, #_creditsEnd
	C$resources.c$313$1_0$254	= .
	.globl	C$resources.c$313$1_0$254
;src/resources/resources.c:313: SMS_setBGScrollX(0);
	ld	(hl), #0x00
	ld	l, (hl)
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setBGScrollX
	C$resources.c$315$1_0$254	= .
	.globl	C$resources.c$315$1_0$254
;src/resources/resources.c:315: SMS_setLineInterruptHandler(&creditsScrollHandler1);
	ld	hl, #_creditsScrollHandler1
	call	_SMS_setLineInterruptHandler
	C$resources.c$316$1_0$254	= .
	.globl	C$resources.c$316$1_0$254
;src/resources/resources.c:316: SMS_setLineCounter(1);    /* we're updating every scanline... */
	ld	l, #0x01
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setLineCounter
	C$resources.c$317$1_0$254	= .
	.globl	C$resources.c$317$1_0$254
;src/resources/resources.c:317: SMS_enableLineInterrupt();
	ld	hl, #0x0010
	call	_SMS_VDPturnOnFeature
	C$resources.c$318$1_0$254	= .
	.globl	C$resources.c$318$1_0$254
;src/resources/resources.c:318: lineCnt = 0;
	ld	hl, #_lineCnt
	ld	(hl), #0x00
	C$resources.c$319$1_0$254	= .
	.globl	C$resources.c$319$1_0$254
;src/resources/resources.c:319: }
	C$resources.c$319$1_0$254	= .
	.globl	C$resources.c$319$1_0$254
	XG$init_credits$0$0	= .
	.globl	XG$init_credits$0$0
	ret
	G$write_credits$0$0	= .
	.globl	G$write_credits$0$0
	C$resources.c$321$1_0$256	= .
	.globl	C$resources.c$321$1_0$256
;src/resources/resources.c:321: void write_credits(u16 counter) {
;	---------------------------------
; Function write_credits
; ---------------------------------
_write_credits::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	ld	c, l
	ld	b, h
	C$resources.c$322$1_0$256	= .
	.globl	C$resources.c$322$1_0$256
;src/resources/resources.c:322: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$323$1_0$256	= .
	.globl	C$resources.c$323$1_0$256
;src/resources/resources.c:323: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$324$1_1$257	= .
	.globl	C$resources.c$324$1_1$257
;src/resources/resources.c:324: u8 currentCharacter = credits[counter>>4] -'A';
	ld	e, c
	ld	d, b
	srl	d
	rr	e
	srl	d
	rr	e
	srl	d
	rr	e
	srl	d
	rr	e
	ld	hl, #_credits
	add	hl, de
	ld	a, (hl)
	add	a, #0xbf
	ld	e, a
	C$resources.c$325$1_1$257	= .
	.globl	C$resources.c$325$1_1$257
;src/resources/resources.c:325: if((counter & 15) == 0) {
	ld	a, c
	and	a, #0x0f
	jp	NZ,00120$
	C$resources.c$326$2_1$258	= .
	.globl	C$resources.c$326$2_1$258
;src/resources/resources.c:326: if(creditsEnd)
	ld	hl, #_creditsEnd
	bit	0, (hl)
	C$resources.c$327$2_1$258	= .
	.globl	C$resources.c$327$2_1$258
;src/resources/resources.c:327: return;
	jp	NZ,00120$
	C$resources.c$328$2_1$258	= .
	.globl	C$resources.c$328$2_1$258
;src/resources/resources.c:328: if(currentCharacter == 26) {
	ld	a, e
	sub	a, #0x1a
	jr	NZ, 00116$
	C$resources.c$329$3_1$259	= .
	.globl	C$resources.c$329$3_1$259
;src/resources/resources.c:329: cursory++;
	ld	hl, #_cursory
	inc	(hl)
	C$resources.c$330$3_1$259	= .
	.globl	C$resources.c$330$3_1$259
;src/resources/resources.c:330: cursorx = cursory+1;
	ld	a, (_cursory+0)
	inc	a
	ld	(_cursorx+0), a
	jr	00120$
00116$:
	C$resources.c$331$2_1$258	= .
	.globl	C$resources.c$331$2_1$258
;src/resources/resources.c:331: } else if(currentCharacter == 28) {
	ld	a, e
	sub	a, #0x1c
	jr	NZ, 00113$
	C$resources.c$332$3_1$260	= .
	.globl	C$resources.c$332$3_1$260
;src/resources/resources.c:332: creditsEnd = true;
	ld	hl, #_creditsEnd
	ld	(hl), #0x01
	jr	00120$
00113$:
	C$resources.c$333$2_1$258	= .
	.globl	C$resources.c$333$2_1$258
;src/resources/resources.c:333: } else if(currentCharacter == 29) {
	ld	a, e
	sub	a, #0x1d
	jr	NZ, 00103$
	C$resources.c$334$3_1$261	= .
	.globl	C$resources.c$334$3_1$261
;src/resources/resources.c:334: cursorx++;
	ld	hl, #_cursorx
	inc	(hl)
	jr	00120$
	C$resources.c$336$3_1$262	= .
	.globl	C$resources.c$336$3_1$262
;src/resources/resources.c:336: SMS_setTileatXY(cursorx, cursory, TILE_USE_SPRITE_PALETTE|(CHARS_BASE_ADDRESS + currentCharacter));
00103$:
	ld	a, (_cursory+0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, (_cursorx+0)
	ld	b, #0x00
	ld	c, a
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	d, #0x00
	ld	hl, #0x0022
	add	hl, de
	ex	de, hl
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	a, d
	or	a, #0x08
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x18
	C$resources.c$337$4_1$264	= .
	.globl	C$resources.c$337$4_1$264
;src/resources/resources.c:337: SMS_setTileatXY(cursorx++, 23 - cursory, TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|(CHARS_BASE_ADDRESS + currentCharacter));
	ld	a, (_cursory+0)
	ld	c, a
	ld	b, #0x00
	ld	hl, #0x0017
	cp	a, a
	sbc	hl, bc
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	c, l
	ld	b, h
	ld	a, (_cursorx+0)
	ld	-1 (ix), a
	ld	hl, #_cursorx
	inc	(hl)
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	a, d
	or	a, #0x0c
	ld	d, a
	ex	de, hl
	rst	#0x18
00120$:
	C$resources.c$340$1_1$256	= .
	.globl	C$resources.c$340$1_1$256
;src/resources/resources.c:340: }
	inc	sp
	pop	ix
	C$resources.c$340$1_1$256	= .
	.globl	C$resources.c$340$1_1$256
	XG$write_credits$0$0	= .
	.globl	XG$write_credits$0$0
	ret
	G$rasterize_credits$0$0	= .
	.globl	G$rasterize_credits$0$0
	C$resources.c$342$1_1$266	= .
	.globl	C$resources.c$342$1_1$266
;src/resources/resources.c:342: void rasterize_credits(void) {
;	---------------------------------
; Function rasterize_credits
; ---------------------------------
_rasterize_credits::
	C$resources.c$343$1_0$266	= .
	.globl	C$resources.c$343$1_0$266
;src/resources/resources.c:343: SMS_disableLineInterrupt();
	ld	hl, #0x0010
	call	_SMS_VDPturnOffFeature
	C$resources.c$344$1_0$266	= .
	.globl	C$resources.c$344$1_0$266
;src/resources/resources.c:344: lineCnt = 0;
	ld	hl, #_lineCnt
	ld	(hl), #0x00
	C$resources.c$345$1_0$266	= .
	.globl	C$resources.c$345$1_0$266
;src/resources/resources.c:345: waveIndex = (waveIndex + 1) & 0x1F;
	ld	a, (_waveIndex+0)
	inc	a
	and	a, #0x1f
	ld	(_waveIndex+0), a
	C$resources.c$347$1_0$266	= .
	.globl	C$resources.c$347$1_0$266
;src/resources/resources.c:347: SMS_mapROMBank(creditsscreenpalette_bin_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x10
	C$resources.c$348$1_0$266	= .
	.globl	C$resources.c$348$1_0$266
;src/resources/resources.c:348: SMS_loadBGPalette(creditsscreenpalette_bin);
	ld	hl, #_creditsscreenpalette_bin
	call	_SMS_loadBGPalette
	C$resources.c$349$1_0$266	= .
	.globl	C$resources.c$349$1_0$266
;src/resources/resources.c:349: SMS_setBGScrollX(0);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setBGScrollX
	C$resources.c$350$1_0$266	= .
	.globl	C$resources.c$350$1_0$266
;src/resources/resources.c:350: SMS_setLineInterruptHandler(&creditsScrollHandler1);
	ld	hl, #_creditsScrollHandler1
	call	_SMS_setLineInterruptHandler
	C$resources.c$351$1_0$266	= .
	.globl	C$resources.c$351$1_0$266
;src/resources/resources.c:351: SMS_enableLineInterrupt();
	ld	hl, #0x0010
	C$resources.c$352$1_0$266	= .
	.globl	C$resources.c$352$1_0$266
;src/resources/resources.c:352: }
	C$resources.c$352$1_0$266	= .
	.globl	C$resources.c$352$1_0$266
	XG$rasterize_credits$0$0	= .
	.globl	XG$rasterize_credits$0$0
	jp	_SMS_VDPturnOnFeature
	G$creditsScrollHandler1$0$0	= .
	.globl	G$creditsScrollHandler1$0$0
	C$resources.c$354$1_0$268	= .
	.globl	C$resources.c$354$1_0$268
;src/resources/resources.c:354: void creditsScrollHandler1(void) {
;	---------------------------------
; Function creditsScrollHandler1
; ---------------------------------
_creditsScrollHandler1::
	C$resources.c$355$1_0$268	= .
	.globl	C$resources.c$355$1_0$268
;src/resources/resources.c:355: if(lineCnt == 48) {        
	ld	a, (_lineCnt+0)
	sub	a, #0x30
	jr	NZ, 00102$
	C$resources.c$356$2_0$269	= .
	.globl	C$resources.c$356$2_0$269
;src/resources/resources.c:356: lineCnt = 0;
	ld	hl, #_lineCnt
	ld	(hl), #0x00
	C$resources.c$357$2_0$269	= .
	.globl	C$resources.c$357$2_0$269
;src/resources/resources.c:357: SMS_disableLineInterrupt();
	ld	hl, #0x0010
	call	_SMS_VDPturnOffFeature
	C$resources.c$358$2_0$269	= .
	.globl	C$resources.c$358$2_0$269
;src/resources/resources.c:358: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$359$2_0$269	= .
	.globl	C$resources.c$359$2_0$269
;src/resources/resources.c:359: SMS_loadBGPalette(credits_secondary_palette);
	ld	hl, #_credits_secondary_palette
	call	_SMS_loadBGPalette
	C$resources.c$360$2_0$269	= .
	.globl	C$resources.c$360$2_0$269
;src/resources/resources.c:360: SMS_setLineInterruptHandler(&creditsScrollHandler2);
	ld	hl, #_creditsScrollHandler2
	call	_SMS_setLineInterruptHandler
	C$resources.c$361$2_0$269	= .
	.globl	C$resources.c$361$2_0$269
;src/resources/resources.c:361: SMS_enableLineInterrupt();
	ld	hl, #0x0010
	call	_SMS_VDPturnOnFeature
00102$:
	C$resources.c$363$1_0$268	= .
	.globl	C$resources.c$363$1_0$268
;src/resources/resources.c:363: lineCnt++;
	ld	hl, #_lineCnt
	inc	(hl)
	C$resources.c$364$1_0$268	= .
	.globl	C$resources.c$364$1_0$268
;src/resources/resources.c:364: }
	C$resources.c$364$1_0$268	= .
	.globl	C$resources.c$364$1_0$268
	XG$creditsScrollHandler1$0$0	= .
	.globl	XG$creditsScrollHandler1$0$0
	ret
	G$creditsScrollHandler2$0$0	= .
	.globl	G$creditsScrollHandler2$0$0
	C$resources.c$366$1_0$271	= .
	.globl	C$resources.c$366$1_0$271
;src/resources/resources.c:366: void creditsScrollHandler2(void) { //should it be covered with previous change of bank
;	---------------------------------
; Function creditsScrollHandler2
; ---------------------------------
_creditsScrollHandler2::
	C$resources.c$367$1_0$271	= .
	.globl	C$resources.c$367$1_0$271
;src/resources/resources.c:367: SMS_setBGScrollX(waveLut[(lineCnt + waveIndex)&0x1F]);
	ld	bc, #_waveLut+0
	ld	a, (_lineCnt+0)
	ld	e, a
	ld	d, #0x00
	ld	a, (_waveIndex+0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
	add	hl, de
	ld	a, l
	and	a, #0x1f
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, bc
	ld	l, (hl)
;	spillPairReg hl
	call	_SMS_setBGScrollX
	C$resources.c$368$1_0$271	= .
	.globl	C$resources.c$368$1_0$271
;src/resources/resources.c:368: lineCnt++;
	ld	hl, #_lineCnt
	inc	(hl)
	C$resources.c$369$1_0$271	= .
	.globl	C$resources.c$369$1_0$271
;src/resources/resources.c:369: }
	C$resources.c$369$1_0$271	= .
	.globl	C$resources.c$369$1_0$271
	XG$creditsScrollHandler2$0$0	= .
	.globl	XG$creditsScrollHandler2$0$0
	ret
	G$disable_credits_effects$0$0	= .
	.globl	G$disable_credits_effects$0$0
	C$resources.c$371$1_0$273	= .
	.globl	C$resources.c$371$1_0$273
;src/resources/resources.c:371: void disable_credits_effects(void) {
;	---------------------------------
; Function disable_credits_effects
; ---------------------------------
_disable_credits_effects::
	C$resources.c$372$1_0$273	= .
	.globl	C$resources.c$372$1_0$273
;src/resources/resources.c:372: SMS_disableLineInterrupt();
	ld	hl, #0x0010
	C$resources.c$374$1_0$273	= .
	.globl	C$resources.c$374$1_0$273
;src/resources/resources.c:374: }
	C$resources.c$374$1_0$273	= .
	.globl	C$resources.c$374$1_0$273
	XG$disable_credits_effects$0$0	= .
	.globl	XG$disable_credits_effects$0$0
	jp	_SMS_VDPturnOffFeature
	G$load_ending1_assets$0$0	= .
	.globl	G$load_ending1_assets$0$0
	C$resources.c$376$1_0$275	= .
	.globl	C$resources.c$376$1_0$275
;src/resources/resources.c:376: void load_ending1_assets(void) {
;	---------------------------------
; Function load_ending1_assets
; ---------------------------------
_load_ending1_assets::
	C$resources.c$377$1_0$275	= .
	.globl	C$resources.c$377$1_0$275
;src/resources/resources.c:377: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$378$1_0$275	= .
	.globl	C$resources.c$378$1_0$275
;src/resources/resources.c:378: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$379$1_0$275	= .
	.globl	C$resources.c$379$1_0$275
;src/resources/resources.c:379: SMS_loadTiles(black_tile,0,32);
	ld	hl, #0x0020
	push	hl
	ld	de, #_black_tile
	ld	hl, #0x4000
	call	_SMS_VRAMmemcpy
	C$resources.c$380$1_0$275	= .
	.globl	C$resources.c$380$1_0$275
;src/resources/resources.c:380: SMS_loadTiles(black_tile_sprite,2,32);
	ld	hl, #0x0020
	push	hl
	ld	de, #_black_tile_sprite
	ld	hl, #0x4040
	call	_SMS_VRAMmemcpy
	C$resources.c$381$1_0$275	= .
	.globl	C$resources.c$381$1_0$275
;src/resources/resources.c:381: }
	C$resources.c$381$1_0$275	= .
	.globl	C$resources.c$381$1_0$275
	XG$load_ending1_assets$0$0	= .
	.globl	XG$load_ending1_assets$0$0
	ret
	G$load_ending2_assets$0$0	= .
	.globl	G$load_ending2_assets$0$0
	C$resources.c$383$1_0$277	= .
	.globl	C$resources.c$383$1_0$277
;src/resources/resources.c:383: void load_ending2_assets(void) {
;	---------------------------------
; Function load_ending2_assets
; ---------------------------------
_load_ending2_assets::
	C$resources.c$384$1_0$277	= .
	.globl	C$resources.c$384$1_0$277
;src/resources/resources.c:384: SMS_mapROMBank(ending2tiles_bin_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x03
	C$resources.c$385$1_0$277	= .
	.globl	C$resources.c$385$1_0$277
;src/resources/resources.c:385: current_resource_bank = ending2tiles_bin_bank;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x03
	C$resources.c$386$1_0$277	= .
	.globl	C$resources.c$386$1_0$277
;src/resources/resources.c:386: load_tiles_in_batches(ending2tiles_bin,BACKGROUND_BASE_ADRESS,ending2tiles_bin_size);
	ld	hl, #0x2b00
	push	hl
	ld	de, #0x003f
	ld	hl, #_ending2tiles_bin
	call	_load_tiles_in_batches
	C$resources.c$387$1_0$277	= .
	.globl	C$resources.c$387$1_0$277
;src/resources/resources.c:387: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$388$1_0$277	= .
	.globl	C$resources.c$388$1_0$277
;src/resources/resources.c:388: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$389$1_0$277	= .
	.globl	C$resources.c$389$1_0$277
;src/resources/resources.c:389: SMS_loadTiles(black_tile_sprite,2,32);
	ld	hl, #0x0020
	push	hl
	ld	de, #_black_tile_sprite
	ld	hl, #0x4040
	call	_SMS_VRAMmemcpy
	C$resources.c$390$1_0$277	= .
	.globl	C$resources.c$390$1_0$277
;src/resources/resources.c:390: SMS_loadTileMap(0, 0, ending2tilemap,896);
	ld	hl, #0x0380
	push	hl
	ld	de, #_ending2tilemap
	ld	hl, #0x7800
	call	_SMS_VRAMmemcpy
	C$resources.c$391$1_0$277	= .
	.globl	C$resources.c$391$1_0$277
;src/resources/resources.c:391: }
	C$resources.c$391$1_0$277	= .
	.globl	C$resources.c$391$1_0$277
	XG$load_ending2_assets$0$0	= .
	.globl	XG$load_ending2_assets$0$0
	ret
	G$ending_sprites$0$0	= .
	.globl	G$ending_sprites$0$0
	C$resources.c$393$1_0$279	= .
	.globl	C$resources.c$393$1_0$279
;src/resources/resources.c:393: void ending_sprites(void) {
;	---------------------------------
; Function ending_sprites
; ---------------------------------
_ending_sprites::
	C$resources.c$396$1_0$279	= .
	.globl	C$resources.c$396$1_0$279
;src/resources/resources.c:396: SMS_initSprites();
	call	_SMS_initSprites
	C$resources.c$397$2_0$280	= .
	.globl	C$resources.c$397$2_0$280
;src/resources/resources.c:397: for(i=1;i<112;i=i+8) {
	ld	c, #0x01
00102$:
	C$resources.c$398$3_0$281	= .
	.globl	C$resources.c$398$3_0$281
;src/resources/resources.c:398: SMS_addSprite(1, i, 2);
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	push	hl
	push	bc
	ld	de, #0x0102
	call	_SMS_addSprite_f
	pop	bc
	pop	hl
	C$resources.c$399$3_0$281	= .
	.globl	C$resources.c$399$3_0$281
;src/resources/resources.c:399: SMS_addSprite(248, i, 2);
	push	bc
	ld	de, #0xf802
	call	_SMS_addSprite_f
	pop	bc
	C$resources.c$397$2_0$280	= .
	.globl	C$resources.c$397$2_0$280
;src/resources/resources.c:397: for(i=1;i<112;i=i+8) {
	ld	a, c
	add	a, #0x08
	ld	c, a
	sub	a, #0x70
	jr	C, 00102$
	C$resources.c$401$2_0$279	= .
	.globl	C$resources.c$401$2_0$279
;src/resources/resources.c:401: }
	C$resources.c$401$2_0$279	= .
	.globl	C$resources.c$401$2_0$279
	XG$ending_sprites$0$0	= .
	.globl	XG$ending_sprites$0$0
	ret
	G$scroll_ending$0$0	= .
	.globl	G$scroll_ending$0$0
	C$resources.c$403$2_0$283	= .
	.globl	C$resources.c$403$2_0$283
;src/resources/resources.c:403: void scroll_ending(void) {
;	---------------------------------
; Function scroll_ending
; ---------------------------------
_scroll_ending::
	C$resources.c$405$1_0$283	= .
	.globl	C$resources.c$405$1_0$283
;src/resources/resources.c:405: if(scroll_x[0]>144) scroll_x[0]--;
	ld	bc, (#_scroll_x + 0)
	ld	a, #0x90
	cp	a, c
	ld	a, #0x00
	sbc	a, b
	jr	NC, 00102$
	dec	bc
	ld	(_scroll_x), bc
00102$:
	C$resources.c$406$1_0$283	= .
	.globl	C$resources.c$406$1_0$283
;src/resources/resources.c:406: SMS_setBGScrollX(scroll_x[0]);
	ld	hl, #_scroll_x
	ld	l, (hl)
;	spillPairReg hl
	call	_SMS_setBGScrollX
	C$resources.c$405$1_0$283	= .
	.globl	C$resources.c$405$1_0$283
;src/resources/resources.c:405: if(scroll_x[0]>144) scroll_x[0]--;
	ld	hl, (#_scroll_x + 0)
	C$resources.c$407$1_0$283	= .
	.globl	C$resources.c$407$1_0$283
;src/resources/resources.c:407: if(scroll_x[0]&7 == 7) {
	bit	0, l
	ret	Z
	C$resources.c$408$2_0$284	= .
	.globl	C$resources.c$408$2_0$284
;src/resources/resources.c:408: currentScrollColumn = scroll_x[0]>>3;
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	ld	a, l
	ld	(#_currentScrollColumn), a
	C$resources.c$409$2_0$284	= .
	.globl	C$resources.c$409$2_0$284
;src/resources/resources.c:409: column2Update = 31-currentScrollColumn;
	ld	a, (_currentScrollColumn+0)
	ld	c, a
	ld	a, #0x1f
	sub	a, c
	ld	c, a
	C$resources.c$410$2_0$284	= .
	.globl	C$resources.c$410$2_0$284
;src/resources/resources.c:410: SMS_loadTileMapArea(column2Update,0,ending2tilemapcolumns+column2Update*14,1,14); 
	ld	de, #_ending2tilemapcolumns+0
	ld	b, #0x00
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, hl
	add	hl, de
	ex	de, hl
	ld	l, c
	ld	h, b
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	bc, #0xe01
	push	bc
	call	_SMS_loadTileMapAreaatAddr
	C$resources.c$415$1_0$283	= .
	.globl	C$resources.c$415$1_0$283
;src/resources/resources.c:415: }
	C$resources.c$415$1_0$283	= .
	.globl	C$resources.c$415$1_0$283
	XG$scroll_ending$0$0	= .
	.globl	XG$scroll_ending$0$0
	ret
	G$endingScrollFinish$0$0	= .
	.globl	G$endingScrollFinish$0$0
	C$resources.c$417$1_0$286	= .
	.globl	C$resources.c$417$1_0$286
;src/resources/resources.c:417: void endingScrollFinish(void) {
;	---------------------------------
; Function endingScrollFinish
; ---------------------------------
_endingScrollFinish::
	C$resources.c$418$1_0$286	= .
	.globl	C$resources.c$418$1_0$286
;src/resources/resources.c:418: SMS_setBGScrollX(0);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$419$1_0$286	= .
	.globl	C$resources.c$419$1_0$286
;src/resources/resources.c:419: }
	C$resources.c$419$1_0$286	= .
	.globl	C$resources.c$419$1_0$286
	XG$endingScrollFinish$0$0	= .
	.globl	XG$endingScrollFinish$0$0
	jp	_SMS_setBGScrollX
	G$ending_update_face$0$0	= .
	.globl	G$ending_update_face$0$0
	C$resources.c$421$1_0$288	= .
	.globl	C$resources.c$421$1_0$288
;src/resources/resources.c:421: void ending_update_face(void) {
;	---------------------------------
; Function ending_update_face
; ---------------------------------
_ending_update_face::
	C$resources.c$422$1_0$288	= .
	.globl	C$resources.c$422$1_0$288
;src/resources/resources.c:422: SMS_loadTiles(ending_face_row1,78,96);
	ld	hl, #0x0060
	push	hl
	ld	de, #_ending_face_row1
	ld	hl, #0x49c0
	call	_SMS_VRAMmemcpy
	C$resources.c$423$1_0$288	= .
	.globl	C$resources.c$423$1_0$288
;src/resources/resources.c:423: SMS_loadTiles(ending_face_row2,102,96);
	ld	hl, #0x0060
	push	hl
	ld	de, #_ending_face_row2
	ld	hl, #0x4cc0
	call	_SMS_VRAMmemcpy
	C$resources.c$424$1_0$288	= .
	.globl	C$resources.c$424$1_0$288
;src/resources/resources.c:424: SMS_loadTiles(ending_face_row3,123,96);
	ld	hl, #0x0060
	push	hl
	ld	de, #_ending_face_row3
	ld	h, #0x4f
	call	_SMS_VRAMmemcpy
	C$resources.c$425$1_0$288	= .
	.globl	C$resources.c$425$1_0$288
;src/resources/resources.c:425: SMS_loadTiles(ending_face_row4,146,96);
	ld	hl, #0x0060
	push	hl
	ld	de, #_ending_face_row4
	ld	hl, #0x5240
	call	_SMS_VRAMmemcpy
	C$resources.c$426$1_0$288	= .
	.globl	C$resources.c$426$1_0$288
;src/resources/resources.c:426: }
	C$resources.c$426$1_0$288	= .
	.globl	C$resources.c$426$1_0$288
	XG$ending_update_face$0$0	= .
	.globl	XG$ending_update_face$0$0
	ret
	G$load_game_assets$0$0	= .
	.globl	G$load_game_assets$0$0
	C$resources.c$428$1_0$290	= .
	.globl	C$resources.c$428$1_0$290
;src/resources/resources.c:428: void load_game_assets(void) {
;	---------------------------------
; Function load_game_assets
; ---------------------------------
_load_game_assets::
	C$resources.c$429$1_0$290	= .
	.globl	C$resources.c$429$1_0$290
;src/resources/resources.c:429: SMS_mapROMBank(backgroundpalette_bin_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x08
	C$resources.c$430$1_0$290	= .
	.globl	C$resources.c$430$1_0$290
;src/resources/resources.c:430: current_resource_bank = backgroundpalette_bin_bank;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x08
	C$resources.c$431$1_0$290	= .
	.globl	C$resources.c$431$1_0$290
;src/resources/resources.c:431: SMS_loadPSGaidencompressedTiles(piecestiles_psgcompr, 0);
	ld	de, #0x4000
	ld	hl, #_piecestiles_psgcompr
	call	_SMS_loadPSGaidencompressedTilesatAddr
	C$resources.c$432$1_0$290	= .
	.globl	C$resources.c$432$1_0$290
;src/resources/resources.c:432: SMS_loadPSGaidencompressedTiles(simplefont_psgcompr,DIGITS_BASE_ADDRESS);
	ld	de, #0x4300
	ld	hl, #_simplefont_psgcompr
	call	_SMS_loadPSGaidencompressedTilesatAddr
	C$resources.c$433$1_0$290	= .
	.globl	C$resources.c$433$1_0$290
;src/resources/resources.c:433: SMS_loadPSGaidencompressedTiles(backgroundtiles_psgcompr, BACKGROUND_BASE_ADRESS);
	ld	de, #0x47e0
	ld	hl, #_backgroundtiles_psgcompr
	call	_SMS_loadPSGaidencompressedTilesatAddr
	C$resources.c$434$1_0$290	= .
	.globl	C$resources.c$434$1_0$290
;src/resources/resources.c:434: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$435$1_0$290	= .
	.globl	C$resources.c$435$1_0$290
;src/resources/resources.c:435: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$436$1_0$290	= .
	.globl	C$resources.c$436$1_0$290
;src/resources/resources.c:436: setSpritePalette(default_pieces_palette, 16);
	ld	a, #0x10
	push	af
	inc	sp
	ld	hl, #_default_pieces_palette
	call	_setSpritePalette
	C$resources.c$438$1_0$290	= .
	.globl	C$resources.c$438$1_0$290
;src/resources/resources.c:438: }
	C$resources.c$438$1_0$290	= .
	.globl	C$resources.c$438$1_0$290
	XG$load_game_assets$0$0	= .
	.globl	XG$load_game_assets$0$0
	ret
	G$game_half_palette$0$0	= .
	.globl	G$game_half_palette$0$0
	C$resources.c$440$1_0$292	= .
	.globl	C$resources.c$440$1_0$292
;src/resources/resources.c:440: void game_half_palette(void) {
;	---------------------------------
; Function game_half_palette
; ---------------------------------
_game_half_palette::
	C$resources.c$441$1_0$292	= .
	.globl	C$resources.c$441$1_0$292
;src/resources/resources.c:441: SMS_loadBGPaletteHalfBrightness(getBGPalette());
	call	_getBGPalette
	ex	de, hl
	call	_SMS_loadBGPaletteHalfBrightness
	C$resources.c$442$1_0$292	= .
	.globl	C$resources.c$442$1_0$292
;src/resources/resources.c:442: SMS_loadSpritePaletteHalfBrightness(getSpritePalette());
	call	_getSpritePalette
	ex	de, hl
	C$resources.c$443$1_0$292	= .
	.globl	C$resources.c$443$1_0$292
;src/resources/resources.c:443: }
	C$resources.c$443$1_0$292	= .
	.globl	C$resources.c$443$1_0$292
	XG$game_half_palette$0$0	= .
	.globl	XG$game_half_palette$0$0
	jp	_SMS_loadSpritePaletteHalfBrightness
	G$game_full_palette$0$0	= .
	.globl	G$game_full_palette$0$0
	C$resources.c$445$1_0$294	= .
	.globl	C$resources.c$445$1_0$294
;src/resources/resources.c:445: void game_full_palette(void) {
;	---------------------------------
; Function game_full_palette
; ---------------------------------
_game_full_palette::
	C$resources.c$446$1_0$294	= .
	.globl	C$resources.c$446$1_0$294
;src/resources/resources.c:446: SMS_loadBGPalette(getBGPalette());
	call	_getBGPalette
	ex	de, hl
	call	_SMS_loadBGPalette
	C$resources.c$447$1_0$294	= .
	.globl	C$resources.c$447$1_0$294
;src/resources/resources.c:447: SMS_loadSpritePalette(getSpritePalette());
	call	_getSpritePalette
	ex	de, hl
	C$resources.c$448$1_0$294	= .
	.globl	C$resources.c$448$1_0$294
;src/resources/resources.c:448: }
	C$resources.c$448$1_0$294	= .
	.globl	C$resources.c$448$1_0$294
	XG$game_full_palette$0$0	= .
	.globl	XG$game_full_palette$0$0
	jp	_SMS_loadSpritePalette
	G$draw_game_background$0$0	= .
	.globl	G$draw_game_background$0$0
	C$resources.c$450$1_0$296	= .
	.globl	C$resources.c$450$1_0$296
;src/resources/resources.c:450: void draw_game_background(void) {
;	---------------------------------
; Function draw_game_background
; ---------------------------------
_draw_game_background::
	C$resources.c$452$1_0$296	= .
	.globl	C$resources.c$452$1_0$296
;src/resources/resources.c:452: SMS_mapROMBank(backgroundtilemap_stmcompr_bank);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x08
	C$resources.c$453$1_0$296	= .
	.globl	C$resources.c$453$1_0$296
;src/resources/resources.c:453: current_resource_bank = backgroundtilemap_stmcompr_bank;
	ld	iy, #_current_resource_bank
	ld	0 (iy), #0x08
	C$resources.c$454$1_0$296	= .
	.globl	C$resources.c$454$1_0$296
;src/resources/resources.c:454: SMS_loadSTMcompressedTileMap(0, 0, backgroundtilemap_stmcompr); 
	ld	de, #_backgroundtilemap_stmcompr
	ld	hl, #0x7800
	C$resources.c$455$1_0$296	= .
	.globl	C$resources.c$455$1_0$296
;src/resources/resources.c:455: }
	C$resources.c$455$1_0$296	= .
	.globl	C$resources.c$455$1_0$296
	XG$draw_game_background$0$0	= .
	.globl	XG$draw_game_background$0$0
	jp	_SMS_loadSTMcompressedTileMapatAddr
	G$initialize_game_lists$0$0	= .
	.globl	G$initialize_game_lists$0$0
	C$resources.c$457$1_0$298	= .
	.globl	C$resources.c$457$1_0$298
;src/resources/resources.c:457: void initialize_game_lists(void) {
;	---------------------------------
; Function initialize_game_lists
; ---------------------------------
_initialize_game_lists::
	C$resources.c$458$1_0$298	= .
	.globl	C$resources.c$458$1_0$298
;src/resources/resources.c:458: active_points_list = createList(sizeof(point_sprite));
	ld	hl, #0x0003
	call	_createList
	ld	(_active_points_list), de
	C$resources.c$459$1_0$298	= .
	.globl	C$resources.c$459$1_0$298
;src/resources/resources.c:459: }
	C$resources.c$459$1_0$298	= .
	.globl	C$resources.c$459$1_0$298
	XG$initialize_game_lists$0$0	= .
	.globl	XG$initialize_game_lists$0$0
	ret
	G$clear_game_lists$0$0	= .
	.globl	G$clear_game_lists$0$0
	C$resources.c$461$1_0$300	= .
	.globl	C$resources.c$461$1_0$300
;src/resources/resources.c:461: void clear_game_lists(void) {
;	---------------------------------
; Function clear_game_lists
; ---------------------------------
_clear_game_lists::
	C$resources.c$462$1_0$300	= .
	.globl	C$resources.c$462$1_0$300
;src/resources/resources.c:462: destroyList(active_points_list);
	ld	hl, (_active_points_list)
	C$resources.c$463$1_0$300	= .
	.globl	C$resources.c$463$1_0$300
;src/resources/resources.c:463: }
	C$resources.c$463$1_0$300	= .
	.globl	C$resources.c$463$1_0$300
	XG$clear_game_lists$0$0	= .
	.globl	XG$clear_game_lists$0$0
	jp	_destroyList
	G$draw_game_screen$0$0	= .
	.globl	G$draw_game_screen$0$0
	C$resources.c$465$1_0$302	= .
	.globl	C$resources.c$465$1_0$302
;src/resources/resources.c:465: void draw_game_screen(void) {
;	---------------------------------
; Function draw_game_screen
; ---------------------------------
_draw_game_screen::
	C$resources.c$468$2_0$303	= .
	.globl	C$resources.c$468$2_0$303
;src/resources/resources.c:468: SMS_setTileatXY(10,0,TILE_USE_SPRITE_PALETTE|7);
	ld	hl, #0x7814
	rst	#0x08
	ld	hl, #0x0807
	rst	#0x18
	C$resources.c$470$1_0$302	= .
	.globl	C$resources.c$470$1_0$302
;src/resources/resources.c:470: SMS_setTileatXY(i,0,TILE_USE_SPRITE_PALETTE|8);
	ld	b, #0x0b
00104$:
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0808
	rst	#0x18
	C$resources.c$469$2_0$304	= .
	.globl	C$resources.c$469$2_0$304
;src/resources/resources.c:469: for(i=11;i < 21;i++) {
	inc	b
	ld	a, b
	sub	a, #0x15
	jr	C, 00104$
	C$resources.c$472$2_0$307	= .
	.globl	C$resources.c$472$2_0$307
;src/resources/resources.c:472: SMS_setTileatXY(10,0,TILE_USE_SPRITE_PALETTE|7);
	ld	hl, #0x7814
	rst	#0x08
	ld	hl, #0x0807
	rst	#0x18
	C$resources.c$473$2_0$308	= .
	.globl	C$resources.c$473$2_0$308
;src/resources/resources.c:473: SMS_setTileatXY(21,0,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|7);
	ld	hl, #0x782a
	rst	#0x08
	ld	hl, #0x0a07
	rst	#0x18
	C$resources.c$475$1_0$302	= .
	.globl	C$resources.c$475$1_0$302
;src/resources/resources.c:475: SMS_setTileatXY(10,j,TILE_USE_SPRITE_PALETTE|9);
	ld	b, #0x01
00114$:
	ld	e, b
	ld	d, #0x00
	ex	de, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	ld	hl, #0x000a
	add	hl, de
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0809
	rst	#0x18
	C$resources.c$477$1_0$302	= .
	.globl	C$resources.c$477$1_0$302
;src/resources/resources.c:477: SMS_setTileatXY(i,j,TILE_USE_SPRITE_PALETTE|0);
	ld	c, #0x0b
00117$:
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, de
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	rst	#0x08
	pop	bc
	ld	hl, #0x0800
	rst	#0x18
	C$resources.c$476$4_0$312	= .
	.globl	C$resources.c$476$4_0$312
;src/resources/resources.c:476: for(i=11;i < 21;i++) {
	inc	c
	ld	a, c
	sub	a, #0x15
	jr	C, 00117$
	C$resources.c$479$4_0$315	= .
	.globl	C$resources.c$479$4_0$315
;src/resources/resources.c:479: SMS_setTileatXY(21,j,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|9);
	ld	hl, #0x0015
	add	hl, de
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0a09
	rst	#0x18
	C$resources.c$474$2_0$309	= .
	.globl	C$resources.c$474$2_0$309
;src/resources/resources.c:474: for(j=1;j<23;j++) {
	inc	b
	ld	a, b
	sub	a, #0x17
	jr	C, 00114$
	C$resources.c$481$2_0$316	= .
	.globl	C$resources.c$481$2_0$316
;src/resources/resources.c:481: SMS_setTileatXY(10,23,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|7);
	ld	hl, #0x7dd4
	rst	#0x08
	ld	hl, #0x0c07
	rst	#0x18
	C$resources.c$482$2_0$317	= .
	.globl	C$resources.c$482$2_0$317
;src/resources/resources.c:482: SMS_setTileatXY(21,23,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|TILE_FLIPPED_Y|7);
	ld	hl, #0x7dea
	rst	#0x08
	ld	hl, #0x0e07
	rst	#0x18
	C$resources.c$484$1_0$302	= .
	.globl	C$resources.c$484$1_0$302
;src/resources/resources.c:484: SMS_setTileatXY(i,23,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|8);
	ld	b, #0x0b
00131$:
	ld	e, b
	ld	d, #0x00
	ld	hl, #0x02e0
	add	hl, de
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	hl, #0x0c08
	rst	#0x18
	C$resources.c$483$2_0$318	= .
	.globl	C$resources.c$483$2_0$318
;src/resources/resources.c:483: for(i=11;i < 21;i++) {
	inc	b
	ld	a, b
	sub	a, #0x15
	jr	C, 00131$
	C$resources.c$488$1_0$302	= .
	.globl	C$resources.c$488$1_0$302
;src/resources/resources.c:488: if(number_human_players == 2) {
	ld	a, (_number_human_players+0)
	sub	a, #0x02
	jr	NZ, 00141$
	C$resources.c$489$2_0$321	= .
	.globl	C$resources.c$489$2_0$321
;src/resources/resources.c:489: print_string(23,17,"PLAYER]A^");
	ld	hl, #___str_2
	push	hl
	ld	l, #0x11
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x17
	call	_print_string
	C$resources.c$490$2_0$321	= .
	.globl	C$resources.c$490$2_0$321
;src/resources/resources.c:490: print_string(23,21,"PLAYER]B^");
	ld	hl, #___str_3
	push	hl
	ld	l, #0x15
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x17
	call	_print_string
	jr	00143$
00141$:
	C$resources.c$491$1_0$302	= .
	.globl	C$resources.c$491$1_0$302
;src/resources/resources.c:491: } else if(number_human_players == 1) {
	ld	a, (_number_human_players+0)
	dec	a
	jr	NZ, 00138$
	C$resources.c$492$2_0$322	= .
	.globl	C$resources.c$492$2_0$322
;src/resources/resources.c:492: print_string(23,17,"PLAYER^");
	ld	hl, #___str_4
	push	hl
	ld	l, #0x11
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x17
	call	_print_string
	C$resources.c$493$2_0$322	= .
	.globl	C$resources.c$493$2_0$322
;src/resources/resources.c:493: print_string(23,21,"CPU^");
	ld	hl, #___str_5
	push	hl
	ld	l, #0x15
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x17
	call	_print_string
	jr	00143$
00138$:
	C$resources.c$494$1_0$302	= .
	.globl	C$resources.c$494$1_0$302
;src/resources/resources.c:494: } else if(number_human_players == 0) {
	ld	a, (_number_human_players+0)
	or	a, a
	jr	NZ, 00143$
	C$resources.c$495$2_0$323	= .
	.globl	C$resources.c$495$2_0$323
;src/resources/resources.c:495: print_string(23,17,"CPU]A^");
	ld	hl, #___str_6
	push	hl
	ld	l, #0x11
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x17
	call	_print_string
	C$resources.c$496$2_0$323	= .
	.globl	C$resources.c$496$2_0$323
;src/resources/resources.c:496: print_string(23,21,"CPU]B^");
	ld	hl, #___str_7
	push	hl
	ld	l, #0x15
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x17
	call	_print_string
	C$resources.c$500$1_0$302	= .
	.globl	C$resources.c$500$1_0$302
;src/resources/resources.c:500: SMS_setTileatXY(27,12,TILE_USE_SPRITE_PALETTE|7);
00143$:
	ld	hl, #0x7b36
	rst	#0x08
	ld	hl, #0x0807
	rst	#0x18
	C$resources.c$501$2_0$325	= .
	.globl	C$resources.c$501$2_0$325
;src/resources/resources.c:501: SMS_setTileatXY(28,12,TILE_USE_SPRITE_PALETTE|8);
	ld	hl, #0x7b38
	rst	#0x08
	ld	hl, #0x0808
	rst	#0x18
	C$resources.c$502$2_0$326	= .
	.globl	C$resources.c$502$2_0$326
;src/resources/resources.c:502: SMS_setTileatXY(29,12,TILE_USE_SPRITE_PALETTE|8);
	ld	hl, #0x7b3a
	rst	#0x08
	ld	hl, #0x0808
	rst	#0x18
	C$resources.c$503$2_0$327	= .
	.globl	C$resources.c$503$2_0$327
;src/resources/resources.c:503: SMS_setTileatXY(30,12,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|7);
	ld	hl, #0x7b3c
	rst	#0x08
	ld	hl, #0x0a07
	rst	#0x18
	C$resources.c$504$2_0$328	= .
	.globl	C$resources.c$504$2_0$328
;src/resources/resources.c:504: SMS_setTileatXY(27,13,TILE_USE_SPRITE_PALETTE|9);
	ld	hl, #0x7b76
	rst	#0x08
	ld	hl, #0x0809
	rst	#0x18
	C$resources.c$505$2_0$329	= .
	.globl	C$resources.c$505$2_0$329
;src/resources/resources.c:505: SMS_setTileatXY(30,13,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|9);
	ld	hl, #0x7b7c
	rst	#0x08
	ld	hl, #0x0a09
	rst	#0x18
	C$resources.c$506$2_0$330	= .
	.globl	C$resources.c$506$2_0$330
;src/resources/resources.c:506: SMS_setTileatXY(27,14,TILE_USE_SPRITE_PALETTE|9);
	ld	hl, #0x7bb6
	rst	#0x08
	ld	hl, #0x0809
	rst	#0x18
	C$resources.c$507$2_0$331	= .
	.globl	C$resources.c$507$2_0$331
;src/resources/resources.c:507: SMS_setTileatXY(30,14,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_X|9);
	ld	hl, #0x7bbc
	rst	#0x08
	ld	hl, #0x0a09
	rst	#0x18
	C$resources.c$508$2_0$332	= .
	.globl	C$resources.c$508$2_0$332
;src/resources/resources.c:508: SMS_setTileatXY(27,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|7);
	ld	hl, #0x7bf6
	rst	#0x08
	ld	hl, #0x0c07
	rst	#0x18
	C$resources.c$509$2_0$333	= .
	.globl	C$resources.c$509$2_0$333
;src/resources/resources.c:509: SMS_setTileatXY(28,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|8);
	ld	hl, #0x7bf8
	rst	#0x08
	ld	hl, #0x0c08
	rst	#0x18
	C$resources.c$510$2_0$334	= .
	.globl	C$resources.c$510$2_0$334
;src/resources/resources.c:510: SMS_setTileatXY(29,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|8);
	ld	hl, #0x7bfa
	rst	#0x08
	ld	hl, #0x0c08
	rst	#0x18
	C$resources.c$511$2_0$335	= .
	.globl	C$resources.c$511$2_0$335
;src/resources/resources.c:511: SMS_setTileatXY(30,15,TILE_USE_SPRITE_PALETTE|TILE_FLIPPED_Y|TILE_FLIPPED_X|7);
	ld	hl, #0x7bfc
	rst	#0x08
	ld	hl, #0x0e07
	rst	#0x18
	C$resources.c$512$1_0$302	= .
	.globl	C$resources.c$512$1_0$302
;src/resources/resources.c:512: print_string(23,15,"NEXT^");
	ld	hl, #___str_8
	push	hl
	ld	l, #0x0f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x17
	call	_print_string
	C$resources.c$513$1_0$302	= .
	.globl	C$resources.c$513$1_0$302
;src/resources/resources.c:513: }
	C$resources.c$513$1_0$302	= .
	.globl	C$resources.c$513$1_0$302
	XG$draw_game_screen$0$0	= .
	.globl	XG$draw_game_screen$0$0
	ret
Fresources$__str_2$0_0$0 == .
___str_2:
	.ascii "PLAYER]A^"
	.db 0x00
Fresources$__str_3$0_0$0 == .
___str_3:
	.ascii "PLAYER]B^"
	.db 0x00
Fresources$__str_4$0_0$0 == .
___str_4:
	.ascii "PLAYER^"
	.db 0x00
Fresources$__str_5$0_0$0 == .
___str_5:
	.ascii "CPU^"
	.db 0x00
Fresources$__str_6$0_0$0 == .
___str_6:
	.ascii "CPU]A^"
	.db 0x00
Fresources$__str_7$0_0$0 == .
___str_7:
	.ascii "CPU]B^"
	.db 0x00
Fresources$__str_8$0_0$0 == .
___str_8:
	.ascii "NEXT^"
	.db 0x00
	G$draw_congratulations$0$0	= .
	.globl	G$draw_congratulations$0$0
	C$resources.c$515$1_0$337	= .
	.globl	C$resources.c$515$1_0$337
;src/resources/resources.c:515: void draw_congratulations(void) {
;	---------------------------------
; Function draw_congratulations
; ---------------------------------
_draw_congratulations::
	C$resources.c$516$1_0$337	= .
	.globl	C$resources.c$516$1_0$337
;src/resources/resources.c:516: print_string(8,10,"CONGRATULATIONS[^");
	ld	hl, #___str_9
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x08
	call	_print_string
	C$resources.c$517$1_0$337	= .
	.globl	C$resources.c$517$1_0$337
;src/resources/resources.c:517: }
	C$resources.c$517$1_0$337	= .
	.globl	C$resources.c$517$1_0$337
	XG$draw_congratulations$0$0	= .
	.globl	XG$draw_congratulations$0$0
	ret
Fresources$__str_9$0_0$0 == .
___str_9:
	.ascii "CONGRATULATIONS[^"
	.db 0x00
	G$draw_point_sprites$0$0	= .
	.globl	G$draw_point_sprites$0$0
	C$resources.c$519$1_0$339	= .
	.globl	C$resources.c$519$1_0$339
;src/resources/resources.c:519: void draw_point_sprites(void) {
;	---------------------------------
; Function draw_point_sprites
; ---------------------------------
_draw_point_sprites::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-7
	add	hl, sp
	ld	sp, hl
	C$resources.c$520$1_0$339	= .
	.globl	C$resources.c$520$1_0$339
;src/resources/resources.c:520: SMS_initSprites();
	call	_SMS_initSprites
	C$resources.c$522$1_1$340	= .
	.globl	C$resources.c$522$1_1$340
;src/resources/resources.c:522: list_node_t* current_element = active_points_list->head;
	ld	hl, (_active_points_list)
	ld	a, (hl)
	ld	-7 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-6 (ix), a
	C$resources.c$529$1_1$340	= .
	.globl	C$resources.c$529$1_1$340
;src/resources/resources.c:529: while(current_element != NULL) {
00109$:
	ld	a, -6 (ix)
	or	a, -7 (ix)
	jp	Z, 00112$
	C$resources.c$530$2_1$342	= .
	.globl	C$resources.c$530$2_1$342
;src/resources/resources.c:530: current_point_sprite = (point_sprite*)(current_element->data);
	pop	hl
	push	hl
	ld	a, (hl)
	ld	-2 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-1 (ix), a
	ld	a, -2 (ix)
	ld	-5 (ix), a
	ld	a, -1 (ix)
	ld	-4 (ix), a
	C$resources.c$531$2_1$342	= .
	.globl	C$resources.c$531$2_1$342
;src/resources/resources.c:531: if(current_point_sprite->firstsprite != 0) {
	ld	a, -5 (ix)
	add	a, #0x02
	ld	-3 (ix), a
	ld	a, -4 (ix)
	adc	a, #0x00
	ld	-2 (ix), a
	ld	l, -3 (ix)
	ld	h, -2 (ix)
	ld	a, (hl)
	ld	-1 (ix), a
	or	a, a
	jr	Z, 00108$
	C$resources.c$532$3_1$343	= .
	.globl	C$resources.c$532$3_1$343
;src/resources/resources.c:532: SMS_addSprite(current_point_sprite->x,current_point_sprite->y, current_point_sprite->firstsprite);
	ld	l, -5 (ix)
	ld	h, -4 (ix)
	ld	c, (hl)
	xor	a, a
	ld	e, -1 (ix)
	ld	d, #0x00
	or	a, e
	ld	e, a
	ld	a, c
	or	a, d
	ld	d, a
	pop	hl
	pop	bc
	push	bc
	push	hl
	inc	bc
	ld	a, (bc)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	call	_SMS_addSprite_f
	pop	bc
	C$resources.c$533$3_1$343	= .
	.globl	C$resources.c$533$3_1$343
;src/resources/resources.c:533: if(current_point_sprite->firstsprite == POINT_100_SPRITE_INDEX || current_point_sprite->firstsprite == POINT_500_SPRITE_INDEX) {
	ld	l, -3 (ix)
	ld	h, -2 (ix)
	ld	a, (hl)
	cp	a, #0x0e
	jr	Z, 00103$
	cp	a, #0x0f
	jr	NZ, 00104$
00103$:
	C$resources.c$534$4_1$344	= .
	.globl	C$resources.c$534$4_1$344
;src/resources/resources.c:534: SMS_addSprite(current_point_sprite->x+dx,current_point_sprite->y, POINT_00_SPRITE_INDEX);
	ld	l, -5 (ix)
	ld	h, -4 (ix)
	ld	e, (hl)
	ld	d, #0x00
	ld	hl, #0x0008
	add	hl, de
;	spillPairReg hl
;	spillPairReg hl
	ld	d, l
	ld	e, #0x00
	set	4, e
	ld	a, (bc)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_addSprite_f
	jr	00108$
00104$:
	C$resources.c$535$3_1$343	= .
	.globl	C$resources.c$535$3_1$343
;src/resources/resources.c:535: } else if(current_point_sprite->firstsprite == BONUS_SPRITE_INDEX) {
	sub	a, #0x16
	jr	NZ, 00108$
	C$resources.c$536$4_1$345	= .
	.globl	C$resources.c$536$4_1$345
;src/resources/resources.c:536: SMS_addSprite(current_point_sprite->x+dx,current_point_sprite->y, BONUS_SPRITE_INDEX+1);
	ld	l, -5 (ix)
	ld	h, -4 (ix)
	ld	e, (hl)
	ld	d, #0x00
	ld	hl, #0x0008
	add	hl, de
;	spillPairReg hl
;	spillPairReg hl
	xor	a, a
	or	a, #0x17
	ld	-4 (ix), a
	ld	-3 (ix), l
	ld	a, (bc)
	ld	-2 (ix), a
	ld	-1 (ix), #0x00
	ld	e, -4 (ix)
	ld	d, -3 (ix)
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_addSprite_f
00108$:
	C$resources.c$539$2_1$342	= .
	.globl	C$resources.c$539$2_1$342
;src/resources/resources.c:539: current_element = current_element->next;
	pop	hl
	push	hl
	inc	hl
	inc	hl
	ld	a, (hl)
	ld	-7 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-6 (ix), a
	jp	00109$
00112$:
	C$resources.c$541$1_1$339	= .
	.globl	C$resources.c$541$1_1$339
;src/resources/resources.c:541: }
	ld	sp, ix
	pop	ix
	C$resources.c$541$1_1$339	= .
	.globl	C$resources.c$541$1_1$339
	XG$draw_point_sprites$0$0	= .
	.globl	XG$draw_point_sprites$0$0
	ret
	G$clear_point_sprites$0$0	= .
	.globl	G$clear_point_sprites$0$0
	C$resources.c$543$1_1$347	= .
	.globl	C$resources.c$543$1_1$347
;src/resources/resources.c:543: void clear_point_sprites(void) {   
;	---------------------------------
; Function clear_point_sprites
; ---------------------------------
_clear_point_sprites::
	C$resources.c$544$1_0$347	= .
	.globl	C$resources.c$544$1_0$347
;src/resources/resources.c:544: while(active_points_list->numElements) {
00101$:
	ld	hl, (_active_points_list)
	ld	de, #0x0004
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00103$
	C$resources.c$545$2_0$348	= .
	.globl	C$resources.c$545$2_0$348
;src/resources/resources.c:545: deleteElementEndList(active_points_list);
	ld	hl, (_active_points_list)
	call	_deleteElementEndList
	jr	00101$
00103$:
	C$resources.c$547$1_0$347	= .
	.globl	C$resources.c$547$1_0$347
;src/resources/resources.c:547: SMS_setSpriteMode(SPRITEMODE_NORMAL);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setSpriteMode
	C$resources.c$548$1_0$347	= .
	.globl	C$resources.c$548$1_0$347
;src/resources/resources.c:548: SMS_initSprites();
	C$resources.c$549$1_0$347	= .
	.globl	C$resources.c$549$1_0$347
;src/resources/resources.c:549: }
	C$resources.c$549$1_0$347	= .
	.globl	C$resources.c$549$1_0$347
	XG$clear_point_sprites$0$0	= .
	.globl	XG$clear_point_sprites$0$0
	jp	_SMS_initSprites
	G$draw_cursor_sprites$0$0	= .
	.globl	G$draw_cursor_sprites$0$0
	C$resources.c$551$1_0$350	= .
	.globl	C$resources.c$551$1_0$350
;src/resources/resources.c:551: void draw_cursor_sprites(u8 x, u8 y) {
;	---------------------------------
; Function draw_cursor_sprites
; ---------------------------------
_draw_cursor_sprites::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	ld	c, a
	ld	-1 (ix), l
	C$resources.c$553$1_0$350	= .
	.globl	C$resources.c$553$1_0$350
;src/resources/resources.c:553: if(current_player == PLAYER_1) {
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00105$
	C$resources.c$554$2_0$351	= .
	.globl	C$resources.c$554$2_0$351
;src/resources/resources.c:554: sprite1 = PLAYER_1_SPRITE_INDEX;
	ld	l, #0x11
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$555$2_0$351	= .
	.globl	C$resources.c$555$2_0$351
;src/resources/resources.c:555: sprite2 = PLAYER_P_SPRITE_INDEX;
	ld	-2 (ix), #0x12
	jr	00106$
00105$:
	C$resources.c$556$1_0$350	= .
	.globl	C$resources.c$556$1_0$350
;src/resources/resources.c:556: } else if(current_player == PLAYER_2) {
	ld	a, (_current_player+0)
	dec	a
	jr	NZ, 00102$
	C$resources.c$557$2_0$352	= .
	.globl	C$resources.c$557$2_0$352
;src/resources/resources.c:557: sprite1 = PLAYER_2_SPRITE_INDEX;
	ld	l, #0x13
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$558$2_0$352	= .
	.globl	C$resources.c$558$2_0$352
;src/resources/resources.c:558: sprite2 = PLAYER_P_SPRITE_INDEX;
	ld	-2 (ix), #0x12
	jr	00106$
00102$:
	C$resources.c$560$2_0$353	= .
	.globl	C$resources.c$560$2_0$353
;src/resources/resources.c:560: sprite1 = PLAYER_CP_SPRITE_INDEX;
	ld	l, #0x14
;	spillPairReg hl
;	spillPairReg hl
	C$resources.c$561$2_0$353	= .
	.globl	C$resources.c$561$2_0$353
;src/resources/resources.c:561: sprite2 = PLAYER_PU_SPRITE_INDEX;
	ld	-2 (ix), #0x15
00106$:
	C$resources.c$563$1_0$350	= .
	.globl	C$resources.c$563$1_0$350
;src/resources/resources.c:563: SMS_initSprites();
	push	hl
	push	bc
	call	_SMS_initSprites
	pop	bc
	pop	hl
	C$resources.c$565$1_0$350	= .
	.globl	C$resources.c$565$1_0$350
;src/resources/resources.c:565: SMS_addSprite(x,y, sprite1);
	ld	b, #0x00
	ld	d, c
;	spillPairReg hl
;	spillPairReg hl
;	spillPairReg hl
;	spillPairReg hl
	xor	a, a
	ld	h, a
	or	a, l
	ld	e, a
	ld	a, d
	or	a, h
	ld	d, a
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	push	hl
	push	bc
	call	_SMS_addSprite_f
	pop	bc
	pop	hl
	C$resources.c$566$1_0$350	= .
	.globl	C$resources.c$566$1_0$350
;src/resources/resources.c:566: SMS_addSprite(x+8,y, sprite2);
	ld	a, c
	add	a, #0x08
	ld	c, a
	jr	NC, 00121$
00121$:
	ld	b, c
	ld	c, #0x00
	ld	a, -2 (ix)
	ld	d, #0x00
	or	a, c
	ld	e, a
	ld	a, d
	or	a, b
	ld	d, a
	call	_SMS_addSprite_f
	C$resources.c$567$1_0$350	= .
	.globl	C$resources.c$567$1_0$350
;src/resources/resources.c:567: }
	ld	sp, ix
	pop	ix
	C$resources.c$567$1_0$350	= .
	.globl	C$resources.c$567$1_0$350
	XG$draw_cursor_sprites$0$0	= .
	.globl	XG$draw_cursor_sprites$0$0
	ret
	G$clear_cursor_sprites$0$0	= .
	.globl	G$clear_cursor_sprites$0$0
	C$resources.c$569$1_0$355	= .
	.globl	C$resources.c$569$1_0$355
;src/resources/resources.c:569: void clear_cursor_sprites(void) {   
;	---------------------------------
; Function clear_cursor_sprites
; ---------------------------------
_clear_cursor_sprites::
	C$resources.c$570$1_0$355	= .
	.globl	C$resources.c$570$1_0$355
;src/resources/resources.c:570: SMS_initSprites();
	C$resources.c$571$1_0$355	= .
	.globl	C$resources.c$571$1_0$355
;src/resources/resources.c:571: }
	C$resources.c$571$1_0$355	= .
	.globl	C$resources.c$571$1_0$355
	XG$clear_cursor_sprites$0$0	= .
	.globl	XG$clear_cursor_sprites$0$0
	jp	_SMS_initSprites
	G$connected_pieces_to_bw_palette$0$0	= .
	.globl	G$connected_pieces_to_bw_palette$0$0
	C$resources.c$573$1_0$357	= .
	.globl	C$resources.c$573$1_0$357
;src/resources/resources.c:573: void connected_pieces_to_bw_palette(void) {
;	---------------------------------
; Function connected_pieces_to_bw_palette
; ---------------------------------
_connected_pieces_to_bw_palette::
	C$resources.c$574$1_0$357	= .
	.globl	C$resources.c$574$1_0$357
;src/resources/resources.c:574: SMS_setNextSpriteColoratIndex(13);
	ld	hl, #0xc01d
	rst	#0x08
	C$resources.c$575$1_0$357	= .
	.globl	C$resources.c$575$1_0$357
;src/resources/resources.c:575: SMS_setColor(WHITE_FFFFFF);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$576$1_0$357	= .
	.globl	C$resources.c$576$1_0$357
;src/resources/resources.c:576: setSpritePaletteColor(13,WHITE_FFFFFF);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0d
	C$resources.c$577$1_0$357	= .
	.globl	C$resources.c$577$1_0$357
;src/resources/resources.c:577: }
	C$resources.c$577$1_0$357	= .
	.globl	C$resources.c$577$1_0$357
	XG$connected_pieces_to_bw_palette$0$0	= .
	.globl	XG$connected_pieces_to_bw_palette$0$0
	jp	_setSpritePaletteColor
	G$connected_pieces_to_normal_palette$0$0	= .
	.globl	G$connected_pieces_to_normal_palette$0$0
	C$resources.c$579$1_0$359	= .
	.globl	C$resources.c$579$1_0$359
;src/resources/resources.c:579: void connected_pieces_to_normal_palette(void) {
;	---------------------------------
; Function connected_pieces_to_normal_palette
; ---------------------------------
_connected_pieces_to_normal_palette::
	C$resources.c$580$1_0$359	= .
	.globl	C$resources.c$580$1_0$359
;src/resources/resources.c:580: SMS_setNextSpriteColoratIndex(13);
	ld	hl, #0xc01d
	rst	#0x08
	C$resources.c$581$1_0$359	= .
	.globl	C$resources.c$581$1_0$359
;src/resources/resources.c:581: SMS_setColor(GREY_AAAAAA);
	ld	l, #0x2a
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$582$1_0$359	= .
	.globl	C$resources.c$582$1_0$359
;src/resources/resources.c:582: setSpritePaletteColor(13,GREY_AAAAAA);
	ld	l, #0x2a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0d
	C$resources.c$583$1_0$359	= .
	.globl	C$resources.c$583$1_0$359
;src/resources/resources.c:583: }
	C$resources.c$583$1_0$359	= .
	.globl	C$resources.c$583$1_0$359
	XG$connected_pieces_to_normal_palette$0$0	= .
	.globl	XG$connected_pieces_to_normal_palette$0$0
	jp	_setSpritePaletteColor
	G$not_connected_pieces_to_red_palette$0$0	= .
	.globl	G$not_connected_pieces_to_red_palette$0$0
	C$resources.c$585$1_0$361	= .
	.globl	C$resources.c$585$1_0$361
;src/resources/resources.c:585: void not_connected_pieces_to_red_palette(void) {
;	---------------------------------
; Function not_connected_pieces_to_red_palette
; ---------------------------------
_not_connected_pieces_to_red_palette::
	C$resources.c$586$1_0$361	= .
	.globl	C$resources.c$586$1_0$361
;src/resources/resources.c:586: SMS_setNextSpriteColoratIndex(8);
	ld	hl, #0xc018
	rst	#0x08
	C$resources.c$587$1_0$361	= .
	.globl	C$resources.c$587$1_0$361
;src/resources/resources.c:587: SMS_setColor(0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$588$1_0$361	= .
	.globl	C$resources.c$588$1_0$361
;src/resources/resources.c:588: SMS_setColor(0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$589$1_0$361	= .
	.globl	C$resources.c$589$1_0$361
;src/resources/resources.c:589: SMS_setColor(0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$590$1_0$361	= .
	.globl	C$resources.c$590$1_0$361
;src/resources/resources.c:590: setSpritePaletteColor(8,0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x08
	call	_setSpritePaletteColor
	C$resources.c$591$1_0$361	= .
	.globl	C$resources.c$591$1_0$361
;src/resources/resources.c:591: setSpritePaletteColor(9,0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x09
	call	_setSpritePaletteColor
	C$resources.c$592$1_0$361	= .
	.globl	C$resources.c$592$1_0$361
;src/resources/resources.c:592: setSpritePaletteColor(10,0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0a
	call	_setSpritePaletteColor
	C$resources.c$593$1_0$361	= .
	.globl	C$resources.c$593$1_0$361
;src/resources/resources.c:593: setSpritePaletteColor(8,0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x08
	call	_setSpritePaletteColor
	C$resources.c$594$1_0$361	= .
	.globl	C$resources.c$594$1_0$361
;src/resources/resources.c:594: setSpritePaletteColor(9,0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x09
	call	_setSpritePaletteColor
	C$resources.c$595$1_0$361	= .
	.globl	C$resources.c$595$1_0$361
;src/resources/resources.c:595: setSpritePaletteColor(10,0x03);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0a
	C$resources.c$596$1_0$361	= .
	.globl	C$resources.c$596$1_0$361
;src/resources/resources.c:596: }
	C$resources.c$596$1_0$361	= .
	.globl	C$resources.c$596$1_0$361
	XG$not_connected_pieces_to_red_palette$0$0	= .
	.globl	XG$not_connected_pieces_to_red_palette$0$0
	jp	_setSpritePaletteColor
	G$not_connected_pieces_to_normal_palette$0$0	= .
	.globl	G$not_connected_pieces_to_normal_palette$0$0
	C$resources.c$598$1_0$363	= .
	.globl	C$resources.c$598$1_0$363
;src/resources/resources.c:598: void not_connected_pieces_to_normal_palette(void) {
;	---------------------------------
; Function not_connected_pieces_to_normal_palette
; ---------------------------------
_not_connected_pieces_to_normal_palette::
	C$resources.c$599$1_0$363	= .
	.globl	C$resources.c$599$1_0$363
;src/resources/resources.c:599: SMS_setNextSpriteColoratIndex(8);
	ld	hl, #0xc018
	rst	#0x08
	C$resources.c$600$1_0$363	= .
	.globl	C$resources.c$600$1_0$363
;src/resources/resources.c:600: SMS_setColor(BLACK_000000);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$601$1_0$363	= .
	.globl	C$resources.c$601$1_0$363
;src/resources/resources.c:601: SMS_setColor(WHITE_FFFFFF);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$602$1_0$363	= .
	.globl	C$resources.c$602$1_0$363
;src/resources/resources.c:602: SMS_setColor(GREY_AAAAAA);
	ld	l, #0x2a
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$603$1_0$363	= .
	.globl	C$resources.c$603$1_0$363
;src/resources/resources.c:603: setSpritePaletteColor(8,BLACK_000000);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x08
	call	_setSpritePaletteColor
	C$resources.c$604$1_0$363	= .
	.globl	C$resources.c$604$1_0$363
;src/resources/resources.c:604: setSpritePaletteColor(9,WHITE_FFFFFF);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x09
	call	_setSpritePaletteColor
	C$resources.c$605$1_0$363	= .
	.globl	C$resources.c$605$1_0$363
;src/resources/resources.c:605: setSpritePaletteColor(10,GREY_AAAAAA);
	ld	l, #0x2a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0a
	C$resources.c$606$1_0$363	= .
	.globl	C$resources.c$606$1_0$363
;src/resources/resources.c:606: }
	C$resources.c$606$1_0$363	= .
	.globl	C$resources.c$606$1_0$363
	XG$not_connected_pieces_to_normal_palette$0$0	= .
	.globl	XG$not_connected_pieces_to_normal_palette$0$0
	jp	_setSpritePaletteColor
	G$swap_fire_colors$0$0	= .
	.globl	G$swap_fire_colors$0$0
	C$resources.c$608$1_0$365	= .
	.globl	C$resources.c$608$1_0$365
;src/resources/resources.c:608: void swap_fire_colors(void) {
;	---------------------------------
; Function swap_fire_colors
; ---------------------------------
_swap_fire_colors::
	C$resources.c$609$1_0$365	= .
	.globl	C$resources.c$609$1_0$365
;src/resources/resources.c:609: SMS_setNextSpriteColoratIndex(14);
	ld	hl, #0xc01e
	rst	#0x08
	C$resources.c$610$1_0$365	= .
	.globl	C$resources.c$610$1_0$365
;src/resources/resources.c:610: SMS_setColor(ORANGE_FF5500);
	ld	l, #0x07
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$611$1_0$365	= .
	.globl	C$resources.c$611$1_0$365
;src/resources/resources.c:611: SMS_setColor(YELLOW_FFFF00);
	ld	l, #0x0f
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$612$1_0$365	= .
	.globl	C$resources.c$612$1_0$365
;src/resources/resources.c:612: setSpritePaletteColor(14,ORANGE_FF5500);
	ld	l, #0x07
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0e
	call	_setSpritePaletteColor
	C$resources.c$613$1_0$365	= .
	.globl	C$resources.c$613$1_0$365
;src/resources/resources.c:613: setSpritePaletteColor(15,YELLOW_FFFF00);
;	spillPairReg hl
;	spillPairReg hl
	ld	a,#0x0f
	ld	l,a
	C$resources.c$614$1_0$365	= .
	.globl	C$resources.c$614$1_0$365
;src/resources/resources.c:614: }
	C$resources.c$614$1_0$365	= .
	.globl	C$resources.c$614$1_0$365
	XG$swap_fire_colors$0$0	= .
	.globl	XG$swap_fire_colors$0$0
	jp	_setSpritePaletteColor
	G$restore_fire_colors$0$0	= .
	.globl	G$restore_fire_colors$0$0
	C$resources.c$616$1_0$367	= .
	.globl	C$resources.c$616$1_0$367
;src/resources/resources.c:616: void restore_fire_colors(void) {
;	---------------------------------
; Function restore_fire_colors
; ---------------------------------
_restore_fire_colors::
	C$resources.c$617$1_0$367	= .
	.globl	C$resources.c$617$1_0$367
;src/resources/resources.c:617: SMS_setNextSpriteColoratIndex(14);
	ld	hl, #0xc01e
	rst	#0x08
	C$resources.c$618$1_0$367	= .
	.globl	C$resources.c$618$1_0$367
;src/resources/resources.c:618: SMS_setColor(YELLOW_FFFF00);
	ld	l, #0x0f
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$619$1_0$367	= .
	.globl	C$resources.c$619$1_0$367
;src/resources/resources.c:619: SMS_setColor(ORANGE_FF5500);
	ld	l, #0x07
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_setColor
	C$resources.c$620$1_0$367	= .
	.globl	C$resources.c$620$1_0$367
;src/resources/resources.c:620: setSpritePaletteColor(14,YELLOW_FFFF00);
	ld	l, #0x0f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0e
	call	_setSpritePaletteColor
	C$resources.c$621$1_0$367	= .
	.globl	C$resources.c$621$1_0$367
;src/resources/resources.c:621: setSpritePaletteColor(15,ORANGE_FF5500);
	ld	l, #0x07
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0f
	C$resources.c$622$1_0$367	= .
	.globl	C$resources.c$622$1_0$367
;src/resources/resources.c:622: }
	C$resources.c$622$1_0$367	= .
	.globl	C$resources.c$622$1_0$367
	XG$restore_fire_colors$0$0	= .
	.globl	XG$restore_fire_colors$0$0
	jp	_setSpritePaletteColor
	G$background_to_level_palette$0$0	= .
	.globl	G$background_to_level_palette$0$0
	C$resources.c$624$1_0$369	= .
	.globl	C$resources.c$624$1_0$369
;src/resources/resources.c:624: void background_to_level_palette(u8 level) {
;	---------------------------------
; Function background_to_level_palette
; ---------------------------------
_background_to_level_palette::
	C$resources.c$625$1_0$369	= .
	.globl	C$resources.c$625$1_0$369
;src/resources/resources.c:625: u8 base_color = level <<4;
	add	a, a
	add	a, a
	add	a, a
	add	a, a
	ld	c, a
	C$resources.c$626$1_0$369	= .
	.globl	C$resources.c$626$1_0$369
;src/resources/resources.c:626: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$627$1_0$369	= .
	.globl	C$resources.c$627$1_0$369
;src/resources/resources.c:627: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$628$1_0$369	= .
	.globl	C$resources.c$628$1_0$369
;src/resources/resources.c:628: fadeToTargetBGPalette(&(background_palette_array[base_color]), 16,1); //TODO check correctness
	ld	hl, #_background_palette_array
	ld	b, #0x00
	add	hl, bc
	ld	de, #0x110
	push	de
	call	_fadeToTargetBGPalette
	C$resources.c$629$1_0$369	= .
	.globl	C$resources.c$629$1_0$369
;src/resources/resources.c:629: }
	C$resources.c$629$1_0$369	= .
	.globl	C$resources.c$629$1_0$369
	XG$background_to_level_palette$0$0	= .
	.globl	XG$background_to_level_palette$0$0
	ret
	G$create_dialog_box$0$0	= .
	.globl	G$create_dialog_box$0$0
	C$resources.c$634$1_0$372	= .
	.globl	C$resources.c$634$1_0$372
;src/resources/resources.c:634: void create_dialog_box(void) {
;	---------------------------------
; Function create_dialog_box
; ---------------------------------
_create_dialog_box::
	C$resources.c$636$2_0$372	= .
	.globl	C$resources.c$636$2_0$372
;src/resources/resources.c:636: for(int j=DIALOG_BOX_FIRST_ROW;j<=DIALOG_BOX_LAST_ROW;j++) {
	ld	de, #0x000f
00107$:
	ld	a, #0x16
	cp	a, e
	ld	a, #0x00
	sbc	a, d
	jp	PO, 00133$
	xor	a, #0x80
00133$:
	ret	M
	C$resources.c$637$3_0$373	= .
	.globl	C$resources.c$637$3_0$373
;src/resources/resources.c:637: SMS_setNextTileatXY(DIALOG_BOX_FIRST_COLUMN,j);
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	inc	hl
	inc	hl
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$resources.c$638$2_0$372	= .
	.globl	C$resources.c$638$2_0$372
;src/resources/resources.c:638: for(int i=DIALOG_BOX_FIRST_COLUMN;i<=DIALOG_BOX_LAST_COLUMN;i++) {            
	ld	bc, #0x0002
00104$:
	ld	a, #0x1d
	cp	a, c
	ld	a, #0x00
	sbc	a, b
	jp	PO, 00134$
	xor	a, #0x80
00134$:
	jp	M, 00108$
	C$resources.c$639$5_0$375	= .
	.globl	C$resources.c$639$5_0$375
;src/resources/resources.c:639: SMS_setTile(0);
	ld	hl, #0x0000
	rst	#0x18
	C$resources.c$638$4_0$374	= .
	.globl	C$resources.c$638$4_0$374
;src/resources/resources.c:638: for(int i=DIALOG_BOX_FIRST_COLUMN;i<=DIALOG_BOX_LAST_COLUMN;i++) {            
	inc	bc
	jr	00104$
00108$:
	C$resources.c$636$2_0$372	= .
	.globl	C$resources.c$636$2_0$372
;src/resources/resources.c:636: for(int j=DIALOG_BOX_FIRST_ROW;j<=DIALOG_BOX_LAST_ROW;j++) {
	inc	de
	C$resources.c$642$2_0$372	= .
	.globl	C$resources.c$642$2_0$372
;src/resources/resources.c:642: }
	C$resources.c$642$2_0$372	= .
	.globl	C$resources.c$642$2_0$372
	XG$create_dialog_box$0$0	= .
	.globl	XG$create_dialog_box$0$0
	jr	00107$
	G$update_dialog_box$0$0	= .
	.globl	G$update_dialog_box$0$0
	C$resources.c$644$2_0$377	= .
	.globl	C$resources.c$644$2_0$377
;src/resources/resources.c:644: void update_dialog_box(u8 currentLastLine, u8 upDown, u8 textNumber) {
;	---------------------------------
; Function update_dialog_box
; ---------------------------------
_update_dialog_box::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	push	af
	dec	sp
	ld	-1 (ix), a
	C$resources.c$648$4_0$381	= .
	.globl	C$resources.c$648$4_0$381
;src/resources/resources.c:648: while(writingLine < DIALOG_BOX_NUMBER_LINES) {
	dec	l
	ld	a, #0x01
	jr	Z, 00142$
	xor	a, a
00142$:
	ld	-7 (ix), a
	ld	a, 4 (ix)
	add	a, #<(_long_texts_size)
	ld	e, a
	ld	a, #0x00
	adc	a, #>(_long_texts_size)
	ld	d, a
	ld	b, #0x00
00110$:
	ld	a, b
	sub	a, #0x04
	jp	NC, 00113$
	C$resources.c$650$2_0$378	= .
	.globl	C$resources.c$650$2_0$378
;src/resources/resources.c:650: if(writingLine < DIALOG_BOX_NUMBER_LINES - currentLastLine) {
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x04
	sub	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	sbc	a, a
	sub	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	-3 (ix), b
	ld	-2 (ix), #0x00
	C$resources.c$651$1_0$377	= .
	.globl	C$resources.c$651$1_0$377
;src/resources/resources.c:651: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	ld	a, b
	add	a, a
	ld	c, a
	add	a, #0x0f
	ld	-6 (ix), a
	C$resources.c$652$1_0$377	= .
	.globl	C$resources.c$652$1_0$377
;src/resources/resources.c:652: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	ld	a, c
	add	a, #0x10
	ld	c, a
	C$resources.c$650$2_0$378	= .
	.globl	C$resources.c$650$2_0$378
;src/resources/resources.c:650: if(writingLine < DIALOG_BOX_NUMBER_LINES - currentLastLine) {
	ld	a, -3 (ix)
	sub	a, l
	ld	a, -2 (ix)
	sbc	a, h
	jp	PO, 00143$
	xor	a, #0x80
00143$:
	jp	P, 00108$
	C$resources.c$651$3_0$379	= .
	.globl	C$resources.c$651$3_0$379
;src/resources/resources.c:651: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	push	bc
	push	de
	ld	hl, #___str_10
	push	hl
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
	C$resources.c$652$3_0$379	= .
	.globl	C$resources.c$652$3_0$379
;src/resources/resources.c:652: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	push	bc
	push	de
	ld	hl, #___str_10
	push	hl
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
	jp	00109$
00108$:
	C$resources.c$654$3_0$380	= .
	.globl	C$resources.c$654$3_0$380
;src/resources/resources.c:654: dialogLine = writingLine - DIALOG_BOX_NUMBER_LINES + currentLastLine;
	ld	a, b
	add	a, #0xfc
	ld	l, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	add	a, l
	C$resources.c$655$3_0$380	= .
	.globl	C$resources.c$655$3_0$380
;src/resources/resources.c:655: current_resource_bank = CONSTANT_DATA_BANK;
	ld	hl, #_current_resource_bank
	ld	(hl), #0x02
	C$resources.c$656$3_0$380	= .
	.globl	C$resources.c$656$3_0$380
;src/resources/resources.c:656: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$resources.c$657$3_0$380	= .
	.globl	C$resources.c$657$3_0$380
;src/resources/resources.c:657: if(dialogLine < long_texts_size[textNumber]) {
	push	af
	ld	a, (de)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	pop	af
	cp	a, l
	jp	NC, 00105$
	C$resources.c$659$1_0$377	= .
	.globl	C$resources.c$659$1_0$377
;src/resources/resources.c:659: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, (u8 *)long_texts[textNumber][dialogLine]);
	ld	l, 4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	-3 (ix), a
	ld	-2 (ix), #0x00
	add	hl, hl
	ld	-5 (ix), l
	ld	-4 (ix), h
	sla	-3 (ix)
	rl	-2 (ix)
	C$resources.c$658$4_0$381	= .
	.globl	C$resources.c$658$4_0$381
;src/resources/resources.c:658: if(upDown == 1) { //UP    
	ld	a, -7 (ix)
	or	a, a
	jr	Z, 00102$
	C$resources.c$659$5_0$382	= .
	.globl	C$resources.c$659$5_0$382
;src/resources/resources.c:659: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, (u8 *)long_texts[textNumber][dialogLine]);
	ld	a, #<(_long_texts)
	add	a, -5 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_long_texts)
	adc	a, -4 (ix)
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	add	a, -3 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, h
	adc	a, -2 (ix)
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	push	bc
	push	de
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	hl
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
	C$resources.c$660$5_0$382	= .
	.globl	C$resources.c$660$5_0$382
;src/resources/resources.c:660: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	push	bc
	push	de
	ld	hl, #___str_10
	push	hl
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
	jr	00109$
00102$:
	C$resources.c$662$5_0$383	= .
	.globl	C$resources.c$662$5_0$383
;src/resources/resources.c:662: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	push	bc
	push	de
	ld	hl, #___str_10
	push	hl
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
	C$resources.c$663$5_0$383	= .
	.globl	C$resources.c$663$5_0$383
;src/resources/resources.c:663: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, (u8 *)long_texts[textNumber][dialogLine]);
	ld	a, -5 (ix)
	add	a, #<(_long_texts)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -4 (ix)
	adc	a, #>(_long_texts)
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	add	a, -3 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, h
	adc	a, -2 (ix)
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	push	bc
	push	de
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	hl
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
	jr	00109$
00105$:
	C$resources.c$666$4_0$384	= .
	.globl	C$resources.c$666$4_0$384
;src/resources/resources.c:666: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	push	bc
	push	de
	ld	hl, #___str_10
	push	hl
	ld	l, -6 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
	C$resources.c$667$4_0$384	= .
	.globl	C$resources.c$667$4_0$384
;src/resources/resources.c:667: print_string(DIALOG_BOX_FIRST_COLUMN, DIALOG_BOX_FIRST_ROW + writingLine*2 + 1, "]]]]]]]]]]]]]]]]]]]]]]]]]]]^");
	push	bc
	push	de
	ld	hl, #___str_10
	push	hl
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	pop	de
	pop	bc
00109$:
	C$resources.c$671$2_0$378	= .
	.globl	C$resources.c$671$2_0$378
;src/resources/resources.c:671: writingLine++;
	inc	b
	jp	00110$
00113$:
	C$resources.c$673$1_0$377	= .
	.globl	C$resources.c$673$1_0$377
;src/resources/resources.c:673: }
	ld	sp, ix
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
Fresources$__str_10$0_0$0 == .
___str_10:
	.ascii "]]]]]]]]]]]]]]]]]]]]]]]]]]]^"
	.db 0x00
	G$load_tiles_in_batches$0$0	= .
	.globl	G$load_tiles_in_batches$0$0
	C$resources.c$675$1_0$386	= .
	.globl	C$resources.c$675$1_0$386
;src/resources/resources.c:675: void load_tiles_in_batches(const u8 *data, u16 position, u16 size) {
;	---------------------------------
; Function load_tiles_in_batches
; ---------------------------------
_load_tiles_in_batches::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	iy, #-14
	add	iy, sp
	ld	sp, iy
	ld	-8 (ix), l
	ld	-7 (ix), h
	ld	-6 (ix), e
	ld	-5 (ix), d
	C$resources.c$677$1_0$386	= .
	.globl	C$resources.c$677$1_0$386
;src/resources/resources.c:677: while(size>BYTES_PER_FRAME) {
	xor	a, a
	ld	-4 (ix), a
	ld	-3 (ix), a
	ld	a, 4 (ix)
	ld	-2 (ix), a
	ld	a, 5 (ix)
	ld	-1 (ix), a
00101$:
	C$resources.c$678$1_0$386	= .
	.globl	C$resources.c$678$1_0$386
;src/resources/resources.c:678: SMS_loadTiles(data+currentByte, position,BYTES_PER_FRAME);
	ld	a, -4 (ix)
	add	a, -8 (ix)
	ld	-10 (ix), a
	ld	a, -3 (ix)
	adc	a, -7 (ix)
	ld	-9 (ix), a
	ld	a, -6 (ix)
	ld	-14 (ix), a
	ld	a, -5 (ix)
	ld	-13 (ix), a
	ld	b, #0x05
00117$:
	sla	-14 (ix)
	rl	-13 (ix)
	djnz	00117$
	ld	a, -10 (ix)
	ld	-12 (ix), a
	ld	a, -9 (ix)
	ld	-11 (ix), a
	ld	a, -14 (ix)
	ld	-10 (ix), a
	ld	a, -13 (ix)
	or	a, #0x40
	ld	-9 (ix), a
	C$resources.c$677$1_0$386	= .
	.globl	C$resources.c$677$1_0$386
;src/resources/resources.c:677: while(size>BYTES_PER_FRAME) {
	ld	a, #0x40
	cp	a, -2 (ix)
	ld	a, #0x01
	sbc	a, -1 (ix)
	jr	NC, 00103$
	C$resources.c$678$2_0$387	= .
	.globl	C$resources.c$678$2_0$387
;src/resources/resources.c:678: SMS_loadTiles(data+currentByte, position,BYTES_PER_FRAME);
	ld	hl, #0x0140
	push	hl
	ld	e, -12 (ix)
	ld	d, -11 (ix)
	ld	l, -10 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -9 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_VRAMmemcpy
	C$resources.c$679$2_0$387	= .
	.globl	C$resources.c$679$2_0$387
;src/resources/resources.c:679: currentByte = currentByte + BYTES_PER_FRAME;
	ld	a, -4 (ix)
	add	a, #0x40
	ld	-4 (ix), a
	ld	a, -3 (ix)
	adc	a, #0x01
	ld	-3 (ix), a
	C$resources.c$680$2_0$387	= .
	.globl	C$resources.c$680$2_0$387
;src/resources/resources.c:680: size = size-BYTES_PER_FRAME;
	ld	a, -2 (ix)
	add	a, #0xc0
	ld	-2 (ix), a
	ld	a, -1 (ix)
	adc	a, #0xfe
	ld	-1 (ix), a
	C$resources.c$681$2_0$387	= .
	.globl	C$resources.c$681$2_0$387
;src/resources/resources.c:681: position = position + TILES_PER_FRAME;        
	ld	a, -6 (ix)
	add	a, #0x0a
	ld	-6 (ix), a
	jr	NC, 00118$
	inc	-5 (ix)
00118$:
	C$resources.c$682$2_0$387	= .
	.globl	C$resources.c$682$2_0$387
;src/resources/resources.c:682: waitForFrame();
	call	_waitForFrame
	jp	00101$
00103$:
	C$resources.c$684$1_0$386	= .
	.globl	C$resources.c$684$1_0$386
;src/resources/resources.c:684: SMS_loadTiles(data+currentByte, position,size);
	ld	l, -2 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -1 (ix)
;	spillPairReg hl
;	spillPairReg hl
	push	hl
	ld	e, -12 (ix)
	ld	d, -11 (ix)
	ld	l, -10 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -9 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	_SMS_VRAMmemcpy
	C$resources.c$685$1_0$386	= .
	.globl	C$resources.c$685$1_0$386
;src/resources/resources.c:685: waitForFrame();
	call	_waitForFrame
	C$resources.c$686$1_0$386	= .
	.globl	C$resources.c$686$1_0$386
;src/resources/resources.c:686: }
	ld	sp, ix
	pop	ix
	pop	hl
	pop	af
	jp	(hl)
	G$print_score_value$0$0	= .
	.globl	G$print_score_value$0$0
	C$resources.c$736$1_0$389	= .
	.globl	C$resources.c$736$1_0$389
;src/resources/resources.c:736: void print_score_value(u8 x, u8 y, u16 number) {
;	---------------------------------
; Function print_score_value
; ---------------------------------
_print_score_value::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	dec	sp
	ld	c, a
	C$resources.c$738$1_0$389	= .
	.globl	C$resources.c$738$1_0$389
;src/resources/resources.c:738: SMS_setNextTileatXY(x,y);
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$resources.c$739$1_0$389	= .
	.globl	C$resources.c$739$1_0$389
;src/resources/resources.c:739: tmpTenThousands = number / 10000;
	ld	de, #0x2710
	ld	l, 4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, 5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	call	__divuint
	C$resources.c$740$1_0$389	= .
	.globl	C$resources.c$740$1_0$389
;src/resources/resources.c:740: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTenThousands);
	ld	-3 (ix), e
	ld	c, e
	ld	b, #0x00
	ld	hl, #0x0018
	add	hl, bc
	set	3, h
	rst	#0x18
	C$resources.c$741$1_0$389	= .
	.globl	C$resources.c$741$1_0$389
;src/resources/resources.c:741: tmpThousands = (number - (tmpTenThousands * 10000)) / 1000;
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, 4 (ix)
	sub	a, l
	ld	c, a
	ld	a, 5 (ix)
	sbc	a, h
	ld	b, a
	push	bc
	ld	de, #0x03e8
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	call	__divuint
	pop	bc
	C$resources.c$742$1_0$389	= .
	.globl	C$resources.c$742$1_0$389
;src/resources/resources.c:742: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpThousands);
	ld	-2 (ix), e
	ld	d, #0x00
	ld	hl, #0x0018
	add	hl, de
	set	3, h
	rst	#0x18
	C$resources.c$743$1_0$389	= .
	.globl	C$resources.c$743$1_0$389
;src/resources/resources.c:743: tmpHundreds = (number - (tmpTenThousands * 10000) - (tmpThousands*1000)) / 100;
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	sub	a, l
	ld	c, a
	ld	a, b
	sbc	a, h
	ld	b, a
	push	bc
	ld	de, #0x0064
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	call	__divuint
	pop	bc
	C$resources.c$744$1_0$389	= .
	.globl	C$resources.c$744$1_0$389
;src/resources/resources.c:744: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpHundreds);
	ld	-1 (ix), e
	ld	d, #0x00
	ld	hl, #0x0018
	add	hl, de
	set	3, h
	rst	#0x18
	C$resources.c$745$1_0$389	= .
	.globl	C$resources.c$745$1_0$389
;src/resources/resources.c:745: tmpTens = (number - (tmpTenThousands * 10000) - (tmpThousands*1000) - (tmpHundreds * 100)) / 10;
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, hl
	ld	a, c
	sub	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	sbc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	de, #0x000a
	call	__divuint
	C$resources.c$746$1_0$389	= .
	.globl	C$resources.c$746$1_0$389
;src/resources/resources.c:746: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTens);
	ld	b,e
	ld	d, #0x00
	ld	hl, #0x0018
	add	hl, de
	set	3, h
	rst	#0x18
	C$resources.c$747$1_0$389	= .
	.globl	C$resources.c$747$1_0$389
;src/resources/resources.c:747: tmpUnits = (number - (tmpTenThousands * 10000) - (tmpThousands*1000) - (tmpHundreds * 100) - (tmpTens * 10));
	ld	c, 4 (ix)
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	sub	a, l
	ld	c, a
	ld	a, -2 (ix)
	ld	e, a
	add	a, a
	add	a, a
	add	a, a
	sub	a, e
	add	a, a
	add	a, a
	add	a, e
	add	a, a
	add	a, a
	add	a, a
	ld	e, a
	ld	a, c
	sub	a, e
	ld	c, a
	ld	e, -1 (ix)
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, de
	add	hl, hl
	add	hl, hl
	ld	a, c
	sub	a, l
	ld	c, a
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	e, l
	add	hl, hl
	add	hl, hl
	add	hl, de
	add	hl, hl
	ld	a, c
	sub	a, l
	C$resources.c$748$1_0$389	= .
	.globl	C$resources.c$748$1_0$389
;src/resources/resources.c:748: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpUnits);
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
	ld	de, #0x0018
	add	hl, de
	set	3, h
	rst	#0x18
	C$resources.c$749$1_0$389	= .
	.globl	C$resources.c$749$1_0$389
;src/resources/resources.c:749: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS);
	ld	hl, #0x0818
	rst	#0x18
	C$resources.c$750$1_0$389	= .
	.globl	C$resources.c$750$1_0$389
;src/resources/resources.c:750: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS);
	ld	hl, #0x0818
	rst	#0x18
	C$resources.c$751$1_0$389	= .
	.globl	C$resources.c$751$1_0$389
;src/resources/resources.c:751: }
	ld	sp, ix
	pop	ix
	pop	hl
	pop	af
	jp	(hl)
	G$print_level_number$0$0	= .
	.globl	G$print_level_number$0$0
	C$resources.c$753$1_0$391	= .
	.globl	C$resources.c$753$1_0$391
;src/resources/resources.c:753: void print_level_number(u8 x, u8 y, u8 level) {
;	---------------------------------
; Function print_level_number
; ---------------------------------
_print_level_number::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	c, a
	ld	b, l
	C$resources.c$755$1_0$391	= .
	.globl	C$resources.c$755$1_0$391
;src/resources/resources.c:755: print_string(x,y,"LEVEL^");
	push	bc
	ld	hl, #___str_11
	push	hl
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_print_string
	pop	bc
	C$resources.c$756$1_0$391	= .
	.globl	C$resources.c$756$1_0$391
;src/resources/resources.c:756: SMS_setNextTileatXY(x+6,y);
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, c
	add	a, #0x06
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	C$resources.c$757$1_0$391	= .
	.globl	C$resources.c$757$1_0$391
;src/resources/resources.c:757: tmpTens = level / 10;
	ld	l, 4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	de, #0x000a
	call	__divsint
	C$resources.c$758$1_0$391	= .
	.globl	C$resources.c$758$1_0$391
;src/resources/resources.c:758: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpTens);
	ld	c, e
	ld	b, #0x00
	ld	hl, #0x0018
	add	hl, bc
	set	3, h
	rst	#0x18
	C$resources.c$759$1_0$391	= .
	.globl	C$resources.c$759$1_0$391
;src/resources/resources.c:759: tmpUnits = (level - (tmpTens * 10));
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, de
	add	hl, hl
	ld	a, 4 (ix)
	sub	a, l
	C$resources.c$760$1_0$391	= .
	.globl	C$resources.c$760$1_0$391
;src/resources/resources.c:760: SMS_setTile(TILE_USE_SPRITE_PALETTE|DIGITS_BASE_ADDRESS + tmpUnits);
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
	ld	de, #0x0018
	add	hl, de
	set	3, h
	rst	#0x18
	C$resources.c$761$1_0$391	= .
	.globl	C$resources.c$761$1_0$391
;src/resources/resources.c:761: }
	pop	ix
	pop	hl
	inc	sp
	jp	(hl)
Fresources$__str_11$0_0$0 == .
___str_11:
	.ascii "LEVEL^"
	.db 0x00
	G$print_string$0$0	= .
	.globl	G$print_string$0$0
	C$resources.c$763$1_0$393	= .
	.globl	C$resources.c$763$1_0$393
;src/resources/resources.c:763: void print_string(u8 x, u8 y, u8* string) {
;	---------------------------------
; Function print_string
; ---------------------------------
_print_string::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	dec	sp
	ld	-2 (ix), a
	ld	-3 (ix), l
	C$resources.c$765$1_0$393	= .
	.globl	C$resources.c$765$1_0$393
;src/resources/resources.c:765: u8 caracter = string[i]-'A';
	ld	e, 4 (ix)
	ld	d, 5 (ix)
	ld	a, (de)
	add	a, #0xbf
	ld	c, a
	C$resources.c$770$1_0$393	= .
	.globl	C$resources.c$770$1_0$393
;src/resources/resources.c:770: while(caracter!=29) { //^ character
	ld	-1 (ix), #0x00
00104$:
	ld	a, c
	sub	a, #0x1d
	jr	Z, 00107$
	C$resources.c$771$3_0$395	= .
	.globl	C$resources.c$771$3_0$395
;src/resources/resources.c:771: SMS_setTileatXY(x+i,y,TILE_USE_SPRITE_PALETTE|CHARS_BASE_ADDRESS+caracter);
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, -2 (ix)
	add	a, -1 (ix)
	ld	b, #0x00
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	rst	#0x08
	pop	bc
	ld	b, #0x00
	ld	hl, #0x0022
	add	hl, bc
	set	3, h
	rst	#0x18
	C$resources.c$772$2_0$394	= .
	.globl	C$resources.c$772$2_0$394
;src/resources/resources.c:772: i++;
	inc	-1 (ix)
	C$resources.c$773$2_0$394	= .
	.globl	C$resources.c$773$2_0$394
;src/resources/resources.c:773: caracter = string[i]-'A';
	ld	l, -1 (ix)
	ld	h, #0x00
	add	hl, de
	ld	a, (hl)
	add	a, #0xbf
	ld	c, a
	jr	00104$
00107$:
	C$resources.c$775$1_0$393	= .
	.globl	C$resources.c$775$1_0$393
;src/resources/resources.c:775: }
	ld	sp, ix
	pop	ix
	pop	hl
	pop	af
	jp	(hl)
	.area _CODE
	.area _INITIALIZER
Fresources$__xinit_tiles$0_0$0 == .
__xinit__tiles:
	.dw _logotiles_psgcompr
	.dw _devkitsmstiles_psgcompr
	.dw _simplefont_psgcompr
	.dw _intro1tiles_psgcompr
	.dw _intro2tiles_bin
	.dw _intro3tiles_bin
	.dw _titlescreentiles_psgcompr
	.dw _creditsscreentiles_psgcompr
	.dw _ending1tiles_psgcompr
Fresources$__xinit_tilemaps$0_0$0 == .
__xinit__tilemaps:
	.dw _logotilemap_stmcompr
	.dw _devkitsmstilemap_stmcompr
	.dw _logotilemap_stmcompr
	.dw _intro1tilemap_stmcompr
	.dw _intro2tilemap_stmcompr
	.dw _intro3tilemap_stmcompr
	.dw _titlescreentilemap_stmcompr
	.dw _creditsscreentilemap_stmcompr
	.dw _ending1tilemap_stmcompr
Fresources$__xinit_palettes$0_0$0 == .
__xinit__palettes:
	.dw _default_pieces_palette
	.dw _logopalette_bin
	.dw _devkitsmspalette_bin
	.dw _intro1palette_bin
	.dw _intro2palette_bin
	.dw _intro3palette_bin
	.dw _title_first_palette
	.dw _titlescreenpalette_bin
	.dw _white_palette
	.dw _creditsscreenpalette_bin
	.dw _default_pieces_palette
	.dw _ending1palette_bin
	.dw _ending2palette_bin
	.area _CABS (ABS)
