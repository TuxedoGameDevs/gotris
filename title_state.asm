;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module title_state
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _PSGSFXStop
	.globl _PSGStop
	.globl _waitForFrame
	.globl _getButtonsPressed
	.globl _update_input
	.globl _clear_input
	.globl _transition_to_state
	.globl _play_sample
	.globl _play_fx
	.globl _play_song
	.globl _start_fadeout_music
	.globl _start_fadein_music
	.globl _clear_scroll_title_screen
	.globl _clear_push_start
	.globl _write_push_start
	.globl _scroll_title_screen
	.globl _enable_scroll_title_screen
	.globl _init_scroll_title_screen
	.globl _change_title_priority
	.globl _sprite_fade_in
	.globl _background_fade_in
	.globl _general_fade_out
	.globl _load_tilemap
	.globl _load_tileset
	.globl _title_state_status
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _title_state_start
	.globl _title_state_update
	.globl _title_state_stop
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
G$title_state_status$0_0$0==.
_title_state_status::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$title_state_start$0$0	= .
	.globl	G$title_state_start$0$0
	C$title_state.c$16$0_0$209	= .
	.globl	C$title_state.c$16$0_0$209
;src/states/title_state.c:16: void title_state_start(void) {
;	---------------------------------
; Function title_state_start
; ---------------------------------
_title_state_start::
	C$title_state.c$17$1_0$209	= .
	.globl	C$title_state.c$17$1_0$209
;src/states/title_state.c:17: load_tileset(TITLE_ASSETS, BACKGROUND_BASE_ADRESS);
	ld	l, #0x3f
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x06
	call	_load_tileset
	C$title_state.c$18$1_0$209	= .
	.globl	C$title_state.c$18$1_0$209
;src/states/title_state.c:18: load_tilemap(TITLE_ASSETS);
	ld	a, #0x06
	call	_load_tilemap
	C$title_state.c$19$1_0$209	= .
	.globl	C$title_state.c$19$1_0$209
;src/states/title_state.c:19: change_title_priority();
	call	_change_title_priority
	C$title_state.c$20$1_0$209	= .
	.globl	C$title_state.c$20$1_0$209
;src/states/title_state.c:20: init_scroll_title_screen();
	call	_init_scroll_title_screen
	C$title_state.c$21$1_0$209	= .
	.globl	C$title_state.c$21$1_0$209
;src/states/title_state.c:21: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$title_state.c$22$1_0$209	= .
	.globl	C$title_state.c$22$1_0$209
;src/states/title_state.c:22: title_state_status = 0;
	ld	iy, #_title_state_status
	ld	0 (iy), #0x00
	C$title_state.c$23$1_0$209	= .
	.globl	C$title_state.c$23$1_0$209
;src/states/title_state.c:23: clear_input();
	C$title_state.c$24$1_0$209	= .
	.globl	C$title_state.c$24$1_0$209
;src/states/title_state.c:24: }
	C$title_state.c$24$1_0$209	= .
	.globl	C$title_state.c$24$1_0$209
	XG$title_state_start$0$0	= .
	.globl	XG$title_state_start$0$0
	jp	_clear_input
	G$title_state_update$0$0	= .
	.globl	G$title_state_update$0$0
	C$title_state.c$26$1_0$211	= .
	.globl	C$title_state.c$26$1_0$211
;src/states/title_state.c:26: void title_state_update(void) {
;	---------------------------------
; Function title_state_update
; ---------------------------------
_title_state_update::
	C$title_state.c$27$1_0$211	= .
	.globl	C$title_state.c$27$1_0$211
;src/states/title_state.c:27: if(title_state_status == 0) {
	ld	a, (_title_state_status+0)
	or	a, a
	jr	NZ, 00129$
	C$title_state.c$28$2_0$212	= .
	.globl	C$title_state.c$28$2_0$212
;src/states/title_state.c:28: background_fade_in(TITLE_FIRST_PALETTE,2);
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x06
	call	_background_fade_in
	C$title_state.c$29$2_0$212	= .
	.globl	C$title_state.c$29$2_0$212
;src/states/title_state.c:29: title_state_status = 1;
	ld	hl, #_title_state_status
	ld	(hl), #0x01
	ret
00129$:
	C$title_state.c$30$1_0$211	= .
	.globl	C$title_state.c$30$1_0$211
;src/states/title_state.c:30: } else if(title_state_status == 1) {
	ld	a, (_title_state_status+0)
	dec	a
	jr	NZ, 00126$
	C$title_state.c$31$2_0$213	= .
	.globl	C$title_state.c$31$2_0$213
;src/states/title_state.c:31: background_fade_in(TITLE_PALETTE,3);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x07
	call	_background_fade_in
	C$title_state.c$32$2_0$213	= .
	.globl	C$title_state.c$32$2_0$213
;src/states/title_state.c:32: sprite_fade_in(FONT_PALETTE,2);
	ld	l, #0x02
;	spillPairReg hl
;	spillPairReg hl
	xor	a, a
	call	_sprite_fade_in
	C$title_state.c$33$2_0$213	= .
	.globl	C$title_state.c$33$2_0$213
;src/states/title_state.c:33: background_fade_in(WHITE_PALETTE,0);
	ld	l, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x08
	call	_background_fade_in
	C$title_state.c$34$2_0$213	= .
	.globl	C$title_state.c$34$2_0$213
;src/states/title_state.c:34: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$title_state.c$35$2_0$213	= .
	.globl	C$title_state.c$35$2_0$213
;src/states/title_state.c:35: title_state_status = 2;
	ld	hl, #_title_state_status
	ld	(hl), #0x02
	ret
00126$:
	C$title_state.c$36$1_0$211	= .
	.globl	C$title_state.c$36$1_0$211
;src/states/title_state.c:36: } else if(title_state_status == 2) {
	ld	a, (_title_state_status+0)
	sub	a, #0x02
	jr	NZ, 00123$
	C$title_state.c$37$2_0$214	= .
	.globl	C$title_state.c$37$2_0$214
;src/states/title_state.c:37: if(frame_cnt > XS_PERIOD) {
	ld	a, #0x18
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jp	NC,_waitForFrame
	C$title_state.c$38$3_0$215	= .
	.globl	C$title_state.c$38$3_0$215
;src/states/title_state.c:38: background_fade_in(TITLE_PALETTE,3);
	ld	l, #0x03
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x07
	call	_background_fade_in
	C$title_state.c$39$3_0$215	= .
	.globl	C$title_state.c$39$3_0$215
;src/states/title_state.c:39: play_sample(THUNDER_SAMPLE);
	xor	a, a
	call	_play_sample
	C$title_state.c$40$3_0$215	= .
	.globl	C$title_state.c$40$3_0$215
;src/states/title_state.c:40: play_sample(GOTRIS_SAMPLE);
	ld	a, #0x01
	call	_play_sample
	C$title_state.c$41$3_0$215	= .
	.globl	C$title_state.c$41$3_0$215
;src/states/title_state.c:41: enable_scroll_title_screen();    
	call	_enable_scroll_title_screen
	C$title_state.c$42$3_0$215	= .
	.globl	C$title_state.c$42$3_0$215
;src/states/title_state.c:42: start_fadein_music();
	call	_start_fadein_music
	C$title_state.c$43$3_0$215	= .
	.globl	C$title_state.c$43$3_0$215
;src/states/title_state.c:43: play_song(TITLE_OST);
	ld	a, #0x01
	call	_play_song
	C$title_state.c$44$3_0$215	= .
	.globl	C$title_state.c$44$3_0$215
;src/states/title_state.c:44: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$title_state.c$45$3_0$215	= .
	.globl	C$title_state.c$45$3_0$215
;src/states/title_state.c:45: title_state_status = 3;
	ld	iy, #_title_state_status
	ld	0 (iy), #0x03
	C$title_state.c$47$2_0$214	= .
	.globl	C$title_state.c$47$2_0$214
;src/states/title_state.c:47: waitForFrame();
	jp	_waitForFrame
00123$:
	C$title_state.c$48$1_0$211	= .
	.globl	C$title_state.c$48$1_0$211
;src/states/title_state.c:48: } else if(title_state_status == 3) {
	ld	a, (_title_state_status+0)
	sub	a, #0x03
	jr	NZ, 00120$
	C$title_state.c$49$2_0$216	= .
	.globl	C$title_state.c$49$2_0$216
;src/states/title_state.c:49: if (!(getButtonsPressed() & KEY_ONE_P1) && !(getButtonsPressed() & KEY_ONE_P2)) {
	call	_getButtonsPressed
	bit	4, e
	jr	NZ, 00111$
	call	_getButtonsPressed
	bit	2, d
	jr	NZ, 00111$
	C$title_state.c$50$3_0$217	= .
	.globl	C$title_state.c$50$3_0$217
;src/states/title_state.c:50: if(frame_cnt > XXL_PERIOD) {
	ld	a, #0xe0
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x01
	sbc	a, 1 (iy)
	jr	NC, 00104$
	C$title_state.c$51$4_0$218	= .
	.globl	C$title_state.c$51$4_0$218
;src/states/title_state.c:51: clear_push_start();
	call	_clear_push_start
	C$title_state.c$52$4_0$218	= .
	.globl	C$title_state.c$52$4_0$218
;src/states/title_state.c:52: start_fadeout_music();
	call	_start_fadeout_music
	C$title_state.c$53$4_0$218	= .
	.globl	C$title_state.c$53$4_0$218
;src/states/title_state.c:53: general_fade_out();
	call	_general_fade_out
	C$title_state.c$54$4_0$218	= .
	.globl	C$title_state.c$54$4_0$218
;src/states/title_state.c:54: title_state_status = 4;
	ld	hl, #_title_state_status
	ld	(hl), #0x04
00104$:
	C$title_state.c$56$3_0$217	= .
	.globl	C$title_state.c$56$3_0$217
;src/states/title_state.c:56: update_input();
	call	_update_input
	C$title_state.c$57$3_0$217	= .
	.globl	C$title_state.c$57$3_0$217
;src/states/title_state.c:57: waitForFrame();
	call	_waitForFrame
	C$title_state.c$58$3_0$217	= .
	.globl	C$title_state.c$58$3_0$217
;src/states/title_state.c:58: scroll_title_screen();
	call	_scroll_title_screen
	C$title_state.c$59$3_0$217	= .
	.globl	C$title_state.c$59$3_0$217
;src/states/title_state.c:59: if(frame_cnt > M_PERIOD) {
	ld	a, #0x78
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	ret	NC
	C$title_state.c$60$4_0$219	= .
	.globl	C$title_state.c$60$4_0$219
;src/states/title_state.c:60: write_push_start(frame_cnt);
	ld	hl, (_frame_cnt)
	jp	_write_push_start
00111$:
	C$title_state.c$63$3_0$220	= .
	.globl	C$title_state.c$63$3_0$220
;src/states/title_state.c:63: play_fx(START_FX);
	xor	a, a
	call	_play_fx
	C$title_state.c$64$3_0$220	= .
	.globl	C$title_state.c$64$3_0$220
;src/states/title_state.c:64: if(getButtonsPressed() & KEY_ONE_P2) {
	call	_getButtonsPressed
	bit	2, d
	jr	Z, 00108$
	C$title_state.c$65$4_0$221	= .
	.globl	C$title_state.c$65$4_0$221
;src/states/title_state.c:65: number_human_players = 2;
	ld	hl, #_number_human_players
	ld	(hl), #0x02
	jr	00109$
00108$:
	C$title_state.c$67$4_0$222	= .
	.globl	C$title_state.c$67$4_0$222
;src/states/title_state.c:67: number_human_players = 1;
	ld	hl, #_number_human_players
	ld	(hl), #0x01
00109$:
	C$title_state.c$69$3_0$220	= .
	.globl	C$title_state.c$69$3_0$220
;src/states/title_state.c:69: clear_input();
	call	_clear_input
	C$title_state.c$70$3_0$220	= .
	.globl	C$title_state.c$70$3_0$220
;src/states/title_state.c:70: clear_push_start();
	call	_clear_push_start
	C$title_state.c$71$3_0$220	= .
	.globl	C$title_state.c$71$3_0$220
;src/states/title_state.c:71: rand_index = frame_cnt;
	ld	a, (_frame_cnt+0)
	ld	(_rand_index+0), a
	C$title_state.c$73$3_0$220	= .
	.globl	C$title_state.c$73$3_0$220
;src/states/title_state.c:73: title_state_status = 5;
	ld	hl, #_title_state_status
	ld	(hl), #0x05
	C$title_state.c$74$3_0$220	= .
	.globl	C$title_state.c$74$3_0$220
;src/states/title_state.c:74: start_fadeout_music();
	call	_start_fadeout_music
	C$title_state.c$75$3_0$220	= .
	.globl	C$title_state.c$75$3_0$220
;src/states/title_state.c:75: general_fade_out();
	jp	_general_fade_out
00120$:
	C$title_state.c$77$1_0$211	= .
	.globl	C$title_state.c$77$1_0$211
;src/states/title_state.c:77: } else if(title_state_status == 4) {
	ld	a, (_title_state_status+0)
	sub	a, #0x04
	jr	NZ, 00117$
	C$title_state.c$78$2_0$223	= .
	.globl	C$title_state.c$78$2_0$223
;src/states/title_state.c:78: transition_to_state(DEMO_STATE);
	ld	a, #0x07
	jp	_transition_to_state
00117$:
	C$title_state.c$79$1_0$211	= .
	.globl	C$title_state.c$79$1_0$211
;src/states/title_state.c:79: } else if(title_state_status == 5) {
	ld	a, (_title_state_status+0)
	sub	a, #0x05
	ret	NZ
	C$title_state.c$80$2_0$224	= .
	.globl	C$title_state.c$80$2_0$224
;src/states/title_state.c:80: transition_to_state(GAME_STATE);
	ld	a, #0x06
	C$title_state.c$82$1_0$211	= .
	.globl	C$title_state.c$82$1_0$211
;src/states/title_state.c:82: }
	C$title_state.c$82$1_0$211	= .
	.globl	C$title_state.c$82$1_0$211
	XG$title_state_update$0$0	= .
	.globl	XG$title_state_update$0$0
	jp	_transition_to_state
	G$title_state_stop$0$0	= .
	.globl	G$title_state_stop$0$0
	C$title_state.c$84$1_0$226	= .
	.globl	C$title_state.c$84$1_0$226
;src/states/title_state.c:84: void title_state_stop(void) {
;	---------------------------------
; Function title_state_stop
; ---------------------------------
_title_state_stop::
	C$title_state.c$85$1_0$226	= .
	.globl	C$title_state.c$85$1_0$226
;src/states/title_state.c:85: clear_scroll_title_screen();
	call	_clear_scroll_title_screen
	C$title_state.c$86$1_0$226	= .
	.globl	C$title_state.c$86$1_0$226
;src/states/title_state.c:86: stop_music();
	call	_PSGStop
	C$title_state.c$87$1_0$226	= .
	.globl	C$title_state.c$87$1_0$226
;src/states/title_state.c:87: stop_fx();
	C$title_state.c$88$1_0$226	= .
	.globl	C$title_state.c$88$1_0$226
;src/states/title_state.c:88: }
	C$title_state.c$88$1_0$226	= .
	.globl	C$title_state.c$88$1_0$226
	XG$title_state_stop$0$0	= .
	.globl	XG$title_state_stop$0$0
	jp	_PSGSFXStop
	.area _CODE
	.area _INITIALIZER
Ftitle_state$__xinit_title_state_status$0_0$0 == .
__xinit__title_state_status:
	.db #0x00	; 0
	.area _CABS (ABS)
