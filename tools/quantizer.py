from PIL import Image
import argparse
import math

color_levels = [0,85,170,255]

def calculateDistance(r,g,b, r2, g2, b2):
	return math.sqrt(math.pow((r-r2),2) + math.pow((g-g2),2) + math.pow((b-b2),2))

def processColor(r,g,b):
	minDistance = 999
	bestr = 0
	bestg = 0
	bestb = 0
	for r2 in color_levels:
		for g2 in color_levels:
			for b2 in color_levels:
				distance = calculateDistance(r,g,b,r2,g2,b2)
				if(distance < minDistance):
					minDistance = distance
					bestr = r2
					bestg = g2
					bestb = b2
	return (bestr, bestg, bestb)

def quantizeImageMethod1(imagefilename, saveimagefilename):
	print("Quantize by method 1")
	try:
		imageObject = Image.open(imagefilename).copy()
	except:
		print("Cannot find file called " + imagefilename)
		exit()
	if imageObject.mode != "P":
		imageObject = imageObject.convert("P")
	originalpalette = imageObject.getpalette()
	newpalette = []
	index = 0
	while((index+2)<len(originalpalette)):
		(newr, newg, newb) = processColor(originalpalette[index], originalpalette[index+1],originalpalette[index+2])
		newpalette.append(newr)
		newpalette.append(newg)
		newpalette.append(newb)
		index = index + 3
	imageObject.putpalette(newpalette)
	imageObject.save(saveimagefilename)


def quantizeImageMethod2(imagefilename, saveimagefilename):
	print("Quantize by method 2")
	try:
		imageObject = Image.open(imagefilename).copy()
	except:
		print("Cannot find file called " + imagefilename)
		exit()
	if imageObject.mode != "RGB":
		imageObject = imageObject.convert("RGB")
	palette = Image.open("palette.png").copy()
	imageObject = imageObject.quantize(colors=16,method=0,palette=palette)
	imageObject.save(saveimagefilename)

def main():
	print("\n****Kusfo's Master System Quantizer***********\n")
	parser = argparse.ArgumentParser(description='Convert images to Master System Palettes')
	parser.add_argument('imagefilename', metavar='F',
	        help='the filename to be quantized')
	parser.add_argument('-o','--outputfile', metavar='o',
	                    help='outputfile for image', default="")
	parser.add_argument('-m','--method', metavar='m',
	                    help='method to quantize', default="1")



	args = parser.parse_args()
	print("processing: " +args.imagefilename)
	if(args.method == "1"):
		quantizeImageMethod1(args.imagefilename, args.outputfile)
	elif(args.method == "2"):
		quantizeImageMethod2(args.imagefilename, args.outputfile)

if __name__ == '__main__':
    main()   