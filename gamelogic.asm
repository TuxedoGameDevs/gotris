;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler 
; Version 4.2.12 #13827 (Linux)
;--------------------------------------------------------
	.module gamelogic
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _draw_line
	.globl _block_line
	.globl _clear_line
	.globl _make_points_float
	.globl _make_pieces_fall
	.globl _destroy_disconnected_pieces
	.globl _compute_disconnected_pieces
	.globl _compute_paths
	.globl _destroy_pieces
	.globl _add_100point_sprite
	.globl _increment_score
	.globl _reverse_pieces
	.globl _fall_pieces
	.globl _fall_bonus_piece
	.globl _generate_bonus_piece
	.globl _fall_square_piece
	.globl _generate_square_piece
	.globl _PSGSFXStop
	.globl _PSGStop
	.globl _init_savesystem
	.globl _deleteElement_Unsafe
	.globl _deleteElementEndList
	.globl _addElementEndList
	.globl _get_rand
	.globl _waitForFrame
	.globl _clear_tilemap
	.globl _init_console
	.globl _decideRotation
	.globl _decideDirection
	.globl _decideNextMove
	.globl _getRotation
	.globl _getDirection
	.globl _update_input
	.globl _clear_input
	.globl _play_sample
	.globl _play_fx
	.globl _play_song
	.globl _start_fadeout_music
	.globl _start_fadein_music
	.globl _print_string
	.globl _print_level_number
	.globl _print_score_value
	.globl _background_to_level_palette
	.globl _restore_fire_colors
	.globl _swap_fire_colors
	.globl _not_connected_pieces_to_normal_palette
	.globl _not_connected_pieces_to_red_palette
	.globl _connected_pieces_to_normal_palette
	.globl _connected_pieces_to_bw_palette
	.globl _clear_cursor_sprites
	.globl _draw_cursor_sprites
	.globl _clear_point_sprites
	.globl _draw_point_sprites
	.globl _draw_congratulations
	.globl _draw_sram_present
	.globl _block_board_position
	.globl _clear_board_position
	.globl _clean_explosion
	.globl _destroy_disconnected
	.globl _mark_as_disconnected
	.globl _compute_path
	.globl _put_bonus_board
	.globl _put_square_pieces_board
	.globl _mark_position_for_change_in_board_rec
	.globl _mark_position_for_change_in_board
	.globl _destroy_piece_board
	.globl _reverse_piece_board
	.globl _put_individual_pieces_board
	.globl _create_individual_pieces_from_board_position
	.globl _create_individual_pieces_from_square
	.globl _delete_bonus_board
	.globl _delete_square_pieces_board
	.globl _delete_individual_pieces_board
	.globl _rotate_inv_square_piece
	.globl _rotate_square_piece
	.globl _clear_suitable_columns
	.globl _SMS_zeroSpritePalette
	.globl _SMS_zeroBGPalette
	.globl _SMS_crt0_RST18
	.globl _SMS_crt0_RST08
	.globl _malloc
	.globl _SMS_SRAM
	.globl _SRAM_bank_to_be_mapped_on_slot2
	.globl _ROM_bank_to_be_mapped_on_slot0
	.globl _ROM_bank_to_be_mapped_on_slot1
	.globl _ROM_bank_to_be_mapped_on_slot2
	.globl _update_player
	.globl _update_level
	.globl _init_game
	.globl _init_gamestatus
	.globl _restore_board
	.globl _decide_next_square
	.globl _end_demo
	.globl _execute_game_logic
	.globl _get_game_actions
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
G$ROM_bank_to_be_mapped_on_slot2$0_0$0 == 0xffff
_ROM_bank_to_be_mapped_on_slot2	=	0xffff
G$ROM_bank_to_be_mapped_on_slot1$0_0$0 == 0xfffe
_ROM_bank_to_be_mapped_on_slot1	=	0xfffe
G$ROM_bank_to_be_mapped_on_slot0$0_0$0 == 0xfffd
_ROM_bank_to_be_mapped_on_slot0	=	0xfffd
G$SRAM_bank_to_be_mapped_on_slot2$0_0$0 == 0xfffc
_SRAM_bank_to_be_mapped_on_slot2	=	0xfffc
G$SMS_SRAM$0_0$0 == 0x8000
_SMS_SRAM	=	0x8000
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
	G$update_player$0$0	= .
	.globl	G$update_player$0$0
	C$gamelogic.c$20$0_0$263	= .
	.globl	C$gamelogic.c$20$0_0$263
;src/engine/gamelogic.c:20: void update_player(void) {
;	---------------------------------
; Function update_player
; ---------------------------------
_update_player::
	C$gamelogic.c$22$1_0$263	= .
	.globl	C$gamelogic.c$22$1_0$263
;src/engine/gamelogic.c:22: if(number_human_players == 2) {
	ld	a, (_number_human_players+0)
	sub	a, #0x02
	jr	NZ, 00114$
	C$gamelogic.c$23$2_0$264	= .
	.globl	C$gamelogic.c$23$2_0$264
;src/engine/gamelogic.c:23: if(current_player == PLAYER_1) { 
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00102$
	C$gamelogic.c$24$3_0$265	= .
	.globl	C$gamelogic.c$24$3_0$265
;src/engine/gamelogic.c:24: current_player = PLAYER_2;
	ld	hl, #_current_player
	ld	(hl), #0x01
	ret
00102$:
	C$gamelogic.c$26$3_0$266	= .
	.globl	C$gamelogic.c$26$3_0$266
;src/engine/gamelogic.c:26: current_player = PLAYER_1;
	ld	hl, #_current_player
	ld	(hl), #0x00
	ret
00114$:
	C$gamelogic.c$28$1_0$263	= .
	.globl	C$gamelogic.c$28$1_0$263
;src/engine/gamelogic.c:28: } else if(number_human_players == 1) {
	ld	a, (_number_human_players+0)
	dec	a
	jr	NZ, 00111$
	C$gamelogic.c$29$2_0$267	= .
	.globl	C$gamelogic.c$29$2_0$267
;src/engine/gamelogic.c:29: if(current_player == PLAYER_1) { 
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00105$
	C$gamelogic.c$30$3_0$268	= .
	.globl	C$gamelogic.c$30$3_0$268
;src/engine/gamelogic.c:30: current_player = PLAYER_CPU_1;
	ld	hl, #_current_player
	ld	(hl), #0x02
	ret
00105$:
	C$gamelogic.c$32$3_0$269	= .
	.globl	C$gamelogic.c$32$3_0$269
;src/engine/gamelogic.c:32: current_player = PLAYER_1;
	ld	hl, #_current_player
	ld	(hl), #0x00
	ret
00111$:
	C$gamelogic.c$35$2_0$270	= .
	.globl	C$gamelogic.c$35$2_0$270
;src/engine/gamelogic.c:35: if(current_player == PLAYER_CPU_1) { 
	ld	a, (_current_player+0)
	sub	a, #0x02
	jr	NZ, 00108$
	C$gamelogic.c$36$3_0$271	= .
	.globl	C$gamelogic.c$36$3_0$271
;src/engine/gamelogic.c:36: current_player = PLAYER_CPU_2;
	ld	hl, #_current_player
	ld	(hl), #0x03
	ret
00108$:
	C$gamelogic.c$38$3_0$272	= .
	.globl	C$gamelogic.c$38$3_0$272
;src/engine/gamelogic.c:38: current_player = PLAYER_CPU_1;
	ld	hl, #_current_player
	ld	(hl), #0x02
	C$gamelogic.c$41$1_0$263	= .
	.globl	C$gamelogic.c$41$1_0$263
;src/engine/gamelogic.c:41: }
	C$gamelogic.c$41$1_0$263	= .
	.globl	C$gamelogic.c$41$1_0$263
	XG$update_player$0$0	= .
	.globl	XG$update_player$0$0
	ret
	G$update_level$0$0	= .
	.globl	G$update_level$0$0
	C$gamelogic.c$43$1_0$274	= .
	.globl	C$gamelogic.c$43$1_0$274
;src/engine/gamelogic.c:43: void update_level(void) {
;	---------------------------------
; Function update_level
; ---------------------------------
_update_level::
	C$gamelogic.c$44$1_0$274	= .
	.globl	C$gamelogic.c$44$1_0$274
;src/engine/gamelogic.c:44: if(level < newLevel) { 
	ld	a, (_level+0)
	ld	iy, #_newLevel
	sub	a, 0 (iy)
	ret	NC
	C$gamelogic.c$45$2_0$275	= .
	.globl	C$gamelogic.c$45$2_0$275
;src/engine/gamelogic.c:45: if(number_human_players == 1 && current_player == PLAYER_CPU_1) { 
	ld	a, (_number_human_players+0)
	dec	a
	ld	a, #0x01
	jr	Z, 00138$
	xor	a, a
00138$:
	ld	c, a
	or	a, a
	jr	Z, 00105$
	ld	a, (_current_player+0)
	sub	a, #0x02
	jr	NZ, 00105$
	C$gamelogic.c$46$3_0$276	= .
	.globl	C$gamelogic.c$46$3_0$276
;src/engine/gamelogic.c:46: game_status = GAME_STATUS_LOOSING_GAME; 
	ld	hl, #_game_status
	ld	(hl), #0x11
	C$gamelogic.c$47$3_0$276	= .
	.globl	C$gamelogic.c$47$3_0$276
;src/engine/gamelogic.c:47: row_index = BOARD_ROWS - 1; 
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$48$3_0$276	= .
	.globl	C$gamelogic.c$48$3_0$276
;src/engine/gamelogic.c:48: stop_music(); 
	call	_PSGStop
	C$gamelogic.c$49$3_0$276	= .
	.globl	C$gamelogic.c$49$3_0$276
;src/engine/gamelogic.c:49: stop_fx(); 
	jp	_PSGSFXStop
00105$:
	C$gamelogic.c$51$3_0$277	= .
	.globl	C$gamelogic.c$51$3_0$277
;src/engine/gamelogic.c:51: level = newLevel; 
	ld	a, (_newLevel+0)
	ld	(_level+0), a
	C$gamelogic.c$52$3_0$277	= .
	.globl	C$gamelogic.c$52$3_0$277
;src/engine/gamelogic.c:52: game_status = GAME_STATUS_LEVEL_COMPLETE; 
	ld	hl, #_game_status
	ld	(hl), #0x0e
	C$gamelogic.c$53$3_0$277	= .
	.globl	C$gamelogic.c$53$3_0$277
;src/engine/gamelogic.c:53: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$54$3_0$277	= .
	.globl	C$gamelogic.c$54$3_0$277
;src/engine/gamelogic.c:54: row_index = BOARD_ROWS - 1;
	ld	iy, #_row_index
	ld	0 (iy), #0x15
	C$gamelogic.c$55$3_0$277	= .
	.globl	C$gamelogic.c$55$3_0$277
;src/engine/gamelogic.c:55: if(number_human_players == 1 && current_player == PLAYER_1) {
	ld	a, c
	or	a, a
	jr	Z, 00102$
	ld	a, (_current_player+0)
	or	a, a
	jr	NZ, 00102$
	C$gamelogic.c$56$4_0$278	= .
	.globl	C$gamelogic.c$56$4_0$278
;src/engine/gamelogic.c:56: score_p2 = (level)<<LEVEL_DELIMITER;
	ld	a, (_level+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	C$gamelogic.c$57$4_0$278	= .
	.globl	C$gamelogic.c$57$4_0$278
;src/engine/gamelogic.c:57: print_score_value(24,23,score_p2);
	ld	(_score_p2), hl
	push	hl
	ld	l, #0x17
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x18
	call	_print_score_value
00102$:
	C$gamelogic.c$60$3_0$277	= .
	.globl	C$gamelogic.c$60$3_0$277
;src/engine/gamelogic.c:60: play_song(CONGRATULATIONS1_OST);
	ld	a, #0x09
	C$gamelogic.c$62$2_0$275	= .
	.globl	C$gamelogic.c$62$2_0$275
;src/engine/gamelogic.c:62: return;
	C$gamelogic.c$64$1_0$274	= .
	.globl	C$gamelogic.c$64$1_0$274
;src/engine/gamelogic.c:64: }
	C$gamelogic.c$64$1_0$274	= .
	.globl	C$gamelogic.c$64$1_0$274
	XG$update_level$0$0	= .
	.globl	XG$update_level$0$0
	jp	_play_song
	G$init_game$0$0	= .
	.globl	G$init_game$0$0
	C$gamelogic.c$66$1_0$280	= .
	.globl	C$gamelogic.c$66$1_0$280
;src/engine/gamelogic.c:66: void init_game(void) {
;	---------------------------------
; Function init_game
; ---------------------------------
_init_game::
	C$gamelogic.c$67$1_0$280	= .
	.globl	C$gamelogic.c$67$1_0$280
;src/engine/gamelogic.c:67: init_console();
	call	_init_console
	C$gamelogic.c$68$1_0$280	= .
	.globl	C$gamelogic.c$68$1_0$280
;src/engine/gamelogic.c:68: blackBackgroundPalette();
	call	_SMS_zeroBGPalette
	C$gamelogic.c$69$1_0$280	= .
	.globl	C$gamelogic.c$69$1_0$280
;src/engine/gamelogic.c:69: blackSpritePalette();
	call	_SMS_zeroSpritePalette
	C$gamelogic.c$70$1_0$280	= .
	.globl	C$gamelogic.c$70$1_0$280
;src/engine/gamelogic.c:70: clear_tilemap();
	call	_clear_tilemap
	C$gamelogic.c$71$1_0$280	= .
	.globl	C$gamelogic.c$71$1_0$280
;src/engine/gamelogic.c:71: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$72$1_0$280	= .
	.globl	C$gamelogic.c$72$1_0$280
;src/engine/gamelogic.c:72: if(init_savesystem()) {
	call	_init_savesystem
	bit	0,a
	jr	Z, 00105$
	C$gamelogic.c$73$2_0$281	= .
	.globl	C$gamelogic.c$73$2_0$281
;src/engine/gamelogic.c:73: draw_sram_present();
	call	_draw_sram_present
	C$gamelogic.c$74$2_0$281	= .
	.globl	C$gamelogic.c$74$2_0$281
;src/engine/gamelogic.c:74: while(frame_cnt < M_PERIOD) {
00101$:
	ld	de, #0x0078
	ld	hl, (_frame_cnt)
	cp	a, a
	sbc	hl, de
	jr	NC, 00105$
	C$gamelogic.c$75$3_0$282	= .
	.globl	C$gamelogic.c$75$3_0$282
;src/engine/gamelogic.c:75: waitForFrame();
	call	_waitForFrame
	jr	00101$
00105$:
	C$gamelogic.c$78$1_0$280	= .
	.globl	C$gamelogic.c$78$1_0$280
;src/engine/gamelogic.c:78: blackBackgroundPalette();
	call	_SMS_zeroBGPalette
	C$gamelogic.c$79$1_0$280	= .
	.globl	C$gamelogic.c$79$1_0$280
;src/engine/gamelogic.c:79: blackSpritePalette();
	C$gamelogic.c$80$1_0$280	= .
	.globl	C$gamelogic.c$80$1_0$280
;src/engine/gamelogic.c:80: }
	C$gamelogic.c$80$1_0$280	= .
	.globl	C$gamelogic.c$80$1_0$280
	XG$init_game$0$0	= .
	.globl	XG$init_game$0$0
	jp	_SMS_zeroSpritePalette
	G$init_gamestatus$0$0	= .
	.globl	G$init_gamestatus$0$0
	C$gamelogic.c$82$1_0$284	= .
	.globl	C$gamelogic.c$82$1_0$284
;src/engine/gamelogic.c:82: void init_gamestatus(u8 startLevel) {
;	---------------------------------
; Function init_gamestatus
; ---------------------------------
_init_gamestatus::
	ld	(_level+0), a
	C$gamelogic.c$83$1_0$284	= .
	.globl	C$gamelogic.c$83$1_0$284
;src/engine/gamelogic.c:83: pause = false;
	ld	hl, #_pause
	ld	(hl), #0x00
	C$gamelogic.c$84$1_0$284	= .
	.globl	C$gamelogic.c$84$1_0$284
;src/engine/gamelogic.c:84: game_status = GAME_STATUS_DRAWING_NEW_LEVEL;
	ld	hl, #_game_status
	ld	(hl), #0x10
	C$gamelogic.c$85$1_0$284	= .
	.globl	C$gamelogic.c$85$1_0$284
;src/engine/gamelogic.c:85: score_p1 = 0;
	ld	hl, #0x0000
	ld	(_score_p1), hl
	C$gamelogic.c$86$1_0$284	= .
	.globl	C$gamelogic.c$86$1_0$284
;src/engine/gamelogic.c:86: score_p2 = 0;
	ld	(_score_p2), hl
	C$gamelogic.c$87$1_0$284	= .
	.globl	C$gamelogic.c$87$1_0$284
;src/engine/gamelogic.c:87: highScoreReached = false;
	ld	iy, #_highScoreReached
	ld	0 (iy), #0x00
	C$gamelogic.c$88$1_0$284	= .
	.globl	C$gamelogic.c$88$1_0$284
;src/engine/gamelogic.c:88: bonusScore = false;
	ld	iy, #_bonusScore
	ld	0 (iy), #0x00
	C$gamelogic.c$89$1_0$284	= .
	.globl	C$gamelogic.c$89$1_0$284
;src/engine/gamelogic.c:89: bonusSamplePlay = false;
	ld	iy, #_bonusSamplePlay
	ld	0 (iy), #0x00
	C$gamelogic.c$90$1_0$284	= .
	.globl	C$gamelogic.c$90$1_0$284
;src/engine/gamelogic.c:90: piece_index = 0;
	ld	iy, #_piece_index
	ld	0 (iy), #0x00
	C$gamelogic.c$92$1_0$284	= .
	.globl	C$gamelogic.c$92$1_0$284
;src/engine/gamelogic.c:92: max_row_index = BOARD_ROWS - 1;
	ld	iy, #_max_row_index
	ld	0 (iy), #0x15
	C$gamelogic.c$94$1_0$284	= .
	.globl	C$gamelogic.c$94$1_0$284
;src/engine/gamelogic.c:94: frame_cnt = 0;
	ld	(_frame_cnt), hl
	C$gamelogic.c$95$1_0$284	= .
	.globl	C$gamelogic.c$95$1_0$284
;src/engine/gamelogic.c:95: level_timer = 0;
	ld	(_level_timer), hl
	C$gamelogic.c$96$1_0$284	= .
	.globl	C$gamelogic.c$96$1_0$284
;src/engine/gamelogic.c:96: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$97$1_0$284	= .
	.globl	C$gamelogic.c$97$1_0$284
;src/engine/gamelogic.c:97: endOfDrawingLevel = false;
	ld	hl, #_endOfDrawingLevel
	ld	(hl), #0x00
	C$gamelogic.c$98$1_0$284	= .
	.globl	C$gamelogic.c$98$1_0$284
;src/engine/gamelogic.c:98: decodingIndex = 0;
	ld	hl, #_decodingIndex
	ld	(hl), #0x00
	C$gamelogic.c$100$1_0$284	= .
	.globl	C$gamelogic.c$100$1_0$284
;src/engine/gamelogic.c:100: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	C$gamelogic.c$101$1_0$284	= .
	.globl	C$gamelogic.c$101$1_0$284
;src/engine/gamelogic.c:101: piecesDestroyed = false;
	ld	hl, #_piecesDestroyed
	ld	(hl), #0x00
	C$gamelogic.c$102$1_0$284	= .
	.globl	C$gamelogic.c$102$1_0$284
;src/engine/gamelogic.c:102: isPaletteRed = false;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x00
	C$gamelogic.c$103$1_0$284	= .
	.globl	C$gamelogic.c$103$1_0$284
;src/engine/gamelogic.c:103: isHurryUp = false;
	ld	hl, #_isHurryUp
	ld	(hl), #0x00
	C$gamelogic.c$104$1_0$284	= .
	.globl	C$gamelogic.c$104$1_0$284
;src/engine/gamelogic.c:104: generateBonus = false;
	ld	hl, #_generateBonus
	ld	(hl), #0x00
	C$gamelogic.c$106$1_0$284	= .
	.globl	C$gamelogic.c$106$1_0$284
;src/engine/gamelogic.c:106: newLevel = 0;
	ld	hl, #_newLevel
	ld	(hl), #0x00
	C$gamelogic.c$107$1_0$284	= .
	.globl	C$gamelogic.c$107$1_0$284
;src/engine/gamelogic.c:107: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$gamelogic.c$108$1_0$284	= .
	.globl	C$gamelogic.c$108$1_0$284
;src/engine/gamelogic.c:108: fallSpeed = levelfallSpeeds[level];
	ld	bc, #_levelfallSpeeds+0
	ld	hl, (_level)
	ld	h, #0x00
	add	hl, bc
	ld	a, (hl)
	ld	(_fallSpeed+0), a
	C$gamelogic.c$109$1_0$284	= .
	.globl	C$gamelogic.c$109$1_0$284
;src/engine/gamelogic.c:109: fallAcum = 0;
	ld	hl, #0x0000
	ld	(_fallAcum), hl
	C$gamelogic.c$110$1_0$284	= .
	.globl	C$gamelogic.c$110$1_0$284
;src/engine/gamelogic.c:110: if(number_human_players > 0)
	ld	a, (_number_human_players+0)
	or	a, a
	jr	Z, 00102$
	C$gamelogic.c$111$1_0$284	= .
	.globl	C$gamelogic.c$111$1_0$284
;src/engine/gamelogic.c:111: current_player = PLAYER_1;
	ld	hl, #_current_player
	ld	(hl), #0x00
	jr	00103$
00102$:
	C$gamelogic.c$113$1_0$284	= .
	.globl	C$gamelogic.c$113$1_0$284
;src/engine/gamelogic.c:113: current_player = PLAYER_CPU_1;
	ld	hl, #_current_player
	ld	(hl), #0x02
00103$:
	C$gamelogic.c$114$1_0$284	= .
	.globl	C$gamelogic.c$114$1_0$284
;src/engine/gamelogic.c:114: last_square_x = (get_rand() & 7) + 1;
	call	_get_rand
	and	a, #0x07
	inc	a
	ld	(_last_square_x+0), a
	C$gamelogic.c$115$1_0$284	= .
	.globl	C$gamelogic.c$115$1_0$284
;src/engine/gamelogic.c:115: decide_next_square();
	C$gamelogic.c$116$1_0$284	= .
	.globl	C$gamelogic.c$116$1_0$284
;src/engine/gamelogic.c:116: }
	C$gamelogic.c$116$1_0$284	= .
	.globl	C$gamelogic.c$116$1_0$284
	XG$init_gamestatus$0$0	= .
	.globl	XG$init_gamestatus$0$0
	jp	_decide_next_square
	G$restore_board$0$0	= .
	.globl	G$restore_board$0$0
	C$gamelogic.c$119$1_0$287	= .
	.globl	C$gamelogic.c$119$1_0$287
;src/engine/gamelogic.c:119: void restore_board(void) {
;	---------------------------------
; Function restore_board
; ---------------------------------
_restore_board::
	C$gamelogic.c$122$4_0$289	= .
	.globl	C$gamelogic.c$122$4_0$289
;src/engine/gamelogic.c:122: draw_board_position(i, 9,board[100+i]);
	ld	e, #0x02
00101$:
	ld	a, e
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	ld	hl, #0x0140
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	c, e
	ld	b, #0x00
	ld	hl,#0x0064 + _board
	add	hl,bc
	ld	l, (hl)
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$gamelogic.c$121$2_0$287	= .
	.globl	C$gamelogic.c$121$2_0$287
;src/engine/gamelogic.c:121: for(i=2;i<7;i++) {
	inc	e
	ld	a, e
	sub	a, #0x07
	jr	C, 00101$
	C$gamelogic.c$124$2_0$287	= .
	.globl	C$gamelogic.c$124$2_0$287
;src/engine/gamelogic.c:124: }
	C$gamelogic.c$124$2_0$287	= .
	.globl	C$gamelogic.c$124$2_0$287
	XG$restore_board$0$0	= .
	.globl	XG$restore_board$0$0
	ret
	G$decide_next_square$0$0	= .
	.globl	G$decide_next_square$0$0
	C$gamelogic.c$126$2_0$291	= .
	.globl	C$gamelogic.c$126$2_0$291
;src/engine/gamelogic.c:126: void decide_next_square(void) {
;	---------------------------------
; Function decide_next_square
; ---------------------------------
_decide_next_square::
	C$gamelogic.c$128$1_0$291	= .
	.globl	C$gamelogic.c$128$1_0$291
;src/engine/gamelogic.c:128: randnumber = get_rand() & 1;
	call	_get_rand
	rrca
	jr	NC, 00102$
	C$gamelogic.c$129$1_0$291	= .
	.globl	C$gamelogic.c$129$1_0$291
;src/engine/gamelogic.c:129: if(randnumber) {
	C$gamelogic.c$130$2_0$292	= .
	.globl	C$gamelogic.c$130$2_0$292
;src/engine/gamelogic.c:130: next_square.color00 = WHITE_PIECE;   
	ld	hl, #(_next_square + 2)
	ld	(hl), #0x01
	jr	00103$
00102$:
	C$gamelogic.c$132$2_0$293	= .
	.globl	C$gamelogic.c$132$2_0$293
;src/engine/gamelogic.c:132: next_square.color00 = BLACK_PIECE;
	ld	hl, #(_next_square + 2)
	ld	(hl), #0x04
00103$:
	C$gamelogic.c$134$1_0$291	= .
	.globl	C$gamelogic.c$134$1_0$291
;src/engine/gamelogic.c:134: randnumber = get_rand() & 1;
	call	_get_rand
	rrca
	jr	NC, 00105$
	C$gamelogic.c$135$1_0$291	= .
	.globl	C$gamelogic.c$135$1_0$291
;src/engine/gamelogic.c:135: if(randnumber) {
	C$gamelogic.c$136$2_0$294	= .
	.globl	C$gamelogic.c$136$2_0$294
;src/engine/gamelogic.c:136: next_square.color01 = WHITE_PIECE;   
	ld	hl, #(_next_square + 3)
	ld	(hl), #0x01
	jr	00106$
00105$:
	C$gamelogic.c$138$2_0$295	= .
	.globl	C$gamelogic.c$138$2_0$295
;src/engine/gamelogic.c:138: next_square.color01 = BLACK_PIECE;
	ld	hl, #(_next_square + 3)
	ld	(hl), #0x04
00106$:
	C$gamelogic.c$140$1_0$291	= .
	.globl	C$gamelogic.c$140$1_0$291
;src/engine/gamelogic.c:140: randnumber = get_rand() & 1;
	call	_get_rand
	rrca
	jr	NC, 00108$
	C$gamelogic.c$141$1_0$291	= .
	.globl	C$gamelogic.c$141$1_0$291
;src/engine/gamelogic.c:141: if(randnumber) {
	C$gamelogic.c$142$2_0$296	= .
	.globl	C$gamelogic.c$142$2_0$296
;src/engine/gamelogic.c:142: next_square.color10 = WHITE_PIECE;   
	ld	hl, #(_next_square + 4)
	ld	(hl), #0x01
	jr	00109$
00108$:
	C$gamelogic.c$144$2_0$297	= .
	.globl	C$gamelogic.c$144$2_0$297
;src/engine/gamelogic.c:144: next_square.color10 = BLACK_PIECE;
	ld	hl, #(_next_square + 4)
	ld	(hl), #0x04
00109$:
	C$gamelogic.c$146$1_0$291	= .
	.globl	C$gamelogic.c$146$1_0$291
;src/engine/gamelogic.c:146: randnumber = get_rand() & 1;
	call	_get_rand
	rrca
	jr	NC, 00111$
	C$gamelogic.c$147$1_0$291	= .
	.globl	C$gamelogic.c$147$1_0$291
;src/engine/gamelogic.c:147: if(randnumber) {
	C$gamelogic.c$148$2_0$298	= .
	.globl	C$gamelogic.c$148$2_0$298
;src/engine/gamelogic.c:148: next_square.color11 = WHITE_PIECE;   
	ld	hl, #(_next_square + 5)
	ld	(hl), #0x01
	ret
00111$:
	C$gamelogic.c$150$2_0$299	= .
	.globl	C$gamelogic.c$150$2_0$299
;src/engine/gamelogic.c:150: next_square.color11 = BLACK_PIECE;
	ld	hl, #(_next_square + 5)
	ld	(hl), #0x04
	C$gamelogic.c$152$1_0$291	= .
	.globl	C$gamelogic.c$152$1_0$291
;src/engine/gamelogic.c:152: }
	C$gamelogic.c$152$1_0$291	= .
	.globl	C$gamelogic.c$152$1_0$291
	XG$decide_next_square$0$0	= .
	.globl	XG$decide_next_square$0$0
	ret
	G$generate_square_piece$0$0	= .
	.globl	G$generate_square_piece$0$0
	C$gamelogic.c$154$1_0$301	= .
	.globl	C$gamelogic.c$154$1_0$301
;src/engine/gamelogic.c:154: void generate_square_piece(void) {
;	---------------------------------
; Function generate_square_piece
; ---------------------------------
_generate_square_piece::
	C$gamelogic.c$156$1_0$301	= .
	.globl	C$gamelogic.c$156$1_0$301
;src/engine/gamelogic.c:156: current_square.x = 4;
	ld	hl, #_current_square
	ld	(hl), #0x04
	C$gamelogic.c$157$1_0$301	= .
	.globl	C$gamelogic.c$157$1_0$301
;src/engine/gamelogic.c:157: current_square.y = 0;
	ld	hl, #(_current_square + 1)
	ld	(hl), #0x00
	C$gamelogic.c$158$1_0$301	= .
	.globl	C$gamelogic.c$158$1_0$301
;src/engine/gamelogic.c:158: if(!is_free_board_position_for_square_creation()) {
	ld	bc, #_board+0
	ld	hl, #_current_square
	ld	e, (hl)
	ld	l, e
	ld	h, #0x00
	add	hl, bc
	ld	a, (hl)
	or	a,a
	jr	NZ, 00101$
	ld	d,a
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00101$
	ld	hl, #0x000a
	add	hl, de
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	NZ, 00101$
	ld	hl, #0x000b
	add	hl, de
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
00101$:
	C$gamelogic.c$159$2_0$302	= .
	.globl	C$gamelogic.c$159$2_0$302
;src/engine/gamelogic.c:159: game_status = GAME_STATUS_LOOSING_GAME;
	ld	hl, #_game_status
	ld	(hl), #0x11
	C$gamelogic.c$160$2_0$302	= .
	.globl	C$gamelogic.c$160$2_0$302
;src/engine/gamelogic.c:160: row_index = BOARD_ROWS - 1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$161$2_0$302	= .
	.globl	C$gamelogic.c$161$2_0$302
;src/engine/gamelogic.c:161: stop_music();
	call	_PSGStop
	C$gamelogic.c$162$2_0$302	= .
	.globl	C$gamelogic.c$162$2_0$302
;src/engine/gamelogic.c:162: stop_fx();
	C$gamelogic.c$163$2_0$302	= .
	.globl	C$gamelogic.c$163$2_0$302
;src/engine/gamelogic.c:163: return;
	jp	_PSGSFXStop
00102$:
	C$gamelogic.c$166$1_0$301	= .
	.globl	C$gamelogic.c$166$1_0$301
;src/engine/gamelogic.c:166: current_square.color00 = next_square.color00;
	ld	bc, #_current_square + 2
	ld	a, (#(_next_square + 2) + 0)
	ld	(bc), a
	C$gamelogic.c$167$1_0$301	= .
	.globl	C$gamelogic.c$167$1_0$301
;src/engine/gamelogic.c:167: current_square.color01 = next_square.color01;
	ld	bc, #_current_square + 3
	ld	a, (#(_next_square + 3) + 0)
	ld	(bc), a
	C$gamelogic.c$168$1_0$301	= .
	.globl	C$gamelogic.c$168$1_0$301
;src/engine/gamelogic.c:168: current_square.color10 = next_square.color10;
	ld	bc, #_current_square + 4
	ld	a, (#(_next_square + 4) + 0)
	ld	(bc), a
	C$gamelogic.c$169$1_0$301	= .
	.globl	C$gamelogic.c$169$1_0$301
;src/engine/gamelogic.c:169: current_square.color11 = next_square.color11;
	ld	bc, #_current_square + 5
	ld	a, (#(_next_square + 5) + 0)
	ld	(bc), a
	C$gamelogic.c$170$1_0$301	= .
	.globl	C$gamelogic.c$170$1_0$301
;src/engine/gamelogic.c:170: decide_next_square();
	call	_decide_next_square
	C$gamelogic.c$171$2_0$303	= .
	.globl	C$gamelogic.c$171$2_0$303
;src/engine/gamelogic.c:171: draw_next_square(next_square);
	ld	hl, #0x7b78
	rst	#0x08
	ld	a, (#(_next_square + 2) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	ld	hl, #0x7b7a
	rst	#0x08
	ld	a, (#(_next_square + 3) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	ld	hl, #0x7bb8
	rst	#0x08
	ld	a, (#(_next_square + 4) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	ld	hl, #0x7bba
	rst	#0x08
	ld	a, (#(_next_square + 5) + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$gamelogic.c$173$1_0$301	= .
	.globl	C$gamelogic.c$173$1_0$301
;src/engine/gamelogic.c:173: game_status = GAME_STATUS_SQUAREPIECEFALLING;
	ld	hl, #_game_status
	ld	(hl), #0x01
	C$gamelogic.c$174$1_0$301	= .
	.globl	C$gamelogic.c$174$1_0$301
;src/engine/gamelogic.c:174: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$175$1_0$301	= .
	.globl	C$gamelogic.c$175$1_0$301
;src/engine/gamelogic.c:175: }
	C$gamelogic.c$175$1_0$301	= .
	.globl	C$gamelogic.c$175$1_0$301
	XG$generate_square_piece$0$0	= .
	.globl	XG$generate_square_piece$0$0
	ret
	G$fall_square_piece$0$0	= .
	.globl	G$fall_square_piece$0$0
	C$gamelogic.c$177$1_0$308	= .
	.globl	C$gamelogic.c$177$1_0$308
;src/engine/gamelogic.c:177: void fall_square_piece(void) {
;	---------------------------------
; Function fall_square_piece
; ---------------------------------
_fall_square_piece::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$gamelogic.c$178$1_0$308	= .
	.globl	C$gamelogic.c$178$1_0$308
;src/engine/gamelogic.c:178: if(downAction) {
	ld	hl, #_downAction
	bit	0, (hl)
	jr	Z, 00102$
	C$gamelogic.c$179$2_0$309	= .
	.globl	C$gamelogic.c$179$2_0$309
;src/engine/gamelogic.c:179: fallAcum += 196;
	ld	hl, #_fallAcum
	ld	a, (hl)
	add	a, #0xc4
	ld	(hl), a
	jr	NC, 00203$
	inc	hl
	inc	(hl)
00203$:
00102$:
	C$gamelogic.c$182$1_0$308	= .
	.globl	C$gamelogic.c$182$1_0$308
;src/engine/gamelogic.c:182: if(rightAction && (current_square.x < BOARD_COLUMNS -2) && is_free_board_right_position_for_square((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
	ld	hl, #_rightAction
	bit	0, (hl)
	jr	Z, 00109$
	ld	a, (#_current_square + 0)
	sub	a, #0x08
	jr	NC, 00109$
	ld	bc, #_current_square+0
	ld	a, (#_current_square + 1)
	ld	d, #0x00
	ld	l, a
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, a
	rl	d
	ld	e, a
	add	hl, de
	ld	a, (bc)
	ld	d, #0x00
	ld	e, a
	add	hl, de
	ld	e, l
	ld	d, h
	inc	de
	inc	de
	ld	a, e
	add	a, #<(_board)
	ld	e, a
	ld	a, d
	adc	a, #>(_board)
	ld	d, a
	ld	a, (de)
	or	a, a
	jr	NZ, 00109$
	ld	de, #0x000c
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00109$
	C$gamelogic.c$183$2_0$310	= .
	.globl	C$gamelogic.c$183$2_0$310
;src/engine/gamelogic.c:183: delete_square_pieces_board();
	push	bc
	call	_delete_square_pieces_board
	pop	bc
	C$gamelogic.c$184$2_0$310	= .
	.globl	C$gamelogic.c$184$2_0$310
;src/engine/gamelogic.c:184: current_square.x++;
	ld	a, (bc)
	inc	a
	ld	(bc), a
	C$gamelogic.c$185$2_0$310	= .
	.globl	C$gamelogic.c$185$2_0$310
;src/engine/gamelogic.c:185: put_square_pieces_board();
	call	_put_square_pieces_board
	C$gamelogic.c$186$2_0$310	= .
	.globl	C$gamelogic.c$186$2_0$310
;src/engine/gamelogic.c:186: play_fx(MOVE_FX);
	ld	a, #0x02
	call	_play_fx
	jr	00110$
00109$:
	C$gamelogic.c$188$1_0$308	= .
	.globl	C$gamelogic.c$188$1_0$308
;src/engine/gamelogic.c:188: else if(leftAction && (current_square.x > 0 && is_free_board_left_position_for_square((current_square.y << 3) + (current_square.y << 1) + current_square.x))) {
	ld	hl, #_leftAction
	bit	0, (hl)
	jr	Z, 00110$
	ld	a, (#_current_square + 0)
	or	a, a
	jr	Z, 00110$
	ld	bc, #_current_square+0
	ld	a, (#_current_square + 1)
	ld	d, #0x00
	ld	l, a
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, a
	rl	d
	ld	e, a
	add	hl, de
	ld	a, (bc)
	ld	d, #0x00
	ld	e, a
	add	hl, de
	ld	e, l
	ld	d, h
	dec	de
	ld	a, e
	add	a, #<(_board)
	ld	e, a
	ld	a, d
	adc	a, #>(_board)
	ld	d, a
	ld	a, (de)
	or	a, a
	jr	NZ, 00110$
	ld	de, #0x0009
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00110$
	C$gamelogic.c$189$2_0$311	= .
	.globl	C$gamelogic.c$189$2_0$311
;src/engine/gamelogic.c:189: delete_square_pieces_board();
	push	bc
	call	_delete_square_pieces_board
	pop	bc
	C$gamelogic.c$190$2_0$311	= .
	.globl	C$gamelogic.c$190$2_0$311
;src/engine/gamelogic.c:190: current_square.x--;
	ld	a, (bc)
	dec	a
	ld	(bc), a
	C$gamelogic.c$191$2_0$311	= .
	.globl	C$gamelogic.c$191$2_0$311
;src/engine/gamelogic.c:191: put_square_pieces_board();
	call	_put_square_pieces_board
	C$gamelogic.c$192$2_0$311	= .
	.globl	C$gamelogic.c$192$2_0$311
;src/engine/gamelogic.c:192: play_fx(MOVE_FX);
	ld	a, #0x02
	call	_play_fx
00110$:
	C$gamelogic.c$195$1_0$308	= .
	.globl	C$gamelogic.c$195$1_0$308
;src/engine/gamelogic.c:195: if(rotation1Action) {
	ld	hl, #_rotation1Action
	bit	0, (hl)
	jr	Z, 00117$
	C$gamelogic.c$196$2_0$312	= .
	.globl	C$gamelogic.c$196$2_0$312
;src/engine/gamelogic.c:196: delete_square_pieces_board();
	call	_delete_square_pieces_board
	C$gamelogic.c$197$2_0$312	= .
	.globl	C$gamelogic.c$197$2_0$312
;src/engine/gamelogic.c:197: rotate_square_piece();
	call	_rotate_square_piece
	C$gamelogic.c$198$2_0$312	= .
	.globl	C$gamelogic.c$198$2_0$312
;src/engine/gamelogic.c:198: put_square_pieces_board();
	call	_put_square_pieces_board
	jr	00118$
00117$:
	C$gamelogic.c$199$1_0$308	= .
	.globl	C$gamelogic.c$199$1_0$308
;src/engine/gamelogic.c:199: } else if(rotation2Action) {
	ld	hl, #_rotation2Action
	bit	0, (hl)
	jr	Z, 00118$
	C$gamelogic.c$200$2_0$313	= .
	.globl	C$gamelogic.c$200$2_0$313
;src/engine/gamelogic.c:200: delete_square_pieces_board();
	call	_delete_square_pieces_board
	C$gamelogic.c$201$2_0$313	= .
	.globl	C$gamelogic.c$201$2_0$313
;src/engine/gamelogic.c:201: rotate_inv_square_piece();
	call	_rotate_inv_square_piece
	C$gamelogic.c$202$2_0$313	= .
	.globl	C$gamelogic.c$202$2_0$313
;src/engine/gamelogic.c:202: put_square_pieces_board();
	call	_put_square_pieces_board
00118$:
	C$gamelogic.c$204$1_0$308	= .
	.globl	C$gamelogic.c$204$1_0$308
;src/engine/gamelogic.c:204: fallAcum = fallAcum + fallSpeed;
	ld	a, (_fallSpeed+0)
	ld	c, #0x00
	ld	hl, #_fallAcum
	add	a, (hl)
	ld	(hl), a
	inc	hl
	ld	a, c
	adc	a, (hl)
	ld	(hl), a
	C$gamelogic.c$205$1_0$308	= .
	.globl	C$gamelogic.c$205$1_0$308
;src/engine/gamelogic.c:205: if((fallAcum >> 8) > 1){
	ld	a, (#_fallAcum + 1)
	ld	c, a
	ld	b, #0x00
	ld	a, #0x01
	cp	a, c
	ld	a, #0x00
	sbc	a, b
	jr	NC, 00126$
	C$gamelogic.c$206$2_0$314	= .
	.globl	C$gamelogic.c$206$2_0$314
;src/engine/gamelogic.c:206: fallAcum = 0;
	ld	hl, #0x0000
	ld	(_fallAcum), hl
	C$gamelogic.c$207$2_0$314	= .
	.globl	C$gamelogic.c$207$2_0$314
;src/engine/gamelogic.c:207: if(current_square.y < (BOARD_ROWS - 2) && is_free_board_down_position_for_square((current_square.y+1 << 3) + (current_square.y+1 << 1) + current_square.x)) {
	ld	bc, #_current_square + 1
	ld	a, (bc)
	ld	e, a
	ld	hl, #_current_square
	ld	a, (hl)
	ld	-1 (ix), a
	ld	a, e
	sub	a, #0x14
	jr	NC, 00120$
	ld	d, #0x00
	inc	de
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	add	hl, hl
	ex	de, hl
	add	hl, de
	ld	e, -1 (ix)
	ld	d, #0x00
	add	hl, de
	ex	de, hl
	ld	hl, #0x0014
	add	hl, de
	push	de
	ld	de, #_board
	add	hl, de
	pop	de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00120$
	ld	hl, #0x0015
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00120$
	C$gamelogic.c$208$3_0$315	= .
	.globl	C$gamelogic.c$208$3_0$315
;src/engine/gamelogic.c:208: delete_square_pieces_board();
	push	bc
	call	_delete_square_pieces_board
	pop	bc
	C$gamelogic.c$209$3_0$315	= .
	.globl	C$gamelogic.c$209$3_0$315
;src/engine/gamelogic.c:209: current_square.y++;
	ld	a, (bc)
	inc	a
	ld	(bc), a
	C$gamelogic.c$210$3_0$315	= .
	.globl	C$gamelogic.c$210$3_0$315
;src/engine/gamelogic.c:210: put_square_pieces_board();
	call	_put_square_pieces_board
	jr	00126$
00120$:
	C$gamelogic.c$212$3_0$316	= .
	.globl	C$gamelogic.c$212$3_0$316
;src/engine/gamelogic.c:212: last_square_x = current_square.x;
	ld	a, -1 (ix)
	ld	(_last_square_x+0), a
	C$gamelogic.c$213$3_0$316	= .
	.globl	C$gamelogic.c$213$3_0$316
;src/engine/gamelogic.c:213: create_individual_pieces_from_square();
	call	_create_individual_pieces_from_square
	C$gamelogic.c$214$3_0$316	= .
	.globl	C$gamelogic.c$214$3_0$316
;src/engine/gamelogic.c:214: game_status = GAME_STATUS_PIECESFALLING;
	ld	hl, #_game_status
	C$gamelogic.c$215$3_0$316	= .
	.globl	C$gamelogic.c$215$3_0$316
;src/engine/gamelogic.c:215: play_fx(FALL_FX);
	ld	a,#0x03
	ld	(hl),a
	call	_play_fx
	C$gamelogic.c$216$3_0$316	= .
	.globl	C$gamelogic.c$216$3_0$316
;src/engine/gamelogic.c:216: return;
00126$:
	C$gamelogic.c$219$1_0$308	= .
	.globl	C$gamelogic.c$219$1_0$308
;src/engine/gamelogic.c:219: }
	inc	sp
	pop	ix
	C$gamelogic.c$219$1_0$308	= .
	.globl	C$gamelogic.c$219$1_0$308
	XG$fall_square_piece$0$0	= .
	.globl	XG$fall_square_piece$0$0
	ret
	G$generate_bonus_piece$0$0	= .
	.globl	G$generate_bonus_piece$0$0
	C$gamelogic.c$221$1_0$318	= .
	.globl	C$gamelogic.c$221$1_0$318
;src/engine/gamelogic.c:221: void generate_bonus_piece(u8 bonusType) {
;	---------------------------------
; Function generate_bonus_piece
; ---------------------------------
_generate_bonus_piece::
	ld	e, a
	C$gamelogic.c$224$1_0$318	= .
	.globl	C$gamelogic.c$224$1_0$318
;src/engine/gamelogic.c:224: current_square.x = 4;
	ld	hl, #_current_square
	ld	(hl), #0x04
	C$gamelogic.c$225$1_0$318	= .
	.globl	C$gamelogic.c$225$1_0$318
;src/engine/gamelogic.c:225: current_square.y = 0;
	ld	bc, #_current_square+0
	ld	hl, #(_current_square + 1)
	ld	(hl), #0x00
	C$gamelogic.c$227$1_0$318	= .
	.globl	C$gamelogic.c$227$1_0$318
;src/engine/gamelogic.c:227: current_square.color00 = INVERSE_BONUS;
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	C$gamelogic.c$226$1_0$318	= .
	.globl	C$gamelogic.c$226$1_0$318
;src/engine/gamelogic.c:226: if(bonusType == 0) {
	ld	a, e
	or	a, a
	jr	NZ, 00104$
	C$gamelogic.c$227$2_0$319	= .
	.globl	C$gamelogic.c$227$2_0$319
;src/engine/gamelogic.c:227: current_square.color00 = INVERSE_BONUS;
	ld	(hl), #0x0c
	jr	00105$
00104$:
	C$gamelogic.c$228$1_0$318	= .
	.globl	C$gamelogic.c$228$1_0$318
;src/engine/gamelogic.c:228: } else if(bonusType == 1){
	dec	e
	jr	NZ, 00105$
	C$gamelogic.c$229$2_0$320	= .
	.globl	C$gamelogic.c$229$2_0$320
;src/engine/gamelogic.c:229: current_square.color00 = DESTROY_BONUS;
	ld	(hl), #0x0d
00105$:
	C$gamelogic.c$231$1_0$318	= .
	.globl	C$gamelogic.c$231$1_0$318
;src/engine/gamelogic.c:231: if(!is_free_board_position_for_bonus_creation()) {
	ld	a, (bc)
	ld	c, a
	ld	hl, #_board
	ld	b, #0x00
	add	hl, bc
	ld	a, (hl)
	or	a, a
	jr	Z, 00107$
	C$gamelogic.c$232$2_0$321	= .
	.globl	C$gamelogic.c$232$2_0$321
;src/engine/gamelogic.c:232: game_status = GAME_STATUS_LOOSING_GAME;
	ld	hl, #_game_status
	ld	(hl), #0x11
	C$gamelogic.c$233$2_0$321	= .
	.globl	C$gamelogic.c$233$2_0$321
;src/engine/gamelogic.c:233: row_index = BOARD_ROWS - 1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$234$2_0$321	= .
	.globl	C$gamelogic.c$234$2_0$321
;src/engine/gamelogic.c:234: stop_music();
	call	_PSGStop
	C$gamelogic.c$235$2_0$321	= .
	.globl	C$gamelogic.c$235$2_0$321
;src/engine/gamelogic.c:235: stop_fx();
	C$gamelogic.c$236$2_0$321	= .
	.globl	C$gamelogic.c$236$2_0$321
;src/engine/gamelogic.c:236: return;
	jp	_PSGSFXStop
00107$:
	C$gamelogic.c$238$1_0$318	= .
	.globl	C$gamelogic.c$238$1_0$318
;src/engine/gamelogic.c:238: game_status = GAME_STATUS_BONUSFALLING;
	ld	hl, #_game_status
	ld	(hl), #0x02
	C$gamelogic.c$239$1_0$318	= .
	.globl	C$gamelogic.c$239$1_0$318
;src/engine/gamelogic.c:239: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$240$1_0$318	= .
	.globl	C$gamelogic.c$240$1_0$318
;src/engine/gamelogic.c:240: }
	C$gamelogic.c$240$1_0$318	= .
	.globl	C$gamelogic.c$240$1_0$318
	XG$generate_bonus_piece$0$0	= .
	.globl	XG$generate_bonus_piece$0$0
	ret
	G$fall_bonus_piece$0$0	= .
	.globl	G$fall_bonus_piece$0$0
	C$gamelogic.c$242$1_0$323	= .
	.globl	C$gamelogic.c$242$1_0$323
;src/engine/gamelogic.c:242: void fall_bonus_piece(void) {
;	---------------------------------
; Function fall_bonus_piece
; ---------------------------------
_fall_bonus_piece::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$gamelogic.c$243$1_0$323	= .
	.globl	C$gamelogic.c$243$1_0$323
;src/engine/gamelogic.c:243: if((frame_cnt & 1) == 0) {
	ld	hl, #_frame_cnt
	bit	0, (hl)
	jr	NZ, 00106$
	C$gamelogic.c$244$2_0$324	= .
	.globl	C$gamelogic.c$244$2_0$324
;src/engine/gamelogic.c:244: if(downAction && current_square.y < (BOARD_ROWS - 1) && is_free_board_down_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
	ld	hl, #_downAction
	bit	0, (hl)
	jr	Z, 00106$
	ld	bc, #_current_square + 1
	ld	a, (bc)
	ld	e, a
	sub	a, #0x15
	jr	NC, 00106$
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	add	hl, hl
	ex	de, hl
	add	hl, de
	ex	de, hl
	ld	a, (#_current_square + 0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, de
	ld	de, #0x000a
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00106$
	C$gamelogic.c$245$3_0$325	= .
	.globl	C$gamelogic.c$245$3_0$325
;src/engine/gamelogic.c:245: delete_bonus_board();
	push	bc
	call	_delete_bonus_board
	pop	bc
	C$gamelogic.c$246$3_0$325	= .
	.globl	C$gamelogic.c$246$3_0$325
;src/engine/gamelogic.c:246: current_square.y++;
	ld	a, (bc)
	inc	a
	ld	(bc), a
	C$gamelogic.c$247$3_0$325	= .
	.globl	C$gamelogic.c$247$3_0$325
;src/engine/gamelogic.c:247: put_bonus_board();
	call	_put_bonus_board
00106$:
	C$gamelogic.c$251$1_0$323	= .
	.globl	C$gamelogic.c$251$1_0$323
;src/engine/gamelogic.c:251: if(current_square.color00 == DESTROY_BONUS) {
	ld	a, (#(_current_square + 2) + 0)
	sub	a, #0x0d
	jr	NZ, 00113$
	C$gamelogic.c$252$2_0$326	= .
	.globl	C$gamelogic.c$252$2_0$326
;src/engine/gamelogic.c:252: if((frame_cnt & 3) == 0) { 
	ld	a, (_frame_cnt+0)
	and	a, #0x03
	jr	NZ, 00113$
	C$gamelogic.c$253$3_0$327	= .
	.globl	C$gamelogic.c$253$3_0$327
;src/engine/gamelogic.c:253: if(isPaletteRed) {
	ld	hl, #_isPaletteRed
	bit	0, (hl)
	jr	Z, 00108$
	C$gamelogic.c$254$4_0$328	= .
	.globl	C$gamelogic.c$254$4_0$328
;src/engine/gamelogic.c:254: restore_fire_colors();
	call	_restore_fire_colors
	C$gamelogic.c$255$4_0$328	= .
	.globl	C$gamelogic.c$255$4_0$328
;src/engine/gamelogic.c:255: isPaletteRed = false;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x00
	jr	00113$
00108$:
	C$gamelogic.c$257$4_0$329	= .
	.globl	C$gamelogic.c$257$4_0$329
;src/engine/gamelogic.c:257: swap_fire_colors();
	call	_swap_fire_colors
	C$gamelogic.c$258$4_0$329	= .
	.globl	C$gamelogic.c$258$4_0$329
;src/engine/gamelogic.c:258: isPaletteRed = true;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x01
00113$:
	C$gamelogic.c$263$1_0$323	= .
	.globl	C$gamelogic.c$263$1_0$323
;src/engine/gamelogic.c:263: if(rightAction && (current_square.x < BOARD_COLUMNS - 1) && is_free_board_right_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
	ld	bc, #_current_square + 1
	ld	hl, #_rightAction
	bit	0, (hl)
	jr	Z, 00119$
	ld	hl, #_current_square
	ld	e, (hl)
	ld	a, e
	sub	a, #0x09
	jr	NC, 00119$
	ld	a, (bc)
	ld	d, #0x00
	ld	l, a
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, a
	rl	d
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, d
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	d, #0x00
	add	hl, de
	inc	hl
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00119$
	C$gamelogic.c$264$2_0$330	= .
	.globl	C$gamelogic.c$264$2_0$330
;src/engine/gamelogic.c:264: delete_bonus_board();
	push	bc
	call	_delete_bonus_board
	pop	bc
	C$gamelogic.c$265$2_0$330	= .
	.globl	C$gamelogic.c$265$2_0$330
;src/engine/gamelogic.c:265: current_square.x++;
	ld	a, (#_current_square + 0)
	inc	a
	ld	(#_current_square),a
	C$gamelogic.c$266$2_0$330	= .
	.globl	C$gamelogic.c$266$2_0$330
;src/engine/gamelogic.c:266: put_bonus_board();
	push	bc
	call	_put_bonus_board
	ld	a, #0x02
	call	_play_fx
	pop	bc
	jr	00120$
00119$:
	C$gamelogic.c$269$1_0$323	= .
	.globl	C$gamelogic.c$269$1_0$323
;src/engine/gamelogic.c:269: else if(leftAction && (current_square.x > 0 && is_free_board_left_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x))) {
	ld	hl, #_leftAction
	bit	0, (hl)
	jr	Z, 00120$
	ld	hl, #_current_square
	ld	e, (hl)
	ld	a, e
	or	a, a
	jr	Z, 00120$
	ld	a, (bc)
	ld	d, #0x00
	ld	l, a
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, a
	rl	d
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, d
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	ld	d, #0x00
	add	hl, de
	dec	hl
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00120$
	C$gamelogic.c$270$2_0$331	= .
	.globl	C$gamelogic.c$270$2_0$331
;src/engine/gamelogic.c:270: delete_bonus_board();
	push	bc
	call	_delete_bonus_board
	pop	bc
	C$gamelogic.c$271$2_0$331	= .
	.globl	C$gamelogic.c$271$2_0$331
;src/engine/gamelogic.c:271: current_square.x--;
	ld	a, (#_current_square + 0)
	dec	a
	ld	(#_current_square),a
	C$gamelogic.c$272$2_0$331	= .
	.globl	C$gamelogic.c$272$2_0$331
;src/engine/gamelogic.c:272: put_bonus_board();
	push	bc
	call	_put_bonus_board
	ld	a, #0x02
	call	_play_fx
	pop	bc
00120$:
	C$gamelogic.c$277$1_0$323	= .
	.globl	C$gamelogic.c$277$1_0$323
;src/engine/gamelogic.c:277: fallAcum = fallAcum + fallSpeed;
	ld	a, (_fallSpeed+0)
	ld	e, #0x00
	ld	hl, #_fallAcum
	add	a, (hl)
	ld	(hl), a
	inc	hl
	ld	a, e
	adc	a, (hl)
	ld	(hl), a
	C$gamelogic.c$278$1_0$323	= .
	.globl	C$gamelogic.c$278$1_0$323
;src/engine/gamelogic.c:278: if((fallAcum >> 8) > 1){
	ld	a, (#_fallAcum + 1)
	ld	e, a
	ld	d, #0x00
	ld	a, #0x01
	cp	a, e
	ld	a, #0x00
	sbc	a, d
	jp	NC, 00137$
	C$gamelogic.c$279$2_0$332	= .
	.globl	C$gamelogic.c$279$2_0$332
;src/engine/gamelogic.c:279: fallAcum = 0;    
	ld	hl, #0x0000
	ld	(_fallAcum), hl
	C$gamelogic.c$280$2_0$332	= .
	.globl	C$gamelogic.c$280$2_0$332
;src/engine/gamelogic.c:280: if(current_square.y < (BOARD_ROWS - 1) && is_free_board_down_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
	ld	a, (bc)
	ld	e, a
	C$gamelogic.c$263$1_0$323	= .
	.globl	C$gamelogic.c$263$1_0$323
;src/engine/gamelogic.c:263: if(rightAction && (current_square.x < BOARD_COLUMNS - 1) && is_free_board_right_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
	ld	hl, #_current_square
	ld	a, (hl)
	ld	-1 (ix), a
	C$gamelogic.c$280$2_0$332	= .
	.globl	C$gamelogic.c$280$2_0$332
;src/engine/gamelogic.c:280: if(current_square.y < (BOARD_ROWS - 1) && is_free_board_down_position_for_bonus((current_square.y << 3) + (current_square.y << 1) + current_square.x)) {
	ld	a, e
	sub	a, #0x15
	jr	NC, 00132$
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ex	de, hl
	add	hl, hl
	ex	de, hl
	add	hl, de
	ld	e, -1 (ix)
	ld	d, #0x00
	add	hl, de
	ld	de, #0x000a
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00132$
	C$gamelogic.c$281$3_0$333	= .
	.globl	C$gamelogic.c$281$3_0$333
;src/engine/gamelogic.c:281: delete_bonus_board();
	push	bc
	call	_delete_bonus_board
	pop	bc
	C$gamelogic.c$282$3_0$333	= .
	.globl	C$gamelogic.c$282$3_0$333
;src/engine/gamelogic.c:282: current_square.y++;
	ld	a, (bc)
	inc	a
	ld	(bc), a
	C$gamelogic.c$283$3_0$333	= .
	.globl	C$gamelogic.c$283$3_0$333
;src/engine/gamelogic.c:283: put_bonus_board();
	call	_put_bonus_board
	jr	00137$
00132$:
	C$gamelogic.c$285$3_0$334	= .
	.globl	C$gamelogic.c$285$3_0$334
;src/engine/gamelogic.c:285: last_square_x = current_square.x; 
	ld	a, -1 (ix)
	ld	(_last_square_x+0), a
	C$gamelogic.c$286$3_0$334	= .
	.globl	C$gamelogic.c$286$3_0$334
;src/engine/gamelogic.c:286: if(current_square.color00 == INVERSE_BONUS) {
	ld	a, (#(_current_square + 2) + 0)
	cp	a, #0x0c
	jr	NZ, 00129$
	C$gamelogic.c$287$4_0$335	= .
	.globl	C$gamelogic.c$287$4_0$335
;src/engine/gamelogic.c:287: if(current_square.y < (BOARD_ROWS -1)) {
	ld	a, (bc)
	sub	a, #0x15
	jr	NC, 00124$
	C$gamelogic.c$288$5_0$336	= .
	.globl	C$gamelogic.c$288$5_0$336
;src/engine/gamelogic.c:288: game_status = GAME_STATUS_PIECESREVERSING;
	ld	hl, #_game_status
	ld	(hl), #0x04
	C$gamelogic.c$289$5_0$336	= .
	.globl	C$gamelogic.c$289$5_0$336
;src/engine/gamelogic.c:289: delete_bonus_board();
	push	bc
	call	_delete_bonus_board
	pop	bc
	C$gamelogic.c$290$5_0$336	= .
	.globl	C$gamelogic.c$290$5_0$336
;src/engine/gamelogic.c:290: row_index = current_square.y+1;
	ld	a, (bc)
	inc	a
	ld	(_row_index+0), a
	jr	00130$
00124$:
	C$gamelogic.c$292$5_0$337	= .
	.globl	C$gamelogic.c$292$5_0$337
;src/engine/gamelogic.c:292: delete_bonus_board();
	call	_delete_bonus_board
	C$gamelogic.c$293$5_0$337	= .
	.globl	C$gamelogic.c$293$5_0$337
;src/engine/gamelogic.c:293: game_status = GAME_STATUS_NOACTION;
	ld	hl, #_game_status
	ld	(hl), #0x00
	C$gamelogic.c$294$5_0$337	= .
	.globl	C$gamelogic.c$294$5_0$337
;src/engine/gamelogic.c:294: update_level();
	call	_update_level
	C$gamelogic.c$295$5_0$337	= .
	.globl	C$gamelogic.c$295$5_0$337
;src/engine/gamelogic.c:295: update_player();
	call	_update_player
	C$gamelogic.c$296$5_0$337	= .
	.globl	C$gamelogic.c$296$5_0$337
;src/engine/gamelogic.c:296: clear_input();
	call	_clear_input
	C$gamelogic.c$297$5_0$337	= .
	.globl	C$gamelogic.c$297$5_0$337
;src/engine/gamelogic.c:297: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$298$5_0$337	= .
	.globl	C$gamelogic.c$298$5_0$337
;src/engine/gamelogic.c:298: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jr	00130$
00129$:
	C$gamelogic.c$300$3_0$334	= .
	.globl	C$gamelogic.c$300$3_0$334
;src/engine/gamelogic.c:300: } else if(current_square.color00 == DESTROY_BONUS){
	sub	a, #0x0d
	jr	NZ, 00130$
	C$gamelogic.c$301$4_0$338	= .
	.globl	C$gamelogic.c$301$4_0$338
;src/engine/gamelogic.c:301: game_status = GAME_STATUS_PIECESDESTROYING;
	ld	hl, #_game_status
	ld	(hl), #0x05
	C$gamelogic.c$302$4_0$338	= .
	.globl	C$gamelogic.c$302$4_0$338
;src/engine/gamelogic.c:302: row_index = current_square.y;
	ld	a, (bc)
	ld	(_row_index+0), a
00130$:
	C$gamelogic.c$304$3_0$334	= .
	.globl	C$gamelogic.c$304$3_0$334
;src/engine/gamelogic.c:304: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$305$3_0$334	= .
	.globl	C$gamelogic.c$305$3_0$334
;src/engine/gamelogic.c:305: play_fx(FALL_FX);
	ld	a, #0x03
	call	_play_fx
	C$gamelogic.c$306$3_0$334	= .
	.globl	C$gamelogic.c$306$3_0$334
;src/engine/gamelogic.c:306: return;
00137$:
	C$gamelogic.c$309$1_0$323	= .
	.globl	C$gamelogic.c$309$1_0$323
;src/engine/gamelogic.c:309: }
	inc	sp
	pop	ix
	C$gamelogic.c$309$1_0$323	= .
	.globl	C$gamelogic.c$309$1_0$323
	XG$fall_bonus_piece$0$0	= .
	.globl	XG$fall_bonus_piece$0$0
	ret
	G$fall_pieces$0$0	= .
	.globl	G$fall_pieces$0$0
	C$gamelogic.c$312$1_0$340	= .
	.globl	C$gamelogic.c$312$1_0$340
;src/engine/gamelogic.c:312: bool fall_pieces(void) {
;	---------------------------------
; Function fall_pieces
; ---------------------------------
_fall_pieces::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-7
	add	hl, sp
	ld	sp, hl
	C$gamelogic.c$313$2_0$340	= .
	.globl	C$gamelogic.c$313$2_0$340
;src/engine/gamelogic.c:313: bool onepieceupdated = false;
	ld	-7 (ix), #0x00
	C$gamelogic.c$315$4_0$343	= .
	.globl	C$gamelogic.c$315$4_0$343
;src/engine/gamelogic.c:315: for(i = 0;i<piece_index;i++) {
	ld	e, #0x00
00109$:
	ld	a, e
	ld	hl, #_piece_index
	sub	a, (hl)
	jp	NC, 00107$
	C$gamelogic.c$316$1_0$340	= .
	.globl	C$gamelogic.c$316$1_0$340
;src/engine/gamelogic.c:316: if(active_pieces[i].color != NULLED_PIECE) {
	push	de
	ld	d, #0x00
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, de
	pop	de
	ld	bc,#_active_pieces
	add	hl,bc
	ld	-2 (ix), l
	ld	-1 (ix), h
	inc	hl
	inc	hl
	ld	c, l
	ld	b, h
	ld	a, (bc)
	or	a, a
	jp	Z, 00110$
	C$gamelogic.c$318$1_0$340	= .
	.globl	C$gamelogic.c$318$1_0$340
;src/engine/gamelogic.c:318: if((active_pieces[i].y < (BOARD_ROWS - 1)) && is_free_board_next_position_for_piece(i)) { 
	ld	a, -2 (ix)
	add	a, #0x01
	ld	-6 (ix), a
	ld	a, -1 (ix)
	adc	a, #0x00
	ld	-5 (ix), a
	ld	l, -6 (ix)
	ld	h, -5 (ix)
	ld	a, (hl)
	ld	-4 (ix), a
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, (hl)
	ld	-3 (ix), a
	ld	a, -4 (ix)
	sub	a, #0x15
	jr	NC, 00102$
	ld	a, -4 (ix)
	ld	d, #0x00
	ld	l, a
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, a
	rl	d
	add	a, l
	ld	-2 (ix), a
	ld	a, d
	adc	a, h
	ld	-1 (ix), a
	ld	l, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -2 (ix)
	add	a, l
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	adc	a, h
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	push	de
	ld	de, #0x000a
	add	hl, de
	ld	de, #_board
	add	hl, de
	pop	de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00102$
	C$gamelogic.c$319$5_0$344	= .
	.globl	C$gamelogic.c$319$5_0$344
;src/engine/gamelogic.c:319: delete_individual_pieces_board(i);
	push	de
	ld	a, e
	call	_delete_individual_pieces_board
	pop	de
	C$gamelogic.c$320$5_0$344	= .
	.globl	C$gamelogic.c$320$5_0$344
;src/engine/gamelogic.c:320: active_pieces[i].y++;
	ld	l,-6 (ix)
	ld	h,-5 (ix)
	inc	(hl)
	C$gamelogic.c$321$5_0$344	= .
	.globl	C$gamelogic.c$321$5_0$344
;src/engine/gamelogic.c:321: put_individual_pieces_board(i);
	push	de
	ld	a, e
	call	_put_individual_pieces_board
	pop	de
	C$gamelogic.c$322$5_0$344	= .
	.globl	C$gamelogic.c$322$5_0$344
;src/engine/gamelogic.c:322: onepieceupdated = true;
	ld	-7 (ix), #0x01
	jr	00110$
00102$:
	C$gamelogic.c$324$5_0$345	= .
	.globl	C$gamelogic.c$324$5_0$345
;src/engine/gamelogic.c:324: mark_position_for_change_in_board(active_pieces[i].x,active_pieces[i].y);
	push	bc
	push	de
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -3 (ix)
	call	_mark_position_for_change_in_board
	pop	de
	pop	bc
	C$gamelogic.c$325$5_0$345	= .
	.globl	C$gamelogic.c$325$5_0$345
;src/engine/gamelogic.c:325: active_pieces[i].color = NULLED_PIECE;
	xor	a, a
	ld	(bc), a
00110$:
	C$gamelogic.c$315$2_0$341	= .
	.globl	C$gamelogic.c$315$2_0$341
;src/engine/gamelogic.c:315: for(i = 0;i<piece_index;i++) {
	inc	e
	jp	00109$
00107$:
	C$gamelogic.c$329$1_0$340	= .
	.globl	C$gamelogic.c$329$1_0$340
;src/engine/gamelogic.c:329: return onepieceupdated;
	ld	a, -7 (ix)
	C$gamelogic.c$330$1_0$340	= .
	.globl	C$gamelogic.c$330$1_0$340
;src/engine/gamelogic.c:330: }
	ld	sp, ix
	pop	ix
	C$gamelogic.c$330$1_0$340	= .
	.globl	C$gamelogic.c$330$1_0$340
	XG$fall_pieces$0$0	= .
	.globl	XG$fall_pieces$0$0
	ret
	G$reverse_pieces$0$0	= .
	.globl	G$reverse_pieces$0$0
	C$gamelogic.c$332$1_0$347	= .
	.globl	C$gamelogic.c$332$1_0$347
;src/engine/gamelogic.c:332: void reverse_pieces(void) {
;	---------------------------------
; Function reverse_pieces
; ---------------------------------
_reverse_pieces::
	C$gamelogic.c$334$1_0$347	= .
	.globl	C$gamelogic.c$334$1_0$347
;src/engine/gamelogic.c:334: if(!(frame_cnt & 3)) {
	ld	a, (_frame_cnt+0)
	and	a, #0x03
	ret	NZ
	C$gamelogic.c$335$2_0$348	= .
	.globl	C$gamelogic.c$335$2_0$348
;src/engine/gamelogic.c:335: original_color= reverse_piece_board(last_square_x, row_index);
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_last_square_x+0)
	call	_reverse_piece_board
	C$gamelogic.c$336$2_0$348	= .
	.globl	C$gamelogic.c$336$2_0$348
;src/engine/gamelogic.c:336: mark_position_for_change_in_board_rec(last_square_x, row_index, original_color);
	push	af
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_last_square_x+0)
	call	_mark_position_for_change_in_board_rec
	C$gamelogic.c$337$2_0$348	= .
	.globl	C$gamelogic.c$337$2_0$348
;src/engine/gamelogic.c:337: if(row_index < (BOARD_ROWS - 1)){
	ld	a, (_row_index+0)
	sub	a, #0x15
	jr	NC, 00104$
	C$gamelogic.c$338$3_0$349	= .
	.globl	C$gamelogic.c$338$3_0$349
;src/engine/gamelogic.c:338: row_index++;
	ld	iy, #_row_index
	inc	0 (iy)
	C$gamelogic.c$339$3_0$349	= .
	.globl	C$gamelogic.c$339$3_0$349
;src/engine/gamelogic.c:339: if(!(row_index & 3)) {
	ld	a, (_row_index+0)
	and	a, #0x03
	ret	NZ
	C$gamelogic.c$340$4_0$350	= .
	.globl	C$gamelogic.c$340$4_0$350
;src/engine/gamelogic.c:340: play_fx(SELECT_FX);
	ld	a, #0x01
	jp	_play_fx
00104$:
	C$gamelogic.c$343$3_0$351	= .
	.globl	C$gamelogic.c$343$3_0$351
;src/engine/gamelogic.c:343: game_status = GAME_STATUS_CALCULATIONS;
	ld	hl, #_game_status
	ld	(hl), #0x06
	C$gamelogic.c$344$3_0$351	= .
	.globl	C$gamelogic.c$344$3_0$351
;src/engine/gamelogic.c:344: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	C$gamelogic.c$347$1_0$347	= .
	.globl	C$gamelogic.c$347$1_0$347
;src/engine/gamelogic.c:347: }
	C$gamelogic.c$347$1_0$347	= .
	.globl	C$gamelogic.c$347$1_0$347
	XG$reverse_pieces$0$0	= .
	.globl	XG$reverse_pieces$0$0
	ret
	G$increment_score$0$0	= .
	.globl	G$increment_score$0$0
	C$gamelogic.c$349$1_0$353	= .
	.globl	C$gamelogic.c$349$1_0$353
;src/engine/gamelogic.c:349: void increment_score(u8 numPiecesDestroyed) {
;	---------------------------------
; Function increment_score
; ---------------------------------
_increment_score::
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	C$gamelogic.c$351$1_0$353	= .
	.globl	C$gamelogic.c$351$1_0$353
;src/engine/gamelogic.c:351: score_p1 = score_p1 + numPiecesDestroyed;
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	c, l
	ld	b, h
	C$gamelogic.c$353$1_0$353	= .
	.globl	C$gamelogic.c$353$1_0$353
;src/engine/gamelogic.c:353: score_p1 = score_p1 + (numPiecesDestroyed<<1);
	add	hl, hl
	ld	d, l
	ld	e, h
	C$gamelogic.c$350$1_0$353	= .
	.globl	C$gamelogic.c$350$1_0$353
;src/engine/gamelogic.c:350: if(current_player == PLAYER_1 || (number_human_players == 0 && current_player == PLAYER_CPU_1)) {
	ld	a, (_current_player+0)
	or	a, a
	jr	Z, 00107$
	ld	a, (_number_human_players+0)
	or	a, a
	jr	NZ, 00108$
	ld	a, (_current_player+0)
	sub	a, #0x02
	jr	NZ, 00108$
00107$:
	C$gamelogic.c$351$2_0$354	= .
	.globl	C$gamelogic.c$351$2_0$354
;src/engine/gamelogic.c:351: score_p1 = score_p1 + numPiecesDestroyed;
	ld	hl, #_score_p1
	ld	a, (hl)
	add	a, c
	ld	(hl), a
	inc	hl
	ld	a, (hl)
	adc	a, b
	ld	(hl), a
	C$gamelogic.c$352$2_0$354	= .
	.globl	C$gamelogic.c$352$2_0$354
;src/engine/gamelogic.c:352: if(bonusScore) {
	ld	iy, #_bonusScore
	bit	0, 0 (iy)
	jr	Z, 00102$
	C$gamelogic.c$353$3_0$355	= .
	.globl	C$gamelogic.c$353$3_0$355
;src/engine/gamelogic.c:353: score_p1 = score_p1 + (numPiecesDestroyed<<1);
	ld	a, d
	ld	hl, #_score_p1
	add	a, (hl)
	ld	(hl), a
	inc	hl
	ld	a, e
	adc	a, (hl)
	ld	(hl), a
	C$gamelogic.c$354$3_0$355	= .
	.globl	C$gamelogic.c$354$3_0$355
;src/engine/gamelogic.c:354: bonusScore = false;
	ld	0 (iy), #0x00
	jr	00103$
00102$:
	C$gamelogic.c$356$3_0$356	= .
	.globl	C$gamelogic.c$356$3_0$356
;src/engine/gamelogic.c:356: score_p1 = score_p1 + numPiecesDestroyed;
	ld	a, c
	ld	iy, #_score_p1
	add	a, 0 (iy)
	ld	(_score_p1+0), a
	ld	a, b
	adc	a, 1 (iy)
	ld	(_score_p1+1), a
00103$:
	C$gamelogic.c$358$2_0$354	= .
	.globl	C$gamelogic.c$358$2_0$354
;src/engine/gamelogic.c:358: newLevel = score_p1 >> LEVEL_DELIMITER; 
	ld	hl, (_score_p1)
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	ld	a, l
	ld	(#_newLevel), a
	C$gamelogic.c$359$2_0$354	= .
	.globl	C$gamelogic.c$359$2_0$354
;src/engine/gamelogic.c:359: print_score_value(24,19,score_p1);
	ld	hl, (_score_p1)
	push	hl
	ld	l, #0x13
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x18
	call	_print_score_value
	ret
00108$:
	C$gamelogic.c$361$2_0$357	= .
	.globl	C$gamelogic.c$361$2_0$357
;src/engine/gamelogic.c:361: if(bonusScore) {
	ld	iy, #_bonusScore
	bit	0, 0 (iy)
	jr	Z, 00105$
	C$gamelogic.c$362$3_0$358	= .
	.globl	C$gamelogic.c$362$3_0$358
;src/engine/gamelogic.c:362: score_p2 = score_p2 + (numPiecesDestroyed<<1);
	ld	a, d
	ld	hl, #_score_p2
	add	a, (hl)
	ld	(hl), a
	inc	hl
	ld	a, e
	adc	a, (hl)
	ld	(hl), a
	C$gamelogic.c$363$3_0$358	= .
	.globl	C$gamelogic.c$363$3_0$358
;src/engine/gamelogic.c:363: bonusScore = false;
	ld	0 (iy), #0x00
	jr	00106$
00105$:
	C$gamelogic.c$365$3_0$359	= .
	.globl	C$gamelogic.c$365$3_0$359
;src/engine/gamelogic.c:365: score_p2 = score_p2 + numPiecesDestroyed;
	ld	a, c
	ld	iy, #_score_p2
	add	a, 0 (iy)
	ld	(_score_p2+0), a
	ld	a, b
	adc	a, 1 (iy)
	ld	(_score_p2+1), a
00106$:
	C$gamelogic.c$367$2_0$357	= .
	.globl	C$gamelogic.c$367$2_0$357
;src/engine/gamelogic.c:367: newLevel = score_p2 >> LEVEL_DELIMITER;
	ld	hl, (_score_p2)
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	srl	h
	rr	l
	ld	a, l
	ld	(#_newLevel), a
	C$gamelogic.c$368$2_0$357	= .
	.globl	C$gamelogic.c$368$2_0$357
;src/engine/gamelogic.c:368: print_score_value(24,23,score_p2);
	ld	hl, (_score_p2)
	push	hl
	ld	l, #0x17
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x18
	call	_print_score_value
	C$gamelogic.c$371$1_0$353	= .
	.globl	C$gamelogic.c$371$1_0$353
;src/engine/gamelogic.c:371: }
	C$gamelogic.c$371$1_0$353	= .
	.globl	C$gamelogic.c$371$1_0$353
	XG$increment_score$0$0	= .
	.globl	XG$increment_score$0$0
	ret
	G$add_100point_sprite$0$0	= .
	.globl	G$add_100point_sprite$0$0
	C$gamelogic.c$373$1_0$361	= .
	.globl	C$gamelogic.c$373$1_0$361
;src/engine/gamelogic.c:373: void add_100point_sprite(u8 x, u8 y) {
;	---------------------------------
; Function add_100point_sprite
; ---------------------------------
_add_100point_sprite::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	push	af
	push	af
	ld	c, a
	ld	-1 (ix), l
	C$gamelogic.c$374$1_0$361	= .
	.globl	C$gamelogic.c$374$1_0$361
;src/engine/gamelogic.c:374: list_node_t* current_element = active_points_list->head;
	ld	hl, (_active_points_list)
	ld	a, (hl)
	ld	-6 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-5 (ix), a
	C$gamelogic.c$376$1_0$361	= .
	.globl	C$gamelogic.c$376$1_0$361
;src/engine/gamelogic.c:376: u8 consecutive100Points = 0;
	ld	e, #0x00
	C$gamelogic.c$378$1_0$361	= .
	.globl	C$gamelogic.c$378$1_0$361
;src/engine/gamelogic.c:378: while(current_element != NULL) { //FIXME: possible error aquí quan peten els punts
	ld	-2 (ix), #0x00
00106$:
	C$gamelogic.c$384$1_0$361	= .
	.globl	C$gamelogic.c$384$1_0$361
;src/engine/gamelogic.c:384: consecutive500Points++;
	ld	a, -2 (ix)
	inc	a
	ld	-4 (ix), a
	C$gamelogic.c$378$1_0$361	= .
	.globl	C$gamelogic.c$378$1_0$361
;src/engine/gamelogic.c:378: while(current_element != NULL) { //FIXME: possible error aquí quan peten els punts
	ld	a, -5 (ix)
	or	a, -6 (ix)
	jr	Z, 00108$
	C$gamelogic.c$379$2_0$362	= .
	.globl	C$gamelogic.c$379$2_0$362
;src/engine/gamelogic.c:379: current_point_sprite = (point_sprite*)(current_element->data);
	pop	hl
	push	hl
	ld	b, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	C$gamelogic.c$380$2_0$362	= .
	.globl	C$gamelogic.c$380$2_0$362
;src/engine/gamelogic.c:380: if(current_point_sprite->firstsprite == POINT_100_SPRITE_INDEX) {
	inc	hl
	inc	hl
	ld	a, (hl)
	cp	a, #0x0e
	jr	NZ, 00104$
	C$gamelogic.c$381$3_0$363	= .
	.globl	C$gamelogic.c$381$3_0$363
;src/engine/gamelogic.c:381: consecutive100Points++;
	inc	e
	jr	00105$
00104$:
	C$gamelogic.c$382$2_0$362	= .
	.globl	C$gamelogic.c$382$2_0$362
;src/engine/gamelogic.c:382: } else if(current_point_sprite->firstsprite == POINT_500_SPRITE_INDEX) {
	C$gamelogic.c$383$3_0$364	= .
	.globl	C$gamelogic.c$383$3_0$364
;src/engine/gamelogic.c:383: consecutive100Points = 0;
	sub	a,#0x0f
	jr	NZ, 00105$
	ld	e,a
	C$gamelogic.c$384$3_0$364	= .
	.globl	C$gamelogic.c$384$3_0$364
;src/engine/gamelogic.c:384: consecutive500Points++;
	ld	a, -4 (ix)
	ld	-2 (ix), a
00105$:
	C$gamelogic.c$386$2_0$362	= .
	.globl	C$gamelogic.c$386$2_0$362
;src/engine/gamelogic.c:386: current_element = current_element->next;
	pop	hl
	push	hl
	inc	hl
	inc	hl
	ld	a, (hl)
	ld	-6 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-5 (ix), a
	jr	00106$
00108$:
	C$gamelogic.c$398$1_0$361	= .
	.globl	C$gamelogic.c$398$1_0$361
;src/engine/gamelogic.c:398: current_point_sprite->x = (x<<3) + 80 + (active_points_list->numElements<<2);
	ld	a, c
	C$gamelogic.c$399$1_0$361	= .
	.globl	C$gamelogic.c$399$1_0$361
;src/engine/gamelogic.c:399: current_point_sprite->y = (y<<3) + 8 + (active_points_list->numElements<<2);
	ld	c, -1 (ix)
	C$gamelogic.c$398$1_0$361	= .
	.globl	C$gamelogic.c$398$1_0$361
;src/engine/gamelogic.c:398: current_point_sprite->x = (x<<3) + 80 + (active_points_list->numElements<<2);
	add	a, a
	add	a, a
	add	a, a
	C$gamelogic.c$399$1_0$361	= .
	.globl	C$gamelogic.c$399$1_0$361
;src/engine/gamelogic.c:399: current_point_sprite->y = (y<<3) + 8 + (active_points_list->numElements<<2);
	push	af
	ld	a, c
	add	a, a
	add	a, a
	add	a, a
	ld	c, a
	pop	af
	C$gamelogic.c$398$1_0$361	= .
	.globl	C$gamelogic.c$398$1_0$361
;src/engine/gamelogic.c:398: current_point_sprite->x = (x<<3) + 80 + (active_points_list->numElements<<2);
	add	a, #0x50
	ld	-3 (ix), a
	C$gamelogic.c$399$1_0$361	= .
	.globl	C$gamelogic.c$399$1_0$361
;src/engine/gamelogic.c:399: current_point_sprite->y = (y<<3) + 8 + (active_points_list->numElements<<2);
	ld	a, c
	add	a, #0x08
	ld	-2 (ix), a
	C$gamelogic.c$388$1_0$361	= .
	.globl	C$gamelogic.c$388$1_0$361
;src/engine/gamelogic.c:388: if(consecutive100Points == 4) {
	ld	a, e
	sub	a, #0x04
	jr	NZ, 00113$
	C$gamelogic.c$389$2_0$365	= .
	.globl	C$gamelogic.c$389$2_0$365
;src/engine/gamelogic.c:389: deleteElementEndList(active_points_list);
	ld	hl, (_active_points_list)
	call	_deleteElementEndList
	C$gamelogic.c$390$2_0$365	= .
	.globl	C$gamelogic.c$390$2_0$365
;src/engine/gamelogic.c:390: deleteElementEndList(active_points_list);
	ld	hl, (_active_points_list)
	call	_deleteElementEndList
	C$gamelogic.c$391$2_0$365	= .
	.globl	C$gamelogic.c$391$2_0$365
;src/engine/gamelogic.c:391: deleteElementEndList(active_points_list);
	ld	hl, (_active_points_list)
	call	_deleteElementEndList
	C$gamelogic.c$392$2_0$365	= .
	.globl	C$gamelogic.c$392$2_0$365
;src/engine/gamelogic.c:392: current_element = active_points_list->tail;
	ld	hl, (_active_points_list)
	inc	hl
	inc	hl
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	C$gamelogic.c$393$2_0$365	= .
	.globl	C$gamelogic.c$393$2_0$365
;src/engine/gamelogic.c:393: current_point_sprite = (point_sprite*)(current_element->data);
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	C$gamelogic.c$394$2_0$365	= .
	.globl	C$gamelogic.c$394$2_0$365
;src/engine/gamelogic.c:394: current_point_sprite->firstsprite = POINT_500_SPRITE_INDEX;
	inc	hl
	inc	hl
	ld	(hl), #0x0f
	C$gamelogic.c$395$2_0$365	= .
	.globl	C$gamelogic.c$395$2_0$365
;src/engine/gamelogic.c:395: consecutive500Points++;
	ld	a, -4 (ix)
	C$gamelogic.c$396$2_0$365	= .
	.globl	C$gamelogic.c$396$2_0$365
;src/engine/gamelogic.c:396: if(consecutive500Points == 4 && !bonusScore) {
	sub	a, #0x04
	jr	NZ, 00115$
	ld	hl, #_bonusScore
	bit	0, (hl)
	jr	NZ, 00115$
	C$gamelogic.c$397$3_0$366	= .
	.globl	C$gamelogic.c$397$3_0$366
;src/engine/gamelogic.c:397: current_point_sprite = malloc(sizeof(point_sprite));
	ld	hl, #0x0003
	call	_malloc
	C$gamelogic.c$398$3_0$366	= .
	.globl	C$gamelogic.c$398$3_0$366
;src/engine/gamelogic.c:398: current_point_sprite->x = (x<<3) + 80 + (active_points_list->numElements<<2);
	ld	hl, (_active_points_list)
	ld	bc, #0x0004
	add	hl, bc
	ld	a, (hl)
	add	a, a
	add	a, a
	add	a, -3 (ix)
	ld	(de), a
	C$gamelogic.c$399$3_0$366	= .
	.globl	C$gamelogic.c$399$3_0$366
;src/engine/gamelogic.c:399: current_point_sprite->y = (y<<3) + 8 + (active_points_list->numElements<<2);
	ld	c, e
	ld	b, d
	inc	bc
	ld	hl, (_active_points_list)
	inc	hl
	inc	hl
	inc	hl
	inc	hl
	ld	a, (hl)
	add	a, a
	add	a, a
	add	a, -2 (ix)
	ld	(bc), a
	C$gamelogic.c$400$3_0$366	= .
	.globl	C$gamelogic.c$400$3_0$366
;src/engine/gamelogic.c:400: current_point_sprite->firstsprite = BONUS_SPRITE_INDEX;
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	(hl), #0x16
	C$gamelogic.c$401$3_0$366	= .
	.globl	C$gamelogic.c$401$3_0$366
;src/engine/gamelogic.c:401: addElementEndList(active_points_list, current_point_sprite);
	ld	hl, (_active_points_list)
	call	_addElementEndList
	C$gamelogic.c$402$3_0$366	= .
	.globl	C$gamelogic.c$402$3_0$366
;src/engine/gamelogic.c:402: bonusScore = true;
	ld	hl, #_bonusScore
	ld	(hl), #0x01
	C$gamelogic.c$403$3_0$366	= .
	.globl	C$gamelogic.c$403$3_0$366
;src/engine/gamelogic.c:403: bonusSamplePlay = true;
	ld	hl, #_bonusSamplePlay
	ld	(hl), #0x01
	jr	00115$
00113$:
	C$gamelogic.c$406$2_0$367	= .
	.globl	C$gamelogic.c$406$2_0$367
;src/engine/gamelogic.c:406: current_point_sprite = malloc(sizeof(point_sprite));
	ld	hl, #0x0003
	call	_malloc
	C$gamelogic.c$407$2_0$367	= .
	.globl	C$gamelogic.c$407$2_0$367
;src/engine/gamelogic.c:407: current_point_sprite->x = (x<<3) + 80 + (active_points_list->numElements<<2);
	ld	iy, (_active_points_list)
	ld	a, 4 (iy)
	add	a, a
	add	a, a
	add	a, -3 (ix)
	ld	(de), a
	C$gamelogic.c$408$2_0$367	= .
	.globl	C$gamelogic.c$408$2_0$367
;src/engine/gamelogic.c:408: current_point_sprite->y = (y<<3) + 8 + (active_points_list->numElements<<2);
	ld	c, e
	ld	b, d
	inc	bc
	ld	iy, (_active_points_list)
	ld	a, 4 (iy)
	add	a, a
	add	a, a
	add	a, -2 (ix)
	ld	(bc), a
	C$gamelogic.c$409$2_0$367	= .
	.globl	C$gamelogic.c$409$2_0$367
;src/engine/gamelogic.c:409: current_point_sprite->firstsprite = POINT_100_SPRITE_INDEX;
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, d
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	(hl), #0x0e
	C$gamelogic.c$410$2_0$367	= .
	.globl	C$gamelogic.c$410$2_0$367
;src/engine/gamelogic.c:410: addElementEndList(active_points_list, current_point_sprite);
	ld	hl, (_active_points_list)
	call	_addElementEndList
00115$:
	C$gamelogic.c$412$1_0$361	= .
	.globl	C$gamelogic.c$412$1_0$361
;src/engine/gamelogic.c:412: }
	ld	sp, ix
	pop	ix
	C$gamelogic.c$412$1_0$361	= .
	.globl	C$gamelogic.c$412$1_0$361
	XG$add_100point_sprite$0$0	= .
	.globl	XG$add_100point_sprite$0$0
	ret
	G$destroy_pieces$0$0	= .
	.globl	G$destroy_pieces$0$0
	C$gamelogic.c$414$1_0$369	= .
	.globl	C$gamelogic.c$414$1_0$369
;src/engine/gamelogic.c:414: void destroy_pieces(void) {
;	---------------------------------
; Function destroy_pieces
; ---------------------------------
_destroy_pieces::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	C$gamelogic.c$415$1_0$369	= .
	.globl	C$gamelogic.c$415$1_0$369
;src/engine/gamelogic.c:415: u8 position = ((row_index) << 3) + ((row_index) << 1) + last_square_x;
	ld	a, (_row_index+0)
	ld	c, a
	add	a, a
	add	a, a
	add	a, a
	sla	c
	add	a, c
	ld	c, a
	ld	a, (_last_square_x+0)
	add	a, c
	ld	b, a
	C$gamelogic.c$416$1_0$369	= .
	.globl	C$gamelogic.c$416$1_0$369
;src/engine/gamelogic.c:416: u8 numPiecesDestroyed = 0;
	ld	c, #0x00
	C$gamelogic.c$417$1_0$369	= .
	.globl	C$gamelogic.c$417$1_0$369
;src/engine/gamelogic.c:417: if(frame_cnt == XS_PERIOD) {
	ld	a, (_frame_cnt+0)
	sub	a, #0x18
	ld	hl, #_frame_cnt + 1
	or	a, (hl)
	jp	NZ,00146$
	C$gamelogic.c$418$2_0$370	= .
	.globl	C$gamelogic.c$418$2_0$370
;src/engine/gamelogic.c:418: destroy_piece_board(last_square_x, row_index);
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_last_square_x+0)
	call	_destroy_piece_board
	pop	bc
	C$gamelogic.c$420$1_0$369	= .
	.globl	C$gamelogic.c$420$1_0$369
;src/engine/gamelogic.c:420: if(!is_free_board_position(position+10)) {
	ld	-2 (ix), b
	ld	-1 (ix), #0x00
	C$gamelogic.c$419$2_0$370	= .
	.globl	C$gamelogic.c$419$2_0$370
;src/engine/gamelogic.c:419: if(row_index < (BOARD_ROWS - 1)) {
	ld	a, (_row_index+0)
	sub	a, #0x15
	jr	NC, 00104$
	C$gamelogic.c$420$3_0$371	= .
	.globl	C$gamelogic.c$420$3_0$371
;src/engine/gamelogic.c:420: if(!is_free_board_position(position+10)) {
	pop	hl
	push	hl
	ld	de, #0x000a
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
	C$gamelogic.c$421$4_0$372	= .
	.globl	C$gamelogic.c$421$4_0$372
;src/engine/gamelogic.c:421: numPiecesDestroyed++;
	ld	c, #0x01
	C$gamelogic.c$422$4_0$372	= .
	.globl	C$gamelogic.c$422$4_0$372
;src/engine/gamelogic.c:422: add_100point_sprite(last_square_x, row_index + 1);
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	l
	push	bc
	ld	a, (_last_square_x+0)
	call	_add_100point_sprite
	pop	bc
00102$:
	C$gamelogic.c$424$3_0$371	= .
	.globl	C$gamelogic.c$424$3_0$371
;src/engine/gamelogic.c:424: destroy_piece_board(last_square_x, row_index + 1);
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	l
	push	bc
	ld	a, (_last_square_x+0)
	call	_destroy_piece_board
	pop	bc
00104$:
	C$gamelogic.c$426$2_0$370	= .
	.globl	C$gamelogic.c$426$2_0$370
;src/engine/gamelogic.c:426: if(row_index < (BOARD_ROWS - 2)) {
	ld	a, (_row_index+0)
	sub	a, #0x14
	jr	NC, 00108$
	C$gamelogic.c$427$3_0$373	= .
	.globl	C$gamelogic.c$427$3_0$373
;src/engine/gamelogic.c:427: if(!is_free_board_position(position+20)) {
	pop	hl
	push	hl
	ld	de, #0x0014
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00106$
	C$gamelogic.c$428$4_0$374	= .
	.globl	C$gamelogic.c$428$4_0$374
;src/engine/gamelogic.c:428: numPiecesDestroyed++;
	inc	c
	C$gamelogic.c$429$4_0$374	= .
	.globl	C$gamelogic.c$429$4_0$374
;src/engine/gamelogic.c:429: add_100point_sprite(last_square_x, row_index + 2);
	ld	a, (_row_index+0)
	add	a, #0x02
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	ld	a, (_last_square_x+0)
	call	_add_100point_sprite
	pop	bc
00106$:
	C$gamelogic.c$431$3_0$373	= .
	.globl	C$gamelogic.c$431$3_0$373
;src/engine/gamelogic.c:431: destroy_piece_board(last_square_x, row_index + 2);
	ld	a, (_row_index+0)
	add	a, #0x02
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	ld	a, (_last_square_x+0)
	call	_destroy_piece_board
	pop	bc
00108$:
	C$gamelogic.c$433$2_0$370	= .
	.globl	C$gamelogic.c$433$2_0$370
;src/engine/gamelogic.c:433: if(last_square_x > 0) {
	ld	a, (_last_square_x+0)
	or	a, a
	jr	Z, 00112$
	C$gamelogic.c$434$3_0$375	= .
	.globl	C$gamelogic.c$434$3_0$375
;src/engine/gamelogic.c:434: if(!is_free_board_position(position-1)) {
	ld	de, #_board+0
	pop	hl
	push	hl
	dec	hl
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00110$
	C$gamelogic.c$435$4_0$376	= .
	.globl	C$gamelogic.c$435$4_0$376
;src/engine/gamelogic.c:435: numPiecesDestroyed++;
	inc	c
	C$gamelogic.c$436$4_0$376	= .
	.globl	C$gamelogic.c$436$4_0$376
;src/engine/gamelogic.c:436: add_100point_sprite(last_square_x - 1, row_index);
	ld	a, (_last_square_x+0)
	ld	b, a
	dec	b
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_add_100point_sprite
	pop	bc
00110$:
	C$gamelogic.c$438$3_0$375	= .
	.globl	C$gamelogic.c$438$3_0$375
;src/engine/gamelogic.c:438: destroy_piece_board(last_square_x - 1, row_index);
	ld	a, (_last_square_x+0)
	ld	b, a
	dec	b
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_destroy_piece_board
	pop	bc
00112$:
	C$gamelogic.c$440$2_0$370	= .
	.globl	C$gamelogic.c$440$2_0$370
;src/engine/gamelogic.c:440: if(last_square_x > 1) {
	ld	a, #0x01
	ld	hl, #_last_square_x
	sub	a, (hl)
	jr	NC, 00116$
	C$gamelogic.c$441$3_0$377	= .
	.globl	C$gamelogic.c$441$3_0$377
;src/engine/gamelogic.c:441: if(!is_free_board_position(position-2)) {
	ld	de, #_board+0
	pop	hl
	push	hl
	dec	hl
	dec	hl
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00114$
	C$gamelogic.c$442$4_0$378	= .
	.globl	C$gamelogic.c$442$4_0$378
;src/engine/gamelogic.c:442: numPiecesDestroyed++;
	inc	c
	C$gamelogic.c$443$4_0$378	= .
	.globl	C$gamelogic.c$443$4_0$378
;src/engine/gamelogic.c:443: add_100point_sprite(last_square_x - 2, row_index);
	ld	a, (_last_square_x+0)
	ld	b, a
	dec	b
	dec	b
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_add_100point_sprite
	pop	bc
00114$:
	C$gamelogic.c$445$3_0$377	= .
	.globl	C$gamelogic.c$445$3_0$377
;src/engine/gamelogic.c:445: destroy_piece_board(last_square_x - 2, row_index);
	ld	a, (_last_square_x+0)
	ld	b, a
	dec	b
	dec	b
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_destroy_piece_board
	pop	bc
00116$:
	C$gamelogic.c$447$2_0$370	= .
	.globl	C$gamelogic.c$447$2_0$370
;src/engine/gamelogic.c:447: if(last_square_x  < (BOARD_COLUMNS - 1)) {
	ld	a, (_last_square_x+0)
	sub	a, #0x09
	jr	NC, 00120$
	C$gamelogic.c$448$3_0$379	= .
	.globl	C$gamelogic.c$448$3_0$379
;src/engine/gamelogic.c:448: if(!is_free_board_position(position+1)) {
	ld	de, #_board+0
	pop	hl
	push	hl
	inc	hl
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00118$
	C$gamelogic.c$449$4_0$380	= .
	.globl	C$gamelogic.c$449$4_0$380
;src/engine/gamelogic.c:449: numPiecesDestroyed++;
	inc	c
	C$gamelogic.c$450$4_0$380	= .
	.globl	C$gamelogic.c$450$4_0$380
;src/engine/gamelogic.c:450: add_100point_sprite(last_square_x + 1, row_index);
	ld	a, (_last_square_x+0)
	ld	b, a
	inc	b
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_add_100point_sprite
	pop	bc
00118$:
	C$gamelogic.c$452$3_0$379	= .
	.globl	C$gamelogic.c$452$3_0$379
;src/engine/gamelogic.c:452: destroy_piece_board(last_square_x + 1, row_index);
	ld	a, (_last_square_x+0)
	ld	b, a
	inc	b
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_destroy_piece_board
	pop	bc
00120$:
	C$gamelogic.c$454$2_0$370	= .
	.globl	C$gamelogic.c$454$2_0$370
;src/engine/gamelogic.c:454: if(last_square_x  < (BOARD_COLUMNS - 2)) {
	ld	a, (_last_square_x+0)
	sub	a, #0x08
	jr	NC, 00124$
	C$gamelogic.c$455$3_0$381	= .
	.globl	C$gamelogic.c$455$3_0$381
;src/engine/gamelogic.c:455: if(!is_free_board_position(position+2)) {
	ld	de, #_board+0
	pop	hl
	push	hl
	inc	hl
	inc	hl
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00122$
	C$gamelogic.c$456$4_0$382	= .
	.globl	C$gamelogic.c$456$4_0$382
;src/engine/gamelogic.c:456: numPiecesDestroyed++;
	inc	c
	C$gamelogic.c$457$4_0$382	= .
	.globl	C$gamelogic.c$457$4_0$382
;src/engine/gamelogic.c:457: add_100point_sprite(last_square_x + 2, row_index);
	ld	a, (_last_square_x+0)
	add	a, #0x02
	ld	b, a
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_add_100point_sprite
	pop	bc
00122$:
	C$gamelogic.c$459$3_0$381	= .
	.globl	C$gamelogic.c$459$3_0$381
;src/engine/gamelogic.c:459: destroy_piece_board(last_square_x + 2, row_index);
	ld	a, (_last_square_x+0)
	add	a, #0x02
	ld	b, a
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_destroy_piece_board
	pop	bc
00124$:
	C$gamelogic.c$461$2_0$370	= .
	.globl	C$gamelogic.c$461$2_0$370
;src/engine/gamelogic.c:461: increment_score(numPiecesDestroyed);
	ld	a, c
	call	_increment_score
	C$gamelogic.c$462$2_0$370	= .
	.globl	C$gamelogic.c$462$2_0$370
;src/engine/gamelogic.c:462: play_fx(DESTROY_FX);
	ld	a, #0x04
	call	_play_fx
	jp	00148$
00146$:
	C$gamelogic.c$463$1_0$369	= .
	.globl	C$gamelogic.c$463$1_0$369
;src/engine/gamelogic.c:463: } else if(frame_cnt == XS_PERIOD + XXXS_PERIOD) {
	ld	a, (_frame_cnt+0)
	sub	a, #0x1d
	ld	hl, #_frame_cnt + 1
	or	a, (hl)
	jp	NZ,00143$
	C$gamelogic.c$464$2_0$383	= .
	.globl	C$gamelogic.c$464$2_0$383
;src/engine/gamelogic.c:464: clean_explosion(last_square_x, row_index, position);
	push	bc
	push	bc
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, (_last_square_x+0)
	call	_clean_explosion
	pop	bc
	C$gamelogic.c$466$1_0$369	= .
	.globl	C$gamelogic.c$466$1_0$369
;src/engine/gamelogic.c:466: clean_explosion(last_square_x, row_index + 1, position + 10); 
	C$gamelogic.c$465$2_0$383	= .
	.globl	C$gamelogic.c$465$2_0$383
;src/engine/gamelogic.c:465: if(row_index < (BOARD_ROWS - 1)) {
	ld	a, (_row_index+0)
	sub	a, #0x15
	jr	NC, 00126$
	C$gamelogic.c$466$3_0$384	= .
	.globl	C$gamelogic.c$466$3_0$384
;src/engine/gamelogic.c:466: clean_explosion(last_square_x, row_index + 1, position + 10); 
	ld	a, b
	add	a, #0x0a
	ld	d, a
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	l
	push	bc
	push	de
	inc	sp
	ld	a, (_last_square_x+0)
	call	_clean_explosion
	pop	bc
00126$:
	C$gamelogic.c$468$2_0$383	= .
	.globl	C$gamelogic.c$468$2_0$383
;src/engine/gamelogic.c:468: if(row_index < (BOARD_ROWS - 2)) {
	ld	a, (_row_index+0)
	sub	a, #0x14
	jr	NC, 00128$
	C$gamelogic.c$469$3_0$385	= .
	.globl	C$gamelogic.c$469$3_0$385
;src/engine/gamelogic.c:469: clean_explosion(last_square_x, row_index + 2, position + 20); 
	ld	a, b
	add	a, #0x14
	ld	d, a
	ld	a, (_row_index+0)
	add	a, #0x02
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	push	de
	inc	sp
	ld	a, (_last_square_x+0)
	call	_clean_explosion
	pop	bc
00128$:
	C$gamelogic.c$471$2_0$383	= .
	.globl	C$gamelogic.c$471$2_0$383
;src/engine/gamelogic.c:471: if(last_square_x > 0) {
	ld	a, (_last_square_x+0)
	or	a, a
	jr	Z, 00130$
	C$gamelogic.c$472$3_0$386	= .
	.globl	C$gamelogic.c$472$3_0$386
;src/engine/gamelogic.c:472: clean_explosion(last_square_x - 1, row_index, position - 1);
	ld	d, b
	dec	d
	ld	a, (_last_square_x+0)
	ld	c, a
	dec	c
	push	bc
	push	de
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_clean_explosion
	pop	bc
00130$:
	C$gamelogic.c$474$2_0$383	= .
	.globl	C$gamelogic.c$474$2_0$383
;src/engine/gamelogic.c:474: if(last_square_x > 1) {
	ld	a, #0x01
	ld	hl, #_last_square_x
	sub	a, (hl)
	jr	NC, 00132$
	C$gamelogic.c$475$3_0$387	= .
	.globl	C$gamelogic.c$475$3_0$387
;src/engine/gamelogic.c:475: clean_explosion(last_square_x - 2, row_index, position - 2);
	ld	d, b
	dec	d
	dec	d
	ld	a, (_last_square_x+0)
	ld	c, a
	dec	c
	dec	c
	push	bc
	push	de
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_clean_explosion
	pop	bc
00132$:
	C$gamelogic.c$477$2_0$383	= .
	.globl	C$gamelogic.c$477$2_0$383
;src/engine/gamelogic.c:477: if(last_square_x < (BOARD_COLUMNS - 1)) {
	ld	a, (_last_square_x+0)
	sub	a, #0x09
	jr	NC, 00134$
	C$gamelogic.c$478$3_0$388	= .
	.globl	C$gamelogic.c$478$3_0$388
;src/engine/gamelogic.c:478: clean_explosion(last_square_x + 1, row_index, position + 1);
	ld	d, b
	inc	d
	ld	a, (_last_square_x+0)
	ld	c, a
	inc	c
	push	bc
	push	de
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_clean_explosion
	pop	bc
00134$:
	C$gamelogic.c$480$2_0$383	= .
	.globl	C$gamelogic.c$480$2_0$383
;src/engine/gamelogic.c:480: if(last_square_x < (BOARD_COLUMNS - 2)) {
	ld	a, (_last_square_x+0)
	sub	a, #0x08
	jr	NC, 00136$
	C$gamelogic.c$481$3_0$389	= .
	.globl	C$gamelogic.c$481$3_0$389
;src/engine/gamelogic.c:481: clean_explosion(last_square_x + 2, row_index, position + 2);
	inc	b
	inc	b
	ld	a, (_last_square_x+0)
	add	a, #0x02
	ld	c, a
	push	bc
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_clean_explosion
00136$:
	C$gamelogic.c$483$2_0$383	= .
	.globl	C$gamelogic.c$483$2_0$383
;src/engine/gamelogic.c:483: game_status = GAME_STATUS_MAKE_PIECES_FALL;
	ld	hl, #_game_status
	ld	(hl), #0x0b
	C$gamelogic.c$484$2_0$383	= .
	.globl	C$gamelogic.c$484$2_0$383
;src/engine/gamelogic.c:484: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$485$2_0$383	= .
	.globl	C$gamelogic.c$485$2_0$383
;src/engine/gamelogic.c:485: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	C$gamelogic.c$486$2_0$383	= .
	.globl	C$gamelogic.c$486$2_0$383
;src/engine/gamelogic.c:486: restore_fire_colors();
	call	_restore_fire_colors
	C$gamelogic.c$487$2_0$383	= .
	.globl	C$gamelogic.c$487$2_0$383
;src/engine/gamelogic.c:487: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	jr	00148$
00143$:
	C$gamelogic.c$489$2_0$390	= .
	.globl	C$gamelogic.c$489$2_0$390
;src/engine/gamelogic.c:489: if((frame_cnt & 3) == 0) { 
	ld	a, (_frame_cnt+0)
	and	a, #0x03
	jr	NZ, 00148$
	C$gamelogic.c$490$3_0$391	= .
	.globl	C$gamelogic.c$490$3_0$391
;src/engine/gamelogic.c:490: if(isPaletteRed) {
	ld	hl, #_isPaletteRed
	bit	0, (hl)
	jr	Z, 00138$
	C$gamelogic.c$491$4_0$392	= .
	.globl	C$gamelogic.c$491$4_0$392
;src/engine/gamelogic.c:491: restore_fire_colors();
	call	_restore_fire_colors
	C$gamelogic.c$492$4_0$392	= .
	.globl	C$gamelogic.c$492$4_0$392
;src/engine/gamelogic.c:492: isPaletteRed = false;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x00
	jr	00148$
00138$:
	C$gamelogic.c$494$4_0$393	= .
	.globl	C$gamelogic.c$494$4_0$393
;src/engine/gamelogic.c:494: swap_fire_colors();
	call	_swap_fire_colors
	C$gamelogic.c$495$4_0$393	= .
	.globl	C$gamelogic.c$495$4_0$393
;src/engine/gamelogic.c:495: isPaletteRed = true;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x01
00148$:
	C$gamelogic.c$500$1_0$369	= .
	.globl	C$gamelogic.c$500$1_0$369
;src/engine/gamelogic.c:500: }
	ld	sp, ix
	pop	ix
	C$gamelogic.c$500$1_0$369	= .
	.globl	C$gamelogic.c$500$1_0$369
	XG$destroy_pieces$0$0	= .
	.globl	XG$destroy_pieces$0$0
	ret
	G$compute_paths$0$0	= .
	.globl	G$compute_paths$0$0
	C$gamelogic.c$502$1_0$395	= .
	.globl	C$gamelogic.c$502$1_0$395
;src/engine/gamelogic.c:502: bool compute_paths(void) {
;	---------------------------------
; Function compute_paths
; ---------------------------------
_compute_paths::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$gamelogic.c$503$2_0$395	= .
	.globl	C$gamelogic.c$503$2_0$395
;src/engine/gamelogic.c:503: bool somePiece = false;
	ld	-1 (ix), #0x00
	C$gamelogic.c$504$2_0$395	= .
	.globl	C$gamelogic.c$504$2_0$395
;src/engine/gamelogic.c:504: bool someChangeHappened = false;
	ld	c, #0x00
	C$gamelogic.c$505$1_0$395	= .
	.globl	C$gamelogic.c$505$1_0$395
;src/engine/gamelogic.c:505: u8 position = (row_index << 3) + (row_index << 1);
	ld	a, (_row_index+0)
	ld	e, a
	add	a, a
	add	a, a
	add	a, a
	sla	e
	add	a, e
	ld	e, a
	C$gamelogic.c$507$3_0$397	= .
	.globl	C$gamelogic.c$507$3_0$397
;src/engine/gamelogic.c:507: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	b, #0x00
00108$:
	C$gamelogic.c$508$3_0$397	= .
	.globl	C$gamelogic.c$508$3_0$397
;src/engine/gamelogic.c:508: if(!is_free_board_position(position)) {
	ld	hl, #_board
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
	C$gamelogic.c$509$4_0$398	= .
	.globl	C$gamelogic.c$509$4_0$398
;src/engine/gamelogic.c:509: someChangeHappened = someChangeHappened || compute_path(i,row_index);
	bit	0, c
	jr	NZ, 00113$
	push	bc
	push	de
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_compute_path
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	pop	de
	pop	bc
	bit	0, l
	ld	c, #0x00
	jr	Z, 00114$
00113$:
	ld	c, #0x01
00114$:
	C$gamelogic.c$510$4_0$398	= .
	.globl	C$gamelogic.c$510$4_0$398
;src/engine/gamelogic.c:510: somePiece = true;
	ld	-1 (ix), #0x01
00102$:
	C$gamelogic.c$512$3_0$397	= .
	.globl	C$gamelogic.c$512$3_0$397
;src/engine/gamelogic.c:512: position++;
	inc	e
	C$gamelogic.c$507$2_0$396	= .
	.globl	C$gamelogic.c$507$2_0$396
;src/engine/gamelogic.c:507: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	b
	ld	a, b
	sub	a, #0x0a
	jr	C, 00108$
	C$gamelogic.c$516$1_0$395	= .
	.globl	C$gamelogic.c$516$1_0$395
;src/engine/gamelogic.c:516: if(row_index > 0 && somePiece) {
	ld	a, (_row_index+0)
	or	a, a
	jr	Z, 00105$
	bit	0, -1 (ix)
	jr	Z, 00105$
	C$gamelogic.c$517$2_0$399	= .
	.globl	C$gamelogic.c$517$2_0$399
;src/engine/gamelogic.c:517: row_index--;
	ld	hl, #_row_index
	dec	(hl)
	jr	00106$
00105$:
	C$gamelogic.c$519$2_0$400	= .
	.globl	C$gamelogic.c$519$2_0$400
;src/engine/gamelogic.c:519: game_status = GAME_STATUS_CHECK_CALCULATIONS; 
	ld	hl, #_game_status
	ld	(hl), #0x07
00106$:
	C$gamelogic.c$521$1_0$395	= .
	.globl	C$gamelogic.c$521$1_0$395
;src/engine/gamelogic.c:521: return someChangeHappened;
	ld	a, c
	C$gamelogic.c$522$1_0$395	= .
	.globl	C$gamelogic.c$522$1_0$395
;src/engine/gamelogic.c:522: }
	inc	sp
	pop	ix
	C$gamelogic.c$522$1_0$395	= .
	.globl	C$gamelogic.c$522$1_0$395
	XG$compute_paths$0$0	= .
	.globl	XG$compute_paths$0$0
	ret
	G$compute_disconnected_pieces$0$0	= .
	.globl	G$compute_disconnected_pieces$0$0
	C$gamelogic.c$524$1_0$402	= .
	.globl	C$gamelogic.c$524$1_0$402
;src/engine/gamelogic.c:524: bool compute_disconnected_pieces(void) {
;	---------------------------------
; Function compute_disconnected_pieces
; ---------------------------------
_compute_disconnected_pieces::
	push	ix
	ld	ix,#0
	add	ix,sp
	dec	sp
	C$gamelogic.c$525$2_0$402	= .
	.globl	C$gamelogic.c$525$2_0$402
;src/engine/gamelogic.c:525: bool somePiece = false;
	ld	c, #0x00
	C$gamelogic.c$526$2_0$402	= .
	.globl	C$gamelogic.c$526$2_0$402
;src/engine/gamelogic.c:526: bool somePieceDestroyed = false;
	ld	-1 (ix), #0x00
	C$gamelogic.c$527$1_0$402	= .
	.globl	C$gamelogic.c$527$1_0$402
;src/engine/gamelogic.c:527: u8 position = (row_index << 3) + (row_index << 1);
	ld	a, (_row_index+0)
	ld	b, a
	add	a, a
	add	a, a
	add	a, a
	sla	b
	add	a, b
	ld	e, a
	C$gamelogic.c$529$3_0$404	= .
	.globl	C$gamelogic.c$529$3_0$404
;src/engine/gamelogic.c:529: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	b, #0x00
00115$:
	C$gamelogic.c$530$3_0$404	= .
	.globl	C$gamelogic.c$530$3_0$404
;src/engine/gamelogic.c:530: if(!is_free_board_position(position)) {
	ld	hl, #_board
	ld	d, #0x00
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
	C$gamelogic.c$531$4_0$405	= .
	.globl	C$gamelogic.c$531$4_0$405
;src/engine/gamelogic.c:531: somePieceDestroyed = mark_as_disconnected(i, row_index, position) || somePieceDestroyed;
	push	bc
	push	de
	ld	a, e
	push	af
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, b
	call	_mark_as_disconnected
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	pop	de
	pop	bc
	bit	0, l
	jr	NZ, 00120$
	bit	0, -1 (ix)
	jr	NZ, 00120$
	xor	a, a
	jr	00121$
00120$:
	ld	a, #0x01
00121$:
	ld	-1 (ix), a
	C$gamelogic.c$532$4_0$405	= .
	.globl	C$gamelogic.c$532$4_0$405
;src/engine/gamelogic.c:532: somePiece = true;
	ld	c, #0x01
00102$:
	C$gamelogic.c$534$3_0$404	= .
	.globl	C$gamelogic.c$534$3_0$404
;src/engine/gamelogic.c:534: position++;
	inc	e
	C$gamelogic.c$529$2_0$403	= .
	.globl	C$gamelogic.c$529$2_0$403
;src/engine/gamelogic.c:529: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	b
	ld	a, b
	sub	a, #0x0a
	jr	C, 00115$
	C$gamelogic.c$536$1_0$402	= .
	.globl	C$gamelogic.c$536$1_0$402
;src/engine/gamelogic.c:536: if(row_index > 0 && somePiece) {
	ld	a, (_row_index+0)
	or	a, a
	jr	Z, 00112$
	bit	0, c
	jr	Z, 00112$
	C$gamelogic.c$537$2_0$406	= .
	.globl	C$gamelogic.c$537$2_0$406
;src/engine/gamelogic.c:537: row_index--;
	ld	hl, #_row_index
	dec	(hl)
	jr	00113$
00112$:
	C$gamelogic.c$539$2_0$407	= .
	.globl	C$gamelogic.c$539$2_0$407
;src/engine/gamelogic.c:539: game_status = GAME_STATUS_ANIMATE_DISCONNECTED;
	ld	hl, #_game_status
	ld	(hl), #0x09
	C$gamelogic.c$540$2_0$407	= .
	.globl	C$gamelogic.c$540$2_0$407
;src/engine/gamelogic.c:540: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$541$2_0$407	= .
	.globl	C$gamelogic.c$541$2_0$407
;src/engine/gamelogic.c:541: connected_pieces_to_bw_palette();
	call	_connected_pieces_to_bw_palette
	C$gamelogic.c$542$2_0$407	= .
	.globl	C$gamelogic.c$542$2_0$407
;src/engine/gamelogic.c:542: max_row_index = row_index;
	ld	a, (_row_index+0)
	ld	(_max_row_index+0), a
	C$gamelogic.c$543$2_0$407	= .
	.globl	C$gamelogic.c$543$2_0$407
;src/engine/gamelogic.c:543: if(row_index < 6) {
	ld	a, (_row_index+0)
	sub	a, #0x06
	jr	NC, 00109$
	C$gamelogic.c$544$3_0$408	= .
	.globl	C$gamelogic.c$544$3_0$408
;src/engine/gamelogic.c:544: if(!isHurryUp) {
	ld	hl, #_isHurryUp
	bit	0, (hl)
	jr	NZ, 00110$
	C$gamelogic.c$545$4_0$409	= .
	.globl	C$gamelogic.c$545$4_0$409
;src/engine/gamelogic.c:545: play_song(HURRYUP_OST);
	ld	a, #0x07
	call	_play_song
	C$gamelogic.c$546$4_0$409	= .
	.globl	C$gamelogic.c$546$4_0$409
;src/engine/gamelogic.c:546: isHurryUp = true;
	ld	hl, #_isHurryUp
	ld	(hl), #0x01
	jr	00110$
00109$:
	C$gamelogic.c$549$3_0$410	= .
	.globl	C$gamelogic.c$549$3_0$410
;src/engine/gamelogic.c:549: if(isHurryUp) {
	ld	hl, #_isHurryUp
	bit	0, (hl)
	jr	Z, 00110$
	C$gamelogic.c$550$4_0$411	= .
	.globl	C$gamelogic.c$550$4_0$411
;src/engine/gamelogic.c:550: play_song(INGAME_OST);
	ld	a, #0x06
	call	_play_song
	C$gamelogic.c$551$4_0$411	= .
	.globl	C$gamelogic.c$551$4_0$411
;src/engine/gamelogic.c:551: isHurryUp = false;
	ld	hl, #_isHurryUp
	ld	(hl), #0x00
00110$:
	C$gamelogic.c$554$2_0$407	= .
	.globl	C$gamelogic.c$554$2_0$407
;src/engine/gamelogic.c:554: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
00113$:
	C$gamelogic.c$556$1_0$402	= .
	.globl	C$gamelogic.c$556$1_0$402
;src/engine/gamelogic.c:556: return somePieceDestroyed;
	ld	a, -1 (ix)
	C$gamelogic.c$557$1_0$402	= .
	.globl	C$gamelogic.c$557$1_0$402
;src/engine/gamelogic.c:557: }
	inc	sp
	pop	ix
	C$gamelogic.c$557$1_0$402	= .
	.globl	C$gamelogic.c$557$1_0$402
	XG$compute_disconnected_pieces$0$0	= .
	.globl	XG$compute_disconnected_pieces$0$0
	ret
	G$destroy_disconnected_pieces$0$0	= .
	.globl	G$destroy_disconnected_pieces$0$0
	C$gamelogic.c$559$1_0$413	= .
	.globl	C$gamelogic.c$559$1_0$413
;src/engine/gamelogic.c:559: void destroy_disconnected_pieces(void) {
;	---------------------------------
; Function destroy_disconnected_pieces
; ---------------------------------
_destroy_disconnected_pieces::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	dec	sp
	C$gamelogic.c$560$2_0$413	= .
	.globl	C$gamelogic.c$560$2_0$413
;src/engine/gamelogic.c:560: bool somePieceInRow = false;
	ld	e, #0x00
	C$gamelogic.c$561$1_0$413	= .
	.globl	C$gamelogic.c$561$1_0$413
;src/engine/gamelogic.c:561: u8 position = (row_index << 3) + (row_index << 1);
	ld	a, (_row_index+0)
	ld	c, a
	add	a, a
	add	a, a
	add	a, a
	sla	c
	add	a, c
	ld	-2 (ix), a
	C$gamelogic.c$563$1_0$413	= .
	.globl	C$gamelogic.c$563$1_0$413
;src/engine/gamelogic.c:563: u8 numPiecesDestroyed = 0;
	ld	-1 (ix), #0x00
	C$gamelogic.c$565$3_0$415	= .
	.globl	C$gamelogic.c$565$3_0$415
;src/engine/gamelogic.c:565: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	c, #0x00
00121$:
	C$gamelogic.c$566$3_0$415	= .
	.globl	C$gamelogic.c$566$3_0$415
;src/engine/gamelogic.c:566: if(!is_free_board_position(position)) {
	ld	a, #<(_board)
	add	a, -2 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, #0x00
	ld	h, a
	ld	b, (hl)
	ld	a, b
	or	a, a
	jr	Z, 00103$
	C$gamelogic.c$567$4_0$416	= .
	.globl	C$gamelogic.c$567$4_0$416
;src/engine/gamelogic.c:567: numPiecesDestroyedPosition = destroy_disconnected(i, row_index, position);
	push	bc
	ld	a, -2 (ix)
	push	af
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_destroy_disconnected
	pop	bc
	ld	-3 (ix), a
	C$gamelogic.c$568$1_0$413	= .
	.globl	C$gamelogic.c$568$1_0$413
;src/engine/gamelogic.c:568: for(j=0;j<numPiecesDestroyedPosition;j++) {
	ld	b, #0x00
00119$:
	ld	a, b
	sub	a, -3 (ix)
	jr	NC, 00101$
	C$gamelogic.c$569$6_0$418	= .
	.globl	C$gamelogic.c$569$6_0$418
;src/engine/gamelogic.c:569: add_100point_sprite(i,row_index);
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_add_100point_sprite
	pop	bc
	C$gamelogic.c$568$5_0$417	= .
	.globl	C$gamelogic.c$568$5_0$417
;src/engine/gamelogic.c:568: for(j=0;j<numPiecesDestroyedPosition;j++) {
	inc	b
	jr	00119$
00101$:
	C$gamelogic.c$571$4_0$416	= .
	.globl	C$gamelogic.c$571$4_0$416
;src/engine/gamelogic.c:571: numPiecesDestroyed = numPiecesDestroyed + numPiecesDestroyedPosition;
	ld	a, -1 (ix)
	add	a, -3 (ix)
	ld	-1 (ix), a
	C$gamelogic.c$572$4_0$416	= .
	.globl	C$gamelogic.c$572$4_0$416
;src/engine/gamelogic.c:572: somePieceInRow = true;
	ld	e, #0x01
00103$:
	C$gamelogic.c$574$3_0$415	= .
	.globl	C$gamelogic.c$574$3_0$415
;src/engine/gamelogic.c:574: if(row_index < BOARD_ROWS - 2) {
	ld	a, (_row_index+0)
	sub	a, #0x14
	jr	NC, 00107$
	C$gamelogic.c$575$4_0$419	= .
	.globl	C$gamelogic.c$575$4_0$419
;src/engine/gamelogic.c:575: if(isExploding(position+20)) {
	ld	a, -2 (ix)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	a, #0x14
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	jr	NC, 00182$
	inc	h
00182$:
	push	de
	ld	de, #_board
	add	hl, de
	pop	de
	ld	a, (hl)
	sub	a, #0x0b
	jr	NZ, 00107$
	C$gamelogic.c$576$5_0$420	= .
	.globl	C$gamelogic.c$576$5_0$420
;src/engine/gamelogic.c:576: clean_explosion(i, row_index+2,position+20);
	ld	a, -2 (ix)
	add	a, #0x14
	ld	b, a
	ld	a, (_row_index+0)
	add	a, #0x02
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	push	bc
	push	bc
	inc	sp
	ld	a, c
	call	_clean_explosion
	pop	bc
	C$gamelogic.c$577$5_0$420	= .
	.globl	C$gamelogic.c$577$5_0$420
;src/engine/gamelogic.c:577: somePieceInRow = true; //TODO: TOO HACKY??
	ld	e, #0x01
00107$:
	C$gamelogic.c$580$3_0$415	= .
	.globl	C$gamelogic.c$580$3_0$415
;src/engine/gamelogic.c:580: position++;
	inc	-2 (ix)
	C$gamelogic.c$565$2_0$414	= .
	.globl	C$gamelogic.c$565$2_0$414
;src/engine/gamelogic.c:565: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	c
	ld	a, c
	sub	a, #0x0a
	jr	C, 00121$
	C$gamelogic.c$582$1_0$413	= .
	.globl	C$gamelogic.c$582$1_0$413
;src/engine/gamelogic.c:582: increment_score(numPiecesDestroyed);
	push	de
	ld	a, -1 (ix)
	call	_increment_score
	pop	de
	C$gamelogic.c$584$1_0$413	= .
	.globl	C$gamelogic.c$584$1_0$413
;src/engine/gamelogic.c:584: if(row_index > 0 && somePieceInRow) {
	ld	a, (_row_index+0)
	or	a, a
	jr	Z, 00115$
	bit	0, e
	jr	Z, 00115$
	C$gamelogic.c$585$2_0$421	= .
	.globl	C$gamelogic.c$585$2_0$421
;src/engine/gamelogic.c:585: row_index--;
	ld	hl, #_row_index
	dec	(hl)
	C$gamelogic.c$586$2_0$421	= .
	.globl	C$gamelogic.c$586$2_0$421
;src/engine/gamelogic.c:586: if((frame_cnt & 3) == 0) { 
	ld	a, (_frame_cnt+0)
	and	a, #0x03
	jr	NZ, 00123$
	C$gamelogic.c$587$3_0$422	= .
	.globl	C$gamelogic.c$587$3_0$422
;src/engine/gamelogic.c:587: if(isPaletteRed) {
	ld	hl, #_isPaletteRed
	bit	0, (hl)
	jr	Z, 00110$
	C$gamelogic.c$588$4_0$423	= .
	.globl	C$gamelogic.c$588$4_0$423
;src/engine/gamelogic.c:588: restore_fire_colors();
	call	_restore_fire_colors
	C$gamelogic.c$589$4_0$423	= .
	.globl	C$gamelogic.c$589$4_0$423
;src/engine/gamelogic.c:589: isPaletteRed = false;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x00
	jr	00123$
00110$:
	C$gamelogic.c$591$4_0$424	= .
	.globl	C$gamelogic.c$591$4_0$424
;src/engine/gamelogic.c:591: swap_fire_colors();
	call	_swap_fire_colors
	C$gamelogic.c$592$4_0$424	= .
	.globl	C$gamelogic.c$592$4_0$424
;src/engine/gamelogic.c:592: isPaletteRed = true;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x01
	jr	00123$
00115$:
	C$gamelogic.c$596$2_0$425	= .
	.globl	C$gamelogic.c$596$2_0$425
;src/engine/gamelogic.c:596: game_status = GAME_STATUS_MAKE_PIECES_FALL;
	ld	hl, #_game_status
	ld	(hl), #0x0b
	C$gamelogic.c$597$2_0$425	= .
	.globl	C$gamelogic.c$597$2_0$425
;src/engine/gamelogic.c:597: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$598$2_0$425	= .
	.globl	C$gamelogic.c$598$2_0$425
;src/engine/gamelogic.c:598: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	C$gamelogic.c$599$2_0$425	= .
	.globl	C$gamelogic.c$599$2_0$425
;src/engine/gamelogic.c:599: restore_fire_colors();
	call	_restore_fire_colors
00123$:
	C$gamelogic.c$601$1_0$413	= .
	.globl	C$gamelogic.c$601$1_0$413
;src/engine/gamelogic.c:601: }
	ld	sp, ix
	pop	ix
	C$gamelogic.c$601$1_0$413	= .
	.globl	C$gamelogic.c$601$1_0$413
	XG$destroy_disconnected_pieces$0$0	= .
	.globl	XG$destroy_disconnected_pieces$0$0
	ret
	G$make_pieces_fall$0$0	= .
	.globl	G$make_pieces_fall$0$0
	C$gamelogic.c$603$1_0$427	= .
	.globl	C$gamelogic.c$603$1_0$427
;src/engine/gamelogic.c:603: bool make_pieces_fall(void) {
;	---------------------------------
; Function make_pieces_fall
; ---------------------------------
_make_pieces_fall::
	push	ix
	ld	ix,#0
	add	ix,sp
	push	af
	C$gamelogic.c$604$2_0$427	= .
	.globl	C$gamelogic.c$604$2_0$427
;src/engine/gamelogic.c:604: bool somePieceFalled = false;
	ld	c, #0x00
	C$gamelogic.c$605$1_0$427	= .
	.globl	C$gamelogic.c$605$1_0$427
;src/engine/gamelogic.c:605: u8 position = (row_index << 3) + (row_index << 1);
	ld	a, (_row_index+0)
	ld	b, a
	add	a, a
	add	a, a
	add	a, a
	sla	b
	add	a, b
	ld	-2 (ix), a
	C$gamelogic.c$607$3_0$429	= .
	.globl	C$gamelogic.c$607$3_0$429
;src/engine/gamelogic.c:607: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	-1 (ix), #0x00
00109$:
	C$gamelogic.c$608$3_0$429	= .
	.globl	C$gamelogic.c$608$3_0$429
;src/engine/gamelogic.c:608: if(!is_free_board_position(position) && is_free_board_next_position(i, row_index)) {
	ld	a, #<(_board)
	add	a, -2 (ix)
	ld	e, a
	ld	a, #>(_board)
	adc	a, #0x00
	ld	d, a
	ld	a, (de)
	or	a, a
	jr	Z, 00102$
	ld	a,(_row_index+0)
	cp	a,#0x15
	jr	NC, 00102$
	ld	d, #0x00
	ld	l, a
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	a, a
	rl	d
	ld	e, a
	add	hl, de
	ld	e, -1 (ix)
	ld	d, #0x00
	add	hl, de
	ld	de, #0x000a
	add	hl, de
	ld	de, #_board
	add	hl, de
	ld	a, (hl)
	or	a, a
	jr	NZ, 00102$
	C$gamelogic.c$609$4_0$430	= .
	.globl	C$gamelogic.c$609$4_0$430
;src/engine/gamelogic.c:609: create_individual_pieces_from_board_position(i, row_index, position);
	ld	a, -2 (ix)
	push	af
	inc	sp
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, -1 (ix)
	call	_create_individual_pieces_from_board_position
	C$gamelogic.c$610$4_0$430	= .
	.globl	C$gamelogic.c$610$4_0$430
;src/engine/gamelogic.c:610: somePieceFalled = true;
	ld	c, #0x01
00102$:
	C$gamelogic.c$612$3_0$429	= .
	.globl	C$gamelogic.c$612$3_0$429
;src/engine/gamelogic.c:612: position++;
	inc	-2 (ix)
	C$gamelogic.c$607$2_0$428	= .
	.globl	C$gamelogic.c$607$2_0$428
;src/engine/gamelogic.c:607: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	-1 (ix)
	ld	a, -1 (ix)
	sub	a, #0x0a
	jr	C, 00109$
	C$gamelogic.c$614$1_0$427	= .
	.globl	C$gamelogic.c$614$1_0$427
;src/engine/gamelogic.c:614: if(row_index > max_row_index) { //FIXME check if it's > or >=
	ld	a, (_max_row_index+0)
	ld	iy, #_row_index
	sub	a, 0 (iy)
	jr	NC, 00107$
	C$gamelogic.c$615$2_0$431	= .
	.globl	C$gamelogic.c$615$2_0$431
;src/engine/gamelogic.c:615: row_index--;
	dec	0 (iy)
	jr	00108$
00107$:
	C$gamelogic.c$617$2_0$432	= .
	.globl	C$gamelogic.c$617$2_0$432
;src/engine/gamelogic.c:617: game_status = GAME_STATUS_CHECK_PIECES_FALL;
	ld	hl, #_game_status
	ld	(hl), #0x0c
	C$gamelogic.c$618$2_0$432	= .
	.globl	C$gamelogic.c$618$2_0$432
;src/engine/gamelogic.c:618: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
00108$:
	C$gamelogic.c$620$1_0$427	= .
	.globl	C$gamelogic.c$620$1_0$427
;src/engine/gamelogic.c:620: return somePieceFalled;
	ld	a, c
	C$gamelogic.c$621$1_0$427	= .
	.globl	C$gamelogic.c$621$1_0$427
;src/engine/gamelogic.c:621: }
	ld	sp, ix
	pop	ix
	C$gamelogic.c$621$1_0$427	= .
	.globl	C$gamelogic.c$621$1_0$427
	XG$make_pieces_fall$0$0	= .
	.globl	XG$make_pieces_fall$0$0
	ret
	G$make_points_float$0$0	= .
	.globl	G$make_points_float$0$0
	C$gamelogic.c$623$1_0$434	= .
	.globl	C$gamelogic.c$623$1_0$434
;src/engine/gamelogic.c:623: void make_points_float(void) {
;	---------------------------------
; Function make_points_float
; ---------------------------------
_make_points_float::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-6
	add	hl, sp
	ld	sp, hl
	C$gamelogic.c$628$1_0$434	= .
	.globl	C$gamelogic.c$628$1_0$434
;src/engine/gamelogic.c:628: current_element = active_points_list->head;
	ld	hl, (_active_points_list)
	ld	a, (hl)
	ld	-6 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-5 (ix), a
	C$gamelogic.c$629$1_0$434	= .
	.globl	C$gamelogic.c$629$1_0$434
;src/engine/gamelogic.c:629: while(current_element != NULL) {
00113$:
	ld	a, -5 (ix)
	or	a, -6 (ix)
	jr	Z, 00115$
	C$gamelogic.c$630$2_0$435	= .
	.globl	C$gamelogic.c$630$2_0$435
;src/engine/gamelogic.c:630: current_point_sprite = (point_sprite*)current_element->data;
	pop	hl
	push	hl
	ld	c, (hl)
	inc	hl
	ld	a, (hl)
	ld	-4 (ix), c
	ld	-3 (ix), a
	C$gamelogic.c$631$2_0$435	= .
	.globl	C$gamelogic.c$631$2_0$435
;src/engine/gamelogic.c:631: if(current_point_sprite->y > 10 && current_point_sprite->y < 250) {
	pop	hl
	pop	bc
	push	bc
	push	hl
	inc	bc
	ld	a, (bc)
	ld	e, a
	C$gamelogic.c$639$1_0$434	= .
	.globl	C$gamelogic.c$639$1_0$434
;src/engine/gamelogic.c:639: current_element = current_element->next;
	ld	a, -6 (ix)
	add	a, #0x02
	ld	-2 (ix), a
	ld	a, -5 (ix)
	adc	a, #0x00
	ld	-1 (ix), a
	C$gamelogic.c$631$2_0$435	= .
	.globl	C$gamelogic.c$631$2_0$435
;src/engine/gamelogic.c:631: if(current_point_sprite->y > 10 && current_point_sprite->y < 250) {
	ld	a, #0x0a
	sub	a, e
	jr	NC, 00110$
	ld	a, e
	sub	a, #0xfa
	jr	NC, 00110$
	C$gamelogic.c$632$3_0$436	= .
	.globl	C$gamelogic.c$632$3_0$436
;src/engine/gamelogic.c:632: if(current_point_sprite->firstsprite == POINT_100_SPRITE_INDEX)
	ld	l, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -3 (ix)
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
	ld	a, (hl)
	C$gamelogic.c$633$1_0$434	= .
	.globl	C$gamelogic.c$633$1_0$434
;src/engine/gamelogic.c:633: current_point_sprite->y = (current_point_sprite->y - 2);
	C$gamelogic.c$632$3_0$436	= .
	.globl	C$gamelogic.c$632$3_0$436
;src/engine/gamelogic.c:632: if(current_point_sprite->firstsprite == POINT_100_SPRITE_INDEX)
	cp	a, #0x0e
	jr	NZ, 00107$
	C$gamelogic.c$633$3_0$436	= .
	.globl	C$gamelogic.c$633$3_0$436
;src/engine/gamelogic.c:633: current_point_sprite->y = (current_point_sprite->y - 2);
	ld	a, e
	dec	a
	dec	a
	ld	(bc), a
	jr	00108$
00107$:
	C$gamelogic.c$634$3_0$436	= .
	.globl	C$gamelogic.c$634$3_0$436
;src/engine/gamelogic.c:634: else if(current_point_sprite->firstsprite == POINT_500_SPRITE_INDEX) {
	cp	a, #0x0f
	jr	NZ, 00104$
	C$gamelogic.c$635$4_0$437	= .
	.globl	C$gamelogic.c$635$4_0$437
;src/engine/gamelogic.c:635: current_point_sprite->y = (current_point_sprite->y - 1);
	ld	a, e
	dec	a
	ld	(bc), a
	jr	00108$
00104$:
	C$gamelogic.c$636$3_0$436	= .
	.globl	C$gamelogic.c$636$3_0$436
;src/engine/gamelogic.c:636: } else if(current_point_sprite->firstsprite == BONUS_SPRITE_INDEX) {
	sub	a, #0x16
	jr	NZ, 00108$
	C$gamelogic.c$637$4_0$438	= .
	.globl	C$gamelogic.c$637$4_0$438
;src/engine/gamelogic.c:637: current_point_sprite->y = (current_point_sprite->y - 3);
	ld	a, e
	add	a, #0xfd
	ld	(bc), a
00108$:
	C$gamelogic.c$639$3_0$436	= .
	.globl	C$gamelogic.c$639$3_0$436
;src/engine/gamelogic.c:639: current_element = current_element->next;
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, (hl)
	ld	-6 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-5 (ix), a
	jr	00113$
00110$:
	C$gamelogic.c$641$3_0$439	= .
	.globl	C$gamelogic.c$641$3_0$439
;src/engine/gamelogic.c:641: element_to_delete = current_element;
	pop	de
	push	de
	C$gamelogic.c$642$3_0$439	= .
	.globl	C$gamelogic.c$642$3_0$439
;src/engine/gamelogic.c:642: current_element = current_element->next;
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	a, (hl)
	ld	-6 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-5 (ix), a
	C$gamelogic.c$643$3_0$439	= .
	.globl	C$gamelogic.c$643$3_0$439
;src/engine/gamelogic.c:643: deleteElement_Unsafe(active_points_list, element_to_delete);
	ld	hl, (_active_points_list)
	call	_deleteElement_Unsafe
	jp	00113$
00115$:
	C$gamelogic.c$646$1_0$434	= .
	.globl	C$gamelogic.c$646$1_0$434
;src/engine/gamelogic.c:646: draw_point_sprites();
	call	_draw_point_sprites
	C$gamelogic.c$647$1_0$434	= .
	.globl	C$gamelogic.c$647$1_0$434
;src/engine/gamelogic.c:647: }
	ld	sp, ix
	pop	ix
	C$gamelogic.c$647$1_0$434	= .
	.globl	C$gamelogic.c$647$1_0$434
	XG$make_points_float$0$0	= .
	.globl	XG$make_points_float$0$0
	ret
	G$end_demo$0$0	= .
	.globl	G$end_demo$0$0
	C$gamelogic.c$649$1_0$441	= .
	.globl	C$gamelogic.c$649$1_0$441
;src/engine/gamelogic.c:649: void end_demo(void) {
;	---------------------------------
; Function end_demo
; ---------------------------------
_end_demo::
	C$gamelogic.c$650$1_0$441	= .
	.globl	C$gamelogic.c$650$1_0$441
;src/engine/gamelogic.c:650: clear_point_sprites();
	call	_clear_point_sprites
	C$gamelogic.c$651$1_0$441	= .
	.globl	C$gamelogic.c$651$1_0$441
;src/engine/gamelogic.c:651: clear_cursor_sprites();
	call	_clear_cursor_sprites
	C$gamelogic.c$652$1_0$441	= .
	.globl	C$gamelogic.c$652$1_0$441
;src/engine/gamelogic.c:652: start_fadeout_music();
	C$gamelogic.c$653$1_0$441	= .
	.globl	C$gamelogic.c$653$1_0$441
;src/engine/gamelogic.c:653: }
	C$gamelogic.c$653$1_0$441	= .
	.globl	C$gamelogic.c$653$1_0$441
	XG$end_demo$0$0	= .
	.globl	XG$end_demo$0$0
	jp	_start_fadeout_music
	G$clear_line$0$0	= .
	.globl	G$clear_line$0$0
	C$gamelogic.c$655$1_0$444	= .
	.globl	C$gamelogic.c$655$1_0$444
;src/engine/gamelogic.c:655: void clear_line(void) {
;	---------------------------------
; Function clear_line
; ---------------------------------
_clear_line::
	C$gamelogic.c$657$2_0$444	= .
	.globl	C$gamelogic.c$657$2_0$444
;src/engine/gamelogic.c:657: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	c, #0x00
00102$:
	C$gamelogic.c$658$3_0$445	= .
	.globl	C$gamelogic.c$658$3_0$445
;src/engine/gamelogic.c:658: clear_board_position(i, row_index);
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_clear_board_position
	pop	bc
	C$gamelogic.c$657$2_0$444	= .
	.globl	C$gamelogic.c$657$2_0$444
;src/engine/gamelogic.c:657: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	c
	ld	a, c
	sub	a, #0x0a
	jr	C, 00102$
	C$gamelogic.c$660$2_0$444	= .
	.globl	C$gamelogic.c$660$2_0$444
;src/engine/gamelogic.c:660: }
	C$gamelogic.c$660$2_0$444	= .
	.globl	C$gamelogic.c$660$2_0$444
	XG$clear_line$0$0	= .
	.globl	XG$clear_line$0$0
	ret
	G$block_line$0$0	= .
	.globl	G$block_line$0$0
	C$gamelogic.c$662$2_0$448	= .
	.globl	C$gamelogic.c$662$2_0$448
;src/engine/gamelogic.c:662: void block_line(void) {
;	---------------------------------
; Function block_line
; ---------------------------------
_block_line::
	C$gamelogic.c$664$2_0$448	= .
	.globl	C$gamelogic.c$664$2_0$448
;src/engine/gamelogic.c:664: for(i=0;i<BOARD_COLUMNS;i++) {
	ld	c, #0x00
00102$:
	C$gamelogic.c$665$3_0$449	= .
	.globl	C$gamelogic.c$665$3_0$449
;src/engine/gamelogic.c:665: block_board_position(i, row_index);
	push	bc
	ld	a, (_row_index+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_block_board_position
	pop	bc
	C$gamelogic.c$664$2_0$448	= .
	.globl	C$gamelogic.c$664$2_0$448
;src/engine/gamelogic.c:664: for(i=0;i<BOARD_COLUMNS;i++) {
	inc	c
	ld	a, c
	sub	a, #0x0a
	jr	C, 00102$
	C$gamelogic.c$667$2_0$448	= .
	.globl	C$gamelogic.c$667$2_0$448
;src/engine/gamelogic.c:667: }
	C$gamelogic.c$667$2_0$448	= .
	.globl	C$gamelogic.c$667$2_0$448
	XG$block_line$0$0	= .
	.globl	XG$block_line$0$0
	ret
	G$draw_line$0$0	= .
	.globl	G$draw_line$0$0
	C$gamelogic.c$669$2_0$451	= .
	.globl	C$gamelogic.c$669$2_0$451
;src/engine/gamelogic.c:669: void draw_line(void) {
;	---------------------------------
; Function draw_line
; ---------------------------------
_draw_line::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	hl, #-11
	add	hl, sp
	ld	sp, hl
	C$gamelogic.c$674$1_0$451	= .
	.globl	C$gamelogic.c$674$1_0$451
;src/engine/gamelogic.c:674: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$gamelogic.c$675$1_0$451	= .
	.globl	C$gamelogic.c$675$1_0$451
;src/engine/gamelogic.c:675: currentlevel = levels[level];
	ld	bc, #_levels+0
	ld	a, (_level+0)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, bc
	ld	a, (hl)
	ld	-11 (ix), a
	inc	hl
	ld	a, (hl)
	ld	-10 (ix), a
	C$gamelogic.c$676$1_0$451	= .
	.globl	C$gamelogic.c$676$1_0$451
;src/engine/gamelogic.c:676: i = 0;
	ld	-1 (ix), #0x00
	C$gamelogic.c$677$1_0$451	= .
	.globl	C$gamelogic.c$677$1_0$451
;src/engine/gamelogic.c:677: position = (row_index << 3) + (row_index << 1);
	ld	a, (_row_index+0)
	ld	c, a
	add	a, a
	add	a, a
	add	a, a
	sla	c
	add	a, c
	ld	-2 (ix), a
	C$gamelogic.c$678$4_0$456	= .
	.globl	C$gamelogic.c$678$4_0$456
;src/engine/gamelogic.c:678: while(1) {
00118$:
	C$gamelogic.c$679$2_0$452	= .
	.globl	C$gamelogic.c$679$2_0$452
;src/engine/gamelogic.c:679: if(currentlevel[decodingIndex] == 0x22) {
	ld	a, -11 (ix)
	ld	hl, #_decodingIndex
	add	a, (hl)
	ld	c, a
	ld	a, -10 (ix)
	adc	a, #0x00
	ld	b, a
	ld	a, (bc)
	ld	-3 (ix), a
	sub	a, #0x22
	jr	NZ, 00102$
	C$gamelogic.c$680$3_0$453	= .
	.globl	C$gamelogic.c$680$3_0$453
;src/engine/gamelogic.c:680: decodingIndex++;
	ld	hl, #_decodingIndex
	inc	(hl)
	C$gamelogic.c$681$3_0$453	= .
	.globl	C$gamelogic.c$681$3_0$453
;src/engine/gamelogic.c:681: return;
	jp	00126$
00102$:
	C$gamelogic.c$683$2_0$452	= .
	.globl	C$gamelogic.c$683$2_0$452
;src/engine/gamelogic.c:683: if(currentlevel[decodingIndex] == 0xEE) {
	ld	a, -3 (ix)
	sub	a, #0xee
	jr	NZ, 00104$
	C$gamelogic.c$684$3_0$454	= .
	.globl	C$gamelogic.c$684$3_0$454
;src/engine/gamelogic.c:684: endOfDrawingLevel = true;
	ld	hl, #_endOfDrawingLevel
	ld	(hl), #0x01
	C$gamelogic.c$685$3_0$454	= .
	.globl	C$gamelogic.c$685$3_0$454
;src/engine/gamelogic.c:685: return;
	jp	00126$
00104$:
	C$gamelogic.c$687$2_0$452	= .
	.globl	C$gamelogic.c$687$2_0$452
;src/engine/gamelogic.c:687: nibble = ((currentlevel[decodingIndex]) & 0xF0) >> 4; //upper nibble
	ld	a, -3 (ix)
	and	a, #0xf0
	ld	c, a
	ld	b, #0x00
	sra	b
	rr	c
	sra	b
	rr	c
	sra	b
	rr	c
	sra	b
	rr	c
	C$gamelogic.c$688$2_0$452	= .
	.globl	C$gamelogic.c$688$2_0$452
;src/engine/gamelogic.c:688: number = (nibble & 0xC) >> 2;
	ld	a, c
	and	a, #0x0c
	ld	e, a
	ld	d, #0x00
	sra	d
	rr	e
	sra	d
	rr	e
	ld	-9 (ix), e
	C$gamelogic.c$689$2_0$452	= .
	.globl	C$gamelogic.c$689$2_0$452
;src/engine/gamelogic.c:689: color = (nibble & 0x3)+1;
	ld	a, c
	and	a, #0x03
	inc	a
	C$gamelogic.c$690$2_0$452	= .
	.globl	C$gamelogic.c$690$2_0$452
;src/engine/gamelogic.c:690: if(color == 2) color = 0;
	ld	-8 (ix), a
	sub	a, #0x02
	jr	NZ, 00132$
	ld	-8 (ix), #0x00
	C$gamelogic.c$691$1_0$451	= .
	.globl	C$gamelogic.c$691$1_0$451
;src/engine/gamelogic.c:691: for(j=0;j<number;j++) {
00132$:
	ld	a, -2 (ix)
	ld	-3 (ix), a
	ld	a, -1 (ix)
	ld	-2 (ix), a
	ld	-1 (ix), #0x00
00121$:
	ld	a, -1 (ix)
	sub	a, -9 (ix)
	jr	NC, 00137$
	C$gamelogic.c$692$5_0$457	= .
	.globl	C$gamelogic.c$692$5_0$457
;src/engine/gamelogic.c:692: draw_board_position(i, row_index,color);
	ld	a, (_row_index+0)
	ld	-7 (ix), a
	ld	-6 (ix), #0x00
	ld	a, -7 (ix)
	add	a, #0x01
	ld	-5 (ix), a
	ld	a, -6 (ix)
	adc	a, #0x00
	ld	-4 (ix), a
	ld	l, -5 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, -4 (ix)
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, -2 (ix)
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	l, -8 (ix)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$gamelogic.c$693$4_0$456	= .
	.globl	C$gamelogic.c$693$4_0$456
;src/engine/gamelogic.c:693: board[position] = color;
	ld	a, #<(_board)
	add	a, -3 (ix)
	ld	c, a
	ld	a, #>(_board)
	adc	a, #0x00
	ld	b, a
	ld	a, -8 (ix)
	ld	(bc), a
	C$gamelogic.c$694$4_0$456	= .
	.globl	C$gamelogic.c$694$4_0$456
;src/engine/gamelogic.c:694: position++;
	inc	-3 (ix)
	C$gamelogic.c$695$4_0$456	= .
	.globl	C$gamelogic.c$695$4_0$456
;src/engine/gamelogic.c:695: i++;
	inc	-2 (ix)
	C$gamelogic.c$691$3_0$455	= .
	.globl	C$gamelogic.c$691$3_0$455
;src/engine/gamelogic.c:691: for(j=0;j<number;j++) {
	inc	-1 (ix)
	jr	00121$
00137$:
	ld	c, -3 (ix)
	ld	b, -2 (ix)
	C$gamelogic.c$697$2_0$452	= .
	.globl	C$gamelogic.c$697$2_0$452
;src/engine/gamelogic.c:697: nibble = (currentlevel[decodingIndex]) & 0x0F; //bottom nibble
	ld	a, -11 (ix)
	ld	hl, #_decodingIndex
	add	a, (hl)
	ld	e, a
	ld	a, -10 (ix)
	adc	a, #0x00
	ld	d, a
	ld	a, (de)
	and	a, #0x0f
	C$gamelogic.c$698$2_0$452	= .
	.globl	C$gamelogic.c$698$2_0$452
;src/engine/gamelogic.c:698: number = (nibble & 0xC) >> 2;
;	spillPairReg hl
;	spillPairReg hl
;	spillPairReg hl
;	spillPairReg hl
	ld	e, a
	and	a, #0x0c
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	sra	h
	rr	l
	sra	h
	rr	l
	ld	-3 (ix), l
	C$gamelogic.c$699$2_0$452	= .
	.globl	C$gamelogic.c$699$2_0$452
;src/engine/gamelogic.c:699: color = (nibble & 0x3)+1;
	ld	a, e
	and	a, #0x03
	C$gamelogic.c$700$2_0$452	= .
	.globl	C$gamelogic.c$700$2_0$452
;src/engine/gamelogic.c:700: if(color == 2) color = 0;
	inc	a
	ld	e, a
	sub	a,#0x02
	jr	NZ, 00135$
	ld	e,a
	C$gamelogic.c$701$1_0$451	= .
	.globl	C$gamelogic.c$701$1_0$451
;src/engine/gamelogic.c:701: for(j=0;j<number;j++) {
00135$:
	ld	-2 (ix), c
	ld	-1 (ix), b
	ld	d, #0x00
00124$:
	ld	a, d
	sub	a, -3 (ix)
	jr	NC, 00138$
	C$gamelogic.c$702$5_0$460	= .
	.globl	C$gamelogic.c$702$5_0$460
;src/engine/gamelogic.c:702: draw_board_position(i, row_index,color);
	ld	a, (_row_index+0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a, -1 (ix)
	add	a, #0x0b
	ld	c, a
	ld	b, #0x00
	add	hl, bc
	add	hl, hl
	ld	a, h
	or	a, #0x78
	ld	h, a
;	spillPairReg hl
;	spillPairReg hl
	rst	#0x08
	ld	l, e
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	set	3, h
	rst	#0x18
	C$gamelogic.c$703$4_0$459	= .
	.globl	C$gamelogic.c$703$4_0$459
;src/engine/gamelogic.c:703: board[position] = color;
	ld	a, #<(_board)
	add	a, -2 (ix)
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #>(_board)
	adc	a, #0x00
	ld	h, a
	ld	(hl), e
	C$gamelogic.c$704$4_0$459	= .
	.globl	C$gamelogic.c$704$4_0$459
;src/engine/gamelogic.c:704: position++;
	inc	-2 (ix)
	C$gamelogic.c$705$4_0$459	= .
	.globl	C$gamelogic.c$705$4_0$459
;src/engine/gamelogic.c:705: i++;
	inc	-1 (ix)
	C$gamelogic.c$701$3_0$458	= .
	.globl	C$gamelogic.c$701$3_0$458
;src/engine/gamelogic.c:701: for(j=0;j<number;j++) {
	inc	d
	jr	00124$
00138$:
	C$gamelogic.c$707$2_0$452	= .
	.globl	C$gamelogic.c$707$2_0$452
;src/engine/gamelogic.c:707: decodingIndex++;
	ld	hl, #_decodingIndex
	inc	(hl)
	jp	00118$
00126$:
	C$gamelogic.c$709$1_0$451	= .
	.globl	C$gamelogic.c$709$1_0$451
;src/engine/gamelogic.c:709: }
	ld	sp, ix
	pop	ix
	C$gamelogic.c$709$1_0$451	= .
	.globl	C$gamelogic.c$709$1_0$451
	XG$draw_line$0$0	= .
	.globl	XG$draw_line$0$0
	ret
	G$execute_game_logic$0$0	= .
	.globl	G$execute_game_logic$0$0
	C$gamelogic.c$711$1_0$462	= .
	.globl	C$gamelogic.c$711$1_0$462
;src/engine/gamelogic.c:711: void execute_game_logic(void) {
;	---------------------------------
; Function execute_game_logic
; ---------------------------------
_execute_game_logic::
	C$gamelogic.c$712$1_0$462	= .
	.globl	C$gamelogic.c$712$1_0$462
;src/engine/gamelogic.c:712: if(game_status == GAME_STATUS_NOACTION) {
	ld	a, (_game_status+0)
	or	a, a
	jr	NZ, 00222$
	C$gamelogic.c$713$2_0$463	= .
	.globl	C$gamelogic.c$713$2_0$463
;src/engine/gamelogic.c:713: u8 randValue = get_rand() & 3;
	call	_get_rand
	and	a, #0x03
	ld	c, a
	C$gamelogic.c$714$2_0$463	= .
	.globl	C$gamelogic.c$714$2_0$463
;src/engine/gamelogic.c:714: if(last_square_x == BOARD_COLUMNS - 1) {
	ld	a, (_last_square_x+0)
	sub	a, #0x09
	jr	NZ, 00102$
	C$gamelogic.c$715$3_0$464	= .
	.globl	C$gamelogic.c$715$3_0$464
;src/engine/gamelogic.c:715: last_square_x = BOARD_COLUMNS - 2;
	ld	hl, #_last_square_x
	ld	(hl), #0x08
00102$:
	C$gamelogic.c$718$2_0$463	= .
	.globl	C$gamelogic.c$718$2_0$463
;src/engine/gamelogic.c:718: if(generateBonus && randValue <= 1) {
	ld	hl, #_generateBonus
	bit	0, (hl)
	jr	Z, 00104$
	ld	a, #0x01
	sub	a, c
	jr	C, 00104$
	C$gamelogic.c$719$3_0$465	= .
	.globl	C$gamelogic.c$719$3_0$465
;src/engine/gamelogic.c:719: generate_bonus_piece(randValue);
	ld	a, c
	call	_generate_bonus_piece
	jr	00105$
00104$:
	C$gamelogic.c$721$3_0$466	= .
	.globl	C$gamelogic.c$721$3_0$466
;src/engine/gamelogic.c:721: generate_square_piece();
	call	_generate_square_piece
00105$:
	C$gamelogic.c$723$2_0$463	= .
	.globl	C$gamelogic.c$723$2_0$463
;src/engine/gamelogic.c:723: generateBonus = false;
	ld	hl, #_generateBonus
	ld	(hl), #0x00
	C$gamelogic.c$724$2_0$463	= .
	.globl	C$gamelogic.c$724$2_0$463
;src/engine/gamelogic.c:724: if(current_player == PLAYER_CPU_1 || current_player == PLAYER_CPU_2) {
	ld	a, (_current_player+0)
	sub	a, #0x02
	jr	Z, 00107$
	ld	a, (_current_player+0)
	sub	a, #0x03
	jp	NZ,00223$
00107$:
	C$gamelogic.c$725$3_0$467	= .
	.globl	C$gamelogic.c$725$3_0$467
;src/engine/gamelogic.c:725: decideNextMove();
	call	_decideNextMove
	C$gamelogic.c$726$3_0$467	= .
	.globl	C$gamelogic.c$726$3_0$467
;src/engine/gamelogic.c:726: clear_suitable_columns();
	call	_clear_suitable_columns
	jp	00223$
00222$:
	C$gamelogic.c$731$1_0$462	= .
	.globl	C$gamelogic.c$731$1_0$462
;src/engine/gamelogic.c:731: } else if(game_status == GAME_STATUS_SQUAREPIECEFALLING) {
	ld	a, (_game_status+0)
	dec	a
	jr	NZ, 00219$
	C$gamelogic.c$732$2_0$468	= .
	.globl	C$gamelogic.c$732$2_0$468
;src/engine/gamelogic.c:732: fall_square_piece();
	call	_fall_square_piece
	C$gamelogic.c$733$2_0$468	= .
	.globl	C$gamelogic.c$733$2_0$468
;src/engine/gamelogic.c:733: draw_cursor_sprites((current_square.x<<3)+88,(current_square.y<<3) - 2);
	ld	a, (#_current_square + 1)
	add	a, a
	add	a, a
	add	a, a
	ld	b, a
	dec	b
	dec	b
	ld	a, (#_current_square + 0)
	add	a, a
	add	a, a
	add	a, a
	add	a, #0x58
	ld	c, a
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_draw_cursor_sprites
	jp	00223$
00219$:
	C$gamelogic.c$734$1_0$462	= .
	.globl	C$gamelogic.c$734$1_0$462
;src/engine/gamelogic.c:734: } else if(game_status == GAME_STATUS_BONUSFALLING) {
	ld	a, (_game_status+0)
	sub	a, #0x02
	jr	NZ, 00216$
	C$gamelogic.c$735$2_0$469	= .
	.globl	C$gamelogic.c$735$2_0$469
;src/engine/gamelogic.c:735: fall_bonus_piece();
	call	_fall_bonus_piece
	C$gamelogic.c$736$2_0$469	= .
	.globl	C$gamelogic.c$736$2_0$469
;src/engine/gamelogic.c:736: draw_cursor_sprites((current_square.x<<3)+80,(current_square.y<<3) - 2);
	ld	a, (#_current_square + 1)
	add	a, a
	add	a, a
	add	a, a
	ld	b, a
	dec	b
	dec	b
	ld	a, (#_current_square + 0)
	add	a, a
	add	a, a
	add	a, a
	add	a, #0x50
	ld	c, a
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_draw_cursor_sprites
	jp	00223$
00216$:
	C$gamelogic.c$737$1_0$462	= .
	.globl	C$gamelogic.c$737$1_0$462
;src/engine/gamelogic.c:737: } else if(game_status == GAME_STATUS_PIECESFALLING) {
	ld	a, (_game_status+0)
	sub	a, #0x03
	jr	NZ, 00213$
	C$gamelogic.c$738$2_0$470	= .
	.globl	C$gamelogic.c$738$2_0$470
;src/engine/gamelogic.c:738: clear_cursor_sprites();
	call	_clear_cursor_sprites
	C$gamelogic.c$739$2_0$470	= .
	.globl	C$gamelogic.c$739$2_0$470
;src/engine/gamelogic.c:739: piecesUpdated = fall_pieces();
	call	_fall_pieces
	ld	(_piecesUpdated+0), a
	C$gamelogic.c$740$2_0$470	= .
	.globl	C$gamelogic.c$740$2_0$470
;src/engine/gamelogic.c:740: if(!piecesUpdated) {
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jp	NZ, 00223$
	C$gamelogic.c$741$3_0$471	= .
	.globl	C$gamelogic.c$741$3_0$471
;src/engine/gamelogic.c:741: game_status = GAME_STATUS_CALCULATIONS;
	ld	hl, #_game_status
	ld	(hl), #0x06
	C$gamelogic.c$742$3_0$471	= .
	.globl	C$gamelogic.c$742$3_0$471
;src/engine/gamelogic.c:742: piece_index = 0;
	ld	hl, #_piece_index
	ld	(hl), #0x00
	C$gamelogic.c$743$3_0$471	= .
	.globl	C$gamelogic.c$743$3_0$471
;src/engine/gamelogic.c:743: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$744$3_0$471	= .
	.globl	C$gamelogic.c$744$3_0$471
;src/engine/gamelogic.c:744: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jp	00223$
00213$:
	C$gamelogic.c$746$1_0$462	= .
	.globl	C$gamelogic.c$746$1_0$462
;src/engine/gamelogic.c:746: } else if(game_status == GAME_STATUS_PIECESREVERSING) {
	ld	a, (_game_status+0)
	sub	a, #0x04
	jr	NZ, 00210$
	C$gamelogic.c$747$2_0$472	= .
	.globl	C$gamelogic.c$747$2_0$472
;src/engine/gamelogic.c:747: reverse_pieces();
	call	_reverse_pieces
	jp	00223$
00210$:
	C$gamelogic.c$748$1_0$462	= .
	.globl	C$gamelogic.c$748$1_0$462
;src/engine/gamelogic.c:748: } else if(game_status == GAME_STATUS_PIECESDESTROYING) {
	ld	a, (_game_status+0)
	sub	a, #0x05
	jr	NZ, 00207$
	C$gamelogic.c$749$2_0$473	= .
	.globl	C$gamelogic.c$749$2_0$473
;src/engine/gamelogic.c:749: destroy_pieces();
	call	_destroy_pieces
	jp	00223$
00207$:
	C$gamelogic.c$750$1_0$462	= .
	.globl	C$gamelogic.c$750$1_0$462
;src/engine/gamelogic.c:750: } else if(game_status == GAME_STATUS_CALCULATIONS) {
	ld	a, (_game_status+0)
	sub	a, #0x06
	jr	NZ, 00204$
	C$gamelogic.c$751$2_0$474	= .
	.globl	C$gamelogic.c$751$2_0$474
;src/engine/gamelogic.c:751: piecesUpdated = compute_paths() || piecesUpdated;
	call	_compute_paths
	ld	c, a
	bit	0, c
	jr	NZ, 00229$
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jr	NZ, 00229$
	xor	a, a
	jr	00230$
00229$:
	ld	a, #0x01
00230$:
	ld	(_piecesUpdated+0), a
	jp	00223$
00204$:
	C$gamelogic.c$752$1_0$462	= .
	.globl	C$gamelogic.c$752$1_0$462
;src/engine/gamelogic.c:752: } else if(game_status == GAME_STATUS_CHECK_CALCULATIONS) {
	ld	a, (_game_status+0)
	sub	a, #0x07
	jr	NZ, 00201$
	C$gamelogic.c$753$2_0$475	= .
	.globl	C$gamelogic.c$753$2_0$475
;src/engine/gamelogic.c:753: if(!piecesUpdated) {
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jr	NZ, 00113$
	C$gamelogic.c$754$3_0$476	= .
	.globl	C$gamelogic.c$754$3_0$476
;src/engine/gamelogic.c:754: game_status = GAME_STATUS_COMPUTE_DISCONNECTED;
	ld	hl, #_game_status
	ld	(hl), #0x08
	C$gamelogic.c$755$3_0$476	= .
	.globl	C$gamelogic.c$755$3_0$476
;src/engine/gamelogic.c:755: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$756$3_0$476	= .
	.globl	C$gamelogic.c$756$3_0$476
;src/engine/gamelogic.c:756: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jp	00223$
00113$:
	C$gamelogic.c$758$3_0$477	= .
	.globl	C$gamelogic.c$758$3_0$477
;src/engine/gamelogic.c:758: game_status = GAME_STATUS_CALCULATIONS;
	ld	hl, #_game_status
	ld	(hl), #0x06
	C$gamelogic.c$759$3_0$477	= .
	.globl	C$gamelogic.c$759$3_0$477
;src/engine/gamelogic.c:759: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$760$3_0$477	= .
	.globl	C$gamelogic.c$760$3_0$477
;src/engine/gamelogic.c:760: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jp	00223$
00201$:
	C$gamelogic.c$762$1_0$462	= .
	.globl	C$gamelogic.c$762$1_0$462
;src/engine/gamelogic.c:762: } else if(game_status == GAME_STATUS_COMPUTE_DISCONNECTED) {
	ld	a, (_game_status+0)
	sub	a, #0x08
	jr	NZ, 00198$
	C$gamelogic.c$763$2_0$478	= .
	.globl	C$gamelogic.c$763$2_0$478
;src/engine/gamelogic.c:763: piecesUpdated = compute_disconnected_pieces() || piecesUpdated;
	call	_compute_disconnected_pieces
	ld	c, a
	bit	0, c
	jr	NZ, 00232$
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jr	NZ, 00232$
	xor	a, a
	jr	00233$
00232$:
	ld	a, #0x01
00233$:
	ld	(_piecesUpdated+0), a
	jp	00223$
00198$:
	C$gamelogic.c$764$1_0$462	= .
	.globl	C$gamelogic.c$764$1_0$462
;src/engine/gamelogic.c:764: } else if(game_status == GAME_STATUS_ANIMATE_DISCONNECTED) {
	ld	a, (_game_status+0)
	sub	a, #0x09
	jr	NZ, 00195$
	C$gamelogic.c$765$2_0$479	= .
	.globl	C$gamelogic.c$765$2_0$479
;src/engine/gamelogic.c:765: connected_pieces_to_normal_palette();
	call	_connected_pieces_to_normal_palette
	C$gamelogic.c$766$2_0$479	= .
	.globl	C$gamelogic.c$766$2_0$479
;src/engine/gamelogic.c:766: if(!piecesUpdated) {
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jr	NZ, 00124$
	C$gamelogic.c$767$3_0$480	= .
	.globl	C$gamelogic.c$767$3_0$480
;src/engine/gamelogic.c:767: game_status = GAME_STATUS_DESTROY_DISCONNECTED;
	ld	hl, #_game_status
	ld	(hl), #0x0a
	C$gamelogic.c$768$3_0$480	= .
	.globl	C$gamelogic.c$768$3_0$480
;src/engine/gamelogic.c:768: isPaletteRed = false;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x00
	jp	00223$
00124$:
	C$gamelogic.c$770$3_0$481	= .
	.globl	C$gamelogic.c$770$3_0$481
;src/engine/gamelogic.c:770: if(frame_cnt > S_PERIOD) {
	ld	a, #0x3c
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jr	NC, 00121$
	C$gamelogic.c$771$4_0$482	= .
	.globl	C$gamelogic.c$771$4_0$482
;src/engine/gamelogic.c:771: game_status = GAME_STATUS_DESTROY_DISCONNECTED;
	ld	hl, #_game_status
	ld	(hl), #0x0a
	C$gamelogic.c$772$4_0$482	= .
	.globl	C$gamelogic.c$772$4_0$482
;src/engine/gamelogic.c:772: not_connected_pieces_to_normal_palette();
	call	_not_connected_pieces_to_normal_palette
	C$gamelogic.c$773$4_0$482	= .
	.globl	C$gamelogic.c$773$4_0$482
;src/engine/gamelogic.c:773: isPaletteRed = false;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x00
	C$gamelogic.c$774$4_0$482	= .
	.globl	C$gamelogic.c$774$4_0$482
;src/engine/gamelogic.c:774: play_fx(DESTROY_FX);
	ld	a, #0x04
	call	_play_fx
	jp	00223$
00121$:
	C$gamelogic.c$776$4_0$483	= .
	.globl	C$gamelogic.c$776$4_0$483
;src/engine/gamelogic.c:776: if((frame_cnt & 7) == 0) {
	ld	a, (_frame_cnt+0)
	and	a, #0x07
	jp	NZ,00223$
	C$gamelogic.c$777$5_0$484	= .
	.globl	C$gamelogic.c$777$5_0$484
;src/engine/gamelogic.c:777: if(!isPaletteRed) {
	ld	hl, #_isPaletteRed
	bit	0, (hl)
	jr	NZ, 00116$
	C$gamelogic.c$778$6_0$485	= .
	.globl	C$gamelogic.c$778$6_0$485
;src/engine/gamelogic.c:778: not_connected_pieces_to_red_palette();
	call	_not_connected_pieces_to_red_palette
	C$gamelogic.c$779$6_0$485	= .
	.globl	C$gamelogic.c$779$6_0$485
;src/engine/gamelogic.c:779: isPaletteRed = true;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x01
	jp	00223$
00116$:
	C$gamelogic.c$781$6_0$486	= .
	.globl	C$gamelogic.c$781$6_0$486
;src/engine/gamelogic.c:781: not_connected_pieces_to_normal_palette();
	call	_not_connected_pieces_to_normal_palette
	C$gamelogic.c$782$6_0$486	= .
	.globl	C$gamelogic.c$782$6_0$486
;src/engine/gamelogic.c:782: isPaletteRed = false;
	ld	hl, #_isPaletteRed
	ld	(hl), #0x00
	jp	00223$
00195$:
	C$gamelogic.c$788$1_0$462	= .
	.globl	C$gamelogic.c$788$1_0$462
;src/engine/gamelogic.c:788: } else if(game_status == GAME_STATUS_DESTROY_DISCONNECTED) {
	ld	a, (_game_status+0)
	sub	a, #0x0a
	jr	NZ, 00192$
	C$gamelogic.c$789$2_0$487	= .
	.globl	C$gamelogic.c$789$2_0$487
;src/engine/gamelogic.c:789: if(!piecesUpdated) {
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jr	NZ, 00129$
	C$gamelogic.c$790$3_0$488	= .
	.globl	C$gamelogic.c$790$3_0$488
;src/engine/gamelogic.c:790: game_status = GAME_STATUS_NOACTION;
	ld	hl, #_game_status
	ld	(hl), #0x00
	C$gamelogic.c$791$3_0$488	= .
	.globl	C$gamelogic.c$791$3_0$488
;src/engine/gamelogic.c:791: if(bonusSamplePlay) {
	ld	hl, #_bonusSamplePlay
	bit	0, (hl)
	jr	Z, 00127$
	C$gamelogic.c$792$4_0$489	= .
	.globl	C$gamelogic.c$792$4_0$489
;src/engine/gamelogic.c:792: play_sample(BONUS_SAMPLE);
	ld	a, #0x03
	call	_play_sample
	C$gamelogic.c$793$4_0$489	= .
	.globl	C$gamelogic.c$793$4_0$489
;src/engine/gamelogic.c:793: bonusSamplePlay = false;
	ld	hl, #_bonusSamplePlay
	ld	(hl), #0x00
00127$:
	C$gamelogic.c$795$3_0$488	= .
	.globl	C$gamelogic.c$795$3_0$488
;src/engine/gamelogic.c:795: update_level();
	call	_update_level
	C$gamelogic.c$796$3_0$488	= .
	.globl	C$gamelogic.c$796$3_0$488
;src/engine/gamelogic.c:796: update_player();
	call	_update_player
	C$gamelogic.c$797$3_0$488	= .
	.globl	C$gamelogic.c$797$3_0$488
;src/engine/gamelogic.c:797: clear_input();
	call	_clear_input
	C$gamelogic.c$798$3_0$488	= .
	.globl	C$gamelogic.c$798$3_0$488
;src/engine/gamelogic.c:798: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$799$3_0$488	= .
	.globl	C$gamelogic.c$799$3_0$488
;src/engine/gamelogic.c:799: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jp	00223$
00129$:
	C$gamelogic.c$801$3_0$490	= .
	.globl	C$gamelogic.c$801$3_0$490
;src/engine/gamelogic.c:801: generateBonus = true;
	ld	hl, #_generateBonus
	ld	(hl), #0x01
	C$gamelogic.c$802$3_0$490	= .
	.globl	C$gamelogic.c$802$3_0$490
;src/engine/gamelogic.c:802: destroy_disconnected_pieces();  
	call	_destroy_disconnected_pieces
	jp	00223$
00192$:
	C$gamelogic.c$804$1_0$462	= .
	.globl	C$gamelogic.c$804$1_0$462
;src/engine/gamelogic.c:804: } else if(game_status == GAME_STATUS_MAKE_PIECES_FALL) {
	ld	a, (_game_status+0)
	sub	a, #0x0b
	jr	NZ, 00189$
	C$gamelogic.c$805$2_0$491	= .
	.globl	C$gamelogic.c$805$2_0$491
;src/engine/gamelogic.c:805: piecesUpdated = make_pieces_fall() || piecesUpdated;
	call	_make_pieces_fall
	ld	c, a
	bit	0, c
	jr	NZ, 00235$
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jr	NZ, 00235$
	xor	a, a
	jr	00236$
00235$:
	ld	a, #0x01
00236$:
	ld	(_piecesUpdated+0), a
	C$gamelogic.c$806$2_0$491	= .
	.globl	C$gamelogic.c$806$2_0$491
;src/engine/gamelogic.c:806: make_points_float();
	call	_make_points_float
	jp	00223$
00189$:
	C$gamelogic.c$807$1_0$462	= .
	.globl	C$gamelogic.c$807$1_0$462
;src/engine/gamelogic.c:807: }  else if(game_status == GAME_STATUS_CHECK_PIECES_FALL) {
	ld	a, (_game_status+0)
	sub	a, #0x0c
	jr	NZ, 00186$
	C$gamelogic.c$808$2_0$492	= .
	.globl	C$gamelogic.c$808$2_0$492
;src/engine/gamelogic.c:808: if(!piecesUpdated) {
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jr	NZ, 00132$
	C$gamelogic.c$809$3_0$493	= .
	.globl	C$gamelogic.c$809$3_0$493
;src/engine/gamelogic.c:809: game_status = GAME_STATUS_CALCULATIONS;
	ld	hl, #_game_status
	ld	(hl), #0x06
	C$gamelogic.c$810$3_0$493	= .
	.globl	C$gamelogic.c$810$3_0$493
;src/engine/gamelogic.c:810: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$811$3_0$493	= .
	.globl	C$gamelogic.c$811$3_0$493
;src/engine/gamelogic.c:811: clear_point_sprites();
	call	_clear_point_sprites
	C$gamelogic.c$812$3_0$493	= .
	.globl	C$gamelogic.c$812$3_0$493
;src/engine/gamelogic.c:812: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jp	00223$
00132$:
	C$gamelogic.c$814$3_0$494	= .
	.globl	C$gamelogic.c$814$3_0$494
;src/engine/gamelogic.c:814: game_status = GAME_STATUS_PIECESFALLING_2;
	ld	hl, #_game_status
	ld	(hl), #0x0d
	C$gamelogic.c$815$3_0$494	= .
	.globl	C$gamelogic.c$815$3_0$494
;src/engine/gamelogic.c:815: play_fx(FALL_FX);
	ld	a, #0x03
	call	_play_fx
	C$gamelogic.c$816$3_0$494	= .
	.globl	C$gamelogic.c$816$3_0$494
;src/engine/gamelogic.c:816: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$817$3_0$494	= .
	.globl	C$gamelogic.c$817$3_0$494
;src/engine/gamelogic.c:817: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jp	00223$
00186$:
	C$gamelogic.c$819$1_0$462	= .
	.globl	C$gamelogic.c$819$1_0$462
;src/engine/gamelogic.c:819: } else if(game_status == GAME_STATUS_PIECESFALLING_2) {
	ld	a, (_game_status+0)
	sub	a, #0x0d
	jr	NZ, 00183$
	C$gamelogic.c$820$2_0$495	= .
	.globl	C$gamelogic.c$820$2_0$495
;src/engine/gamelogic.c:820: piecesUpdated = fall_pieces();
	call	_fall_pieces
	ld	(_piecesUpdated+0), a
	C$gamelogic.c$821$2_0$495	= .
	.globl	C$gamelogic.c$821$2_0$495
;src/engine/gamelogic.c:821: make_points_float();
	call	_make_points_float
	C$gamelogic.c$822$2_0$495	= .
	.globl	C$gamelogic.c$822$2_0$495
;src/engine/gamelogic.c:822: if(!piecesUpdated) {
	ld	hl, #_piecesUpdated
	bit	0, (hl)
	jp	NZ, 00223$
	C$gamelogic.c$823$3_0$496	= .
	.globl	C$gamelogic.c$823$3_0$496
;src/engine/gamelogic.c:823: game_status = GAME_STATUS_MAKE_PIECES_FALL;
	ld	hl, #_game_status
	ld	(hl), #0x0b
	C$gamelogic.c$824$3_0$496	= .
	.globl	C$gamelogic.c$824$3_0$496
;src/engine/gamelogic.c:824: piece_index = 0;
	ld	hl, #_piece_index
	ld	(hl), #0x00
	C$gamelogic.c$825$3_0$496	= .
	.globl	C$gamelogic.c$825$3_0$496
;src/engine/gamelogic.c:825: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$826$3_0$496	= .
	.globl	C$gamelogic.c$826$3_0$496
;src/engine/gamelogic.c:826: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	jp	00223$
00183$:
	C$gamelogic.c$828$1_0$462	= .
	.globl	C$gamelogic.c$828$1_0$462
;src/engine/gamelogic.c:828: } else if (game_status == GAME_STATUS_LEVEL_COMPLETE) {
	ld	a, (_game_status+0)
	sub	a, #0x0e
	jr	NZ, 00180$
	C$gamelogic.c$829$2_0$497	= .
	.globl	C$gamelogic.c$829$2_0$497
;src/engine/gamelogic.c:829: if(frame_cnt > M_PERIOD) {
	ld	a, #0x78
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jp	NC, 00223$
	C$gamelogic.c$830$3_0$498	= .
	.globl	C$gamelogic.c$830$3_0$498
;src/engine/gamelogic.c:830: game_status = GAME_STATUS_ERASING_OLD_LEVEL;
	ld	hl, #_game_status
	ld	(hl), #0x0f
	jp	00223$
00180$:
	C$gamelogic.c$832$1_0$462	= .
	.globl	C$gamelogic.c$832$1_0$462
;src/engine/gamelogic.c:832: } else if(game_status == GAME_STATUS_ERASING_OLD_LEVEL) {
	ld	a, (_game_status+0)
	sub	a, #0x0f
	jr	NZ, 00177$
	C$gamelogic.c$833$2_0$499	= .
	.globl	C$gamelogic.c$833$2_0$499
;src/engine/gamelogic.c:833: if(row_index > 0) {
	ld	a, (_row_index+0)
	or	a, a
	jr	Z, 00142$
	C$gamelogic.c$834$3_0$500	= .
	.globl	C$gamelogic.c$834$3_0$500
;src/engine/gamelogic.c:834: clear_line();
	call	_clear_line
	C$gamelogic.c$835$3_0$500	= .
	.globl	C$gamelogic.c$835$3_0$500
;src/engine/gamelogic.c:835: row_index--;
	ld	hl, #_row_index
	dec	(hl)
	jp	00223$
00142$:
	C$gamelogic.c$837$3_0$501	= .
	.globl	C$gamelogic.c$837$3_0$501
;src/engine/gamelogic.c:837: clear_line();
	call	_clear_line
	C$gamelogic.c$838$3_0$501	= .
	.globl	C$gamelogic.c$838$3_0$501
;src/engine/gamelogic.c:838: if(level < NUMBER_LEVELS) { 
	ld	a, (_level+0)
	sub	a, #0x0e
	jr	NC, 00139$
	C$gamelogic.c$840$4_0$502	= .
	.globl	C$gamelogic.c$840$4_0$502
;src/engine/gamelogic.c:840: game_status = GAME_STATUS_DRAWING_NEW_LEVEL;
	ld	hl, #_game_status
	ld	(hl), #0x10
	C$gamelogic.c$841$4_0$502	= .
	.globl	C$gamelogic.c$841$4_0$502
;src/engine/gamelogic.c:841: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$842$4_0$502	= .
	.globl	C$gamelogic.c$842$4_0$502
;src/engine/gamelogic.c:842: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$843$4_0$502	= .
	.globl	C$gamelogic.c$843$4_0$502
;src/engine/gamelogic.c:843: endOfDrawingLevel = false;
	ld	hl, #_endOfDrawingLevel
	ld	(hl), #0x00
	C$gamelogic.c$844$4_0$502	= .
	.globl	C$gamelogic.c$844$4_0$502
;src/engine/gamelogic.c:844: decodingIndex = 0;
	ld	hl, #_decodingIndex
	ld	(hl), #0x00
	jp	00223$
00139$:
	C$gamelogic.c$846$4_0$503	= .
	.globl	C$gamelogic.c$846$4_0$503
;src/engine/gamelogic.c:846: game_status = GAME_STATUS_ENDGAME;
	ld	hl, #_game_status
	ld	(hl), #0x13
	C$gamelogic.c$847$4_0$503	= .
	.globl	C$gamelogic.c$847$4_0$503
;src/engine/gamelogic.c:847: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	jp	00223$
00177$:
	C$gamelogic.c$851$1_0$462	= .
	.globl	C$gamelogic.c$851$1_0$462
;src/engine/gamelogic.c:851: } else if(game_status == GAME_STATUS_DRAWING_NEW_LEVEL) {
	ld	a, (_game_status+0)
	sub	a, #0x10
	jp	NZ,00174$
	C$gamelogic.c$852$2_0$504	= .
	.globl	C$gamelogic.c$852$2_0$504
;src/engine/gamelogic.c:852: if(row_index > 0 && !endOfDrawingLevel) {
	ld	a, (_row_index+0)
	or	a, a
	jr	Z, 00145$
	ld	hl, #_endOfDrawingLevel
	bit	0, (hl)
	jr	NZ, 00145$
	C$gamelogic.c$853$3_0$505	= .
	.globl	C$gamelogic.c$853$3_0$505
;src/engine/gamelogic.c:853: draw_line();
	call	_draw_line
	C$gamelogic.c$854$3_0$505	= .
	.globl	C$gamelogic.c$854$3_0$505
;src/engine/gamelogic.c:854: row_index--;
	ld	hl, #_row_index
	dec	(hl)
	jp	00223$
00145$:
	C$gamelogic.c$856$3_0$506	= .
	.globl	C$gamelogic.c$856$3_0$506
;src/engine/gamelogic.c:856: SMS_mapROMBank(CONSTANT_DATA_BANK);
	ld	hl, #_ROM_bank_to_be_mapped_on_slot2
	ld	(hl), #0x02
	C$gamelogic.c$857$3_0$506	= .
	.globl	C$gamelogic.c$857$3_0$506
;src/engine/gamelogic.c:857: fallSpeed = levelfallSpeeds[level];
	ld	bc, #_levelfallSpeeds+0
	ld	hl, (_level)
	ld	h, #0x00
	add	hl, bc
	ld	a, (hl)
	ld	(_fallSpeed+0), a
	C$gamelogic.c$858$3_0$506	= .
	.globl	C$gamelogic.c$858$3_0$506
;src/engine/gamelogic.c:858: background_to_level_palette(level);
	ld	a, (_level+0)
	call	_background_to_level_palette
	C$gamelogic.c$859$3_0$506	= .
	.globl	C$gamelogic.c$859$3_0$506
;src/engine/gamelogic.c:859: print_level_number(2,23,level+1);
	ld	a, (_level+0)
	inc	a
	push	af
	inc	sp
	ld	l, #0x17
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_level_number
	C$gamelogic.c$860$3_0$506	= .
	.globl	C$gamelogic.c$860$3_0$506
;src/engine/gamelogic.c:860: print_string(2,19,"LVL]GOAL^");
	ld	hl, #___str_0
	push	hl
	ld	l, #0x13
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_string
	C$gamelogic.c$861$3_0$506	= .
	.globl	C$gamelogic.c$861$3_0$506
;src/engine/gamelogic.c:861: print_score_value(2,21,(level+1)<<LEVEL_DELIMITER);
	ld	a, (_level+0)
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	push	hl
	ld	l, #0x15
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x02
	call	_print_score_value
	C$gamelogic.c$862$3_0$506	= .
	.globl	C$gamelogic.c$862$3_0$506
;src/engine/gamelogic.c:862: game_status = GAME_STATUS_NOACTION;
	ld	hl, #_game_status
	ld	(hl), #0x00
	C$gamelogic.c$863$3_0$506	= .
	.globl	C$gamelogic.c$863$3_0$506
;src/engine/gamelogic.c:863: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	C$gamelogic.c$864$3_0$506	= .
	.globl	C$gamelogic.c$864$3_0$506
;src/engine/gamelogic.c:864: level_timer = 0;
	ld	(_level_timer), hl
	C$gamelogic.c$865$3_0$506	= .
	.globl	C$gamelogic.c$865$3_0$506
;src/engine/gamelogic.c:865: clear_input();
	call	_clear_input
	C$gamelogic.c$866$3_0$506	= .
	.globl	C$gamelogic.c$866$3_0$506
;src/engine/gamelogic.c:866: row_index = BOARD_ROWS-1;
	ld	hl, #_row_index
	ld	(hl), #0x15
	C$gamelogic.c$867$3_0$506	= .
	.globl	C$gamelogic.c$867$3_0$506
;src/engine/gamelogic.c:867: piecesUpdated = false;
	ld	hl, #_piecesUpdated
	ld	(hl), #0x00
	C$gamelogic.c$868$3_0$506	= .
	.globl	C$gamelogic.c$868$3_0$506
;src/engine/gamelogic.c:868: play_song(INGAME_OST);
	ld	a, #0x06
	call	_play_song
	jp	00223$
00174$:
	C$gamelogic.c$871$1_0$462	= .
	.globl	C$gamelogic.c$871$1_0$462
;src/engine/gamelogic.c:871: } else if(game_status == GAME_STATUS_LOOSING_GAME) {
	ld	a, (_game_status+0)
	sub	a, #0x11
	jr	NZ, 00171$
	C$gamelogic.c$872$2_0$507	= .
	.globl	C$gamelogic.c$872$2_0$507
;src/engine/gamelogic.c:872: if(row_index > 0) {
	ld	a, (_row_index+0)
	or	a, a
	jr	Z, 00149$
	C$gamelogic.c$873$3_0$508	= .
	.globl	C$gamelogic.c$873$3_0$508
;src/engine/gamelogic.c:873: block_line();
	call	_block_line
	C$gamelogic.c$874$3_0$508	= .
	.globl	C$gamelogic.c$874$3_0$508
;src/engine/gamelogic.c:874: row_index--;
	ld	hl, #_row_index
	dec	(hl)
	jp	00223$
00149$:
	C$gamelogic.c$876$3_0$509	= .
	.globl	C$gamelogic.c$876$3_0$509
;src/engine/gamelogic.c:876: block_line();
	call	_block_line
	C$gamelogic.c$877$3_0$509	= .
	.globl	C$gamelogic.c$877$3_0$509
;src/engine/gamelogic.c:877: game_status = GAME_STATUS_GAMEOVER;
	ld	hl, #_game_status
	ld	(hl), #0x12
	C$gamelogic.c$878$3_0$509	= .
	.globl	C$gamelogic.c$878$3_0$509
;src/engine/gamelogic.c:878: frame_cnt = 0;
	ld	hl, #0x0000
	ld	(_frame_cnt), hl
	jp	00223$
00171$:
	C$gamelogic.c$881$1_0$462	= .
	.globl	C$gamelogic.c$881$1_0$462
;src/engine/gamelogic.c:881: if(frame_cnt == XS_PERIOD) {
	ld	a, (_frame_cnt+0)
	sub	a, #0x18
	ld	hl, #_frame_cnt + 1
	or	a, (hl)
	ld	a, #0x01
	jr	Z, 00544$
	xor	a, a
00544$:
	ld	c, a
	C$gamelogic.c$880$1_0$462	= .
	.globl	C$gamelogic.c$880$1_0$462
;src/engine/gamelogic.c:880: } else if(game_status == GAME_STATUS_GAMEOVER) {
	ld	a, (_game_status+0)
	C$gamelogic.c$881$2_0$510	= .
	.globl	C$gamelogic.c$881$2_0$510
;src/engine/gamelogic.c:881: if(frame_cnt == XS_PERIOD) {
	sub	a,#0x12
	jr	NZ, 00168$
	or	a,c
	jr	Z, 00152$
	C$gamelogic.c$882$3_0$511	= .
	.globl	C$gamelogic.c$882$3_0$511
;src/engine/gamelogic.c:882: print_string(11,10,"GAME^");
	ld	hl, #___str_1
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x0b
	call	_print_string
	C$gamelogic.c$883$3_0$511	= .
	.globl	C$gamelogic.c$883$3_0$511
;src/engine/gamelogic.c:883: print_string(17,10,"OVER^");
	ld	hl, #___str_2
	push	hl
	ld	l, #0x0a
;	spillPairReg hl
;	spillPairReg hl
	ld	a, #0x11
	call	_print_string
	C$gamelogic.c$884$3_0$511	= .
	.globl	C$gamelogic.c$884$3_0$511
;src/engine/gamelogic.c:884: play_sample(GAMEOVER1_SAMPLE);
	ld	a, #0x04
	call	_play_sample
	C$gamelogic.c$885$3_0$511	= .
	.globl	C$gamelogic.c$885$3_0$511
;src/engine/gamelogic.c:885: play_sample(GAMEOVER2_SAMPLE);
	ld	a, #0x05
	call	_play_sample
	C$gamelogic.c$886$3_0$511	= .
	.globl	C$gamelogic.c$886$3_0$511
;src/engine/gamelogic.c:886: play_song(GAMEOVER_OST);
	ld	a, #0x08
	call	_play_song
	C$gamelogic.c$887$3_0$511	= .
	.globl	C$gamelogic.c$887$3_0$511
;src/engine/gamelogic.c:887: start_fadein_music();
	call	_start_fadein_music
00152$:
	C$gamelogic.c$889$2_0$510	= .
	.globl	C$gamelogic.c$889$2_0$510
;src/engine/gamelogic.c:889: if(frame_cnt > XL_PERIOD) {
	ld	a, #0xf0
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x00
	sbc	a, 1 (iy)
	jp	NC, 00223$
	C$gamelogic.c$890$3_0$512	= .
	.globl	C$gamelogic.c$890$3_0$512
;src/engine/gamelogic.c:890: if((score_p1 > highscore_table.table[9].score) || (score_p2 > highscore_table.table[9].score)) {
	ld	bc, (#(_highscore_table + 50) + 0)
	ld	a, c
	ld	iy, #_score_p1
	sub	a, 0 (iy)
	ld	a, b
	sbc	a, 1 (iy)
	jr	C, 00153$
	ld	a, c
	ld	iy, #_score_p2
	sub	a, 0 (iy)
	ld	a, b
	sbc	a, 1 (iy)
	jr	NC, 00154$
00153$:
	C$gamelogic.c$891$4_0$513	= .
	.globl	C$gamelogic.c$891$4_0$513
;src/engine/gamelogic.c:891: highScoreReached = true;
	ld	hl, #_highScoreReached
	ld	(hl), #0x01
00154$:
	C$gamelogic.c$893$3_0$512	= .
	.globl	C$gamelogic.c$893$3_0$512
;src/engine/gamelogic.c:893: game_status = GAME_STATUS_RESET;
	ld	hl, #_game_status
	ld	(hl), #0x15
	C$gamelogic.c$894$3_0$512	= .
	.globl	C$gamelogic.c$894$3_0$512
;src/engine/gamelogic.c:894: stop_music();
	call	_PSGStop
	C$gamelogic.c$895$3_0$512	= .
	.globl	C$gamelogic.c$895$3_0$512
;src/engine/gamelogic.c:895: stop_fx();
	call	_PSGSFXStop
	jr	00223$
00168$:
	C$gamelogic.c$897$1_0$462	= .
	.globl	C$gamelogic.c$897$1_0$462
;src/engine/gamelogic.c:897: } else if(game_status == GAME_STATUS_ENDGAME) {
	ld	a, (_game_status+0)
	C$gamelogic.c$898$2_0$514	= .
	.globl	C$gamelogic.c$898$2_0$514
;src/engine/gamelogic.c:898: if(frame_cnt == XS_PERIOD) {        
	sub	a,#0x13
	jr	NZ, 00223$
	or	a,c
	jr	Z, 00159$
	C$gamelogic.c$899$3_0$515	= .
	.globl	C$gamelogic.c$899$3_0$515
;src/engine/gamelogic.c:899: draw_congratulations();
	call	_draw_congratulations
	C$gamelogic.c$900$3_0$515	= .
	.globl	C$gamelogic.c$900$3_0$515
;src/engine/gamelogic.c:900: play_sample(CONGRATULATIONS1_SAMPLE);
	ld	a, #0x06
	call	_play_sample
	C$gamelogic.c$901$3_0$515	= .
	.globl	C$gamelogic.c$901$3_0$515
;src/engine/gamelogic.c:901: play_sample(CONGRATULATIONS2_SAMPLE);
	ld	a, #0x07
	call	_play_sample
	C$gamelogic.c$902$3_0$515	= .
	.globl	C$gamelogic.c$902$3_0$515
;src/engine/gamelogic.c:902: play_song(CONGRATULATIONS2_OST);
	ld	a, #0x0a
	call	_play_song
00159$:
	C$gamelogic.c$904$2_0$514	= .
	.globl	C$gamelogic.c$904$2_0$514
;src/engine/gamelogic.c:904: if(frame_cnt > XXL_PERIOD) {
	ld	a, #0xe0
	ld	iy, #_frame_cnt
	cp	a, 0 (iy)
	ld	a, #0x01
	sbc	a, 1 (iy)
	jr	NC, 00223$
	C$gamelogic.c$905$3_0$516	= .
	.globl	C$gamelogic.c$905$3_0$516
;src/engine/gamelogic.c:905: stop_music();
	call	_PSGStop
	C$gamelogic.c$906$3_0$516	= .
	.globl	C$gamelogic.c$906$3_0$516
;src/engine/gamelogic.c:906: stop_fx();
	call	_PSGSFXStop
	C$gamelogic.c$907$3_0$516	= .
	.globl	C$gamelogic.c$907$3_0$516
;src/engine/gamelogic.c:907: if((score_p1 > highscore_table.table[9].score) || (score_p2 > highscore_table.table[9].score)) {
	ld	bc, (#(_highscore_table + 50) + 0)
	ld	a, c
	ld	iy, #_score_p1
	sub	a, 0 (iy)
	ld	a, b
	sbc	a, 1 (iy)
	jr	C, 00160$
	ld	a, c
	ld	iy, #_score_p2
	sub	a, 0 (iy)
	ld	a, b
	sbc	a, 1 (iy)
	jr	NC, 00161$
00160$:
	C$gamelogic.c$908$4_0$517	= .
	.globl	C$gamelogic.c$908$4_0$517
;src/engine/gamelogic.c:908: highScoreReached = true;
	ld	hl, #_highScoreReached
	ld	(hl), #0x01
00161$:
	C$gamelogic.c$910$3_0$516	= .
	.globl	C$gamelogic.c$910$3_0$516
;src/engine/gamelogic.c:910: game_status = GAME_STATUS_ENDGAME2;
	ld	hl, #_game_status
	ld	(hl), #0x14
00223$:
	C$gamelogic.c$913$1_0$462	= .
	.globl	C$gamelogic.c$913$1_0$462
;src/engine/gamelogic.c:913: level_timer++;
	ld	hl, (_level_timer)
	inc	hl
	ld	(_level_timer), hl
	C$gamelogic.c$914$1_0$462	= .
	.globl	C$gamelogic.c$914$1_0$462
;src/engine/gamelogic.c:914: if(level_timer == XXXXL_PERIOD) {
	ld	a, (_level_timer+0)
	sub	a, #0x10
	ret	NZ
	ld	a, (_level_timer+1)
	sub	a, #0x0e
	ret	NZ
	C$gamelogic.c$915$2_0$518	= .
	.globl	C$gamelogic.c$915$2_0$518
;src/engine/gamelogic.c:915: fallSpeed += 32;
	ld	a, (_fallSpeed+0)
	add	a, #0x20
	ld	(_fallSpeed+0), a
	C$gamelogic.c$917$1_0$462	= .
	.globl	C$gamelogic.c$917$1_0$462
;src/engine/gamelogic.c:917: }
	C$gamelogic.c$917$1_0$462	= .
	.globl	C$gamelogic.c$917$1_0$462
	XG$execute_game_logic$0$0	= .
	.globl	XG$execute_game_logic$0$0
	ret
Fgamelogic$__str_0$0_0$0 == .
___str_0:
	.ascii "LVL]GOAL^"
	.db 0x00
Fgamelogic$__str_1$0_0$0 == .
___str_1:
	.ascii "GAME^"
	.db 0x00
Fgamelogic$__str_2$0_0$0 == .
___str_2:
	.ascii "OVER^"
	.db 0x00
	G$get_game_actions$0$0	= .
	.globl	G$get_game_actions$0$0
	C$gamelogic.c$919$1_0$520	= .
	.globl	C$gamelogic.c$919$1_0$520
;src/engine/gamelogic.c:919: void get_game_actions(void) {
;	---------------------------------
; Function get_game_actions
; ---------------------------------
_get_game_actions::
	C$gamelogic.c$922$1_0$520	= .
	.globl	C$gamelogic.c$922$1_0$520
;src/engine/gamelogic.c:922: leftAction = false;
	ld	hl, #_leftAction
	ld	(hl), #0x00
	C$gamelogic.c$923$1_0$520	= .
	.globl	C$gamelogic.c$923$1_0$520
;src/engine/gamelogic.c:923: rightAction = false;
	ld	hl, #_rightAction
	ld	(hl), #0x00
	C$gamelogic.c$924$1_0$520	= .
	.globl	C$gamelogic.c$924$1_0$520
;src/engine/gamelogic.c:924: downAction = false;
	ld	hl, #_downAction
	ld	(hl), #0x00
	C$gamelogic.c$925$1_0$520	= .
	.globl	C$gamelogic.c$925$1_0$520
;src/engine/gamelogic.c:925: rotation1Action = false;
	ld	hl, #_rotation1Action
	ld	(hl), #0x00
	C$gamelogic.c$926$1_0$520	= .
	.globl	C$gamelogic.c$926$1_0$520
;src/engine/gamelogic.c:926: rotation2Action = false;
	ld	hl, #_rotation2Action
	ld	(hl), #0x00
	C$gamelogic.c$927$1_0$520	= .
	.globl	C$gamelogic.c$927$1_0$520
;src/engine/gamelogic.c:927: if(current_player == PLAYER_CPU_1 || current_player == PLAYER_CPU_2) {
	ld	a,(_current_player+0)
	cp	a,#0x02
	jr	Z, 00101$
	sub	a, #0x03
	jr	NZ, 00102$
00101$:
	C$gamelogic.c$928$2_0$521	= .
	.globl	C$gamelogic.c$928$2_0$521
;src/engine/gamelogic.c:928: direction = decideDirection(current_square.x, current_square.y+1);
	ld	a, (#(_current_square + 1) + 0)
	ld	b, a
	inc	b
	ld	hl, #_current_square
	ld	c, (hl)
	ld	l, b
;	spillPairReg hl
;	spillPairReg hl
	ld	a, c
	call	_decideDirection
	ld	c, e
	ld	b, d
	C$gamelogic.c$929$2_0$521	= .
	.globl	C$gamelogic.c$929$2_0$521
;src/engine/gamelogic.c:929: rotation = decideRotation(current_square.y+1); 
	ld	a, (#(_current_square + 1) + 0)
	ld	e, a
	inc	e
	push	bc
	ld	a, e
	call	_decideRotation
	pop	bc
	jr	00103$
00102$:
	C$gamelogic.c$931$2_0$522	= .
	.globl	C$gamelogic.c$931$2_0$522
;src/engine/gamelogic.c:931: update_input();
	call	_update_input
	C$gamelogic.c$932$2_0$522	= .
	.globl	C$gamelogic.c$932$2_0$522
;src/engine/gamelogic.c:932: direction = getDirection();
	call	_getDirection
	C$gamelogic.c$933$2_0$522	= .
	.globl	C$gamelogic.c$933$2_0$522
;src/engine/gamelogic.c:933: rotation = getRotation();
	push	de
	call	_getRotation
	pop	bc
00103$:
	C$gamelogic.c$935$1_0$520	= .
	.globl	C$gamelogic.c$935$1_0$520
;src/engine/gamelogic.c:935: if(direction & DOWN_ACTION) {
	bit	1, c
	jr	Z, 00111$
	C$gamelogic.c$936$2_0$523	= .
	.globl	C$gamelogic.c$936$2_0$523
;src/engine/gamelogic.c:936: downAction = true;
	ld	hl, #_downAction
	ld	(hl), #0x01
	jr	00112$
00111$:
	C$gamelogic.c$937$1_0$520	= .
	.globl	C$gamelogic.c$937$1_0$520
;src/engine/gamelogic.c:937: } else if(direction & LEFT_ACTION) {
	bit	2, c
	jr	Z, 00108$
	C$gamelogic.c$938$2_0$524	= .
	.globl	C$gamelogic.c$938$2_0$524
;src/engine/gamelogic.c:938: leftAction = true;
	ld	hl, #_leftAction
	ld	(hl), #0x01
	jr	00112$
00108$:
	C$gamelogic.c$939$1_0$520	= .
	.globl	C$gamelogic.c$939$1_0$520
;src/engine/gamelogic.c:939: } else if(direction & RIGHT_ACTION) {
	bit	3, c
	jr	Z, 00112$
	C$gamelogic.c$940$2_0$525	= .
	.globl	C$gamelogic.c$940$2_0$525
;src/engine/gamelogic.c:940: rightAction = true;
	ld	hl, #_rightAction
	ld	(hl), #0x01
00112$:
	C$gamelogic.c$942$1_0$520	= .
	.globl	C$gamelogic.c$942$1_0$520
;src/engine/gamelogic.c:942: if(rotation & ROTATION_ONE) {
	bit	4, e
	jr	Z, 00116$
	C$gamelogic.c$943$2_0$526	= .
	.globl	C$gamelogic.c$943$2_0$526
;src/engine/gamelogic.c:943: rotation1Action = true;
	ld	hl, #_rotation1Action
	ld	(hl), #0x01
	ret
00116$:
	C$gamelogic.c$944$1_0$520	= .
	.globl	C$gamelogic.c$944$1_0$520
;src/engine/gamelogic.c:944: } else if(rotation & ROTATION_TWO) {
	bit	5, e
	ret	Z
	C$gamelogic.c$945$2_0$527	= .
	.globl	C$gamelogic.c$945$2_0$527
;src/engine/gamelogic.c:945: rotation2Action = true;
	ld	hl, #_rotation2Action
	ld	(hl), #0x01
	C$gamelogic.c$947$1_0$520	= .
	.globl	C$gamelogic.c$947$1_0$520
;src/engine/gamelogic.c:947: }
	C$gamelogic.c$947$1_0$520	= .
	.globl	C$gamelogic.c$947$1_0$520
	XG$get_game_actions$0$0	= .
	.globl	XG$get_game_actions$0$0
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
