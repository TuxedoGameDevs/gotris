echo Copiar musicas PAL
cp ./music-source/pal/credits.psg ./assets
cp ./music-source/pal/gameover.psg ./assets
cp ./music-source/pal/highscore.psg ./assets
cp ./music-source/pal/hurryup.psg ./assets
cp ./music-source/pal/ingame.psg ./assets
cp ./music-source/pal/intro.psg ./assets
cp ./music-source/pal/startjingle.psg ./assets
cp ./music-source/pal/titlescreen.psg ./assets
cp ./music-source/pal/fanfa1.psg ./assets
cp ./music-source/pal/fanfa2.psg ./assets
cp ./music-source/pal/move.psg ./assets
cp ./music-source/pal/select.psg ./assets
cp ./music-source/pal/start.psg ./assets
assets2banks assets --firstbank=3 --compile --singleheader
cp ./assets2banks.h ./src
SYSTEM="PAL_MACHINE"
docker run  --rm -v $PWD:/home/sms-tk/host -w /home/sms-tk/host -t retcon85/toolchain-sms -c "make build"
java -jar Emulicious.jar gotris.sms